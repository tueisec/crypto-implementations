# Changelog

All notable changes to this  will be documented in this file.
            

## [Unreleased]

## [1.0.1]
### Notes
This release fixes a potential timing violation and enhances usability.

### Added
- [CHANGELOG.md](CHANGELOG.md)
- [README.md](wrappers/uart-axis/README.md) for uart-axis wrapper
- [gimli_uart.tcl](scripts/vivado-2018.3/gimli_uart.tcl) 
- Command line options for [pyhton script](wrappers/uart-axis/python/cli.py)
- [fifo.vhd](wrappers/uart-axis/vhdl/uart/fifo.vhd)

### Changed
- [rs232_ctrlr.vhd](wrappers/uart-axis/vhdl/uart/rs232_ctrlr.vhd): improved coding style to avoid "unused sequential" warning
- Replaced all [FIFOs](wrappers/uart-axis/vhdl/uart/fifo.vhd) with a new version to avoid routing and timing problems.
- [pyhton script](wrappers/uart-axis/python/cli.py): improved coding style

### Removed
- [fwft_th_fifo.vhd](wrappers/uart-axis/vhdl/uart/fwft_th_fifo.vhd)

## [1.0.0] 
Initial release of the uart-axis-wrapper.
  
[unreleased]: https://gitlab.lrz.de/tueisec/crypto-implementations/compare/v1.0.1...master
[1.0.1]: https://gitlab.lrz.de/tueisec/crypto-implementations/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.lrz.de/tueisec/crypto-implementations/-/tags/v1.0.0

