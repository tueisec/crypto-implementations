-------------------------------------------------------------------------------
--! @file       design_pkg.vhd
--! @brief      Design package for dummy1
--! @project    CAESAR Candidate Evaluation
--! @author     Ekawat (ice) Homsirikamol
--! @copyright  Copyright (c) 2017 Cryptographic Engineering Research Group
--!             ECE Department, George Mason University Fairfax, VA, U.S.A.
--!             All rights Reserved.
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       This is publicly available encryption source code that falls
--!             under the License Exception TSU (Technology and software-
--!             —unrestricted)
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;


package design_pkg is
    
    --! I/O parameters
    constant PW             : integer := 32;
    constant SW             : integer := 32;
     
end design_pkg;
