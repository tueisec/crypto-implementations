-------------------------------------------------------------------------------
--! @file       Deoxys_BC.vhd
--! @brief      deoxys block cipher for encrypting a message block
--!				
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.AES_PKG.all;

entity Deoxys_BC is
    generic (
        --! Reset behavior
        G_ASYNC_RSTN    : boolean := False --! Async active low reset
	);
	port (
		bc_ctr			: in std_logic_vector(5 downto 0);
		data_in			: in std_logic_vector(127 downto 0);
		TK1_in			: in std_logic_vector(127 downto 0); --key
		TK2_in			: in std_logic_vector(127 downto 0); --tweak
		data_out		: out std_logic_vector(127 downto 0);
		TK1_out			: out std_logic_vector(127 downto 0); --key
		TK2_out			: out std_logic_vector(127 downto 0) --tweak
	);
end Deoxys_BC;

architecture Behavioral of Deoxys_BC is


-------------------------------------------------------------------------------------
-- Signals
-------------------------------------------------------------------------------------

	--! signals for AES
    signal rdkey            : std_logic_vector(127 downto 0);
    signal from_round   	: std_logic_vector(127 downto 0);
    signal state_map        : t_AES_state;
    signal rdkey_map        : t_AES_state;
    signal from_round_map 	: t_AES_state;

	--! signals for other block cipher mdoules
	signal from_TK1_calc	: std_logic_vector(127 downto 0);
	signal from_TK2_calc	: std_logic_vector(127 downto 0);
	signal TK1_pre			: std_logic_vector(127 downto 0);
	signal TK1_buff			: std_logic_vector(127 downto 0);
	signal TK2_pre			: std_logic_vector(127 downto 0);
	signal TK2_post			: std_logic_vector(127 downto 0);
	signal TK1_post			: std_logic_vector(127 downto 0);
    signal from_round_RCON	: std_logic_vector(127 downto 0);
    signal bc_ctr_buff		: std_logic_vector(5 downto 0);

begin -- architecture Behavioral

	--! AES
    u_map1: entity work.AES_map(structure)
    port map ( ii => data_in, oo => state_map);
    u_map2: entity work.AES_map(structure)
    port map ( ii => rdkey, oo => rdkey_map);

    u: entity work.AES_Round_Basic
    port map (
        din     => state_map,
        rkey    => rdkey_map,
        dout    => from_round_map);

    u_imap_enc: entity work.AES_invmap(structure)
    port map ( ii => from_round_map, oo => from_round);

	--! other block cipher modules
	u_perm_h1: entity work.perm_h(behavioral)
	port map (
		data_in 	=> TK1_pre,
		data_out	=> TK1_buff);

	u_perm_h2: entity work.perm_h(behavioral)
	port map (
		data_in 	=> TK2_pre,
		data_out	=> TK2_post);

	u_lfsr2: entity work.lfsr2(behavioral)
	port map (
		data_in 	=> TK1_buff,
		data_out	=> TK1_post);

	u_RCON: entity work.RCON(behavioral)
	port map (
		round	 	=> bc_ctr_buff,
		data_out	=> from_round_RCON);

    --! =======================================================================
    --! Datapath
    --! =======================================================================


    process(bc_ctr, data_in, TK1_in, TK2_in, TK1_pre, TK2_pre, from_TK1_calc, 
			from_TK2_calc, TK1_buff, TK2_post, TK1_post, from_round_RCON, from_round_map,
			from_round)
    begin

		bc_ctr_buff <= bc_ctr;
		TK1_pre			<= TK1_in;
		TK2_pre			<= TK2_in;

		if to_integer(unsigned(bc_ctr)) = 0 then
			data_out		<= data_in xor TK2_in xor TK1_in xor from_round_RCON;
			rdkey			<= (others => '0');
			TK1_out			<= TK1_in;
			TK2_out			<= TK2_in;
		else
			rdkey			<= TK1_post xor TK2_post xor from_round_RCON;
			data_out		<= from_round;
			TK1_out			<= TK1_post;
			TK2_out			<= TK2_post;
		end if;
	end process;

end architecture Behavioral;
