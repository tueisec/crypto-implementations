-------------------------------------------------------------------------------
--! @file       CipherCore.vhd
--! @brief      CipherCore for Deoxys II, an AES-based authenticated encryption
--!				algorithm with associated data

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.AEAD_pkg.ALL;

entity CipherCore is
    generic (
        --! Reset behavior
        G_ASYNC_RSTN    : boolean := False; --! Async active low reset
        --! Block size (bits)
        G_DBLK_SIZE     : integer := 128;   --! Data
        G_KEY_SIZE      : integer := 128;   --! Key
        G_TAG_SIZE      : integer := 128;   --! Tag
        --! The number of bits required to hold block size expressed in
        --! bytes = log2_ceil(G_DBLK_SIZE/8)
        G_LBS_BYTES     : integer := 4;
        --! Maximum supported AD/message/ciphertext length = 2^G_MAX_LEN-1
        G_MAX_LEN       : integer := SINGLE_PASS_MAX
    );
    port (
        --! Global
        clk             : in  std_logic;
        rst             : in  std_logic;
        --! PreProcessor (data)
        key             : in  std_logic_vector(G_KEY_SIZE       -1 downto 0);
        bdi             : in  std_logic_vector(G_DBLK_SIZE      -1 downto 0);
        --! PreProcessor (controls)
        key_ready       : out std_logic;
        key_valid       : in  std_logic;
        key_update      : in  std_logic;
        decrypt         : in  std_logic;
        bdi_ready       : out std_logic;
        bdi_valid       : in  std_logic;
        bdi_type        : in  std_logic_vector(3                -1 downto 0);
        bdi_partial     : in  std_logic;
        bdi_eot         : in  std_logic;
        bdi_eoi         : in  std_logic;
        bdi_size        : in  std_logic_vector(G_LBS_BYTES+1    -1 downto 0);
        bdi_valid_bytes : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);
        bdi_pad_loc     : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);

        --! PostProcessor
        bdo             : out std_logic_vector(G_DBLK_SIZE      -1 downto 0);
        bdo_valid       : out std_logic;
        bdo_ready       : in  std_logic;
        bdo_size        : out std_logic_vector(G_LBS_BYTES+1    -1 downto 0);
        msg_auth       	: out std_logic;
        msg_auth_valid  : out std_logic;
        msg_auth_ready  : in std_logic
    );
end entity CipherCore;

architecture structure of CipherCore is


    --! =======================================================================
    --! Datapath Signals
    --! =======================================================================

    --! Registers
    signal vbytes_r     : std_logic_vector(128/8            -1 downto 0);
    signal padloc_r     : std_logic_vector(128/8            -1 downto 0);
    signal key_block    : std_logic_vector(128              -1 downto 0);
    signal tag_block    : std_logic_vector(128              -1 downto 0);
    signal npub_block   : std_logic_vector(120              -1 downto 0);
    signal ctr_block    : std_logic_vector(G_MAX_LEN	    -1 downto 0);
    signal tag          : std_logic_vector(128              -1 downto 0);

    --! Signals
    signal to_output    : std_logic_vector(128              -1 downto 0);

    --! =======================================================================
    --! Control Signals
    --! =======================================================================

    --!     Register
    signal is_decrypt   : std_logic;
    signal is_eoi       : std_logic;
    signal is_eot       : std_logic;
	signal is_bc_valid  : std_logic;
    --!     Combinatorial
    signal clr_accum    : std_logic;
    signal clr_ctr	    : std_logic;
	signal clr_bc_valid	: std_logic;
    signal ld_key       : std_logic;
    signal ld_tag       : std_logic;
    signal ld_npub      : std_logic;
    signal ld_input     : std_logic;
    signal update_tag  : std_logic;
    signal update_ctr	: std_logic;
	signal update_bc_ctr : std_logic;
    signal sel_final    : std_logic;
	signal sel_bc		: std_logic;
    signal bdi_ready_s  : std_logic;
    signal tweak_mode   : std_logic_vector(3 downto 0);

    --! =======================================================================
    --! Component Signals
    --! =======================================================================

	signal bc_ctr			: std_logic_vector(5 downto 0);
	signal data_input_bc	: std_logic_vector(128	-1 downto 0);
	signal TK1_in			: std_logic_vector(128	-1 downto 0);
	signal TK2_in			: std_logic_vector(128	-1 downto 0);
	signal data_bc_next		: std_logic_vector(128	-1 downto 0);
	signal TK1_next			: std_logic_vector(128	-1 downto 0);
	signal TK2_next			: std_logic_vector(128 -1 downto 0);

	signal TP_din			: std_logic_vector(128 -1 downto 0);
	signal TP_wr_valid		: std_logic;
	signal TP_wr_ready		: std_logic;
	signal TP_dout			: std_logic_vector(128 -1 downto 0);
	signal TP_rd_valid		: std_logic;
	signal TP_rd_ready		: std_logic;

    type t_state is (S_INIT, S_WAIT_START, S_WAIT_KEY, S_WAIT_TAG, S_WAIT_NPUB, S_INIT_MSG,
		S_PROC_TAG, S_PROC_TAG_DEC, S_PROC_TAG_END,
		S_OUT_TAG, S_WAIT_TAG_AUTH, S_OUT_ENC, S_OUT_DEC);
    signal state        : t_state;
    signal nstate       : t_state;

begin

    --! =======================================================================
	--! Component instantiations
    --! =======================================================================

		Deoxys_BC_inst: entity work.Deoxys_BC(behavioral) port map(
			bc_ctr		=> bc_ctr,
			data_in 	=> data_input_bc,
			TK1_in 		=> TK1_in,
			TK2_in 		=> TK2_in,
			data_out	=> data_bc_next,
			TK1_out 	=> TK1_next,
			TK2_out		=> TK2_next
			);

		TP_buffer_4096Byte:
		entity work.fwft_fifo(structure)
		generic map (
		    G_W                 => 128,
		    G_LOG2DEPTH         => 8,
			--! ((2^8)*128)bit = 32768 bit = 4096 Byte
		    G_ASYNC_RSTN        => G_ASYNC_RSTN
		)
		port map (
		    clk                 => clk,
		    rst                 => rst,
		    din                 => TP_din,
		    din_valid           => TP_wr_valid,
		    din_ready           => TP_wr_ready,
		    dout                => TP_dout,
		    dout_valid          => TP_rd_valid,
		    dout_ready          => TP_rd_ready
		);

    --! =======================================================================
    --! Datapath
    --! =======================================================================
    process(clk)
    begin
        if rising_edge(clk) then

			--! After reset
		    if (clr_accum = '1') then
				--! minimal necessary reset
                ctr_block	<= (others => '0');
                tag			<= (others => '0');
		    end if;

			------------------------
			-- load inputs ---------
			------------------------

            if (ld_input = '1' or state = S_OUT_DEC) then
                vbytes_r  <= bdi_valid_bytes;
                padloc_r  <= bdi_pad_loc;
            end if;

			--! Save secret key
            if (ld_key = '1') then
                key_block <= key;
            end if;

			--! Save tag
            if (ld_tag = '1') then
                tag_block <= bdi;
            end if;

			--! save public number
            if (ld_npub = '1') then
                npub_block <= bdi(127 downto 8);
            end if;

			------------------------
			-- fill registers ------
			------------------------

			--! calculate tag (= authentication)
            if (update_tag = '1') then
				if not(state=S_PROC_TAG_END) then
					tag	<= tag xor data_bc_next;
				else
					tag	<= data_bc_next;
				end if;
            end if;
			
			--! store bdi_eoi and bdi_eot of last bdi_valid
            if (bdi_ready_s = '1') then 
                is_eoi <= bdi_eoi;
                is_eot <= bdi_eot;
            end if;

			--! store if the output of the block cipher is valid
			--! and update block cipher roundcounter
			if (clr_accum = '1' or clr_bc_valid = '1') then
				is_bc_valid <= '0';
                bc_ctr		<= (others => '0');
			elsif unsigned(bc_ctr) = 14 then
				is_bc_valid <= '1';
			elsif unsigned(bc_ctr) = 13 then
				is_bc_valid <= '1';
                bc_ctr			<= std_logic_vector(unsigned(bc_ctr) + 1);
			elsif (update_bc_ctr = '1') then
                bc_ctr			<= std_logic_vector(unsigned(bc_ctr) + 1);
			end if;

            --! store decrypt signal internally for use during the tag
            --! authentication state.
            if (ld_npub = '1') then
                is_decrypt <= decrypt;
			end if;

			----------------------------------------------------------------
			-------- feed the block cipher and update block number ---------
			----------------------------------------------------------------

			--! Set the correct inputs for the tweakable block cipher
			if (sel_bc = '1') then 
				TK1_in					<= key_block;
				TK2_in (127 downto 124)	<= tweak_mode;
				TK2_in (123 downto 31)	<= (others => '0');
				--! update block counter
				if (update_ctr = '1') then
					TK2_in (31 downto 0)	<= std_logic_vector(unsigned(ctr_block) + 1);
                	ctr_block				<= std_logic_vector(unsigned(ctr_block) + 1);
				elsif (clr_ctr = '1') then
					TK2_in (31 downto 0)	<= (others => '0');
                	ctr_block				<= (others => '0');
				else
					TK2_in (31 downto 0)	<= ctr_block;
				end if;
				--! lookup deoxys whitepaper for choosing correct inputs
				if (nstate = S_PROC_TAG_DEC) then
					data_input_bc 			<= to_output;
				elsif (nstate = S_OUT_DEC) then
					TK2_in(127) 		<= '1';
					TK2_in(126 downto 31)<= tag_block(126 downto 31);
					if clr_ctr = '1' then
						TK2_in(31 downto 0)	<= tag_block(31 downto 0);
					else
						TK2_in(31 downto 0)	<= tag_block(31 downto 0) xor std_logic_vector(unsigned(ctr_block) + 1);
					end if;
					data_input_bc (127 downto 120)	<= (others => '0');
					data_input_bc (119 downto 0)	<= npub_block;
				elsif (tweak_mode = "0001") then
					TK2_in (119 downto 0)	<= npub_block;
					if (update_tag = '1') then
						data_input_bc 			<= tag xor data_bc_next;
					else
						data_input_bc 			<= tag;
					end if;
				elsif tweak_mode(3) = '1' then
					if (update_tag = '1') then
						TK2_in 				<= data_bc_next;
						TK2_in(127) 		<= '1';
					else
						if is_decrypt = '1' then
							TK2_in(127 downto 31)<= tag_block(127 downto 31);
							TK2_in(31 downto 0)	<= tag_block(31 downto 0) xor std_logic_vector(unsigned(ctr_block) + 1);
						else
							TK2_in(127 downto 31)<= tag(127 downto 31);
							TK2_in(31 downto 0)	<= tag(31 downto 0) xor std_logic_vector(unsigned(ctr_block) + 1);
						end if;
						TK2_in(127) 		<= '1';
					end if;
					data_input_bc (127 downto 120)	<= (others => '0');
					data_input_bc (119 downto 0)	<= npub_block;
				else
					data_input_bc 			<= bdi;
				end if;
			elsif update_bc_ctr = '1' and is_bc_valid = '0' then 
				--! feed the tweakable block cipher with its output (until finished)
				data_input_bc	<= data_bc_next;
				TK1_in	 		<= TK1_next;
				TK2_in			<= TK2_next;
			end if;
        end if;
    end process;


	--! block for padding and choosing correct output and fifo input
    aBlock: block
        signal vbits : std_logic_vector(G_DBLK_SIZE-1 downto 0);
        signal pad   : std_logic_vector(G_DBLK_SIZE-1 downto 0);
    begin

        gVbits:
        for i in 0 to G_DBLK_SIZE/8-1 generate
            --! Get valid bits from valid bytes
            vbits(8*i+7 downto 8*i) <= (others => vbytes_r(i));
            --! Get padding bit
            pad  (8*i+7           ) <= padloc_r(i);
            pad  (8*i+6 downto 8*i) <= (others => '0');
        end generate;

		--! Choose correct calculation for output
		to_output	<= (TP_dout xor data_bc_next) when TP_rd_ready = '1' else 
						(pad or ((bdi xor data_bc_next) and vbits)) when (state = S_OUT_ENC or state = S_OUT_DEC)  else 
						(pad or (data_bc_next and vbits));
		--! Choose correct output
		bdo			<= tag when sel_final = '1' else to_output;
		--! Choose correct input for the fifo
		TP_din <= bdi when is_decrypt = '0' else (pad or ((bdi xor data_bc_next) and vbits));

    end block;

    --! check if tag is correct
    msg_auth <= '1' when tag = tag_block else '0';
    --! don't care
    bdo_size <= (others => '-');

    --! =======================================================================
    --! Control
    --! =======================================================================
    gSyncRst:
    if (not G_ASYNC_RSTN) generate
        process(clk)
        begin
            if rising_edge(clk) then
                if (rst = '1') then
                    state <= S_INIT;
                else
                    state <= nstate;
                end if;
            end if;
        end process;
    end generate;

	--! signal needed because: if (bdi_ready_s = '1')
    bdi_ready <= bdi_ready_s;
	
	FSM:
    process(
        state, key_valid, key_update, is_eoi,
        bdi_valid, bdi_type, bdi_eot,  is_eot, is_bc_valid,
        bdi_eoi, bdi_size, bdo_ready, msg_auth_ready, 
		bc_ctr)
    begin
        --! Internal
        nstate      	<= state;
        clr_accum   	<= '0';
        clr_ctr   		<= '0';
		clr_bc_valid	<= '0';
        ld_key      	<= '0';
        ld_tag      	<= '0';
        ld_npub     	<= '0';
        ld_input    	<= '0';
        update_tag		<= '0';
        update_ctr		<= '0';
        update_bc_ctr	<= '0';
        sel_final   	<= '0';
        sel_bc			<= '0';
		tweak_mode(3) 	<= '0';
        --! TP FIFO
		TP_wr_valid		<= '0';
		TP_rd_ready		<= '0';
        --! External
        key_ready   	<= '0';
        bdi_ready_s 	<= '0';
        bdo_valid   	<= '0';
        msg_auth_valid 	<= '0';
		--! anti-latch
		tweak_mode 	<= "----";

        case state is
            when S_INIT =>
                --! After reset
                clr_accum	<= '1';
                nstate 		<= S_WAIT_START;

            when S_WAIT_START =>
                --! Needs to check whether a new input is available first
                --! prior to checking key to ensure the correct operational
                --! step.
                if (bdi_valid = '1') then
                    if (key_update = '1') then
                        nstate <= S_WAIT_KEY;
                    elsif (bdi_type = BDI_TYPE_TAG) then
                        nstate <= S_WAIT_TAG;
					else
                        nstate <= S_WAIT_NPUB;
                    end if;
                end if;

            when S_WAIT_KEY =>
                --! Wait for key
                key_ready <= '1';
                if (key_valid = '1') then
                    ld_key    <= '1';
                    if (bdi_type = BDI_TYPE_TAG) then
                        nstate <= S_WAIT_TAG;
					else
                        nstate <= S_WAIT_NPUB;
                    end if;
                end if;

            when S_WAIT_TAG =>
				--! Wait for public number
                bdi_ready_s <= '1';
                if (bdi_valid = '1') then
                    ld_tag 	<= '1';
					nstate  	<= S_WAIT_NPUB;
                end if;

            when S_WAIT_NPUB =>
				--! Wait for public number
                bdi_ready_s <= '1';
                if (bdi_valid = '1') then
                    ld_npub 	<= '1';
					nstate  	<= S_INIT_MSG;
                end if;

            when S_INIT_MSG =>
                --! for first msg initialization
				if (is_eoi = '1') then
					sel_bc 		<= '1';
					clr_bc_valid <= '1';
					nstate   	<= S_PROC_TAG_END;
					tweak_mode 	<= "0001";
				end if;
				clr_bc_valid	<= '1';
                if (bdi_valid = '1') then
                    ld_input 	<= '1';
               		bdi_ready_s <= '1';
					clr_bc_valid <= '1';
					sel_bc 		<= '1';
					nstate 		<= S_PROC_TAG;
					if (bdi_type(2 downto 1) = BDI_TYPE_ASS) then
						tweak_mode(1 downto 0) 	<= "10";
					else
						if is_decrypt = '0' then
							TP_wr_valid				<= '1';
							tweak_mode(1 downto 0) 	<= "00";
						else
							bdi_ready_s				<= '0';
							tweak_mode(3) 			<= '1';
							nstate 					<= S_OUT_DEC;
	                    	clr_ctr  				<= '1';
						end if;
					end if;
					if bdi_size(G_LBS_BYTES) = '1' then
						tweak_mode(2) 	<= '0';
					else
						tweak_mode(2) 	<= '1';
					end if;
                end if;

            when S_PROC_TAG =>
				--! Generate tag using associated data
				--! only for encryption: generate tag using messages stored in fifo
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
                    update_tag		<= '1';
	                if (is_eoi = '1') then
						sel_bc 		<= '1';
						clr_bc_valid <= '1';
						nstate   	<= S_PROC_TAG_END;
						tweak_mode 	<= "0001";
	                end if;
			        if (bdi_valid = '1') then
			            ld_input 	<= '1';
						if (bdi_type(2 downto 1) = BDI_TYPE_ASS or bdi_type(2 downto 1) = BDI_TYPE_DAT) then
							if is_eot = '1' then
	                    		clr_ctr  		<= '1';
							else
								update_ctr  	<= '1';
							end if;
					   		bdi_ready_s <= '1';
							clr_bc_valid <= '1';
							sel_bc 		<= '1';
							nstate 		<= S_PROC_TAG;
							if (bdi_type(2 downto 1) = BDI_TYPE_ASS) then
								tweak_mode(1 downto 0) 	<= "10";
							else
								tweak_mode(1 downto 0) 	<= "00";
								if is_decrypt = '0' then
									TP_wr_valid				<= '1';
								else
									bdi_ready_s				<= '0';
									tweak_mode(3) 			<= '1';
									nstate 					<= S_OUT_DEC;
								end if;
							end if;
							if bdi_size(G_LBS_BYTES) = '1' then
								tweak_mode(2) 	<= '0';
							else
								tweak_mode(2) 	<= '1';
							end if;
						end if;
			        end if;
                end if;

			when S_PROC_TAG_DEC =>
				--! generate tag using output calculated in S_OUT_DEC
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
                    update_tag		<= '1';
	                if (bdi_eoi = '1') then
						sel_bc			<= '1';
						clr_bc_valid	<= '1';
						nstate			<= S_PROC_TAG_END;
						bdi_ready_s		<= '1';
						tweak_mode		<= "0001";
	               elsif (bdi_valid = '1') then
				        ld_input		<= '1';
				   		bdi_ready_s		<= '1';
						clr_bc_valid	<= '1';
						sel_bc			<= '1';
	                    update_ctr  	<= '1';
						tweak_mode(3) 	<= '1';
						nstate 			<= S_OUT_DEC;
						if bdi_size(G_LBS_BYTES) = '1' then
							tweak_mode(2) 	<= '0';
						else
							tweak_mode(2) 	<= '1';
						end if;
			        end if;
                end if;

            when S_PROC_TAG_END =>
				--! Process last step for tag generation
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
		            if (bdo_ready = '1' or is_decrypt = '1') then
						update_tag    	<= '1';
						if TP_rd_valid = '1' then
							clr_ctr  		<= '1';
							sel_bc 			<= '1';
							clr_bc_valid	<= '1';
							tweak_mode(3)	<= '1';
		                    nstate    		<= S_OUT_ENC;
		                elsif (is_decrypt = '1') then
		                    nstate    		<= S_WAIT_TAG_AUTH;
		                else
		                    nstate    		<= S_OUT_TAG;
		                end if;
		            end if;
                end if;

            when S_OUT_ENC =>
				--! Process and output data for decryption
				--! also store it in fifo
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
					if TP_rd_valid = '1' then
		                if (bdo_ready = '1') then
		                    bdo_valid   	<= '1';
		                    update_ctr  	<= '1';
							sel_bc 			<= '1';
							clr_bc_valid	<= '1';
							TP_rd_ready		<= '1';
							tweak_mode(3)	<= '1';
						end if;
	                else
	                    nstate    		<= S_OUT_TAG;
					end if;
				elsif TP_rd_valid = '0' then
					nstate    		<= S_OUT_TAG;
                end if;

            when S_OUT_DEC =>
				--! Process and output data for decryption
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
	                if (bdo_ready = '1') then
	                    bdo_valid   	<= '1';
						sel_bc 			<= '1';
						clr_bc_valid	<= '1';
	                	nstate    		<= S_PROC_TAG_DEC;
						tweak_mode(1 downto 0) 	<= "00";
						if bdi_size(G_LBS_BYTES) = '1' then
							tweak_mode(2) 	<= '0';
						else
							tweak_mode(2) 	<= '1';
						end if;
					end if;
                end if;

            when S_OUT_TAG =>
				--! Output the Tag (Encryption)
                if (bdo_ready = '1') then
                    sel_final		<= '1';
                	bdo_valid		<= '1';
                    nstate  		<= S_INIT;
                end if;

            when S_WAIT_TAG_AUTH =>
				--! Authenticate Tag (Decryption)
                if (msg_auth_ready = '1') then
                    msg_auth_valid	<= '1';
                    nstate			<= S_INIT;
                end if;
        end case;
    end process;
end structure;
