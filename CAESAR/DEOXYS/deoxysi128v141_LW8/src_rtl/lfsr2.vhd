-------------------------------------------------------------------------------
--! @file       lfsr2.vhd
--! @brief      applying a bit shift and xor operation onto 1 Byte for updating
--!				the tweakey

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------------

entity lfsr2 is
	port(
		data_in		: in std_logic_vector (7 downto 0);
		data_out	: out std_logic_vector (7 downto 0)
		);
end lfsr2;

architecture behavioral of lfsr2 is


begin

	data_out (7 downto 1) 	<= 	data_in (6 downto 0);
	data_out (0)			<=	data_in (7) xor data_in (5);

end architecture behavioral;
