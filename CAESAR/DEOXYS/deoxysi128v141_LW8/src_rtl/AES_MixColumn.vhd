-------------------------------------------------------------------------------
--! @file       AES_MixColumn.vhd
--! @brief      A single operation of MixColumns operation
--! @project    CAESAR Candidate Evaluation
--! @author     Marcin Rogawski   
--! @author     Ekawat (ice) Homsirikamol
--! @copyright  Copyright (c) 2014 Cryptographic Engineering Research Group
--!             ECE Department, George Mason University Fairfax, VA, U.S.A.
--!             All rights Reserved.
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at 
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       This is publicly available encryption source code that falls
--!             under the License Exception TSU (Technology and software-
--!             —unrestricted)
-------------------------------------------------------------------------------
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @note       File altered for an 8 bit lightweight implementation.
--!				The inputs are a whole 32 bit column which is provided by a
--!				Serial-In Parallel-Out Shift Register.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity AES_MixColumn is
    port(
        in_0c       : in  std_logic_vector(7 downto 0);
        in_1c       : in  std_logic_vector(7 downto 0);
        in_2c       : in  std_logic_vector(7 downto 0);
        in_3c       : in  std_logic_vector(7 downto 0);
        mode_MC     : in  std_logic_vector(1 downto 0);
        output      : out std_logic_vector(7 downto 0)
    );
end AES_MixColumn;

-------------------------------------------------------------------------------
--! @brief  Architecture definition of AES_MixColumn
-------------------------------------------------------------------------------

architecture behavioral of AES_MixColumn is
    signal mulx2_in		: std_logic_vector(7 downto 0);
    signal mulx3_in		: std_logic_vector(7 downto 0);
    signal mulx2_out	: std_logic_vector(7 downto 0);
    signal mulx3_out	: std_logic_vector(7 downto 0);
begin

    m2  : entity work.AES_mul(AES_mulx02)
        port map (  input  => mulx2_in,
                    output => mulx2_out);
    m3  : entity work.AES_mul(AES_mulx03)
        port map (  input  => mulx3_in,
                    output => mulx3_out);

	MixColumn: process (in_0c, in_1c, in_2c, in_3c, mode_MC, mulx2_in, mulx2_out, mulx3_in, mulx3_out)
	begin
		case to_integer(unsigned(mode_MC)) is
			when 0 =>
				mulx2_in	<= in_0c;
				mulx3_in	<= in_1c;
				output		<= mulx2_out xor mulx3_out xor in_2c xor in_3c;
			when 1 =>
				mulx2_in	<= in_1c;
				mulx3_in	<= in_2c;
				output		<= 	in_0c xor mulx2_out xor mulx3_out xor in_3c;
			when 2 =>
				mulx2_in	<= in_2c;
				mulx3_in	<= in_3c;
				output		<= in_0c xor in_1c xor mulx2_out xor mulx3_out;
			when 3 =>
				mulx2_in	<= in_3c;
				mulx3_in	<= in_0c;
				output		<= mulx3_out xor in_1c xor in_2c xor mulx2_out;
			when others =>
				output		<= (others => '0');
		end case;
	end process;
end behavioral;
