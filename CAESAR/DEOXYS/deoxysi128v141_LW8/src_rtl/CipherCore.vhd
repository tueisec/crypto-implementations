--! @file       CipherCore.vhd
--! @brief      CipherCore for Deoxys, an AES-based authenticated encryption
--!				algorithm with associated data. This is an 8 bit lightweight
--!				implementation.

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use IEEE.math_real."ceil";
use IEEE.math_real."log2";
use work.GeneralComponents_pkg.ALL;
use work.design_pkg.all;
use work.CAESAR_LWAPI_pkg.all;

entity CipherCore is
    generic (
        G_DBLK_SIZE     : integer := 128
    );
    Port (
            clk             : in   STD_LOGIC;
            rst             : in   STD_LOGIC;
            --PreProcessor===============================================
            ----!key----------------------------------------------------
            key             : in   STD_LOGIC_VECTOR (SW      -1 downto 0);
            key_valid       : in   STD_LOGIC;
            key_ready       : out  STD_LOGIC;
            ----!Data----------------------------------------------------
            bdi             : in   STD_LOGIC_VECTOR (PW      -1 downto 0);
            bdi_valid       : in   STD_LOGIC;
            bdi_ready       : out  STD_LOGIC;
            bdi_partial     : in   STD_LOGIC;
            bdi_pad_loc     : in   STD_LOGIC_VECTOR (PWdiv8  -1 downto 0);
            bdi_valid_bytes : in   STD_LOGIC_VECTOR (PWdiv8  -1 downto 0);
            bdi_size        : in   STD_LOGIC_VECTOR (3       -1 downto 0);
            bdi_eot         : in   STD_LOGIC;
            bdi_eoi         : in   STD_LOGIC;
            bdi_type        : in   STD_LOGIC_VECTOR (4       -1 downto 0);
            decrypt_in      : in   STD_LOGIC;
            key_update      : in   STD_LOGIC;
            --!Post Processor=========================================
            bdo             : out  STD_LOGIC_VECTOR (PW      -1 downto 0);
            bdo_valid       : out  STD_LOGIC;
            bdo_ready       : in   STD_LOGIC;
            bdo_type        : out  STD_LOGIC_VECTOR (4       -1 downto 0);
            bdo_valid_bytes : out  STD_LOGIC_VECTOR (PWdiv8  -1 downto 0);
            end_of_block    : out  STD_LOGIC;
            decrypt_out     : out  STD_LOGIC;
            msg_auth_valid  : out  STD_LOGIC ;
            msg_auth_ready  : in   STD_LOGIC ;
            msg_auth        : out  STD_LOGIC 
            
         );

end CipherCore;

architecture structure of CipherCore is

	-------------------------------------------------------------------
	--! begin signals for Datapath-------------------------------------
	constant	NumOfWords		: integer:=G_DBLK_SIZE/PW;
	constant	NumOfNWords		: integer:= 64/PW;
	constant	addrWidth		: integer:=integer(ceil(log2(real(NumOfWords))));
	--! key RAM
	signal		wen_kram		: std_logic;
	signal		addr_kram		: std_logic_vector(addrWidth  -1 downto 0);
	signal		dout_kram		: std_logic_vector(PW         -1 downto 0);
	--! public number RAM
	signal		wen_npub		: std_logic;
	signal		addr_npub		: std_logic_vector(addrWidth  -1 downto 0);
	signal		din_npub1		: std_logic_vector(4		  -1 downto 0);
	signal		dout_npub1		: std_logic_vector(4         -1 downto 0);
	signal		din_npub2		: std_logic_vector(4		  -1 downto 0);
	signal		dout_npub2		: std_logic_vector(4        -1 downto 0);
	signal		dout_npub2_buff	: std_logic_vector(4        -1 downto 0);
	--! 2x tweakey1 RAM
	signal		wen_tk1_even	: std_logic;
	signal		wen_tk1_odd		: std_logic;
	signal		addr_tk1_even	: std_logic_vector(addrWidth  -1 downto 0);
	signal		addr_tk1_odd	: std_logic_vector(addrWidth  -1 downto 0);
	signal		din_tk1			: std_logic_vector(PW		  -1 downto 0);
	signal		dout_tk1_even	: std_logic_vector(PW         -1 downto 0);
	signal		dout_tk1_odd	: std_logic_vector(PW         -1 downto 0);
	--! 2x tweakey2 RAM
	signal		wen_tk2_even	: std_logic;
	signal		wen_tk2_odd		: std_logic;
	signal		addr_tk2_even	: std_logic_vector(addrWidth  -1 downto 0);
	signal		addr_tk2_odd	: std_logic_vector(addrWidth  -1 downto 0);
	signal		din_tk2			: std_logic_vector(PW		  -1 downto 0);
	signal		dout_tk2_even: std_logic_vector(PW         -1 downto 0);
	signal		dout_tk2_odd	: std_logic_vector(PW         -1 downto 0);
	--! Rcon RAM
	signal		wen_RCON		: std_logic;
	signal		addr_RCON		: std_logic_vector(addrWidth  -1 downto 0);
	signal		din_RCON		: std_logic_vector(PW		  -1 downto 0);
	signal		dout_RCON		: std_logic_vector(PW         -1 downto 0);
	--! 2x data RAM
	signal		wen_data_pre	: std_logic;
	signal		wen_data_post	: std_logic;
	signal		addr_data_pre	: std_logic_vector(addrWidth  -1 downto 0);
	signal		addr_data_pre_enc : std_logic_vector(addrWidth  -1 downto 0);
	signal		addr_data_pre_dec : std_logic_vector(addrWidth  -1 downto 0);
	signal		addr_data_post	: std_logic_vector(addrWidth  -1 downto 0);
	signal		din_data		: std_logic_vector(PW         -1 downto 0);
	signal		dout_data_pre	: std_logic_vector(PW         -1 downto 0);
	signal		dout_data_post	: std_logic_vector(PW         -1 downto 0);
	--! serial to parallel converter register
	signal		wen_Reg8x32		: std_logic;
	signal		din_Reg8x32		: std_logic_vector(PW         -1 downto 0);
	signal		addr_count_4	: std_logic_vector(2		  -1 downto 0);
	signal		dout0_Reg8x32	: std_logic_vector(PW         -1 downto 0);
	signal		dout1_Reg8x32	: std_logic_vector(PW         -1 downto 0);
	signal		dout2_Reg8x32	: std_logic_vector(PW         -1 downto 0);
	signal		dout3_Reg8x32	: std_logic_vector(PW         -1 downto 0);
	--! tag RAM
	signal		wen_tag			: std_logic;
	signal		addr_tag		: std_logic_vector(addrWidth  -1 downto 0);
	signal		din_tag			: std_logic_vector(PW         -1 downto 0);
	signal		dout_tag		: std_logic_vector(PW         -1 downto 0);
	--! checksum RAM
	signal		wen_check		: std_logic;
	signal		addr_check		: std_logic_vector(addrWidth  -1 downto 0);
	signal		din_check		: std_logic_vector(PW         -1 downto 0);
	signal		dout_check		: std_logic_vector(PW         -1 downto 0);
	--! output RAM
	signal		wen_out			: std_logic;
	signal		addr_out		: std_logic_vector(addrWidth  -1 downto 0);
	signal		dout_out		: std_logic_vector(PW         -1 downto 0);
	--! tweakey 1 update
	signal		addrin_tk1		: std_logic_vector(addrWidth  -1 downto 0);
	signal		datain_tk1		: std_logic_vector(PW         -1 downto 0);
	signal		addrout_tk1		: std_logic_vector(addrWidth  -1 downto 0);
	signal		dataout_tk1		: std_logic_vector(PW         -1 downto 0);
	--! tweakey 2 update
	signal		addrin_tk2		: std_logic_vector(addrWidth  -1 downto 0);
	signal		datain_tk2		: std_logic_vector(PW         -1 downto 0);
	signal		addrout_tk2		: std_logic_vector(addrWidth  -1 downto 0);
	signal		dataout_tk2		: std_logic_vector(PW         -1 downto 0);
	--! RCON update
	signal		dataout_RCON	: std_logic_vector(7 downto 0);
	--! Registers
	signal		decrypt_bc		: std_logic;    
	signal 		is_decrypt   	: std_logic;
	signal		last_type		: std_logic_vector(4         -1 downto 0);
	signal		current_type	: std_logic_vector(4         -1 downto 0);
	signal		partial_blk_size : std_logic_vector(addrWidth -1 downto 0);
	signal		pad_blk_proc_next : std_logic;
	signal		partial_next	: std_logic;
	signal		bdi_eot_next	: std_logic;
	signal		bdi_eoi_next	: std_logic;
	signal		pad_blk_proc_r	: std_logic;
	signal		partial_r		: std_logic;
	signal		bdi_eot_r		: std_logic;
	signal		bdi_eoi_r		: std_logic;
	signal		count_up_r		: std_logic;
	signal		count_up_next	: std_logic;
	signal		tk_mode_r		: std_logic_vector(4         -1 downto 0);
	signal		tk_mode_next	: std_logic_vector(4         -1 downto 0);
	--! other modules
	signal		addrin_sb		: std_logic_vector(addrWidth  -1 downto 0);
	signal		datain_sb		: std_logic_vector(PW         -1 downto 0);
	signal		addrout_sb		: std_logic_vector(addrWidth  -1 downto 0);
	signal		addrout_sb_enc	: std_logic_vector(addrWidth  -1 downto 0);
	signal		addrout_sb_dec	: std_logic_vector(addrWidth  -1 downto 0);
	signal		dataout_sb_enc	: std_logic_vector(PW         -1 downto 0);
	signal		dataout_sb_dec	: std_logic_vector(PW         -1 downto 0);
	signal		datain_0c		: std_logic_vector(PW         -1 downto 0);
	signal		datain_1c		: std_logic_vector(PW         -1 downto 0);
	signal		datain_2c		: std_logic_vector(PW         -1 downto 0);
	signal		datain_3c		: std_logic_vector(PW         -1 downto 0);
	signal		dataout_xc_enc	: std_logic_vector(PW         -1 downto 0);
	signal		dataout_xc_dec	: std_logic_vector(PW         -1 downto 0);
	--! buffer signals
	signal		tk_buff			: std_logic_vector(PW         -1 downto 0);
	signal		data_buff		: std_logic_vector(PW         -1 downto 0);
	--! end signals for Datapath---------------------------------------
	-------------------------------------------------------------------

	-------------------------------------------------------------------
	--! begin signals for Control--------------------------------------
	signal		clr_accum		: std_logic; 
	signal		tk_mode			: std_logic_vector(3 downto 0);
	--! counter signals
	signal		rst_wcnt    	: std_logic; 
	signal		rst_32cnt    	: std_logic;  
	signal		rst_4cnt    	: std_logic; 
	signal		en_wcnt     	: std_logic; 
	signal		en_32cnt     	: std_logic; 
	signal		en_4cnt     	: std_logic;
	signal		wrd_count		: std_logic_vector(addrWidth -1 downto 0);
	signal		count_32		: std_logic_vector(32-1 downto 0);
	signal		count_4			: std_logic_vector(1 downto 0);
	signal		zcount			: integer;
	signal		zdone			: std_logic;
	signal		zpdone			: std_logic;
	signal		zdone_ct		: std_logic;
	signal		ndone			: std_logic;
	signal		count_4_done	: std_logic;
	signal		init_bc_rnd		: std_logic;
	signal		update_bc_rnd	: std_logic;
	signal		bc_rnd			: std_logic_vector(3 downto 0); -- 0 to 14 rounds
	--! output function signals
	signal		end_of_block_s	: std_logic; 
	signal		sel_npub_din  	: std_logic; 
	signal		sel_npub      	: std_logic;
	signal		bdo_valid_internal : std_logic;
	--! end signals for Control----------------------------------------
	-------------------------------------------------------------------

	--! begin signals for padding--------------------------------------
	signal		rst_pad           	: std_logic;
	signal		end_of_block_in_s	: std_logic;
	signal		bdi_valid_s			: std_logic;
    signal 		bdi_ready_s  		: std_logic;
	signal		bdi_eot_pad			: std_logic;
	signal		pad_block			: std_logic;
	signal		bdo_s             	: std_logic_vector(PW    -1 downto 0);
	signal		bdo_padded        	: std_logic_vector(PW    -1 downto 0);
	signal		bdi_padded        	: std_logic_vector(PW    -1 downto 0);
	--signal		round_padded      	: std_logic_vector(PW    -1 downto 0);
	--signal		round             	: std_logic_vector(PW    -1 downto 0);
	--! end signals for padding----------------------------------------
	-------------------------------------------------------------------

    type t_state is (S_INIT, S_WAIT_START, 
		S_LD_KEY, S_WAIT_NPUB, S_LD_NPUB, S_WAIT_MSG,
		S_OUT_TAG, S_LD_MSG, S_INIT_END, S_PRE_INIT_END,
		S_UPDATE_RCON_TK1_TK2, S_ADDSHIFTBOX,
		S_SIN_POUT, S_PIN_SOUT, S_PROC_TAG, S_OUT_CT,
		S_INIT_DEC);
    signal state        : t_state;
    signal nstate       : t_state;


begin

    --  =======================================================================
    --  Padding unit
    --  =======================================================================
        padding_unit: entity work.padding_unit(behavioral)
            port map(
                    rst             => rst_pad,
                    clk             => clk,
                    bdi_in          => bdi,
                    bdi_valid_in    => bdi_valid,
                    bdi_ready_in    => bdi_ready_s,
                    bdo_in          => bdo_s,
                    --round_in        => round,
                    bdi_out         => bdi_padded,
                    bdi_valid_out   => bdi_valid_s,
                    bdi_ready_out   => bdi_ready,
                    bdo_out         => bdo_padded,
                    --round_out       => round_padded,
                    bdi_eot_in      => bdi_eot,
                    bdi_eot_out     => bdi_eot_pad,
                    bdi_pad_loc     => bdi_pad_loc,
                    bdi_valid_bytes => bdi_valid_bytes,
                    end_of_block    => end_of_block_in_s,
                    bdi_type        => bdi_type,
                    pad_block       => pad_block
            );

    --! =======================================================================
    --! Datapath
    --! =======================================================================

		--! Registers----------------------------------------------------------
        KRAM: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_kram,
                addr	=> addr_kram,
                din		=> key,
                dout	=> dout_kram
            );
        NRAM1: SPDRam
        generic map(4,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_npub,
                addr	=> addr_npub,
                din		=> din_npub1,
                dout	=> dout_npub1
            );
		din_npub1          <= (others => '0') when (sel_npub_din = '1') else bdi(PW      -1 downto PW/2);
        NRAM2: SPDRam
        generic map(4,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_npub,
                addr	=> addr_npub,
                din		=> din_npub2,
                dout	=> dout_npub2
            );
		din_npub2          <= (others => '0') when (sel_npub_din = '1') else bdi(PW/2      -1 downto 0);

        TK1RAM_even: SPDRam
        generic map(PW,addrWidth)
        port map
            (
				clk		=> clk,
                wen		=> wen_tk1_even,
                addr	=> addr_tk1_even,
                din		=> din_tk1,
                dout	=> dout_tk1_even
            );
        TK1RAM_odd: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_tk1_odd,
                addr	=> addr_tk1_odd,
                din		=> din_tk1,
                dout	=> dout_tk1_odd
            );
        TK2RAM_even: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_tk2_even,
                addr	=> addr_tk2_even,
                din  	=> din_tk2,
                dout 	=> dout_tk2_even
            );
        TK2RAM_odd: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_tk2_odd,
                addr	=> addr_tk2_odd,
                din		=> din_tk2,
                dout	=> dout_tk2_odd
            );
        RCONRAM: entity work.SPDRam_RCON(behavioral)
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_RCON,
                addr	=> addr_RCON,
                din		=> din_RCON,
                dout	=> dout_RCON
            );
        DRAM_pre: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_data_pre,
                addr	=> addr_data_pre,
                din		=> din_data,
                dout	=> dout_data_pre
            );
        Reg8x32: entity work.Reg8x32(Reg)
        port map
            (
                clk		=> clk,
                wen		=> wen_Reg8x32,
                addr	=> addr_count_4,
                din		=> din_Reg8x32,
                dout0	=> dout0_Reg8x32,
                dout1	=> dout1_Reg8x32,
                dout2	=> dout2_Reg8x32,
                dout3	=> dout3_Reg8x32
            );
        DRAM_post: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_data_post,
                addr	=> addr_data_post,
                din		=> din_data,
                dout	=> dout_data_post
            );
        TRAM: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_tag,
                addr	=> addr_tag,
                din		=> din_tag,
                dout	=> dout_tag
            );
        CRAM: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_check,
                addr	=> addr_check,
                din		=> din_check,
                dout	=> dout_check
            );

        ORAM: SPDRam
        generic map(PW,addrWidth)
        port map
            (
                clk		=> clk,
                wen		=> wen_out,
                addr	=> addr_out,
                din		=> din_data,
                dout	=> dout_out
            );

		--! other modules--------------------------------------------------
		up_TK1: entity work.tweakey_update(structure)
		generic map(1)
		port map
			(
				decrypt_bc	=> decrypt_bc,
				data_in		=> datain_tk1,
				addr_in		=> addrin_tk1,
				data_out	=> dataout_tk1,
				addr_out	=> addrout_tk1
			);

		up_TK2: entity work.tweakey_update(structure)
		generic map(2)
		port map
			(
				decrypt_bc	=> decrypt_bc,
				data_in		=> datain_tk2,
				addr_in		=> addrin_tk2,
				data_out	=> dataout_tk2,
				addr_out	=> addrout_tk2
			);
		up_RCON: entity work.RCON(behavioral)
		port map
			(
				round		=> bc_rnd,
				data_out	=> dataout_rcon
			);

		do_SHIFTBOX: entity work.AES_ShiftBox(structure)
		port map
			(
				data_in		=> datain_sb,
				addr_in		=> addrin_sb,
				data_out	=> dataout_sb_enc,
				addr_out	=> addrout_sb_enc
			);

		do_INV_SHIFTBOX: entity work.AES_InvShiftBox(structure)
		port map
			(
				data_in		=> datain_sb,
				addr_in		=> addrin_sb,
				data_out	=> dataout_sb_dec,
				addr_out	=> addrout_sb_dec
			);

		do_MIXCOLUM: entity work.AES_MixColumn(behavioral)
		port map
			(
				in_0c		=> datain_0c,
				in_1c		=> datain_1c,
				in_2c		=> datain_2c,
				in_3c		=> datain_3c,
				mode_MC		=> addr_count_4,
				output		=> dataout_xc_enc
			);

		do_INV_MIXCOLUM: entity work.AES_InvMixColumn(behavioral)
		port map
			(
				in_0c		=> datain_0c,
				in_1c		=> datain_1c,
				in_2c		=> datain_2c,
				in_3c		=> datain_3c,
				mode_MC		=> addr_count_4,
				output		=> dataout_xc_dec
			);

    --! =======================================================================
    --! Control
    --! =======================================================================
    gSyncRst:
    process(clk)
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                state <= S_INIT;
            else
                state <= nstate;
            end if;
        end if;
    end process;

	ctrlregs:
    process(clk)
    begin
        if rising_edge(clk) then
            --! store decrypt signal internally for use during the tag
            --! authentication state.
            if (state = S_LD_NPUB) then
                is_decrypt <= decrypt_in;
            end if;

			--! store 4bit of npub for next clk cycle
            if (clr_accum = '1') then --! After reset
				dout_npub2_buff	<= (others => '0');
            else
				dout_npub2_buff	<= dout_npub2;
            end if;

			if clr_accum = '1' then
				last_type	<= (others => '0');
			elsif (bdi_valid = '1' and bdi_eoi = '1') then
				last_type	<= bdi_type;
			end if;

			if clr_accum = '1' then
				current_type	<= (others => '0');
			elsif (bdi_valid = '1' and (state = S_WAIT_MSG or state = S_LD_NPUB)) then
				current_type	<= bdi_type;
			end if;

			--! and update block cipher roundcounter
			if clr_accum = '1' or init_bc_rnd = '1' then
                bc_rnd		<= std_logic_vector(to_unsigned(0, bc_rnd'length));
			elsif (update_bc_rnd = '1') then
				if count_up_next = '0' then
                	bc_rnd			<= std_logic_vector(unsigned(bc_rnd) - 1);
				else
                	bc_rnd			<= std_logic_vector(unsigned(bc_rnd) + 1);
				end if;
			end if;

			if clr_accum = '1' then
			    partial_blk_size <= (others => '0');
			    partial_next <= '0';
			elsif ((bdi_ready_s = '1') and (bdi_valid = '1') and (pad_block = '0')) then
			    if (bdi_type = HDR_MSG) then
			        if (bdi_eot = '1') and (end_of_block_s = '0') then
			            partial_next <= '1';
			            partial_blk_size <= wrd_count;
			        end if;
			    end if;
			end if;
			bdi_eot_r <= bdi_eot_next;
			bdi_eoi_r <= bdi_eoi_next;
			partial_r <= partial_next;
			pad_blk_proc_r <= pad_blk_proc_next;

			tk_mode_r	<= tk_mode_next;
			count_up_r	<= count_up_next;

        end if;
    end process;

    --==========================================================================
    --!next state function
    --==========================================================================
	next_state_FSM:
    process(
        state, key_valid, key_update, is_decrypt, 
        bdi_valid, bdi_type, bdi_eot, bdi_valid_s,
        bdi_eoi, bdi_size, bdo_ready, msg_auth_ready, 
		zdone,zdone_ct, count_4_done, bdi_eot_r,bdi_eoi_r, partial_r, pad_blk_proc_r,
		count_up_r, last_type, current_type, pad_block, tk_mode_r,tk_mode_next, bc_rnd,
		count_up_next)
    begin
		nstate			<= state;
		init_bc_rnd 	<= '0';
		rst_32cnt		<= '1';
        en_32cnt     	<= '0';
		tk_mode_next	<= tk_mode_r; --to avoid latch
		
        case state is
            when S_INIT =>
                nstate 		<= S_WAIT_START;
				rst_32cnt		<= '0';
				en_32cnt     	<= '1';
				tk_mode_next	<= (others => '1');

            when S_WAIT_START =>
                if (key_update = '1' and key_valid = '1') then
                    nstate <= S_LD_KEY;
                elsif (bdi_valid = '1') then
                    nstate <= S_WAIT_NPUB;
                end if;

            when S_LD_KEY=>
                if(key_valid='1' and zdone='1') then
                    nstate <= S_WAIT_NPUB;
                end if;

            when S_WAIT_NPUB=>
                if(bdi_valid_s ='1' and bdi_type=HDR_NPUB)then
                    nstate <= S_LD_NPUB;
                end if;


            when S_LD_NPUB=>
                if(zdone ='1') then
					nstate <= S_WAIT_MSG;
                end if;

            when S_WAIT_MSG =>
				tk_mode_next	<= (others => '1'); --! unused default
	            if (last_type = HDR_NPUB) then
	                nstate	<= S_UPDATE_RCON_TK1_TK2;
					tk_mode_next	<= "0001";
					init_bc_rnd <= '1';
				elsif (bdi_valid_s='1') then
						nstate	<= S_LD_MSG;
					if bdi_type=HDR_AD then
						tk_mode_next (1 downto 0) <= "10";
					else
						tk_mode_next (1 downto 0) <= "00";
					end if;
	            end if;

			when S_LD_MSG =>
                if(zdone ='1') then
					init_bc_rnd <= '1';
					nstate <= S_UPDATE_RCON_TK1_TK2;
					if pad_block = '0' then
						tk_mode_next (3 downto 2) <= "00";
					else
						tk_mode_next	(3 downto 2) <= "01";
					end if;
				end if;

			when S_UPDATE_RCON_TK1_TK2 =>
                if(zdone ='1') then
					if count_up_next = '1' and is_decrypt = '1' and tk_mode_next = "0000" then
						if to_integer(unsigned(bc_rnd)) = 14 then
							nstate <= S_INIT_DEC;
						end if;
					else
						nstate <= S_ADDSHIFTBOX;
					end if;
				end if;
		
			when S_INIT_DEC =>
				nstate <= S_ADDSHIFTBOX;

			when S_ADDSHIFTBOX =>
                if(zdone ='1') then
					if (count_up_next = '1' and to_integer(unsigned(bc_rnd)) = 14) or (count_up_next = '0' and to_integer(unsigned(bc_rnd)) = 0) then
						if bdi_eot_r = '1' then	
							rst_32cnt	<= '0';
						else
							en_32cnt	<= '1';
						end if;
						if tk_mode_next (1 downto 0) = "00" then
							nstate <= S_OUT_CT;
							if (bdi_eoi_r = '1' and bdi_eot_r = '1') then
								rst_32cnt	<= '1';
							end if;
						elsif tk_mode_next (1 downto 0) = "10" then
							nstate <= S_WAIT_MSG;
							if (bdi_eoi_r = '1' and bdi_eot_r = '1') then
								nstate	<= S_INIT_END;
								rst_32cnt	<= '1';
							end if;
						else
							nstate <= S_PROC_TAG;
						end if;
					else
						nstate <= S_SIN_POUT;
					end if;
				end if;

			when S_SIN_POUT =>
                if(count_4_done = '1') then
					nstate <= S_PIN_SOUT;
				end if;

			when S_PIN_SOUT =>
                if(count_4_done = '1') then
					if (zdone = '1') then
						nstate	<= S_UPDATE_RCON_TK1_TK2;
					else
						nstate <= S_SIN_POUT;
					end if;
				end if;

            when S_OUT_CT=>
                if (zdone_ct='1') and (bdo_ready='1')then
					if (bdi_eoi_r = '1' and bdi_eot_r = '1') then
						if partial_r = '1' and current_type = HDR_MSG and is_decrypt = '1' then
							nstate	<= S_PRE_INIT_END;
						else
					    	nstate	<= S_INIT_END;
						end if;
                    else
                        nstate <= S_WAIT_MSG;
                    end if;
				end if;

			when S_PRE_INIT_END =>
				nstate	<= S_INIT_END;

			when S_INIT_END =>
                nstate	<= S_UPDATE_RCON_TK1_TK2;
				init_bc_rnd <= '1';
				if partial_r= '1' then
					tk_mode_next	<= "0101";
					en_32cnt	<= '1';
				else
					tk_mode_next	<= "0001";
					if last_type = HDR_MSG then
						en_32cnt	<= '1';
					elsif last_type = HDR_AD then
						rst_32cnt	<= '0';
					end if;
				end if;

			when S_PROC_TAG=>
				if(zdone='1') then
					nstate	<= S_OUT_TAG;
				end if;

            when S_OUT_TAG =>
				--! Output the Tag to the PostProcessor
                if (zdone = '1' and bdo_ready = '1') then
                    nstate  	<= S_INIT;
                end if;
                
			when others =>
                
        end case;
    end process;


    --==========================================================================
    --!output function
    --==========================================================================
    process(state,key_valid, bdi_valid, bdo_ready, bdi_pad_loc, bdi_valid_bytes,
            bdi_size, bdi_eot,bdi_eoi, bdi_type, key_update, zcount, zdone,
			dout_tk1_even, dout_tk1_odd, dout_tk2_even, dout_tk2_odd, bc_rnd,
			dout_kram, dataout_RCON, tk_mode_r,tk_mode_next, dout_npub1, dout_npub2, dout_npub2_buff,
			count_32, dataout_tk1, dataout_tk2, tk_buff, data_buff, dataout_xc_enc,
			dataout_xc_dec, count_4_done, dataout_sb_enc, dataout_sb_dec, zdone_ct,
			bdi_eot_r,bdi_eoi_r, partial_r, pad_blk_proc_r, zpdone, bdi_valid_s,
			bdi_padded, count_up_r,count_up_next, pad_block, is_decrypt, dout_check, dout_RCON,
			dout_data_post, dout_tag, dout_out, dout_data_pre, dout0_Reg8x32,
			dout1_Reg8x32, dout2_Reg8x32, dout3_Reg8x32, current_type)


    begin

		key_ready		<='0';
        bdi_ready_s   	<='0';
        wen_kram		<='0';
        wen_npub		<='0';
        wen_RCON		<='0';
        wen_tk1_even	<='0';
        wen_tk1_odd		<='0';
        wen_tk2_even	<='0';
        wen_tk2_odd		<='0';
        wen_data_pre	<='0';
        wen_Reg8x32		<='0';
        wen_data_post	<='0';
        wen_tag			<='0';
        wen_check		<='0';
        wen_out			<='0';
        rst_wcnt    	<='1';
        rst_4cnt    	<='1';
        en_wcnt     	<='0';
        en_4cnt     	<='0';
        en_4cnt     	<='0';
        end_of_block_s	<='0';
        bdo_type    	<= (others=>'-');
        msg_auth_valid	<='0';
        msg_auth		<='0';
        bdo_valid_bytes	<= (others=>'-');
        sel_npub_din  	<= '0';
        sel_npub      	<= '0';
        clr_accum 		<= '0';
		tk_buff			<= (others => '-');
		data_buff		<= (others => '-');
		rst_pad			<='0';
		bdo_valid_internal <= '0';
        bdi_eot_next  <= bdi_eot_r; --to avoid latch
        bdi_eoi_next  <= bdi_eoi_r; --to avoid latch
        pad_blk_proc_next <= pad_blk_proc_r; --to avoid latch
		count_up_next	<= count_up_r; --to avoid latch
		update_bc_rnd	<= '0';
		din_data		<= (others => '-');
		din_tk1			<= (others => '-');
		din_tk2			<= (others => '-');
		din_RCON		<= (others => '-');
		din_check		<= (others => '-');
		din_tag			<= (others => '-');
		datain_sb		<= (others => '-');
		datain_0c		<= (others => '-');
		datain_1c		<= (others => '-');
		datain_2c		<= (others => '-');
		datain_3c		<= (others => '-');
		datain_tk1		<= (others => '-');
		datain_tk2		<= (others => '-');
		din_Reg8x32		<= (others => '-');


        case state is

            when S_INIT=>
                rst_wcnt			<= '0';
        		rst_4cnt			<= '0';
                clr_accum			<= '1';
				rst_pad				<= '1';
                bdi_eot_next		<= '0';
                bdi_eoi_next		<= '0';
                pad_blk_proc_next	<= '0';
				count_up_next		<= '1';


			when S_WAIT_START=>
				rst_pad			<= '1';

            when S_LD_KEY=>
                key_ready		<='1';
                wen_kram		<=key_valid;
                en_wcnt			<=key_valid;
                rst_wcnt		<=not(key_valid and zdone);
				rst_pad			<= '1';
                

            when S_WAIT_NPUB=>
				rst_pad			<= '1';

            when S_LD_NPUB=>
                sel_npub		<= '1';
				wen_tag			<= '1';
    			din_tag			<= (others => '0');
				wen_check		<= '1';
    			din_check		<= (others => '0');
                if (zcount < NumOfNWords) then
                    bdi_ready_s			<='1';
                    wen_npub			<=bdi_valid_s;
                    en_wcnt				<=bdi_valid_s;
                else
                    sel_npub_din		<= '1';
                    wen_npub			<= '1';
                    en_wcnt				<= '1';
                    rst_wcnt			<= not zdone;
                end if;
                if (bdi_eoi = '1') then
                    bdi_eoi_next          <= '1';
                end if;

			when S_WAIT_MSG =>
				bdi_eoi_next		<= '0';
				bdi_eot_next		<= '0';

			when S_LD_MSG =>
				count_up_next		<= '1';
				bdi_ready_s			<= '1';
				en_wcnt				<= bdi_valid_s;
				wen_data_post		<= bdi_valid_s;
				wen_out				<= bdi_valid_s; --! only needed here if last msg padded (tk_mode = "0100")
                rst_wcnt			<= not (zdone and bdi_valid_s);
				din_data			<= bdi_padded;
				end_of_block_s		<= zdone;
                if (bdi_eot = '1') then
                    bdi_eot_next		<= '1';
                end if;
                if (bdi_eoi = '1') and (pad_block = '0') then
                    bdi_eoi_next		<= '1';
                end if;
				--! save checksum
				if is_decrypt = '0' and current_type = HDR_MSG  then
					wen_check	<= bdi_valid_s;
					din_check	<= bdi_padded xor dout_check;
				end if;

			when S_UPDATE_RCON_TK1_TK2 =>
				--! before processing the message, the round constant and tweakeys are calculated
                en_wcnt				<= '1';
                rst_wcnt			<= not zdone;
				--! update round constant
				din_RCON			<= dataout_RCON;
                wen_RCON			<= '1';
				--! update tweakey 1
				if to_integer(unsigned(bc_rnd)) mod 2 = 0 then --! if round even: read from odd, write to even
					wen_tk1_even	<= '1';
					if (count_up_next = '1' and to_integer(unsigned(bc_rnd)) = 0) then
						din_tk1			<= dout_kram;
					else
						datain_tk1		<= dout_tk1_odd;
						din_tk1			<= dataout_tk1;
					end if;
				elsif to_integer(unsigned(bc_rnd)) mod 2 = 1 then
					wen_tk1_odd		<= '1';
					datain_tk1		<= dout_tk1_even;
					din_tk1			<= dataout_tk1;
				end if;
				--! update tweakey 2
				if to_integer(unsigned(bc_rnd)) mod 2 = 0 then --! if round even: read from odd, write to even
					wen_tk2_even	<= '1';
					if (count_up_next = '1' and to_integer(unsigned(bc_rnd)) = 0) then
						if tk_mode_next (1 downto 0) = "10" then
							if zcount = 0 then
								din_tk2		<= tk_mode_next & "0000";
							elsif zcount >= 12 then
								din_tk2		<= count_32(31-(8*(zcount-12)) downto 31 -7 -(8*(zcount-12)));
							else
								din_tk2		<= (others => '0');
							end if;
						else
							if zcount = 0 then
								din_tk2		<= tk_mode_next & dout_npub1;
							elsif zcount < 8 then
								din_tk2		<= dout_npub2_buff & dout_npub1;
							elsif zcount = 8 then
								din_tk2		<= dout_npub2_buff & "0000";
							elsif zcount >= 12 then
								din_tk2		<= count_32(31-(8*(zcount-12)) downto 31 -7 -(8*(zcount-12)));
							else
								din_tk2		<= (others => '0');
							end if;
						end if;
					else
						datain_tk2		<= dout_tk2_odd;
						din_tk2			<= dataout_tk2;
					end if;
				elsif to_integer(unsigned(bc_rnd)) mod 2 = 1 then
					wen_tk2_odd		<= '1';
					datain_tk2		<= dout_tk2_even;
					din_tk2			<= dataout_tk2;
				end if;
				if count_up_next = '1' and is_decrypt = '1' and tk_mode_next = "0000" and not(to_integer(unsigned(bc_rnd)) = 14) then
					update_bc_rnd	<= zdone;
				end if;

			when S_INIT_DEC =>
				--! the last round tweakeys were calculated for decryption
				--! here we change from counting upwards to counting downwards
				count_up_next	<= '0';

			when S_ADDSHIFTBOX =>
				--! Addroundkey + Sbox + ShiftRow
                en_wcnt			<= '1';
                rst_wcnt		<= not zdone;
				--! calculate the round tweakey
				if to_integer(unsigned(bc_rnd)) mod 2 = 0 then
					tk_buff			<= dout_RCON xor dout_tk1_even xor dout_tk2_even;
				else
					tk_buff			<= dout_RCON xor dout_tk1_odd xor dout_tk2_odd;
				end if;
				--! choose correct data and perform Addroundtweakey
				if count_up_next = '1' then
					if to_integer(unsigned(bc_rnd)) = 0 then 
						if tk_mode_next = "0001" or tk_mode_next = "0101" then
							data_buff	<= dout_check xor tk_buff;
						elsif tk_mode_next = "0100" then
							data_buff	<= tk_buff; --! xor zeros
						else
							data_buff	<= dout_data_post xor tk_buff;
						end if;
					else
						data_buff	<= dout_data_post xor tk_buff;
					end if;
					--! perform Sbox + ShiftRows (if not last round)
					if not(to_integer(unsigned(bc_rnd)) = 14) then
						wen_data_pre	<= '1';
						datain_sb	<= data_buff;
						din_data	<= dataout_sb_enc;
					else 
						--! save auth or checksum
						if tk_mode_next = "0010" or tk_mode_next = "0110" then
							wen_tag		<= '1';
							din_tag		<= data_buff xor dout_tag;
						elsif tk_mode_next = "0001" or tk_mode_next = "0101" then
							wen_check	<= '1';
							din_check	<= data_buff;
						end if;
						--! write correctly into output register
						if tk_mode_next = "0000" then
							wen_out		<= '1';
							din_data	<= data_buff;
						elsif tk_mode_next = "0100" then
							wen_out		<= '1';
							din_data	<= data_buff xor dout_out;
						end if;
					end if;
				else
					--! perform decryption
					wen_data_pre <= '1';
					if to_integer(unsigned(bc_rnd)) = 14 then
						data_buff	<= dout_data_post;
						din_data	<= data_buff xor tk_buff;
					else
						datain_sb	<= dout_data_post;
						data_buff	<= dataout_sb_dec;
						din_data	<= data_buff xor tk_buff;
						if to_integer(unsigned(bc_rnd)) = 0 then
							wen_check	<= '1';
							din_check	<= data_buff xor tk_buff xor dout_check;
							wen_out		<= '1';
							din_data	<= data_buff xor tk_buff;
						end if;
					end if;
				end if;

			when S_SIN_POUT =>
				--! serial-in and parallel-out for a mixcolumn operation
				en_4cnt			<= '1';
				rst_4cnt		<= not(count_4_done);
	            en_wcnt			<= not(count_4_done);
				wen_Reg8x32		<= '1';
				din_Reg8x32		<= dout_data_pre;

			when S_PIN_SOUT =>
				--! parallel-in and serial-out for a mixcolumn operation
				en_4cnt			<= '1';
				rst_4cnt		<= not(count_4_done);
				if count_4_done = '1' then
	            	en_wcnt			<= '1';
			        rst_wcnt		<= not zdone;
				end if;
				datain_0c		<= dout0_Reg8x32;
				datain_1c		<= dout1_Reg8x32;
				datain_2c		<= dout2_Reg8x32;
				datain_3c		<= dout3_Reg8x32;
				wen_data_post	<= '1';
				if count_up_next = '1' then
					din_data		<= dataout_xc_enc;
				else
					din_data		<= dataout_xc_dec;
				end if;
				update_bc_rnd	<= count_4_done and zdone;


			when S_OUT_CT=>
				--! output the encrypted/decrypted message block	  
				bdo_valid_internal		<= '1';
				end_of_block_s			<= zdone_ct;
				en_wcnt					<= bdo_ready;
				rst_wcnt				<= not(bdo_ready and zdone_ct);
				if tk_mode_next = "0100" and is_decrypt = '1' then
					wen_check				<= bdo_ready;
					din_check				<= dout_check xor dout_out;
					rst_wcnt				<= '1';
				end if;

			when S_PRE_INIT_END =>
				--! calculate the checksum correctly if the last message block is padded
				--! here the rst_wctn signal is delayed one clock cycle to add a padding '1'
				wen_check				<= '1';
				en_wcnt					<= '1';
				din_check(7) 			<= dout_check(7) xor '1'; --! insert a padding '1' at the msb of the byte after the last byte
				din_check(6 downto 0)	<= dout_check(6 downto 0);
				rst_wcnt				<= '0';

			when S_INIT_END =>
				--! initilazing the end (encrypt the checksum for the tag)
				count_up_next			<= '1';

            when S_PROC_TAG=>
				--! process the tag (xor the checksum with the authentication)
                en_wcnt					<= '1';
                rst_wcnt				<= not(zdone);
                wen_out                 <= '1';
				din_data				<= (dout_check xor dout_tag);
                end_of_block_s			<= zdone;

            when S_OUT_TAG=>
                bdo_valid_internal		<= '1';
                en_wcnt					<= bdo_ready;
                end_of_block_s			<= zdone;
                rst_wcnt				<= not(bdo_ready and zdone);
                bdo_type				<= HDR_TAG;

            when others =>
            
        end case;
    end process;
    --=========================================================================
    WCNT: UpCountRst
      GENERIC MAP(addrWidth) --! counter for number of byte in data block
      PORT MAP     (
                        clk   => clk,
                        rst   => rst_wcnt,
                        en    => en_wcnt,
                        count => wrd_count
                    );
    WCNT_32: UpCountRst --! counter for number of data blocks
      GENERIC MAP(32)
      PORT MAP     (
                        clk   => clk,
                        rst   => rst_32cnt,
                        en    => en_32cnt,
                        count => count_32
                    );
    WCNT_2: UpCountRst --! counter to 4 (needed for reg8x32)
      GENERIC MAP(2)
      PORT MAP     (
                        clk   => clk,
                        rst   => rst_4cnt,
                        en    => en_4cnt,
                        count => count_4
                    );

	decrypt_bc		<= not(count_up_next);

    addr_kram   	<= wrd_count;
    addr_npub   	<= wrd_count;
    addr_RCON   	<= wrd_count;
	addrin_tk1		<= wrd_count;
	addrin_tk2		<= wrd_count;
    addr_tk1_even	<= wrd_count when wen_tk1_even = '0' or ((count_up_next = '1' and to_integer(unsigned(bc_rnd)) = 0) or (count_up_next = '0' and to_integer(unsigned(bc_rnd)) = 14)) else addrout_tk1;
    addr_tk2_even	<= wrd_count when wen_tk2_even = '0' or ((count_up_next = '1' and to_integer(unsigned(bc_rnd)) = 0) or (count_up_next = '0' and to_integer(unsigned(bc_rnd)) = 14)) else addrout_tk2;
    addr_tk1_odd	<= wrd_count when wen_tk1_odd = '0' else addrout_tk1;
    addr_tk2_odd	<= wrd_count when wen_tk2_odd = '0' else addrout_tk2;
	addrin_sb		<= wrd_count;
	addr_data_pre_enc <= wrd_count when wen_data_pre = '0' else addrout_sb_enc;
	addr_data_pre_dec <= wrd_count;
    addr_data_pre  	<= addr_data_pre_enc when count_up_next = '1' else addr_data_pre_dec;
	addr_data_post	<= wrd_count when ((wen_data_post = '0' or state = S_LD_MSG) and count_up_next = '1') or (wen_data_post = '0' and count_up_next = '0' and to_integer(unsigned(bc_rnd)) = 14) else 
						addrout_sb_enc when (count_up_next = '0' and state = S_ADDSHIFTBOX) else
						std_logic_vector((unsigned(wrd_count) - 3 + unsigned(count_4)));
	addr_count_4	<= count_4;
    addr_out		<= wrd_count;
    addr_tag		<= wrd_count;
    addr_check		<= wrd_count;
    zcount      	<= to_integer(unsigned(wrd_count));
    zdone       	<= '1' when zcount = (NumOfWords-1) else '0';
    ndone       	<= '1' when zcount = (NumOfNWords-1) else '0';
    count_4_done	<= '1' when to_integer(unsigned(count_4)) = 3 else '0';
    zpdone      	<= '1' when zcount = to_integer(unsigned(partial_blk_size)) else '0';
    zdone_ct    	<= zpdone when partial_r='1' else zdone;
    bdo_valid   	<= bdo_valid_internal;
	bdo_s			<= dout_out when bdo_valid_internal='1' else (others=>'0');
	bdo				<= bdo_padded when (state = S_ADDSHIFTBOX)and(end_of_block_s='1') else bdo_s;
	end_of_block	<= end_of_block_s;
	end_of_block_in_s <= ndone when (sel_npub = '1') else zdone;

	decrypt_out		<= decrypt_in;

end structure;
