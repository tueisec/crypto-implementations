-------------------------------------------------------------------------------
--! @file       AES_ShiftBox.vhd
--! @brief      Combining Sbox and Shift Rows in one file

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity AES_ShiftBox is
    port(
        data_in 		: in  std_logic_vector(7 downto 0);
        addr_in 		: in  std_logic_vector(3 downto 0);
        data_out 		: out std_logic_vector(7 downto 0);
        addr_out 		: out std_logic_vector(3 downto 0)
    );
end AES_ShiftBox;

-------------------------------------------------------------------------------
--! @brief  Look-Table based implementation
-------------------------------------------------------------------------------

architecture structure of AES_ShiftBox is

begin
    sbox: entity work.AES_Sbox(distributed_rom)
		port map (	input	=>	data_in,
					output	=>	data_out);
    ShiftRows: entity work.AES_ShiftRows(basic)
		port map (	input	=>	addr_in,
					output	=>	addr_out);
end structure;

