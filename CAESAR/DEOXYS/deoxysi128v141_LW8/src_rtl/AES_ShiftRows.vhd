-------------------------------------------------------------------------------
--! @file       AES_ShiftRows.vhd
--! @brief      AES Shift Rows operation for an 8 bit lightweight
--!				implementation
--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;


entity AES_ShiftRows is
    port(
        input 		: in  std_logic_vector(3 downto 0);
        output 		: out std_logic_vector(3 downto 0)
    );
end AES_ShiftRows;

-------------------------------------------------------------------------------
--! @brief  Architecture definition of AES_ShiftRows
-------------------------------------------------------------------------------

architecture basic of AES_ShiftRows is
begin
	ShiftByte: process (input)
	begin
		case (to_integer(unsigned(input)) mod 4) is
			when 0 =>
				output	<= std_logic_vector(unsigned(input) - 0);
			when 1 =>
				output	<= std_logic_vector(unsigned(input) - 4);
			when 2 =>
				output	<= std_logic_vector(unsigned(input) - 8);
			when 3 =>
				output	<= std_logic_vector(unsigned(input) - 12);
			when others =>
				output	<= (others => '0');
		end case;
	end process;
end basic;
