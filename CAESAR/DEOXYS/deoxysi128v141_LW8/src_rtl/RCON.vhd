-------------------------------------------------------------------------------
--! @file       RCON.vhd
--! @brief      getting the correct roundconstant

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------------

entity RCON is
	port(
		round		: in std_logic_vector (3 downto 0);
		data_out	: out std_logic_vector (7 downto 0)
		);
end RCON;

architecture behavioral of RCON is

type ARRAY_17_8bit  is ARRAY (0 to 16) of  STD_LOGIC_VECTOR(7 downto 0);
constant rcon : ARRAY_17_8bit :=  (X"2f",X"5e",X"bc",X"63",X"c6",X"97",X"35",X"6a",X"d4",X"b3",X"7d",X"fa",X"ef",X"c5",X"91",X"39",X"72");

begin

	data_out	<= rcon(to_integer(unsigned(round)));

end architecture behavioral;
