-------------------------------------------------------------------------------
--! @file       AES_InvMixColumn.vhd
--! @brief      A single InvMixcolumn operation
--! @project    CAESAR Candidate Evaluation  
--! @author     Marcin Rogawski
--! @author     Ekawat (ice) Homsirikamol
--! @copyright  Copyright (c) 2014 Cryptographic Engineering Research Group
--!             ECE Department, George Mason University Fairfax, VA, U.S.A.
--!             All rights Reserved.
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       This is publicly available encryption source code that falls
--!             under the License Exception TSU (Technology and software-
--!             —unrestricted)
-------------------------------------------------------------------------------
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @note       File altered for an 8 bit lightweight implementation.
--!				The inputs are a whole 32 bit column which is provided by a
--!				Serial-In Parallel-Out Shift Register.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity AES_InvMixColumn is
    port(
        in_0c       : in  std_logic_vector(7 downto 0);
        in_1c       : in  std_logic_vector(7 downto 0);
        in_2c       : in  std_logic_vector(7 downto 0);
        in_3c       : in  std_logic_vector(7 downto 0);
        mode_MC     : in  std_logic_vector(1 downto 0);
        output      : out std_logic_vector(7 downto 0)
    );
end AES_InvMixColumn;

-------------------------------------------------------------------------------
--! @brief  Architecture definition of AES_InvMixColumn
-------------------------------------------------------------------------------

architecture behavioral of AES_InvMixColumn is
    signal mulx14_in	: std_logic_vector(7 downto 0);
    signal mulx13_in	: std_logic_vector(7 downto 0);
    signal mulx11_in	: std_logic_vector(7 downto 0);
    signal mulx09_in	: std_logic_vector(7 downto 0);
    signal mulx14_out	: std_logic_vector(7 downto 0);
    signal mulx13_out	: std_logic_vector(7 downto 0);
    signal mulx11_out	: std_logic_vector(7 downto 0);
    signal mulx09_out	: std_logic_vector(7 downto 0);
begin

    m14  : entity work.AES_mul(AES_mulx14)
        port map (  input  =>  mulx14_in,
                    output => mulx14_out);
    m13  : entity work.AES_mul(AES_mulx13)
        port map (  input  =>  mulx13_in,
                    output => mulx13_out);
    m11  : entity work.AES_mul(AES_mulx11)
        port map (  input  =>  mulx11_in,
                    output => mulx11_out);
    m09  : entity work.AES_mul(AES_mulx09)
        port map (  input  =>  mulx09_in,
                    output => mulx09_out);

	InvMixColumn: process (in_0c, in_1c, in_2c, in_3c, mode_MC, mulx14_in, mulx14_out, mulx13_in, mulx13_out, mulx11_in, mulx11_out, mulx09_in, mulx09_out)
	begin
		case to_integer(unsigned(mode_MC)) is
			when 0 =>
				mulx14_in	<= in_0c;
				mulx11_in	<= in_1c;
				mulx13_in	<= in_2c;
				mulx09_in	<= in_3c;
			when 1 =>
				mulx14_in	<= in_1c;
				mulx11_in	<= in_2c;
				mulx13_in	<= in_3c;
				mulx09_in	<= in_0c;
			when 2 =>
				mulx14_in	<= in_2c;
				mulx11_in	<= in_3c;
				mulx13_in	<= in_0c;
				mulx09_in	<= in_1c;
			when 3 =>
				mulx14_in	<= in_3c;
				mulx11_in	<= in_0c;
				mulx13_in	<= in_1c;
				mulx09_in	<= in_2c;
			when others =>
		end case;
		output		<= mulx14_out xor mulx11_out xor mulx13_out xor mulx09_out;
	end process;

end behavioral;
