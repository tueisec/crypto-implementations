--! @file       tweakey_update.vhd
--! @brief      the tweakeys are updated for the next round of 
--!				encryption or decryption

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------------

entity tweakey_update is 
	generic (nr   	: integer := 1);
	port(
		decrypt_bc	: in std_logic;
		addr_in		: in std_logic_vector(3 downto 0);
		data_in		: in std_logic_vector(7 downto 0);
		addr_out	: out std_logic_vector(3 downto 0);
		data_out	: out std_logic_vector(7 downto 0)
		);
end tweakey_update;

-------------------------------------------------------------------------------
--! @brief  Primary architecture definition of tweakey_update
-------------------------------------------------------------------------------

architecture structure of tweakey_update is
begin
	g_tk1 : if nr = 1 generate
		update : entity work.tweakey_update(tk1_update)
			port map(
				decrypt_bc => decrypt_bc,
				addr_in => addr_in,
				data_in => data_in,
				addr_out => addr_out,
				data_out => data_out
			);

	end generate;
	g_tk2 : if nr = 2 generate
		update : entity work.tweakey_update(tk2_update)
			port map(
				decrypt_bc => decrypt_bc,
				addr_in => addr_in,
				data_in => data_in,
				addr_out => addr_out,
				data_out => data_out
			);
	end generate;
end structure;

-------------------------------------------------------------------------------
--! @brief  update tweakey 1 for encryption and decryption
-------------------------------------------------------------------------------

architecture tk1_update of tweakey_update is

	signal addr_out_enc1	: std_logic_vector(3 downto 0);
	signal data_out_enc1	: std_logic_vector(7 downto 0);
	signal addr_out_dec1	: std_logic_vector(3 downto 0);
	signal data_out_dec1	: std_logic_vector(7 downto 0);

begin

	u_perm_h: entity work.perm_h(behavioral)
	port map (
		addr_in 	=> addr_in,
		addr_out	=> addr_out_enc1);

	u_lfsr2: entity work.lfsr2(behavioral)
	port map (
		data_in 	=> data_in,
		data_out	=> data_out_enc1);

	u_perm_h_inv: entity work.perm_h_inv(behavioral)
	port map (
		addr_in 	=> addr_in,
		addr_out	=> addr_out_dec1);

	u_lfsr2_inv: entity work.lfsr2_inv(behavioral)
	port map (
		data_in 	=> data_in,
		data_out	=> data_out_dec1);

process (data_in, addr_in, decrypt_bc, addr_out_enc1, data_out_enc1, addr_out_dec1, data_out_dec1)
begin
	if decrypt_bc = '0' then
		addr_out	<= addr_out_enc1;
		data_out	<= data_out_enc1;
	else
		addr_out	<= addr_out_dec1;
		data_out	<= data_out_dec1;
	end if;
end process;

end tk1_update;

-------------------------------------------------------------------------------
--! @brief  update tweakey 2 for encryption and decryption
-------------------------------------------------------------------------------

architecture tk2_update of tweakey_update is

	signal addr_out_enc2	: std_logic_vector(3 downto 0);
	signal addr_out_dec2	: std_logic_vector(3 downto 0);

begin

	u_perm_h: entity work.perm_h(behavioral)
	port map (
		addr_in 	=> addr_in,
		addr_out	=> addr_out_enc2);

	u_perm_h_inv: entity work.perm_h_inv(behavioral)
	port map (
		addr_in 	=> addr_in,
		addr_out	=> addr_out_dec2);

process (data_in, addr_in, decrypt_bc, addr_out_enc2, addr_out_dec2)
begin
	data_out	<= data_in;
	if decrypt_bc = '0' then
		addr_out	<= addr_out_enc2;
	else
		addr_out	<= addr_out_dec2;
	end if;
end process;

end tk2_update;
