-------------------------------------------------------------------------------
--! @file       Reg8x32.vhd
--! @brief      serial to parallel converter register for MixColumn operation

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity Reg8x32 is 
    port    (
                clk		: in  std_logic;
                wen		: in  std_logic;
                din		: in  std_logic_vector(7 downto 0);
                addr	: in  std_logic_vector(1 downto 0);
                dout0	: out std_logic_vector(7 downto 0);
                dout1	: out std_logic_vector(7 downto 0);
                dout2	: out std_logic_vector(7 downto 0);
                dout3	: out std_logic_vector(7 downto 0)
             );
end Reg8x32;

architecture Reg of Reg8x32 is 
	signal	dout0_r	: std_logic_vector(7 downto 0);
	signal	dout1_r	: std_logic_vector(7 downto 0);
	signal	dout2_r	: std_logic_vector(7 downto 0);
	signal	dout3_r	: std_logic_vector(7 downto 0);
begin   
    
	Reg8x32: process(clk)
		begin
            if rising_edge(clk) then
				dout0	<= dout0_r; 
				dout1	<= dout1_r; 
				dout2	<= dout2_r; 
				dout3	<= dout3_r; 
                if wen ='1' then
					case to_integer(unsigned(addr)) is
						when 0 =>
                    	dout0	<= din;
                    	dout0_r	<= din;
						when 1 =>
                    	dout1	<= din;
                    	dout1_r	<= din;
						when 2 =>
                    	dout2	<= din;
                    	dout2_r	<= din;
						when 3 =>
                    	dout3	<= din;
                    	dout3_r	<= din;
						when others =>
                    	dout0_r	<= (others => '0');
                    	dout1_r	<= (others => '0');
                    	dout2_r	<= (others => '0');
                    	dout3_r	<= (others => '0');
					end case;
                end if;
            end if; 
        end process;  
end Reg;
