-------------------------------------------------------------------------------
--! @file       perm_h.vhd
--! @brief      applying the inverse permutation h onto 1 Byte for updating
--!				the tweakey

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------------

entity perm_h_inv is
	port(
		addr_in		: in std_logic_vector (3 downto 0);
		addr_out	: out std_logic_vector (3 downto 0)
		);
end perm_h_inv;

architecture behavioral of perm_h_inv is


begin

	perm_h_inv: process(addr_in)
		begin
		case to_integer(unsigned(addr_in)) is
			when 1 =>
				addr_out	<= std_logic_vector(to_unsigned(0, addr_out'length));
			when 6 =>
				addr_out	<= std_logic_vector(to_unsigned(1, addr_out'length));
			when 11 =>
				addr_out	<= std_logic_vector(to_unsigned(2, addr_out'length));
			when 12 =>
				addr_out	<= std_logic_vector(to_unsigned(3, addr_out'length));
			when 5 =>
				addr_out	<= std_logic_vector(to_unsigned(4, addr_out'length));
			when 10 =>
				addr_out	<= std_logic_vector(to_unsigned(5, addr_out'length));
			when 15 =>
				addr_out	<= std_logic_vector(to_unsigned(6, addr_out'length));
			when 0 =>
				addr_out	<= std_logic_vector(to_unsigned(7, addr_out'length));
			when 9 =>
				addr_out	<= std_logic_vector(to_unsigned(8, addr_out'length));
			when 14 =>
				addr_out	<= std_logic_vector(to_unsigned(9, addr_out'length));
			when 3 =>
				addr_out	<= std_logic_vector(to_unsigned(10, addr_out'length));
			when 4 =>
				addr_out	<= std_logic_vector(to_unsigned(11, addr_out'length));
			when 13 =>
				addr_out	<= std_logic_vector(to_unsigned(12, addr_out'length));
			when 2 =>
				addr_out	<= std_logic_vector(to_unsigned(13, addr_out'length));
			when 7 =>
				addr_out	<= std_logic_vector(to_unsigned(14, addr_out'length));
			when 8 =>
				addr_out	<= std_logic_vector(to_unsigned(15, addr_out'length));
			when others =>
				addr_out	<= (others => '0');
		end case;
	end process;


end architecture behavioral;
