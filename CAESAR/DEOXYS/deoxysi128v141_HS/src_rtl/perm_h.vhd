-------------------------------------------------------------------------------
--! @file       perm_h.vhd
--! @brief      permutation h. this operation is needed for
--!				updating the tweakey	
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--! @project    Master's thesis: CAESAR Candidate Evaluation
--! @author     Simon Schilling

--! module: permutation h
-------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------------

entity perm_h is
	port(
		data_in		: in std_logic_vector (127 downto 0);
		data_out	: out std_logic_vector (127 downto 0)
		);
end perm_h;

architecture behavioral of perm_h is


begin

	--permutation h
	--first column
	data_out(119 downto 112)	<=	data_in(127 downto 120);
	data_out(79 downto 72)		<=	data_in(119 downto 112);
	data_out(39 downto 32)		<=	data_in(111 downto 104);
	data_out(31 downto 24)		<=	data_in(103 downto 96);
	--second column
	data_out(87 downto 80)		<=	data_in(95 downto 88);
	data_out(47 downto 40)		<=	data_in(87 downto 80);
	data_out(7 downto 0)		<=	data_in(79 downto 72);
	data_out(127 downto 120)	<=	data_in(71 downto 64);
	--third column
	data_out(55 downto 48)		<=	data_in(63 downto 56);
	data_out(15 downto 8)		<=	data_in(55 downto 48);
	data_out(103 downto 96)		<=	data_in(47 downto 40);
	data_out(95 downto 88)		<=	data_in(39 downto 32);
	--fourth column
	data_out(23 downto 16)		<=	data_in(31 downto 24);
	data_out(111 downto 104)	<=	data_in(23 downto 16);
	data_out(71 downto 64)		<=	data_in(15 downto 8);
	data_out(63 downto 56)		<=	data_in(7 downto 0);


end architecture behavioral;
