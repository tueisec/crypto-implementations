-------------------------------------------------------------------------------
--! @file       CipherCore.vhd
--! @brief      CipherCore for Deoxys, an AES-based authenticated encryption
--!				algorithm with associated data

--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.AEAD_pkg.ALL;

entity CipherCore is
    generic (
        --! Reset behavior
        G_ASYNC_RSTN    : boolean := False; --! Async active low reset
        --! Block size (bits)
        G_DBLK_SIZE     : integer := 128;   --! Data
        G_KEY_SIZE      : integer := 128;   --! Key
        G_TAG_SIZE      : integer := 128;   --! Tag
        --! The number of bits required to hold block size expressed in
        --! bytes = log2_ceil(G_DBLK_SIZE/8)
        G_LBS_BYTES     : integer := 4;
        --! Maximum supported AD/message/ciphertext length = 2^G_MAX_LEN-1
        G_MAX_LEN       : integer := SINGLE_PASS_MAX
    );
    port (
        --! Global
        clk             : in  std_logic;
        rst             : in  std_logic;
        --! PreProcessor (data)
        key             : in  std_logic_vector(G_KEY_SIZE       -1 downto 0);
        bdi             : in  std_logic_vector(G_DBLK_SIZE      -1 downto 0);
        --! PreProcessor (controls)
        key_ready       : out std_logic;
        key_valid       : in  std_logic;
        key_update      : in  std_logic;
        decrypt         : in  std_logic;
        bdi_ready       : out std_logic;
        bdi_valid       : in  std_logic;
        bdi_type        : in  std_logic_vector(3                -1 downto 0);
        bdi_partial     : in  std_logic;
        bdi_eot         : in  std_logic;
        bdi_eoi         : in  std_logic;
        bdi_size        : in  std_logic_vector(G_LBS_BYTES+1    -1 downto 0);
        bdi_valid_bytes : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);
        bdi_pad_loc     : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);

        --! PostProcessor
        bdo             : out std_logic_vector(G_DBLK_SIZE      -1 downto 0);
        bdo_valid       : out std_logic;
        bdo_ready       : in  std_logic;
        bdo_size        : out std_logic_vector(G_LBS_BYTES+1    -1 downto 0);
        msg_auth       	: out std_logic;
        msg_auth_valid  : out std_logic;
        msg_auth_ready  : in std_logic
    );
end entity CipherCore;

architecture structure of CipherCore is


    --! =======================================================================
    --! Datapath Signals
    --! =======================================================================

    --! Registers
    signal bdi_r        : std_logic_vector(128              -1 downto 0);
    signal vbytes_r     : std_logic_vector(128/8            -1 downto 0);
    signal padloc_r     : std_logic_vector(128/8            -1 downto 0);
    signal key_block    : std_logic_vector(128              -1 downto 0);
    signal npub_block   : std_logic_vector(128              -1 downto 0);
    signal ctr_block    : std_logic_vector(G_MAX_LEN	    -1 downto 0);
    signal tag          : std_logic_vector(128              -1 downto 0);
    signal check        : std_logic_vector(128              -1 downto 0);
    signal auth         : std_logic_vector(128              -1 downto 0);

    --! Signals
    signal to_output    : std_logic_vector(128              -1 downto 0);

    --! =======================================================================
    --! Control Signals
    --! =======================================================================

    --!     Register
    signal is_decrypt   : std_logic;
    signal is_eoi       : std_logic;
    signal is_eot       : std_logic;
	signal is_bc_valid  : std_logic;
    --!     Combinatorial
    signal clr_accum    : std_logic;
    signal clr_ctr	    : std_logic;
	signal clr_bc_valid	: std_logic;
    signal ld_key       : std_logic;
    signal ld_npub      : std_logic;
    signal ld_input     : std_logic;
    signal update_auth  : std_logic;
    signal update_check : std_logic;
    signal update_ctr	: std_logic;
	signal update_bc_ctr : std_logic;
    signal en_tag       : std_logic;
    signal sel_final    : std_logic;
	signal sel_bc		: std_logic;
    signal bdi_ready_s  : std_logic;
    signal tweak_mode   : std_logic_vector(3 downto 0);
    signal tweak_mode_r : std_logic_vector(3 downto 0);
	signal not_empty	: std_logic;

    --! =======================================================================
    --! Component Signals
    --! =======================================================================

	signal bc_ctr			: std_logic_vector(5 downto 0);
	signal data_input_bc	: std_logic_vector(128	-1 downto 0);
	signal TK1_in			: std_logic_vector(128	-1 downto 0);
	signal TK2_in			: std_logic_vector(128	-1 downto 0);
	signal data_bc_next		: std_logic_vector(128	-1 downto 0);
	signal TK1_next			: std_logic_vector(128	-1 downto 0);
	signal TK2_next			: std_logic_vector(128	-1 downto 0);
	signal decrypt_bc		: std_logic;

    type t_state is (S_INIT, S_WAIT_START, S_WAIT_KEY,
		S_INIT_KEY, S_WAIT_NPUB, S_INIT_MSG,
		S_WAIT_AD, S_PROC_AD, S_WAIT_DATA,
		S_PROC_DATA, S_INIT_LAST_DATA_EMPTY, S_LAST_DATA_EMPTY,
		S_WAIT_LAST_DATA1, S_PROC_LAST_DATA1, S_WAIT_LAST_DATA2,
		S_PROC_LAST_DATA2, S_OUT_TAG, S_WAIT_TAG_AUTH);
    signal state        : t_state;
    signal nstate       : t_state;

begin

    --! =======================================================================
	--! Component instantiations
    --! =======================================================================

		Deoxys_BC_inst: entity work.Deoxys_BC(behavioral) port map(
			bc_ctr		=> bc_ctr,
			decrypt_bc	=> decrypt_bc,
			data_in 	=> data_input_bc,
			TK1_in 		=> TK1_in,
			TK2_in 		=> TK2_in,
			data_out	=> data_bc_next,
			TK1_out 	=> TK1_next,
			TK2_out		=> TK2_next
			);

    --! =======================================================================
    --! Datapath
    --! =======================================================================
    process(clk)
    begin
        if rising_edge(clk) then
            if (ld_input = '1') then
                bdi_r     <= bdi;
                vbytes_r  <= bdi_valid_bytes;
                padloc_r  <= bdi_pad_loc;
            end if;

			--! Save secret key
            if (ld_key = '1') then
                key_block <= key;
            end if;

			--! save public number
            if (ld_npub = '1') then
                npub_block <= bdi;
            end if;

			--! calculate authentication part of the tag
            if (clr_accum = '1') then
                auth	<= (others => '0');
            elsif (update_auth = '1') then
				auth	<= auth xor data_bc_next;
            end if;

			--! calculate checksum
            if (clr_accum = '1') then
                check	<= (others => '0');
            elsif (update_check = '1') then
					if is_decrypt = '1' then
						check	<= check xor to_output;
					else
						check	<= check xor bdi_r;
					end if;
            end if;

			--! calculate the tag
            if (en_tag = '1') then
                tag <= data_bc_next xor auth;
            end if;

			----------------------------------------------------------------
			-------- feed the block cipher ---------------------------------
			----------------------------------------------------------------
			--! Set the correct inputs for the block cipher
			tweak_mode_r 	<= tweak_mode;
		    if (clr_accum = '1') then --! After reset
				data_input_bc 	<= (others => '0');
				TK1_in 			<= (others => '0');
				TK2_in 			<= (others => '0');
		    elsif (sel_bc = '1') then 
				decrypt_bc 				<= '0';
				TK1_in					<= key_block;
				TK2_in (127 downto 124)	<= tweak_mode_r;
				TK2_in (123 downto 31)	<= (others => '0');
				TK2_in (31 downto 0)	<= ctr_block;
				if (tweak_mode_r = "0010" or tweak_mode_r = "0110") then
					data_input_bc 			<= bdi_r;
				elsif (tweak_mode_r = "0000") then
					TK2_in (123 downto 60)	<= npub_block (127 downto 64);
					data_input_bc 			<= bdi_r;
					if is_decrypt = '1' then
				    	decrypt_bc 				<= '1';
					end if;
				elsif (tweak_mode_r = "0001") then
					TK2_in (123 downto 60)	<= npub_block (127 downto 64);
					data_input_bc 				<= check;
				elsif (tweak_mode_r = "0100") then
					TK2_in (123 downto 60)	<= npub_block (127 downto 64);
					data_input_bc 				<= (others => '0');
				elsif (tweak_mode_r = "0101") then
					TK2_in (123 downto 60)	<= npub_block (127 downto 64);
					data_input_bc 				<= check;
				end if;
			elsif update_bc_ctr = '1' and is_bc_valid = '0' then 
				data_input_bc	<= data_bc_next;
				TK1_in	 		<= TK1_next;
				TK2_in			<= TK2_next;
			end if;
        end if;
    end process;



    aBlock: block
        signal vbits : std_logic_vector(G_DBLK_SIZE-1 downto 0);
        signal pad   : std_logic_vector(G_DBLK_SIZE-1 downto 0);
    begin

        gVbits:
        for i in 0 to G_DBLK_SIZE/8-1 generate
            --! Get valid bits from valid bytes
            vbits(8*i+7 downto 8*i) <= (others => vbytes_r(i));
            --! Get padding bit
            pad  (8*i+7           ) <= padloc_r(i);
            pad  (8*i+6 downto 8*i) <= (others => '0');
        end generate;

		--! Choose correct calculation for output
		to_output	<= pad or ((bdi_r xor data_bc_next) and vbits) when not_empty = '1' else pad or (data_bc_next and vbits);
		bdo			<= data_bc_next xor auth when sel_final = '1' else to_output;

    end block;

    msg_auth <= '1' when tag = bdi else '0';
    
    bdo_size <= (others => '-');

    --! =======================================================================
    --! Control
    --! =======================================================================
    gSyncRst:
    if (not G_ASYNC_RSTN) generate
        process(clk)
        begin
            if rising_edge(clk) then
                if (rst = '1') then
                    state <= S_INIT;
                else
                    state <= nstate;
                end if;
            end if;
        end process;
    end generate;
    gAsyncRstn:
    if (G_ASYNC_RSTN) generate
        process(clk, rst)
        begin
            if (rst = '0') then
                state <= S_INIT;
            elsif rising_edge(clk) then
                state <= nstate;
            end if;
        end process;
    end generate;

	ctrlregs:
    process(clk)
    begin
        if rising_edge(clk) then
			--! count the number of block
            if (clr_accum = '1' or clr_ctr = '1') then --! After reset
                ctr_block <= (others => '0');
            elsif (update_ctr = '1') then
                ctr_block <= std_logic_vector(unsigned(ctr_block) + 1);
            end if;
			
			--! store bdi_eoi and bdi_eot of last bdi_valid
            if (clr_accum = '1') then --! After reset
                is_eoi <= '1';
                is_eot <= '1';
            elsif (bdi_ready_s = '1') then 
                is_eoi <= bdi_eoi;
                is_eot <= bdi_eot;
            end if;

			--! store if the output of the block cipher is valid
			--! and update block cipher roundcounter
			if (clr_accum = '1' or clr_bc_valid = '1') then
				is_bc_valid <= '0';
                bc_ctr		<= (others => '0');
			elsif unsigned(bc_ctr) = 14 then
				is_bc_valid <= '1';
			elsif unsigned(bc_ctr) = 13 then
				is_bc_valid <= '1';
                bc_ctr			<= std_logic_vector(unsigned(bc_ctr) + 1);
			elsif (update_bc_ctr = '1') then
                bc_ctr			<= std_logic_vector(unsigned(bc_ctr) + 1);
			end if;

            --! store decrypt signal internally for use during the tag
            --! authentication state.
            if (state = S_WAIT_START) then
                is_decrypt <= decrypt;
            end if;
        end if;
    end process;


    bdi_ready <= bdi_ready_s; --! needed because: if (bdi_ready_s = '1')
	
	FSM:
    process(
        state, key_valid, key_update, is_decrypt, is_eoi,
        bdi_valid, bdi_type, bdi_eot,  is_eot, is_bc_valid,
        bdi_eoi, bdi_size, bdo_ready, msg_auth_ready, 
		bc_ctr)
    begin
        --! Internal
        nstate      	<= state;
        clr_accum   	<= '0';
        clr_ctr   		<= '0';
		clr_bc_valid	<= '0';
        ld_key      	<= '0';
        ld_npub     	<= '0';
        ld_input    	<= '0';
        update_check	<= '0';
        update_auth		<= '0';
        update_ctr		<= '0';
        update_bc_ctr	<= '0';
        en_tag      	<= '0';
        sel_final   	<= '0';
        sel_bc			<= '0';
		not_empty		<= '0';
		tweak_mode 		<= (others => '0');
        --! External
        key_ready   	<= '0';
        bdi_ready_s 	<= '0';
        bdo_valid   	<= '0';
        msg_auth_valid 	<= '0';

        case state is
            when S_INIT =>
                --! After reset
                clr_accum	<= '1';
                nstate 		<= S_WAIT_START;

            when S_WAIT_START =>
                --! Needs to check whether a new input is available first
                --! prior to checking key to ensure the correct operational
                --! step.
                if (bdi_valid = '1') then
                    if (key_update = '1') then
                        nstate <= S_WAIT_KEY;
                    else
                        nstate <= S_WAIT_NPUB;
                    end if;
                end if;

            when S_WAIT_KEY =>
                --! Wait for key
                key_ready <= '1';
                if (key_valid = '1') then
                    ld_key    <= '1';
                    nstate    <= S_INIT_KEY;
                end if;

            when S_INIT_KEY =>
                --! key initialization within 1 clock cycle
                nstate <= S_WAIT_NPUB;

            when S_WAIT_NPUB =>
				--! Wait for public number
                bdi_ready_s <= '1';
                if (bdi_valid = '1') then
                    ld_npub 	<= '1';
					if (bdi_eoi = '1') then
						nstate 		<= S_INIT_LAST_DATA_EMPTY;
						tweak_mode 	<= "0001";
					else
						nstate  	<= S_INIT_MSG;
					end if;
                end if;

            when S_INIT_MSG =>
                --! first msg initialization within 1 clock cycle
				clr_bc_valid	<= '1';
                if (bdi_valid = '1') then
                    ld_input 	<= '1';
                    if (bdi_type(2 downto 1) = BDI_TYPE_ASS) then
                        --! Note: Assume ST_AD is used (see: AEAD_pkg.vhd)
                   		bdi_ready_s <= '1';
                        nstate <= S_WAIT_AD;
						if bdi_size = "10000" then
							tweak_mode 	<= "0010";  
						else
							tweak_mode 	<= "0110";  
						end if;                      
                    elsif (bdi_type(2 downto 1) = BDI_TYPE_DAT) then
                   		bdi_ready_s <= '1';
						if bdi_size = "10000" then
		                    nstate 		<= S_WAIT_DATA;
							tweak_mode 	<= "0000";
						else
		                    nstate 		<= S_WAIT_LAST_DATA1;
							tweak_mode 	<= "0100";
						end if;
					end if;
                end if;

            when S_WAIT_AD =>
				clr_bc_valid	<= '1';
				sel_bc 		<= '1';
				nstate 		<= S_PROC_AD;

            when S_PROC_AD =>
				--! Process associated data
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
                    update_auth		<= '1';
                    if (is_eoi = '1') then
                        nstate 			<= S_INIT_LAST_DATA_EMPTY;
						tweak_mode 	<= "0001";
                    elsif (is_eot = '1') then
                        nstate   		<= S_INIT_MSG;
                        clr_ctr  		<= '1';
					else
                        nstate   		<= S_INIT_MSG;
                        update_ctr  	<= '1';
                    end if;
					--! start copy pasted S_INIT_MSG
		            if (bdi_valid = '1') then --! possibly skip 1 clock cycle
		                ld_input 	<= '1';
		                if (bdi_type(2 downto 1) = BDI_TYPE_ASS) then
		                    --! Note: Assume ST_AD is used (see: AEAD_pkg.vhd)
		               		bdi_ready_s <= '1';
		                    nstate <= S_WAIT_AD;
							if bdi_size = "10000" then
								tweak_mode 	<= "0010";  
							else
								tweak_mode 	<= "0110";  
							end if;                      
		                elsif (bdi_type(2 downto 1) = BDI_TYPE_DAT) then
		               		bdi_ready_s <= '1';
							if bdi_size = "10000" then
				                nstate 		<= S_WAIT_DATA;
								tweak_mode 	<= "0000";
							else
				                nstate 		<= S_WAIT_LAST_DATA1;
								tweak_mode 	<= "0100";
							end if;
						end if;
		            end if;
					--! end copy pasted S_INIT_MSG
                end if;

			when S_WAIT_DATA =>
				sel_bc 		<= '1';
				clr_bc_valid	<= '1';
				nstate 		<= S_PROC_DATA;

            when S_PROC_DATA =>
				--! Process data/message
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
                    if (bdo_ready = '1') then
		            	update_check 	<= '1';
                        bdo_valid   	<= '1';
                        update_ctr  	<= '1';
		                if (is_eoi = '1') then
		                    nstate 			<= S_INIT_LAST_DATA_EMPTY;
							tweak_mode 	<= "0001";
		                else
							nstate      	<= S_INIT_MSG;
						end if;
					end if;
					--! start copy pasted S_INIT_MSG
		            if (bdi_valid = '1') then --! possibly skip 1 clock cycle
		                ld_input 	<= '1';
		                if (bdi_type(2 downto 1) = BDI_TYPE_ASS) then
		                    --! Note: Assume ST_AD is used (see: AEAD_pkg.vhd)
		               		bdi_ready_s <= '1';
		                    nstate <= S_WAIT_AD;
							if bdi_size = "10000" then
								tweak_mode 	<= "0010";  
							else
								tweak_mode 	<= "0110";  
							end if;                      
		                elsif (bdi_type(2 downto 1) = BDI_TYPE_DAT) then
		               		bdi_ready_s <= '1';
							if bdi_size = "10000" then
				                nstate 		<= S_WAIT_DATA;
								tweak_mode 	<= "0000";
							else
				                nstate 		<= S_WAIT_LAST_DATA1;
								tweak_mode 	<= "0100";
							end if;
						end if;
		            end if;
					--! end copy pasted S_INIT_MSG
                end if;

            when S_INIT_LAST_DATA_EMPTY =>
				--! initialize special state S_LAST_DATA_EMPTY
				sel_bc 		<= '1';
				clr_bc_valid	<= '1';
            	nstate   	<= S_LAST_DATA_EMPTY;

            when S_LAST_DATA_EMPTY =>
				--! Process the checksum if last data/message block was empty
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
                    if (bdo_ready = '1' or is_decrypt = '1') then
                        sel_final		<= '1';
                        if (is_decrypt = '1') then
                            en_tag    		<= '1';
                            nstate    		<= S_WAIT_TAG_AUTH;
                        else
                            nstate    		<= S_OUT_TAG;
                        end if;
                    end if;
                end if;

			when S_WAIT_LAST_DATA1 =>
				sel_bc 		<= '1';
				clr_bc_valid	<= '1';
				nstate 		<= S_PROC_LAST_DATA1;

            when S_PROC_LAST_DATA1 =>
				--! Process the last data/message block if it's padded
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
                    if (bdo_ready = '1') then
		            	update_check 	<= '1';
		                update_ctr  	<= '1';
						not_empty		<= '1';
                        bdo_valid		<= '1';
                        nstate			<= S_WAIT_LAST_DATA2;
						tweak_mode 	<= "0101";
                    end if;
                end if;

            when S_WAIT_LAST_DATA2 =>
                --! initialize special state S_LAST_DATA2
        		nstate      <= S_PROC_LAST_DATA2;
				clr_bc_valid	<= '1';
				sel_bc 		<= '1';

            when S_PROC_LAST_DATA2 =>
				--! Process the checksum if last data/message block was padded
        		update_bc_ctr	<= '1';
                if is_bc_valid = '1' then
        			update_bc_ctr	<= '0';
                    if (bdo_ready = '1' or is_decrypt = '1') then
                        sel_final 		<= '1';
                        if (is_decrypt = '1') then
                            en_tag    		<= '1';
                            nstate    		<= S_WAIT_TAG_AUTH;
                        else
                            nstate    		<= S_OUT_TAG;
                        end if;
                    end if;
                end if;

            when S_OUT_TAG =>
				--! Output the Tag (Encryption)
                if (bdo_ready = '1') then
					clr_bc_valid	<= '1';
                    sel_final	<= '1';
                	bdo_valid	<= '1';
                    en_tag		<= '1';
                    nstate  	<= S_INIT;
                end if;

            when S_WAIT_TAG_AUTH =>
				--! Authenticate Tag (Decryption)
                if (bdi_valid = '1' and  msg_auth_ready = '1') then
					clr_bc_valid	<= '1';
                	bdi_ready_s 	<= '1';
                    msg_auth_valid	<= '1';
                    nstate			<= S_INIT;
                end if;
        end case;
    end process;
end structure;
