-------------------------------------------------------------------------------
--! @file       tweakey_only_full.vhd
--! @brief      updating the tweakeys for the first round of the deoxys block
--!				cipher decryption
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


-------------------------------------------------------------------------------------

entity tweakey_only_full is
	port(	--subtweakeys
		in_ART_W1	: in std_logic_vector (127 downto 0);
		in_ART_W2	: in std_logic_vector (127 downto 0);
		out_ART_W1	: out std_logic_vector (127 downto 0);
		out_ART_W2	: out std_logic_vector (127 downto 0)
	);
end tweakey_only_full;

architecture Behavioral of tweakey_only_full is

-------------------------------------------------------------------------------------
-- Component declarations
-------------------------------------------------------------------------------------

component tweakey_only
	port(
		in_ART_W1	: in std_logic_vector (127 downto 0);
		in_ART_W2	: in std_logic_vector (127 downto 0);
		out_ART_W1	: out std_logic_vector (127 downto 0);
		out_ART_W2	: out std_logic_vector (127 downto 0)
	);
end component tweakey_only;

-------------------------------------------------------------------------------------
-- Signals
-------------------------------------------------------------------------------------

signal in_W1_0		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_0		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_0		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_0		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_1		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_1		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_1		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_1		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_2		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_2		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_2		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_2		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_3		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_3		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_3		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_3		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_4		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_4		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_4		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_4		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_5		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_5		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_5		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_5		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_6		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_6		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_6		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_6		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_7		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_7		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_7		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_7		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_8		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_8		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_8		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_8		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_9		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_9		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_9		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_9		: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_10		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_10		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_10	: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_10	: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_11		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_11		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_11	: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_11	: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_12		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_12		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_12	: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_12	: std_logic_vector (127 downto 0) := (others => '0');

signal in_W1_13		: std_logic_vector (127 downto 0) := (others => '0');
signal in_W2_13		: std_logic_vector (127 downto 0) := (others => '0');
signal out_W1_13	: std_logic_vector (127 downto 0) := (others => '0');
signal out_W2_13	: std_logic_vector (127 downto 0) := (others => '0');



begin -- architecture Behavioral

---------------------------------------------------------------------------------------------
---------- Component instantiations
---------------------------------------------------------------------------------------------

	inst_subtweakey_0: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_0,
		in_ART_W2	=> in_W2_0,
		out_ART_W1	=> out_W1_0,
		out_ART_W2	=> out_W2_0
		);

	inst_subtweakey_1: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_1,
		in_ART_W2	=> in_W2_1,
		out_ART_W1	=> out_W1_1,
		out_ART_W2	=> out_W2_1
		);

	inst_subtweakey_2: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_2,
		in_ART_W2	=> in_W2_2,
		out_ART_W1	=> out_W1_2,
		out_ART_W2	=> out_W2_2
		);

	inst_subtweakey_3: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_3,
		in_ART_W2	=> in_W2_3,
		out_ART_W1	=> out_W1_3,
		out_ART_W2	=> out_W2_3
		);

	inst_subtweakey_4: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_4,
		in_ART_W2	=> in_W2_4,
		out_ART_W1	=> out_W1_4,
		out_ART_W2	=> out_W2_4
		);

	inst_subtweakey_5: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_5,
		in_ART_W2	=> in_W2_5,
		out_ART_W1	=> out_W1_5,
		out_ART_W2	=> out_W2_5
		);

	inst_subtweakey_6: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_6,
		in_ART_W2	=> in_W2_6,
		out_ART_W1	=> out_W1_6,
		out_ART_W2	=> out_W2_6
		);

	inst_subtweakey_7: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_7,
		in_ART_W2	=> in_W2_7,
		out_ART_W1	=> out_W1_7,
		out_ART_W2	=> out_W2_7
		);

	inst_subtweakey_8: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_8,
		in_ART_W2	=> in_W2_8,
		out_ART_W1	=> out_W1_8,
		out_ART_W2	=> out_W2_8
		);

	inst_subtweakey_9: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_9,
		in_ART_W2	=> in_W2_9,
		out_ART_W1	=> out_W1_9,
		out_ART_W2	=> out_W2_9
		);

	inst_subtweakey_10: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_10,
		in_ART_W2	=> in_W2_10,
		out_ART_W1	=> out_W1_10,
		out_ART_W2	=> out_W2_10
		);

	inst_subtweakey_11: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_11,
		in_ART_W2	=> in_W2_11,
		out_ART_W1	=> out_W1_11,
		out_ART_W2	=> out_W2_11
		);

	inst_subtweakey_12: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_12,
		in_ART_W2	=> in_W2_12,
		out_ART_W1	=> out_W1_12,
		out_ART_W2	=> out_W2_12
		);

	inst_subtweakey_13: entity work.tweakey_only(behavioral) port map(
		in_ART_W1	=> in_W1_13,
		in_ART_W2	=> in_W2_13,
		out_ART_W1	=> out_W1_13,
		out_ART_W2	=> out_W2_13
		);

-------------------------------------------------------------------------------------
-- do purpose of existence
-------------------------------------------------------------------------------------

	in_W1_0		<= in_ART_W1;
	in_W2_0		<= in_ART_W2;
	in_W1_1		<= out_W1_0;
	in_W2_1		<= out_W2_0;
	in_W1_2		<= out_W1_1;
	in_W2_2		<= out_W2_1;
	in_W1_3		<= out_W1_2;
	in_W2_3		<= out_W2_2;
	in_W1_4		<= out_W1_3;
	in_W2_4		<= out_W2_3;
	in_W1_5		<= out_W1_4;
	in_W2_5		<= out_W2_4;
	in_W1_6		<= out_W1_5;
	in_W2_6		<= out_W2_5;
	in_W1_7		<= out_W1_6;
	in_W2_7		<= out_W2_6;
	in_W1_8		<= out_W1_7;
	in_W2_8		<= out_W2_7;
	in_W1_9		<= out_W1_8;
	in_W2_9		<= out_W2_8;
	in_W1_10	<= out_W1_9;
	in_W2_10	<= out_W2_9;
	in_W1_11	<= out_W1_10;
	in_W2_11	<= out_W2_10;
	in_W1_12	<= out_W1_11;
	in_W2_12	<= out_W2_11;
	in_W1_13	<= out_W1_12;
	in_W2_13	<= out_W2_12;
	out_ART_W1	<= out_W1_13;
	out_ART_W2	<= out_W2_13;

end architecture Behavioral;
