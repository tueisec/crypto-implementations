-------------------------------------------------------------------------------
--! @file       tweakey_only.vhd
--! @brief      updating the tweakeys for the next round of the deoxys block
--!				cipher
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


-------------------------------------------------------------------------------------

entity tweakey_only is
	port(	--subtweakeys
		in_ART_W1	: in std_logic_vector (127 downto 0);
		in_ART_W2	: in std_logic_vector (127 downto 0);
		out_ART_W1	: out std_logic_vector (127 downto 0);
		out_ART_W2	: out std_logic_vector (127 downto 0)
	);
end tweakey_only;

architecture Behavioral of tweakey_only is

-------------------------------------------------------------------------------------
-- Signals
-------------------------------------------------------------------------------------

signal buffer_ART_W1		: std_logic_vector (127 downto 0) := (others => '0');
signal RCi			: std_logic_vector (127 downto 0) := (others => '0');

begin -- architecture Behavioral

-------------------------------------------------------------------------------------
-- do purpose of existence
-------------------------------------------------------------------------------------

	
	do_LFSR2: for i in 0 to 15 generate
	begin
		buffer_ART_W1((127-(i*8)) downto (121-(i*8))) 	<= 	in_ART_W1((126-(i*8)) downto (120-(i*8)));
		buffer_ART_W1(120-(i*8))			<=	in_ART_W1(127-(i*8)) xor in_ART_W1(125-(i*8));
	end generate;
	
	--permutation h
	--first column
	out_ART_W2(119 downto 112)	<=	in_ART_W2(127 downto 120);
	out_ART_W2(79 downto 72)	<=	in_ART_W2(119 downto 112);
	out_ART_W2(39 downto 32)	<=	in_ART_W2(111 downto 104);
	out_ART_W2(31 downto 24)	<=	in_ART_W2(103 downto 96);
	--second column
	out_ART_W2(87 downto 80)	<=	in_ART_W2(95 downto 88);
	out_ART_W2(47 downto 40)	<=	in_ART_W2(87 downto 80);
	out_ART_W2(7 downto 0)		<=	in_ART_W2(79 downto 72);
	out_ART_W2(127 downto 120)	<=	in_ART_W2(71 downto 64);
	--third column
	out_ART_W2(55 downto 48)	<=	in_ART_W2(63 downto 56);
	out_ART_W2(15 downto 8)		<=	in_ART_W2(55 downto 48);
	out_ART_W2(103 downto 96)	<=	in_ART_W2(47 downto 40);
	out_ART_W2(95 downto 88)	<=	in_ART_W2(39 downto 32);
	--fourth column
	out_ART_W2(23 downto 16)	<=	in_ART_W2(31 downto 24);
	out_ART_W2(111 downto 104)	<=	in_ART_W2(23 downto 16);
	out_ART_W2(71 downto 64)	<=	in_ART_W2(15 downto 8);
	out_ART_W2(63 downto 56)	<=	in_ART_W2(7 downto 0);
	--permutation h
	--first column
	out_ART_W1(119 downto 112)	<=	buffer_ART_W1(127 downto 120);
	out_ART_W1(79 downto 72)	<=	buffer_ART_W1(119 downto 112);
	out_ART_W1(39 downto 32)	<=	buffer_ART_W1(111 downto 104);
	out_ART_W1(31 downto 24)	<=	buffer_ART_W1(103 downto 96);
	--second column
	out_ART_W1(87 downto 80)	<=	buffer_ART_W1(95 downto 88);
	out_ART_W1(47 downto 40)	<=	buffer_ART_W1(87 downto 80);
	out_ART_W1(7 downto 0)		<=	buffer_ART_W1(79 downto 72);
	out_ART_W1(127 downto 120)	<=	buffer_ART_W1(71 downto 64);
	--third column
	out_ART_W1(55 downto 48)	<=	buffer_ART_W1(63 downto 56);
	out_ART_W1(15 downto 8)		<=	buffer_ART_W1(55 downto 48);
	out_ART_W1(103 downto 96)	<=	buffer_ART_W1(47 downto 40);
	out_ART_W1(95 downto 88)	<=	buffer_ART_W1(39 downto 32);
	--fourth column
	out_ART_W1(23 downto 16)	<=	buffer_ART_W1(31 downto 24);
	out_ART_W1(111 downto 104)	<=	buffer_ART_W1(23 downto 16);
	out_ART_W1(71 downto 64)	<=	buffer_ART_W1(15 downto 8);
	out_ART_W1(63 downto 56)	<=	buffer_ART_W1(7 downto 0);


end architecture Behavioral;
