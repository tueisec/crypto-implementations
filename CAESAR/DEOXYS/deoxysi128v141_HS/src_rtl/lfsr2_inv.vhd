-------------------------------------------------------------------------------
--! @file       lfsr2_inv.vhd
--! @brief      inverse left shift feedback register. this operation is needed for
--!				updating the tweakey for decryption
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------------

entity lfsr2_inv is
	port(
		data_in		: in std_logic_vector (127 downto 0);
		data_out	: out std_logic_vector (127 downto 0)
		);
end lfsr2_inv;

architecture behavioral of lfsr2_inv is


begin

	do_LFSR2_inv: for i in 0 to 15 generate
				data_out((126-(i*8)) downto (120-(i*8)))	<= 	data_in((127-(i*8)) downto (121-(i*8)));
				data_out(127-(i*8))							<=	data_in(120-(i*8)) xor data_in(126-(i*8));
	end generate;


end architecture behavioral;
