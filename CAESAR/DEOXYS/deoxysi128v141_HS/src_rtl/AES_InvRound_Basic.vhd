-------------------------------------------------------------------------------
--! @file       AES_InvRound_Basic.vhd
--! @brief      An InvRound used by AES decryption operation
--! @project    CAESAR Candidate Evaluation
--! @author     Ekawat (ice) Homsirikamol
--! @version    1.0
--! @copyright  Copyright (c) 2014 Cryptographic Engineering Research Group
--!             ECE Department, George Mason University Fairfax, VA, U.S.A.
--!             All rights Reserved.
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       This is publicly available encryption source code that falls
--!             under the License Exception TSU (Technology and software-
--!             —unrestricted)
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.AES_pkg.all;

entity AES_InvRound_Basic is
    port(
        din 		: in  t_AES_state;
        rkey        : in  t_AES_state;
        dout		: out t_AES_state
    );
end AES_InvRound_Basic;

-------------------------------------------------------------------------------
--! @brief  Architecture definition of AES_InvRound_Basic
-------------------------------------------------------------------------------

architecture structure of AES_InvRound_Basic is
    signal	after_invmixcolumns     : t_AES_state;
    signal	after_invshiftrows		: t_AES_state;
    signal	after_invsubbytes		: t_AES_state;
    signal	after_addroundkey		: t_AES_state;
begin
    --! InvMixColumns
    mc	: entity work.AES_InvMixColumns(basic)	port map (input=>din, output=>after_invmixcolumns);

    --! InvShiftRows
    sr	: entity work.AES_InvShiftRows(basic)	port map (input=>after_invmixcolumns, output=>after_invshiftrows);

    --! InvSubBytes
    sb	: entity work.AES_InvSubBytes(basic)	port map (input=>after_invshiftrows, output=>after_invsubbytes);


    --! AddRoundKey
    gAddRoundKeyRow: for i in 0 to 3 generate
        gAddRoundKeyCol: for j in 0 to 3 generate
            after_addroundkey(j,i) <= after_invsubbytes(j,i) xor rkey(j,i);
        end generate;
    end generate;

    dout <= after_addroundkey;


end structure;