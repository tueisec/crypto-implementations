-------------------------------------------------------------------------------
--! @file       Deoxys_BC.vhd
--! @brief      deoxys block cipher for encrypting and decrypting a message block
--!				
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Simon Schilling
--! @copyright  Copyright © 2018 Simon Schilling
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.AES_PKG.all;

entity Deoxys_BC is
    generic (
        --! Reset behavior
        G_ASYNC_RSTN    : boolean := False --! Async active low reset
	);
	port (
		bc_ctr			: in std_logic_vector(5 downto 0);
		decrypt_bc		: in std_logic; --'0': encryption ; '1': decrpytion
		data_in			: in std_logic_vector(127 downto 0);
		TK1_in			: in std_logic_vector(127 downto 0); --key
		TK2_in			: in std_logic_vector(127 downto 0); --tweak
		data_out		: out std_logic_vector(127 downto 0);
		TK1_out			: out std_logic_vector(127 downto 0); --key
		TK2_out			: out std_logic_vector(127 downto 0) --tweak
	);
end Deoxys_BC;

architecture Behavioral of Deoxys_BC is


-------------------------------------------------------------------------------------
-- Signals
-------------------------------------------------------------------------------------

	--! signals for AES
    signal rdkey            	: std_logic_vector(127 downto 0);
    signal from_round_enc   	: std_logic_vector(127 downto 0);
    signal from_round_dec   	: std_logic_vector(127 downto 0);
    signal state_map        	: t_AES_state;
    signal rdkey_map        	: t_AES_state;
    signal from_round_enc_map 	: t_AES_state;
    signal from_round_dec_map 	: t_AES_state;

	--! signals for other block cipher mdoules
	signal from_TK1_calc	: std_logic_vector(127 downto 0);
	signal from_TK2_calc	: std_logic_vector(127 downto 0);
	signal TK1_pre			: std_logic_vector(127 downto 0);
	signal TK1_buff_enc		: std_logic_vector(127 downto 0);
	signal TK1_buff_dec		: std_logic_vector(127 downto 0);
	signal TK2_pre			: std_logic_vector(127 downto 0);
	signal TK2_post_enc		: std_logic_vector(127 downto 0);
	signal TK2_post_dec		: std_logic_vector(127 downto 0);
	signal TK1_post_enc		: std_logic_vector(127 downto 0);
	signal TK1_post_dec		: std_logic_vector(127 downto 0);
    signal from_round_RCON	: std_logic_vector(127 downto 0);
    signal bc_ctr_buff		: std_logic_vector(5 downto 0);

begin -- architecture Behavioral

	--! AES
    u_map1: entity work.AES_map(structure)
    port map ( ii => data_in, oo => state_map);
    u_map2: entity work.AES_map(structure)
    port map ( ii => rdkey, oo => rdkey_map);

    u_enc: entity work.AES_Round_Basic
    port map (
        din     => state_map,
        rkey    => rdkey_map,
        dout    => from_round_enc_map);
    u_dec: entity work.AES_InvRound_Basic
    port map (
        din     => state_map,
        rkey    => rdkey_map,
        dout    => from_round_dec_map);

    u_imap_enc: entity work.AES_invmap(structure)
    port map ( ii => from_round_enc_map, oo => from_round_enc);
    u_imap_dec: entity work.AES_invmap(structure)
    port map ( ii => from_round_dec_map, oo => from_round_dec);

	--! other block cipher modules

	inst_calculate_endtweakey: entity work.tweakey_only_full(behavioral) port map(
		in_ART_W1	=> TK1_in,
		in_ART_W2	=> TK2_in,
		out_ART_W1	=> from_TK1_calc,
		out_ART_W2	=> from_TK2_calc
		);

	u_perm_h_enc1: entity work.perm_h(behavioral)
	port map (
		data_in 	=> TK1_pre,
		data_out	=> TK1_buff_enc);

	u_perm_h_dec1: entity work.perm_h_inv(behavioral)
	port map (
		data_in 	=> TK1_pre,
		data_out	=> TK1_buff_dec);

	u_perm_h_enc2: entity work.perm_h(behavioral)
	port map (
		data_in 	=> TK2_pre,
		data_out	=> TK2_post_enc);

	u_perm_h_dec2: entity work.perm_h_inv(behavioral)
	port map (
		data_in 	=> TK2_pre,
		data_out	=> TK2_post_dec);

	u_lfsr2_enc: entity work.lfsr2(behavioral)
	port map (
		data_in 	=> TK1_buff_enc,
		data_out	=> TK1_post_enc);

	u_lfsr2_dec: entity work.lfsr2_inv(behavioral)
	port map (
		data_in 	=> TK1_buff_dec,
		data_out	=> TK1_post_dec);

	u_RCON: entity work.RCON(behavioral)
	port map (
		round	 	=> bc_ctr_buff,
		data_out	=> from_round_RCON);

    --! =======================================================================
    --! Datapath
    --! =======================================================================


    process(bc_ctr, decrypt_bc, data_in, TK1_in, TK2_in, TK1_pre, TK2_pre, from_TK1_calc, 
			from_TK2_calc, TK1_buff_enc, TK1_buff_dec, TK2_post_enc, TK2_post_dec,
			TK1_post_enc, TK1_post_dec, from_round_RCON, from_round_enc_map,
			from_round_dec_map, from_round_enc, from_round_dec, bc_ctr_buff)
    begin

		if decrypt_bc = '0' then
			bc_ctr_buff <= bc_ctr;
		else
			bc_ctr_buff <= std_logic_vector(14 - unsigned(bc_ctr));
		end if;

		if to_integer(unsigned(bc_ctr)) = 0 then
			if decrypt_bc = '0' then
				data_out		<= data_in xor TK2_in xor TK1_in xor from_round_RCON;
				rdkey			<= (others => '0');
				TK1_out			<= TK1_in;
				TK2_out			<= TK2_in;
			else
				data_out		<= data_in xor from_TK1_calc xor from_TK2_calc xor from_round_RCON;
				rdkey			<= (others => '0');
				TK1_out			<= from_TK1_calc;
				TK2_out			<= from_TK2_calc;
			end if;
		else
			if decrypt_bc = '0' then
				TK1_pre			<= TK1_in;
				TK2_pre			<= TK2_in;
				rdkey			<= TK1_post_enc xor TK2_post_enc xor from_round_RCON;
				data_out		<= from_round_enc;
				TK1_out			<= TK1_post_enc;
				TK2_out			<= TK2_post_enc;
			else
				TK1_pre			<= TK1_in;
				TK2_pre			<= TK2_in;
				rdkey			<= TK1_post_dec xor TK2_post_dec xor from_round_RCON;
				data_out		<= from_round_dec;
				TK1_out			<= TK1_post_dec;
				TK2_out			<= TK2_post_dec;
			end if;
		end if;
	end process;

end architecture Behavioral;
