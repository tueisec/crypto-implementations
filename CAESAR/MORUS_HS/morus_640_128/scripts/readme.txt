This script starts ModelSim simulation of the provided reference code.

To run, perform the following steps:
1) Copy KAT files from kat folder
    or
   Generate KAT files using _REF_PRJ_NAME_.py 
   located in software/aeadtvgen/examples
2) Open ModelSim
3) In ModelSim console, type:
    > cd _PATH_TO_THIS_DIRECTORY_
    > source modelsim.tcl
    > ldd

