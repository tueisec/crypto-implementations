-------------------------------------------------------------------------------
--! @file       mours_pkg.vhd
--! @brief      Package for the Morus implementation.
--! @project    CAESAR Candidate Morus
--! @author     Michael Tempelmeier
--! @date       2019-29-01
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Muncih, GERMANY
--!             All rights Reserved.
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package morus_pkg is

type MORUS_FLAVOR_t is (MORUS_640_128, MORUS_1280_128, MORUS_1280_256);

-- You also need to change the values G_ABLK_SIZE and G_DBLK_SIZE in AEAD.vhd !!
-- Attention: Due to a unfixed Bug in the PreProcessor, G_W, PDI_SIZE, G_DBLK_SIZE, must alle be the SAME
--            The PreProcessor provides wrong data (on real hardware - not in simulation) on bdi_size, if they don't match

----Mours-640
--constant MORUS_FLAVOR : MORUS_FLAVOR_t := MORUS_640_128;
--constant MORUS_STATE_SIZE : integer := 640;
--constant MORUS_KEY_SIZE : integer :=128;
--constant b0 : integer :=5;
--constant b1 : integer :=31;
--constant b2 : integer :=7;
--constant b3 : integer :=22;
--constant b4 : integer :=13;

--constant w0 : integer := 32;
--constant w1 : integer := 64;
--constant w2 : integer := 96;
--constant w3 : integer := 64;
--constant w4 : integer := 32;


--Mours-1280-256
constant MORUS_FLAVOR : MORUS_FLAVOR_t := MORUS_1280_256;
constant MORUS_STATE_SIZE : integer := 1280;
constant MORUS_KEY_SIZE : integer :=256;

constant b0 : integer :=13;
constant b1 : integer :=46;
constant b2 : integer :=38;
constant b3 : integer :=7;
constant b4 : integer :=4;

constant w0 : integer := 64;
constant w1 : integer := 128;
constant w2 : integer := 192;
constant w3 : integer := 128;
constant w4 : integer := 64;



----Mours-1280-128
--constant MORUS_FLAVOR : MORUS_FLAVOR_t := MORUS_1280_128;
--constant MORUS_STATE_SIZE : integer := 1280;
--constant MORUS_KEY_SIZE : integer :=128;

--constant b0 : integer :=13;
--constant b1 : integer :=46;
--constant b2 : integer :=38;
--constant b3 : integer :=7;
--constant b4 : integer :=4;

--constant w0 : integer := 64;
--constant w1 : integer := 128;
--constant w2 : integer := 192;
--constant w3 : integer := 128;
--constant w4 : integer := 64;


--- no need for configuration here
-- all constants are either the same for all flavors or are calculated from the values above

constant MORUS_WORD_SIZE : integer := MORUS_STATE_SIZE/5;
constant MORUS_NPUB_SIZE : integer := 128;
constant MORUS_TAG_SIZE  : integer := 128;
constant ONES_128 : std_logic_vector(127 downto 0) := (others =>'1');
constant ONES_256 : std_logic_vector(255 downto 0) := (others =>'1');
constant ZEROS_128 : std_logic_vector(127 downto 0) := (others =>'0');
constant ZEROS_256 : std_logic_vector(255 downto 0) := (others =>'0');
constant morus_const0: std_logic_vector(127 downto 0) := X"00_01_01_02_03_05_08_0d_15_22_37_59_90_e9_79_62";  -- fibonaci sequence
constant morus_const1: std_logic_vector(127 downto 0) := X"db_3d_18_55_6d_c2_2f_f1_20_11_31_42_73_b5_28_dd";  -- modullo 256



function rotl(vector_in: std_logic_vector; shift: integer) return std_logic_vector;
function rotr(vector_in: std_logic_vector; shift: integer) return std_logic_vector;
function Rotl_xxx_yy (vector_in: std_logic_vector; shift: integer) return std_logic_vector;
function ByteToBits (bytes_in : std_logic_vector) return std_logic_vector;
function convert_endianness(vector_in : std_logic_vector) return std_logic_vector;
end;



package body morus_pkg is

function convert_endianness(
    vector_in : std_logic_vector
    ) return std_logic_vector is
    

constant vector_size : integer := vector_in'length;    
alias vector_in_a : std_logic_vector(vector_in'length -1 downto 0) is vector_in;
variable vector_r : std_logic_vector (vector_size -1 downto 0);
begin

   for i in 0 to (vector_size/8)-1 loop
        vector_r(vector_size-8*i-1 downto vector_size-8*(i+1) ) := vector_in_a(8*i+7 downto i*8);
    end loop;
   return vector_r;

end convert_endianness;

function rotr (
   vector_in : std_logic_vector; 
   shift     : integer            --w
) return std_logic_vector is

begin
   return vector_in(vector_in'right+shift-1 downto vector_in'right) & vector_in(vector_in'left downto vector_in'right+shift);
end rotr;



function rotl (
   vector_in : std_logic_vector; 
   shift     : integer            --w
) return std_logic_vector is

begin
   return vector_in(vector_in'left - shift downto vector_in'right) & vector_in(vector_in'left downto vector_in'left - shift + 1);
end rotl;


function Rotl_xxx_yy (
    vector_in   : std_logic_vector;  --Si
    shift       : integer            --bi
) return std_logic_vector is

alias vector_in_a : std_logic_vector(vector_in'length -1 downto 0) is vector_in;


constant vector_size : integer := vector_in'length; --xxx
constant word_size : integer := vector_size / 4;    --yy

variable word0, word1, word2, word3 : std_logic_vector (word_size-1 downto 0);
variable word0_r, word1_r, word2_r, word3_r : std_logic_vector (word_size-1 downto 0);
	
begin
  -- word3 := rotl(vector_in(vector_size            -1 downto vector_size-  word_size), shift);
  -- word2 := rotl(vector_in(vector_size-  word_size-1 downto vector_size-2*word_size), shift);
  -- word1 := rotl(vector_in(vector_size-2*word_size-1 downto vector_size-3*word_size), shift);
  -- word0 := rotl(vector_in(vector_size-3*word_size-1 downto                       0), shift);
   
   word3 := vector_in_a(vector_size            -1 downto vector_size-  word_size);
   word2 := vector_in_a(vector_size-  word_size-1 downto vector_size-2*word_size);
   word1 := vector_in_a(vector_size-2*word_size-1 downto vector_size-3*word_size);
   word0 := vector_in_a(vector_size-3*word_size-1 downto                       0);
   
   for i in 0 to (word_size/8)-1 loop
        word0_r(word_size-8*i-1 downto word_size-8*(i+1) ) := word0(8*i+7 downto i*8);
        word1_r(word_size-8*i-1 downto word_size-8*(i+1) ) := word1(8*i+7 downto i*8);
        word2_r(word_size-8*i-1 downto word_size-8*(i+1) ) := word2(8*i+7 downto i*8);
        word3_r(word_size-8*i-1 downto word_size-8*(i+1) ) := word3(8*i+7 downto i*8);
    end loop;
   
   
   word3 := rotl(word3_r, shift);
   word2 := rotl(word2_r, shift);
   word1 := rotl(word1_r, shift);
   word0 := rotl(word0_r, shift);
   
   for i in 0 to (word_size/8)-1 loop
       word0_r(word_size-8*i-1 downto word_size-8*(i+1) ) := word0(8*i+7 downto i*8);
       word1_r(word_size-8*i-1 downto word_size-8*(i+1) ) := word1(8*i+7 downto i*8);
       word2_r(word_size-8*i-1 downto word_size-8*(i+1) ) := word2(8*i+7 downto i*8);
       word3_r(word_size-8*i-1 downto word_size-8*(i+1) ) := word3(8*i+7 downto i*8);
   end loop;
      
   return word3_r & word2_r & word1_r & word0_r;
   
end Rotl_xxx_yy;


function ByteToBits (
    bytes_in : std_logic_vector
    ) return std_logic_vector is
    
    variable bits : std_logic_vector ( (8* bytes_in'length) -1 downto 0);
begin

    for i in 0 to bytes_in'length-1 loop
        if (bytes_in(i) = '1') then
            bits(8*(i+1)-1 downto 8*i) := (others =>'1');
        else
            bits(8*(i+1)-1 downto 8*i) := (others =>'0');
        end if;
    end loop;

    return bits;
end ByteToBits;    
    
end package body;
