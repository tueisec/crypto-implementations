###############################################################################
# do.txt
# This file was auto-generated by aeadtvgen v(N/A - Local package)
###############################################################################
# Parameter:
#
# add_partial            - False
# block_size             - 256
# block_size_ad          - 256
# cc_hls                 - False
# cc_pad_ad              - 0
# cc_pad_d               - 0
# cc_pad_enable          - False
# cc_pad_style           - 1
# ciph_exp               - False
# ciph_exp_noext         - False
# gen_custom_mode        - 0
# io (W,SW)              - [256, 256]
# key_size               - 256
# lib_name               - morus1280256v2
# max_ad                 - 80
# max_block_per_sgmt     - 9999
# max_d                  - 80
# max_io_per_line        - 8
# min_ad                 - 0
# min_d                  - 0
# msg_format             - ['npub', 'ad', 'data', 'tag']
# npub_size              - 128
# nsec_size              - 0
# offline                - False
# reverse_ciph           - False
# tag_size               - 128
###############################################################################

#### Authenticated Encryption
#### MsgID=  1, KeyID=  1 Ad Size =    0, Pt Size =    0
# Instruction: Opcode=Authenticated Encryption
# TB :20101 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=0 bytes
HDR = 5200000000000000000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = EC9FFBDD228705C0841F3D581408AFF900000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  2, KeyID=  1 Ad Size =    0, Ct Size =    0
# Instruction: Opcode=Authenticated Decryption
# TB :30102 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=0 bytes
HDR = 4300000000000000000000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID=  3, KeyID=  2 Ad Size =    1, Pt Size =    0
# Instruction: Opcode=Authenticated Encryption
# TB :20203 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=0 bytes
HDR = 5200000000000000000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 68194AB1D9E80D365FE8129BAC1A85FB00000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  4, KeyID=  2 Ad Size =    1, Ct Size =    0
# Instruction: Opcode=Authenticated Decryption
# TB :30204 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=0 bytes
HDR = 4300000000000000000000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID=  5, KeyID=  3 Ad Size =    0, Pt Size =    1
# Instruction: Opcode=Authenticated Encryption
# TB :20305 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=1 bytes
HDR = 5200000100000000000000000000000000000000000000000000000000000000
DAT = DD00000000000000000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 9A018BBB56A9D7496F7050014AA77D0500000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  6, KeyID=  3 Ad Size =    0, Ct Size =    1
# Instruction: Opcode=Authenticated Decryption
# TB :30306 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=1 bytes
HDR = 4300000100000000000000000000000000000000000000000000000000000000
DAT = 8900000000000000000000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID=  7, KeyID=  4 Ad Size =    1, Pt Size =    1
# Instruction: Opcode=Authenticated Encryption
# TB :20407 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=1 bytes
HDR = 5200000100000000000000000000000000000000000000000000000000000000
DAT = 3C00000000000000000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = C0EE13CC385E53E263E7EF42033AAC8E00000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  8, KeyID=  4 Ad Size =    1, Ct Size =    1
# Instruction: Opcode=Authenticated Decryption
# TB :30408 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=1 bytes
HDR = 4300000100000000000000000000000000000000000000000000000000000000
DAT = AA00000000000000000000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID=  9, KeyID=  5 Ad Size =   32, Pt Size =   32
# Instruction: Opcode=Authenticated Encryption
# TB :20509 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=32 bytes
HDR = 5200002000000000000000000000000000000000000000000000000000000000
DAT = FFBDB55726C07C53F3C4B97A459808C3ABCD5DE9779738BEFDDEE8E3A3C213C6
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = DE955DCA345C1C882EA466E14D8BA9C000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 10, KeyID=  5 Ad Size =   32, Ct Size =   32
# Instruction: Opcode=Authenticated Decryption
# TB :3050A (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=32 bytes
HDR = 4300002000000000000000000000000000000000000000000000000000000000
DAT = 4B1B7E3F1807DCFFC15FE580B03DDC4AC5F3536F6A9C47A9BD3E158F679FF950
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 11, KeyID=  6 Ad Size =   31, Pt Size =   31
# Instruction: Opcode=Authenticated Encryption
# TB :2060B (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=31 bytes
HDR = 5200001F00000000000000000000000000000000000000000000000000000000
DAT = 10CB38DD72936B4C4C5805D8BEF61C7B5DCC4DAF10758B6233D777A86C38B500
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 2C76B431D797D30BA5F7F75F3B2E797B00000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 12, KeyID=  6 Ad Size =   31, Ct Size =   31
# Instruction: Opcode=Authenticated Decryption
# TB :3060C (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=31 bytes
HDR = 4300001F00000000000000000000000000000000000000000000000000000000
DAT = 5D2636C668A44B4F7BE9666FC2EF4F8428610418E44966697279C8D57E519000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 13, KeyID=  7 Ad Size =   33, Pt Size =   33
# Instruction: Opcode=Authenticated Encryption
# TB :2070D (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=33 bytes
HDR = 5200002100000000000000000000000000000000000000000000000000000000
DAT = B7A379CB71FD1FF0E09FF7D6A0E527B3D284A3B5AA6588CD680CDC299BDDAFB62D00000000000000000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = ECFC03FB0E566B61164C7488A9845F4700000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 14, KeyID=  7 Ad Size =   33, Ct Size =   33
# Instruction: Opcode=Authenticated Decryption
# TB :3070E (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=33 bytes
HDR = 4300002100000000000000000000000000000000000000000000000000000000
DAT = 1D37D88E4A2F59A0D2D401E5655838527179F2AF4173999DBA7A7C4E6EF93B8B3C00000000000000000000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 15, KeyID=  8 Ad Size =   64, Pt Size =   64
# Instruction: Opcode=Authenticated Encryption
# TB :2080F (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=64 bytes
HDR = 5200004000000000000000000000000000000000000000000000000000000000
DAT = EE45D17CD321825E36B1CC2E4727E660EE2A8A52ABD20AB425585F69E05A2050DB2A285F2355EBA44B950FF509E48A02E7027477FC5E8A636ED9C2D71A520BE2
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = E934EC863D2A8A576D3045FDD2FB155B00000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 16, KeyID=  8 Ad Size =   64, Ct Size =   64
# Instruction: Opcode=Authenticated Decryption
# TB :30810 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=64 bytes
HDR = 4300004000000000000000000000000000000000000000000000000000000000
DAT = F83523A06758400991CC74DB9700DCFB3AAAADFFB18DA34FA201A2378516CDFDF86EFEF086CCC30779C4DF0168ED96D778ACBCA2D13955E774BCC29E1B0C2AF5
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 17, KeyID=  9 Ad Size =   96, Pt Size =   96
# Instruction: Opcode=Authenticated Encryption
# TB :20911 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=96 bytes
HDR = 5200006000000000000000000000000000000000000000000000000000000000
DAT = 2AEAEF6BB313B107EF246DBD46506CCA17BEA6422D0FD12D73C555EC336FF79770C70358E3D2835E2DF65E9A4CCEEBD31DFC1ABF2D11ABB5DE672E2F4B3C1C2BDB623F1A70E303011A10E3622B97E3834778C377D18EFB41DF791FF2FA0E9E37
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = A35908704CF889960FC93B83B25F97A600000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 18, KeyID=  9 Ad Size =   96, Ct Size =   96
# Instruction: Opcode=Authenticated Decryption
# TB :30912 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=96 bytes
HDR = 4300006000000000000000000000000000000000000000000000000000000000
DAT = EB922759D16741AAD321BC8058114ED0E64195EAA8715DA6781991C7DD7514B15952EFE4F0F0500DAA3D84BA816D1CF9B011ADD38123EDF9EEBD41B7A30F50224CBB9D753C908CA34B6DAEF9D2DCCEAD04B41CA46A030C42C94914EB6EFC305E
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 19, KeyID= 10 Ad Size =  128, Pt Size =  128
# Instruction: Opcode=Authenticated Encryption
# TB :20A13 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=128 bytes
HDR = 5200008000000000000000000000000000000000000000000000000000000000
DAT = 06F72B14DF08062CB69807F40075C234BDD6379E62CA4BF96F2C544613A7ED775F37C751AE45581D729C6D09DE6EE8CFBEB0C6308D5D7E7DE7138402A53A55DFBED6E06464CA44DC7682C6C5E40D3A9B3B5D19BDC44A6C4F127053479A9FB5A00B0CAED41DEA68D652BDAD6D703268C5ED43F75D4E670F7768DD76729750394D
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = AAE072BD92C272ADE15445CEEFB5BA7300000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 20, KeyID= 10 Ad Size =  128, Ct Size =  128
# Instruction: Opcode=Authenticated Decryption
# TB :30A14 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=128 bytes
HDR = 4300008000000000000000000000000000000000000000000000000000000000
DAT = 92061AD0C26EAB2F2313DF156934A20A5FA27298699F2B8942ABBEBBC26D5D591DC07D185E63374163736D7534FBB95FD1CCB6AEE7A0273E0FE642B6BB776ECB69E32241E564F20D81803AEB2B628673DC670381C3EA329CE81355ADE1499327FFD4FAD75582ADE4F66F40F01F39D18B7B404D8BB3216B54B1A0DA18B4A5B989
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 21, KeyID= 11 Ad Size =   12, Pt Size =   15
# Instruction: Opcode=Authenticated Encryption
# TB :20B15 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=15 bytes
HDR = 5200000F00000000000000000000000000000000000000000000000000000000
DAT = D9F58F68D3241B0445124CBD73F72A0000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = ECD9945CC402D31416A4CB51168A265500000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 22, KeyID= 11 Ad Size =   70, Ct Size =   29
# Instruction: Opcode=Authenticated Decryption
# TB :30B16 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=29 bytes
HDR = 4300001D00000000000000000000000000000000000000000000000000000000
DAT = 67EC462999169D37A68F0F806406AECB0D4FE8F7DC099AE7C64E04FC5C000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 23, KeyID= 11 Ad Size =   43, Ct Size =   12
# Instruction: Opcode=Authenticated Decryption
# TB :30B17 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=12 bytes
HDR = 4300000C00000000000000000000000000000000000000000000000000000000
DAT = 9ABF3208C3908693E32C547A0000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 24, KeyID= 12 Ad Size =   19, Ct Size =   52
# Instruction: Opcode=Authenticated Decryption
# TB :30C18 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=52 bytes
HDR = 4300003400000000000000000000000000000000000000000000000000000000
DAT = A6D6FE56195BFD0BB3B9078FE04F0A9546B07F09298D29D4FCD251B1BCD8247C6755B07DC949A36075E87C59ECADFCEB54724191000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 25, KeyID= 13 Ad Size =    0, Ct Size =   55
# Instruction: Opcode=Authenticated Decryption
# TB :30D19 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=55 bytes
HDR = 4300003700000000000000000000000000000000000000000000000000000000
DAT = E276BE0FCBEB8B3FAA51F9E4F3170069E12838986333F76224062EE03E737F4757CDB2CB22D7A82A76B00E09B5CBEB5F3EFF9894D16B34000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 26, KeyID= 14 Ad Size =   11, Pt Size =   57
# Instruction: Opcode=Authenticated Encryption
# TB :20E1A (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=57 bytes
HDR = 5200003900000000000000000000000000000000000000000000000000000000
DAT = 9F1F9F744D7CF6CC46171FB19691462EF67374117CBD4DFDAFE053737EFBDADE860FA9F81EF64B29384B1FB13E38F0C643F4202A7EB82DC62E00000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = BE329C6BCD65D30993DDB51960DFD03D00000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 27, KeyID= 14 Ad Size =   43, Pt Size =   72
# Instruction: Opcode=Authenticated Encryption
# TB :20E1B (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=72 bytes
HDR = 5200004800000000000000000000000000000000000000000000000000000000
DAT = 477E97FCA737D04E65DFF9AD0702A70AED38AF5BBFAC1D59B9D578417C7910BC83F33A7882293B2960AD1ACA0A73735EC7EBFF8AD90405BA17FACEF0A26FCD3B8A3D707F10E98E23000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = C506CD02CF831723CAAB13B494AE22E500000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 28, KeyID= 14 Ad Size =   67, Ct Size =   42
# Instruction: Opcode=Authenticated Decryption
# TB :30E1C (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=42 bytes
HDR = 4300002A00000000000000000000000000000000000000000000000000000000
DAT = 5F08127EDFF9D3A4DA1364A1B42766BF163217146921971C0CC310A48E266609A42315091852181CE07E00000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 29, KeyID= 15 Ad Size =   58, Pt Size =   80
# Instruction: Opcode=Authenticated Encryption
# TB :20F1D (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=80 bytes
HDR = 5200005000000000000000000000000000000000000000000000000000000000
DAT = 3FE707A797E1EC9892669777A6C00BAF53E7D125F962B9619B7E0337D7CC4AB059E604B21409D0866CAEEC8F84AE5DB68138975BACEBEB4205F71EDD09CB1CF88DFC5A99A9050BE89EFAA2C58744FD6D00000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 804739A4A6D91122A9687B0BA9D9BEA600000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 30, KeyID= 16 Ad Size =   59, Ct Size =   41
# Instruction: Opcode=Authenticated Decryption
# TB :3101E (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=41 bytes
HDR = 4300002900000000000000000000000000000000000000000000000000000000
DAT = DB4A6260CB3CF4AFCA9CEE39C9337B82F801A5C30057CACB4B832680373565AE26115378DE56A90DA20000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 31, KeyID= 17 Ad Size =   78, Ct Size =   51
# Instruction: Opcode=Authenticated Decryption
# TB :3111F (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=51 bytes
HDR = 4300003300000000000000000000000000000000000000000000000000000000
DAT = 57DA9A666B28F3D6F1EC9D94E3E5D546C321F02591FB7B15FDFFB4E08F84CFE7A4765A660DB040827C06EC3836B34E035B7E9B00000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 32, KeyID= 18 Ad Size =   55, Pt Size =   39
# Instruction: Opcode=Authenticated Encryption
# TB :21220 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=39 bytes
HDR = 5200002700000000000000000000000000000000000000000000000000000000
DAT = 6F85AC4BB88C86C8C9425A242C66FE44AF26D125934AAE95EC6CC2DFB407672D983D5E6E00143700000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 4F8025FCBDD4ADC9185045FDA544486400000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 33, KeyID= 18 Ad Size =   31, Pt Size =   62
# Instruction: Opcode=Authenticated Encryption
# TB :21221 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=62 bytes
HDR = 5200003E00000000000000000000000000000000000000000000000000000000
DAT = 33C75B3497999DF99E9EF9B81CFB2BA25F3280F70EE24427B8DF5E16B5388879C3D06801256FA173FEDFE7FDC5EA306879B80B34B57210379C50C242F2F40000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 4066B3968910B207EBAD645AAAF425E300000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 34, KeyID= 19 Ad Size =   54, Pt Size =   60
# Instruction: Opcode=Authenticated Encryption
# TB :21322 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=60 bytes
HDR = 5200003C00000000000000000000000000000000000000000000000000000000
DAT = C10ED7375F59B9C24E2F4A62E8D5E55E6480414AD347CA61A46D1BD5EDC5E5197EC980D13B71C93903551CFDD6BF93A0FB66F5D2D860A44C610D75D600000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 93F3125A1D1FA754E4BFBC75D4104BE900000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 35, KeyID= 19 Ad Size =   42, Pt Size =   52
# Instruction: Opcode=Authenticated Encryption
# TB :21323 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=52 bytes
HDR = 5200003400000000000000000000000000000000000000000000000000000000
DAT = 8B99C3B0CFD57F4006F241645184333CBA70FD1EC29798A12DAF9F4B0EC3A44B1B5BBE70887DFE981309FA837954502E8C22B122000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 5E3E423EA33B07AE7F1581EC5457D2D300000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 36, KeyID= 19 Ad Size =   37, Pt Size =   59
# Instruction: Opcode=Authenticated Encryption
# TB :21324 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=59 bytes
HDR = 5200003B00000000000000000000000000000000000000000000000000000000
DAT = 7031B60EC3775855144F8C6A0DB30A4B4B84B6EEB9577E3B7AAF070A3312A330D4AC700B63290143051905D0ACFBFBE3AC168E3DE8339DE08A74C20000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 2F1AD984F0493269C9DB88C2E93A9AD300000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 37, KeyID= 20 Ad Size =    1, Ct Size =   10
# Instruction: Opcode=Authenticated Decryption
# TB :31425 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=10 bytes
HDR = 4300000A00000000000000000000000000000000000000000000000000000000
DAT = 1BB6D02E5423456D5F1A00000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 38, KeyID= 20 Ad Size =    6, Ct Size =   34
# Instruction: Opcode=Authenticated Decryption
# TB :31426 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=34 bytes
HDR = 4300002200000000000000000000000000000000000000000000000000000000
DAT = C246A6A5596F08CDE467A43E920597CEE501BBFC61BEBF21282AD421EAC5798AEFFA000000000000000000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 39, KeyID= 20 Ad Size =   21, Ct Size =   10
# Instruction: Opcode=Authenticated Decryption
# TB :31427 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=10 bytes
HDR = 4300000A00000000000000000000000000000000000000000000000000000000
DAT = 2AADAB025FD7291BAABB00000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 40, KeyID= 20 Ad Size =   76, Ct Size =   50
# Instruction: Opcode=Authenticated Decryption
# TB :31428 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=50 bytes
HDR = 4300003200000000000000000000000000000000000000000000000000000000
DAT = AA89EEC204D7A671FAA10E3F34EDBE74DE4C2186A7A5DE40CC75C89626562FB1923D16AD24910AC2F7E493F36309915DDE470000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 41, KeyID= 20 Ad Size =   75, Pt Size =   36
# Instruction: Opcode=Authenticated Encryption
# TB :21429 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=36 bytes
HDR = 5200002400000000000000000000000000000000000000000000000000000000
DAT = 03FF5D707509E4F330AFA2736B5E9667D9B1EBBF57C512801EA178E1B55CCFBEA2C0F91200000000000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 3B910A9BEBD4DDD010184CB6952651C500000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 42, KeyID= 20 Ad Size =   53, Ct Size =   65
# Instruction: Opcode=Authenticated Decryption
# TB :3142A (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=65 bytes
HDR = 4300004100000000000000000000000000000000000000000000000000000000
DAT = BF85940B07BE0D85321F1F1FB90B35AC0633314D87456EB6E86AE59066A3F7B1E2F0839C5165B3C609165B85963806AB8AF638D060D154E091FED64E56701EC8FD00000000000000000000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 43, KeyID= 20 Ad Size =   67, Ct Size =   31
# Instruction: Opcode=Authenticated Decryption
# TB :3142B (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=31 bytes
HDR = 4300001F00000000000000000000000000000000000000000000000000000000
DAT = 9A6048E6A6273B2DFC199DCD4856E6434D9D27C2D9D2B8CD88FB7C526C5DEB00
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 44, KeyID= 20 Ad Size =   45, Ct Size =   67
# Instruction: Opcode=Authenticated Decryption
# TB :3142C (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=67 bytes
HDR = 4300004300000000000000000000000000000000000000000000000000000000
DAT = B3A8DA335CF78E879101A0C6C65BD48C88D444BBE627CAD9F3BE8F16499EEDF1B571A6C653B53E00BC923E68C7D1E7993B5FEA605525B23D970F90297D61C2BBD45B690000000000000000000000000000000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 45, KeyID= 20 Ad Size =    5, Pt Size =    8
# Instruction: Opcode=Authenticated Encryption
# TB :2142D (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=8 bytes
HDR = 5200000800000000000000000000000000000000000000000000000000000000
DAT = D477A4F591526548000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 91AEDC074547A0F7253F5EDB0D5FD5D800000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 46, KeyID= 20 Ad Size =   12, Pt Size =   80
# Instruction: Opcode=Authenticated Encryption
# TB :2142E (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=80 bytes
HDR = 5200005000000000000000000000000000000000000000000000000000000000
DAT = 780F324CA3DA2D977453C457C9563B5DDE5E7B2281E14BCF8EC40E5AE2563BFEFF5B71897AE3D97B4EDF27D903B02D5DD78ADCB4E72A09C44546DB772FE3A14C470EBB7852D913A0A28F77C21F40A28C00000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = C59F3794D97C0BC4DF4739B7BC7D2BC900000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 47, KeyID= 21 Ad Size =   12, Pt Size =   60
# Instruction: Opcode=Authenticated Encryption
# TB :2152F (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=60 bytes
HDR = 5200003C00000000000000000000000000000000000000000000000000000000
DAT = 8972C5D3C8D6F65B9FE7552EB39087559872ACAAFEEDE08307A5861F43EB77D97A4532386E25277F602A6AAD56ABEA7B332FD1C68D37CFA78AACA3D500000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = DA4681F9DB336768BEE6058B12E5330500000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 48, KeyID= 22 Ad Size =   55, Pt Size =   39
# Instruction: Opcode=Authenticated Encryption
# TB :21630 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=39 bytes
HDR = 5200002700000000000000000000000000000000000000000000000000000000
DAT = 6CAC41EC578EEAB0044CFE20732E068D61AE934E54DB66214EB05D0C408B33F3610864842AC1B800000000000000000000000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = 6D9348BC6B0FC4D4342F2B864B37B43200000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 49, KeyID= 22 Ad Size =   25, Ct Size =   19
# Instruction: Opcode=Authenticated Decryption
# TB :31631 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=19 bytes
HDR = 4300001300000000000000000000000000000000000000000000000000000000
DAT = 129FD2125E3A5CE08993407B9AD9B0A28D6D6900000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 50, KeyID= 23 Ad Size =   56, Pt Size =   26
# Instruction: Opcode=Authenticated Encryption
# TB :21732 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=26 bytes
HDR = 5200001A00000000000000000000000000000000000000000000000000000000
DAT = 69EAE188379631AD3BB57AA00DD416A5EB711ACDC549060495A6000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 8300001000000000000000000000000000000000000000000000000000000000
DAT = B0465CBB9A16B8F360B5F081AD446E0300000000000000000000000000000000
# Status: Success
STT = E000000000000000000000000000000000000000000000000000000000000000

###EOF
