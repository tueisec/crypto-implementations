set INTERFACE_REPO "../../AEAD"
set TOP_LEVEL_NAME AEAD_TB
set STOP_AT_FAULT True
set CUSTOM_DO_FILE "wave.do"
set PWIDTH 256
set SWIDTH 256

# ----------------------------------------
# Set implementation files
set src_vhdl [subst {
    "$INTERFACE_REPO/src_rtl/AEAD_pkg.vhd"
    "../src_rtl/morus_pkg.vhd"
    "../../src_rtl/StateUpdate.vhd"
    "../../src_rtl/CipherCore.vhd"
    "$INTERFACE_REPO/src_rtl/PreProcessor.vhd"
    "$INTERFACE_REPO/src_rtl/PostProcessor.vhd"
    "$INTERFACE_REPO/src_rtl/fwft_fifo.vhd"
    "../src_rtl/AEAD.vhd"
    "$INTERFACE_REPO/src_rtl/AEAD_Arch.vhd"
}]

# ----------------------------------------
# Set simulation files
set tb_vhdl [subst {
    "../src_tb/design_pkg.vhd"
    "$INTERFACE_REPO/src_tb/std_logic_1164_additions.vhd"
    "$INTERFACE_REPO/src_tb/$TOP_LEVEL_NAME.vhd"
}]
# ----------------------------------------
# Python interface for creating a distro
proc get_src {src}  {return $src}

# ----------------------------------------
# Create compilation libs
proc ensure_lib { lib } { if ![file isdirectory $lib] { vlib $lib } }
ensure_lib          ./libs/
ensure_lib          ./libs/work/
vmap       work     ./libs/work/

# ----------------------------------------
# Compile implementation files
alias imp_com {
    echo "imp_com"
    foreach f $src_vhdl {vcom -quiet -work work $f}
}

# ----------------------------------------
# Compile simulation files
alias sim_com {
    echo "sim_com"
    foreach f $tb_vhdl {vcom -quiet -work work $f}
}

# ----------------------------------------
# Compile simulation files
alias com {
    echo "com"
    imp_com
    sim_com
}

# ----------------------------------------
# Add wave form and run
alias run_wave {
    echo "\[exec\] run_wave"
    add wave -group PreProcessor  -ports     -radix hexadecimal $TOP_LEVEL_NAME/uut/u_input/*
    add wave -group PreProcessor  -internals -radix hexadecimal $TOP_LEVEL_NAME/uut/u_input/*
    add wave -group CipherCore    -ports     -radix hexadecimal $TOP_LEVEL_NAME/uut/u_cc/*
    add wave -group CipherCore    -internals -radix hexadecimal $TOP_LEVEL_NAME/uut/u_cc/*
    add wave -group PostProcessor -ports     -radix hexadecimal $TOP_LEVEL_NAME/uut/u_output/*
    add wave -group PostProcessor -internals -radix hexadecimal $TOP_LEVEL_NAME/uut/u_output/*

    # Configure wave panel
    configure wave -namecolwidth 180
    configure wave -valuecolwidth 200
    configure wave -signalnamewidth 1
    configure wave -timelineunits ns
    WaveRestoreZoom {0 ps} {2000 ns}
    configure wave -justifyvalue right
    configure wave -rowmargin 8
    configure wave -childrowmargin 5
}


# ----------------------------------------
# Compile all the design files and elaborate the top level design with -novopt
alias ldd {
    com
    set run_do_file [file isfile $CUSTOM_DO_FILE]
    if {$run_do_file == 1} {
        vsim -novopt -t ps -L work $TOP_LEVEL_NAME -gG_PWIDTH=$PWIDTH -gG_SWIDTH=$SWIDTH  -do $CUSTOM_DO_FILE  -gG_STOP_AT_FAULT=$STOP_AT_FAULT
    } else {
      vsim -novopt -t ps -L work $TOP_LEVEL_NAME -gG_PWIDTH=$PWIDTH -gG_SWIDTH=$SWIDTH  -gG_STOP_AT_FAULT=$STOP_AT_FAULT
      run_wave
    }
    run 30 us
}

# ----------------------------------------
# Print out user commmand line aliases
alias h {
    echo "List Of Command Line Aliases"
    echo
    echo "imp_com                       -- Compile implementation files"
    echo
    echo "sim_com                       -- Compile simulation files"
    echo
    echo "com                           -- Compile files in the correct order"
    echo
    echo "ldd                           -- Compile and run"
    echo
}
h
