-------------------------------------------------------------------------------
--! @file       CipherCore.vhd
--! @brief      The CipherCore is the top level modul of the Morus implementation.
--! @project    CAESAR Candidate Morus
--! @author     Michael Tempelmeier
--! @date       2019-29-01
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Muncih, GERMANY
--!             All rights Reserved.
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
-------------------------------------------------------------------------------


--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL; 
use work.morus_pkg.all;
use work.AEAD_pkg.all;
--! @endcond

--! Entity description of \link CipherCore.vhd CipherCore\endlink.
entity CipherCore is
    generic (
        -- Reset behavior
        G_ASYNC_RSTN    : boolean := False;                                        --! Async active low reset
        -- Block size (bits)
        G_DBLK_SIZE     : integer := MORUS_WORD_SIZE;                                    --! Data block size
        G_KEY_SIZE      : integer := MORUS_KEY_SIZE;                                     --! Key block size
        G_TAG_SIZE      : integer := MORUS_TAG_SIZE;                                     --! Tag block size
               
        G_LBS_BYTES     : integer := 4;                                            --! The number of bits required to hold block size expressed in bytes = log2_ceil(G_DBLK_SIZE/8)
        
        --G_MAX_LEN       : integer := SINGLE_PASS_MAX;
        G_MAX_LEN       : integer := 32;                                           --! Maximum supported AD/message/ciphertext length = 2^G_MAX_LEN-1
        -- Maximum supported AD/message/ciphertext length = 2^G_MAX_LEN-1
        -- G_MAX_LEN       : integer := SINGLE_PASS_MAX;
        -- Algorithm parameter to simulate run-time behavior
        -- Warning: Do not set any number higher than 32
        G_LAT_KEYINIT   : integer := 4;                                            --! Key inialization latency
        G_LAT_PREP      : integer := 12;                                           --! Pre-processing latency (init)
        G_LAT_PROC_AD   : integer := 10;                                           --! Processing latency (per block)
        G_LAT_PROC_DATA : integer := 16;                                           --! Processing latency (per block)
        G_LAT_POST      : integer := 20                                            --! Post-processing latency (tag)
    );
  Port (        
        -- Global
        clk             : in  std_logic;                                           --! Clock signal.
        rst             : in  std_logic;                                           --! Reset signal.
        -- PreProcessor (data)
        key             : in  std_logic_vector(G_KEY_SIZE       -1 downto 0);      --! Signal containing the SDI data.
        bdi             : in  std_logic_vector(G_DBLK_SIZE      -1 downto 0);      --! All public data is sent over this signal.
        -- PreProcessor (controls)
        key_ready       : out std_logic;                                           --! This signal indicates that the key has successfully processed.
        key_valid       : in  std_logic;                                           --! This signal is asserted when data on the key signal is valid. 
        key_update      : in  std_logic;                                           --! If key_update is asserted the data_path stores the new key.
        decrypt         : in  std_logic;                                           --! This signal indicates a decryption or a encryption.
        bdi_ready       : out std_logic;                                           --! This signal acknowledges the data assert on the bdi signal.
        bdi_valid       : in  std_logic;                                           --! This signal is asserted when data on bdi is valid. 
        bdi_type        : in  std_logic_vector(3                -1 downto 0);      --! bdi_type indicates which data is transmitted over the bdi signal.
        bdi_partial     : in  std_logic;                                           --! Not used in this implementation.
        bdi_eot         : in  std_logic;                                           --! This signal indicates that the current bdi block is the last of the current type.
        bdi_eoi         : in  std_logic;                                           --! This signal indicates that the current bdi block is the last of the whole input.
        bdi_size        : in  std_logic_vector(G_LBS_BYTES+1    -1 downto 0);      --! This signal indicates how many bytes are valid in this block.
        bdi_valid_bytes : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);      --! Not used in this implementation.
        bdi_pad_loc     : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);      --! Not used in this implementation.
        -- PostProcessor
        bdo             : out std_logic_vector(G_DBLK_SIZE      -1 downto 0);      --! All public data which is sent to the PostProcessor
        bdo_valid       : out std_logic;                                           --! This signal indicates the validness of the current bdo block.
        bdo_ready       : in  std_logic;                                           --! This signal acknowledges the data assert on the bod signal.
        bdo_size        : out std_logic_vector(G_LBS_BYTES+1    -1 downto 0);      --! Indicates the number of valid bytes in the current bdo block.
        msg_auth        : out std_logic;                                           --! Indicates if authentication was valid.                                          
        msg_auth_valid  : out std_logic;                                           --! Strobe signal for msg_auth.
        msg_auth_ready  : in  std_logic                                            --! Acknowledge signal for msg_auth.
     );
end CipherCore;

--! Architecture of \link CipherCore.vhd CipherCore\endlink entity.
architecture Structure of CipherCore is


signal morus_state, morus_nstate : std_logic_vector(MORUS_STATE_SIZE-1 downto 0);  --morus state
--aliases for better readability
alias S0 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_state(MORUS_STATE_SIZE-                   1 downto MORUS_STATE_SIZE-  MORUS_WORD_SIZE);
alias S1 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_state(MORUS_STATE_SIZE-   MORUS_WORD_SIZE-1 downto MORUS_STATE_SIZE-2*MORUS_WORD_SIZE);
alias S2 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_state(MORUS_STATE_SIZE-2* MORUS_WORD_SIZE-1 downto MORUS_STATE_SIZE-3*MORUS_WORD_SIZE);
alias S3 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_state(MORUS_STATE_SIZE-3* MORUS_WORD_SIZE-1 downto MORUS_STATE_SIZE-4*MORUS_WORD_SIZE);
alias S4 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_state(MORUS_STATE_SIZE-4* MORUS_WORD_SIZE-1 downto 0);

alias nS0 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_nstate(MORUS_STATE_SIZE-                   1 downto MORUS_STATE_SIZE-  MORUS_WORD_SIZE);
alias nS1 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_nstate(MORUS_STATE_SIZE-   MORUS_WORD_SIZE-1 downto MORUS_STATE_SIZE-2*MORUS_WORD_SIZE);
alias nS2 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_nstate(MORUS_STATE_SIZE-2* MORUS_WORD_SIZE-1 downto MORUS_STATE_SIZE-3*MORUS_WORD_SIZE);
alias nS3 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_nstate(MORUS_STATE_SIZE-3* MORUS_WORD_SIZE-1 downto MORUS_STATE_SIZE-4*MORUS_WORD_SIZE);
alias nS4 : std_logic_vector(MORUS_WORD_SIZE-1 downto 0) is morus_nstate(MORUS_STATE_SIZE-4* MORUS_WORD_SIZE-1 downto 0);


-- inputs and outputs for the state update function
signal morus_updated_state : std_logic_vector(MORUS_STATE_SIZE-1 downto 0); 
signal morus_message, morus_finalize_tmp : std_logic_vector(MORUS_WORD_SIZE-1 downto 0);


signal key_r : std_logic_vector(MORUS_KEY_SIZE-1 downto 0);
signal key_0  : std_logic_vector(MORUS_WORD_SIZE-1 downto 0);

signal bdi_padded : std_logic_vector(MORUS_WORD_SIZE-1 downto 0);
signal load_key : boolean;
signal latch_decryption_status: boolean;
signal decrypt_r : std_logic;
signal is_eoi, is_eot : std_logic;
signal bdi_ready_intern : std_logic;

--ToDO: Perhaps we can improve this here by only counting words and ORing the bdi_size only once - as there is at most one partal block!!!!
--ToDo: We can decrease the counter if we know the maximum length of MSG and AD. The algorithm supports up to 2^64 bits.
signal counter, ncounter : unsigned (63-3 downto 0); -- this is the real counter, but we only count bytes not bits for AD and MSG. This coutner is also used for round
signal ad_count, nad_count : unsigned (63-3 downto 0); -- this is the reg for AD length
signal msg_count, nmsg_count : unsigned (63-3 downto 0); -- this is the reg for MSG lengt

-- signals for better readability
signal morus_init : std_logic_vector(MORUS_STATE_SIZE-1 downto 0); 
signal msg_length, ad_length : std_logic_vector(63 downto 0);
signal tag_prime : std_logic_vector (MORUS_WORD_SIZE-1 downto 0);
signal tag :std_logic_vector (MORUS_WORD_SIZE-1 downto 0);
signal bdi_valid_bits : std_logic_vector (MORUS_WORD_SIZE-1 downto 0);
signal key_stream: std_logic_vector(MORUS_WORD_SIZE -1 downto 0);


--FSM
type FSM_STATES is (IDLE, SET_KEY_IV, INIT16, AD, MSG, GENTAG);   
signal current_state, next_state : FSM_STATES;

begin

StateUpdate:  entity work.StateUpdate(Behavioral)

    port map ( 
      state      => morus_state,
      state_next => morus_updated_state,
      message    => morus_message
    );


KEY_REG: process(clk)
begin
    if rising_edge(clk) then
        if (load_key) then
            key_r <= key;
        end if;

	   if(bdi_valid = '1' AND bdi_ready_intern = '1') then
		  is_eoi <= bdi_eoi;
	   end if;
	
        if (latch_decryption_status) then
           decrypt_r <= decrypt;
        end if;
    end if;
end process KEY_REG;

MORUS_STATE_REG: process(clk)
begin
    if rising_edge(clk) then
        morus_state <= morus_nstate;
    end if;
end process MORUS_STATE_REG;



FSM_REGS: process(clk)
begin
    if rising_edge(clk) then
        if (rst = '1') then
            current_state <= IDLE;
            counter <= (others =>'0');
            ad_count <= (others =>'0');
            msg_count <= (others =>'0');
        else
            current_state <= next_state;
            counter <=ncounter;
            ad_count <= nad_count;
            msg_count <= nmsg_count;
        end if;
    end if;
end process FSM_REGS;


FSM_COMB: process(current_state, key_stream, bdi, ad_count, msg_count, bdi_padded, bdi_eot, bdi_eoi, is_eoi, key_0, morus_state, bdi_size,  bdi_valid, bdi_type, bdi_valid_bits, key_update, key_valid, morus_init, morus_updated_state, counter, bdo_ready,msg_auth_ready , decrypt_r, morus_finalize_tmp)
begin
   key_ready <='0';
   load_key <=false;
   latch_decryption_status <= false;
   ncounter <= counter;
   nad_count <= ad_count;
   nmsg_count <= msg_count;
   next_state <= current_state;
   morus_nstate <= morus_state;
   morus_message <= (others =>'0'); --ToDo change to '-'
   bdi_ready_intern <= '0';
   bdo_valid <= '0';
   msg_auth_valid <= '0';
   
   case current_state is 
       when IDLE =>
           ncounter <= (others =>'0');
           nad_count <= (others => '0');
           nmsg_count <= (others=> '0');
           if bdi_valid = '1' and bdi_type = BDI_TYPE_NPUB then
               latch_decryption_status <= true;
               if (key_update = '1' and key_valid = '1') then --there should be no case where key_update = 1 and key_valid = 0 - this gets important if we need more than one cycle to laod the key
                   load_key <= true; 
                   key_ready <= '1'; --FIXME: Bad practise if ready depends on valid
               end if;
               next_state <=SET_KEY_IV;
           end if;
           
        
        when SET_KEY_IV =>
           bdi_ready_intern <= '1'; --we know that bdi_valid was  '1' one cycle before and according to the spec of the PreProssor it won't geht low until there was a read
           morus_nstate <= morus_init;
           next_state <= INIT16;
           ncounter <= (others =>'0');

        when INIT16=>
           
           if counter = 15 then
               ncounter <= counter +1; 
               morus_nstate <= morus_updated_state; 
               morus_message <= (others => '0');
           
               if (bdi_valid = '1' AND bdi_type(2 downto 1) = BDI_TYPE_ASS ) then
                   next_state <= AD;
               elsif (bdi_valid = '1' AND bdi_type(2 downto 1) = BDI_TYPE_DAT) then
                   next_state <= MSG;
               elsif (is_eoi = '1') then --we need the stored value, as the information was provided with nbpub (15 cycles before)
                   next_state <= GENTAG; --in encryption mode
               else
                   next_state <= AD; --If we dont't know what to do we go to AD and wait there. We might loose one cycle in the case of an empty decryption
               end if;
               ncounter <= (others=> '0'); --if we optimze we can move the coutner reset
               morus_nstate(MORUS_STATE_SIZE -1 downto MORUS_STATE_SIZE-MORUS_WORD_SIZE) <= morus_updated_state(MORUS_STATE_SIZE -1 downto MORUS_STATE_SIZE-MORUS_WORD_SIZE);
               morus_nstate(MORUS_STATE_SIZE-MORUS_WORD_SIZE-1 downto MORUS_STATE_SIZE- 2 * MORUS_WORD_SIZE) <= morus_updated_state (MORUS_STATE_SIZE-MORUS_WORD_SIZE-1 downto MORUS_STATE_SIZE- 2 * MORUS_WORD_SIZE) XOR key_0;
               morus_nstate(MORUS_STATE_SIZE- 2 * MORUS_WORD_SIZE-1 downto 0) <= morus_updated_state (MORUS_STATE_SIZE-2*MORUS_WORD_SIZE-1 downto 0);
           else
               ncounter <= counter +1; 
               morus_nstate <= morus_updated_state; 
               morus_message <= (others => '0');
           end if;
        
        
        when AD =>
                                
            if (bdi_valid = '1' and bdi_type(2 downto 1) = BDI_TYPE_ASS) then
             bdo_valid <= '0';
             bdi_ready_intern <= '1';
                ncounter <= counter + unsigned(bdi_size);
                morus_nstate <= morus_updated_state;
                morus_message <= bdi_padded;
                if (bdi_eoi = '1') then 
                    next_state <= GENTAG;
                    nad_count <= counter + unsigned(bdi_size); --ToDo optimise the addition to a ORing
                    ncounter <= (others=>'0');
                elsif (bdi_eot = '1') then 
                   next_state <= MSG; --its end of type, but not end of input
                   nad_count <= counter + unsigned(bdi_size); --ToDo optimise the addition to a ORing
                   ncounter <= (others=>'0');
                end if;
                
            elsif (bdi_valid = '1' AND bdi_type(2 downto 1) = BDI_TYPE_DAT AND bdo_ready ='1') then --this should only happen if we're coming from INIT16
            bdo_valid <= bdi_valid;
            bdi_ready_intern <= bdo_ready;

                 morus_nstate <= morus_updated_state;
                 ncounter <= counter + unsigned(bdi_size);
		 if decrypt_r = '1' then
                    morus_message <= (bdi XOR key_stream) AND bdi_valid_bits; --encrypted plaintext 
                else
                    morus_message <= bdi_padded; -- input is plaintext
                end if;
                 if (bdi_eoi = '1' or bdi_eot = '1') then --for robustness
                    next_state <=GENTAG;
                    nmsg_count <= counter + unsigned(bdi_size); --ToDo optimise the addition to a ORing
                    ncounter <= (others=>'0');
                 end if;
             elsif (bdi_valid = '1' and bdi_type = BDI_TYPE_TAG) then
		          next_state <= GENTAG;
    	     end if;           
            
            
        when MSG =>
            bdo_valid <= bdi_valid;
            bdi_ready_intern <= bdo_ready;
            
            if (bdi_valid = '1'  AND bdi_type(2 downto 1) = BDI_TYPE_DAT AND bdo_ready ='1') then
                ncounter <= counter + unsigned(bdi_size);
                morus_nstate <= morus_updated_state;
                if decrypt_r = '1' then
                    morus_message <= (bdi XOR key_stream) AND bdi_valid_bits; --encrypted plaintext 
                else
                    morus_message <= bdi_padded; -- input is plaintext
                end if;
                if (bdi_eoi = '1' or bdi_eot = '1') then --for robustness we dont need the stored values
                    next_state <= GENTAG;
                    nmsg_count <= counter + unsigned(bdi_size); --ToDo optimise the addition to a ORing
                    ncounter <= (others =>'0');
                end if;
            end if; 
        
        when GENTAG =>            
            if counter = 0 then 
                ncounter <= counter +1;
                morus_nstate(MORUS_STATE_SIZE -1 downto MORUS_WORD_SIZE) <= morus_state(MORUS_STATE_SIZE -1 downto MORUS_WORD_SIZE);
                morus_nstate(MORUS_WORD_SIZE -1 downto 0) <= S0 XOR S4;
            elsif counter = 11 then
                if decrypt_r = '1' then
                    if msg_auth_ready = '1' then
                        bdi_ready_intern <= '1';
                    end if;
                    
                    if (bdi_valid = '1' AND bdi_type = BDI_TYPE_TAG) then
                        msg_auth_valid <= '1';
                        if msg_auth_ready = '1' then
                            next_state <= IDLE;
                        end if;
                    end if;                
                else
                    bdo_valid <= '1';
                    if bdo_ready = '1' then
                        next_state <= IDLE;
                    end if;
                end if;
            else --1 to 10
                ncounter <= counter +1;
                morus_nstate <= morus_updated_state;
                morus_message <= morus_finalize_tmp;
            end if;
        
    end case;
end process FSM_COMB;              

--output generation

bdo <= tag                       when current_state = GENTAG                --Tag has the highest priority
       else (bdi XOR key_stream) when bdi_type(2 downto 1) = BDI_TYPE_DAT   --Enc/Dec is next
       else bdi;                                                            --ToDo; Optimize here to Zeros or dont cares

msg_auth <= '1' when (bdi AND bdi_valid_bits)  = tag else '0';

bdo_size <= bdi_size; --ToDo: Although it works, this should be redefined for Morus_1280's tag generation
bdi_ready <= bdi_ready_intern;

tag_prime <= key_stream;

--padding
bdi_valid_bits <= ByteToBits(bdi_valid_bytes);
bdi_padded <= bdi AND bdi_valid_bits;

ad_length <= std_logic_vector(ad_count) & "000"; -- we only count bytes, but we need bits
msg_length <= std_logic_vector(msg_count) & "000"; -- we only count bytes, but we need bits


--generate FLAVOR specific signals

FLAVOR_640: if MORUS_FLAVOR = MORUS_640_128 generate
    tag <= tag_prime(MORUS_TAG_SIZE-1 downto 0);
    key_stream <= S0 XOR rotr(S1, 96) XOR (S2 AND S3);
    key_0 <= key_r;
    morus_init <= bdi & key_0 & ONES_128 & morus_const0 & morus_const1;
    morus_finalize_tmp <= convert_endianness(ad_length) & convert_endianness(msg_length);
end generate;

FLAVOR_1280_128: if MORUS_FLAVOR = MORUS_1280_128 generate
    tag <= tag_prime(MORUS_WORD_SIZE -1 downto MORUS_TAG_SIZE) & ZEROS_128;
    key_stream <= S0 XOR rotr(S1, 192) XOR (S2 AND S3);
    key_0 <= key_r & key_r;
    morus_init <= bdi(255 downto 128)& ZEROS_128 & key_0 & ONES_256 & ZEROS_256 & morus_const0 & morus_const1;
    morus_finalize_tmp <= convert_endianness(ad_length) & convert_endianness(msg_length) & ZEROS_128;
end generate;

FLAVOR_1280_256: if MORUS_FLAVOR = MORUS_1280_256 generate
    tag <= tag_prime(MORUS_WORD_SIZE -1 downto MORUS_TAG_SIZE) & ZEROS_128;
    key_stream <= S0 XOR rotr(S1, 192) XOR (S2 AND S3);
    key_0 <= key_r;
    morus_init <= bdi(255 downto 128) & ZEROS_128 & key_0 & ONES_256 & ZEROS_256 & morus_const0 & morus_const1;
    morus_finalize_tmp <= convert_endianness(ad_length) & convert_endianness(msg_length) & ZEROS_128;
end generate;


end structure;
