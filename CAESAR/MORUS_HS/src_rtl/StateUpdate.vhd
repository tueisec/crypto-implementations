-------------------------------------------------------------------------------
--! @file       StateUpdate.vhd
--! @brief      This is the State Update Funktion of the Morus implementation.
--! @project    CAESAR Candidate Morus
--! @author     Michael Tempelmeier
--! @date       2019-29-01
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Muncih, GERMANY
--!             All rights Reserved.
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.morus_pkg.ALL;


entity StateUpdate is
    Port ( state      : in  STD_LOGIC_VECTOR (MORUS_STATE_SIZE-1 downto 0);
           state_next : out STD_LOGIC_VECTOR (MORUS_STATE_SIZE-1 downto 0);
           message    : in  STD_LOGIC_VECTOR (MORUS_WORD_SIZE -1 downto 0)
           );
end StateUpdate;

architecture Behavioral of StateUpdate is



signal S0_0, S0_1, S0_2, S0_3, S0_4 : std_logic_vector (MORUS_WORD_SIZE - 1 downto 0);  --input

signal S1_0, S1_1, S1_2, S1_3, S1_4 : std_logic_vector (MORUS_WORD_SIZE - 1 downto 0);
signal S2_0, S2_1, S2_2, S2_3, S2_4 : std_logic_vector (MORUS_WORD_SIZE - 1 downto 0);
signal S3_0, S3_1, S3_2, S3_3, S3_4 : std_logic_vector (MORUS_WORD_SIZE - 1 downto 0);
signal S4_0, S4_1, S4_2, S4_3, S4_4 : std_logic_vector (MORUS_WORD_SIZE - 1 downto 0);
signal S5_0, S5_1, S5_2, S5_3, S5_4 : std_logic_vector (MORUS_WORD_SIZE - 1 downto 0);  --output



begin

S0_0 <= state (MORUS_STATE_SIZE -                     1 downto MORUS_STATE_SIZE -    MORUS_WORD_SIZE);
S0_1 <= state (MORUS_STATE_SIZE -    MORUS_WORD_SIZE -1 downto MORUS_STATE_SIZE - 2* MORUS_WORD_SIZE);
S0_2 <= state (MORUS_STATE_SIZE - 2* MORUS_WORD_SIZE -1 downto MORUS_STATE_SIZE - 3* MORUS_WORD_SIZE);
S0_3 <= state (MORUS_STATE_SIZE - 3* MORUS_WORD_SIZE -1 downto MORUS_STATE_SIZE - 4* MORUS_WORD_SIZE);
S0_4 <= state (MORUS_STATE_SIZE - 4* MORUS_WORD_SIZE -1 downto                         0);

S1_0 <= Rotl_xxx_yy(S0_0 XOR (S0_1 AND S0_2) XOR S0_3, b0);
S1_1 <= S0_1;
S1_2 <= S0_2;
S1_3 <= rotr(S0_3, w0);
S1_4 <= S0_4;


S2_0 <= S1_0;
S2_1 <= Rotl_xxx_yy(S1_1 XOR (S1_2 AND S1_3) XOR S1_4 XOR message, b1);
S2_2 <= S1_2;
S2_3 <= S1_3;
S2_4 <= rotr(S1_4, w1);

S3_0 <= rotr(S2_0, w2);
S3_1 <= S2_1;
S3_2 <= Rotl_xxx_yy(S2_2 XOR (S2_3 AND S2_4) XOR S2_0 XOR message, b2);
S3_3 <= S2_3;
S3_4 <= S2_4;

S4_0 <= S3_0;
S4_1 <= rotr(S3_1,w3);
S4_2 <= S3_2;
S4_3 <= Rotl_xxx_yy(S3_3 XOR (S3_0 AND S3_4) XOR S3_1 XOR message, b3);
S4_4 <= S3_4;

S5_0 <= S4_0;
S5_1 <= S4_1;
S5_2 <= rotr(S4_2,w4);
S5_3 <= S4_3;
S5_4 <= Rotl_xxx_yy(S4_4 XOR (S4_0 AND S4_1) XOR S4_2 XOR message, b4);


state_next <= S5_0 & S5_1 & S5_2 & S5_3 & S5_4;


end Behavioral;
