v1
    Reference software:
        aegis128, aegis256

    Architecture description:
        

    Key setup time:
        aegis128: 11
        aegis256: 17

    Execution time of authenticated encryption:
        aegis128: adlen/128 + msglen/128 + 6
        aegis256: adlen/128 + msglen/128 + 6

    Execution time of authenticated decryption:
        aegis128: adlen/128 + msglen/128 + 6 + 1
        aegis256: adlen/128 + msglen/128 + 6 + 1

    Non-default generic settings:
        N/A