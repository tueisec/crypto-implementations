----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-10
--! @brief      The aegis_package is a helper package which contains all the types and helper functions used in the implementation.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE; 
use IEEE.STD_LOGIC_1164.all; 
--! @endcond

--! Package description of \link aegis_package.vhd aegis_package\endlink.
package aegis_package is 

    -- GLOBAL Type
    type AEGIS_FLAVOR is (AEGIS128, AEGIS256); --! Type is used to perform flavor dependent VHDL generation.
  
  -- ACTIVE_AEGIS_FLAVOR  -------------------------
  -- If flavor is changed please comment in appropriate global constants. 
    constant ACTIVE_AEGIS_FLAVOR : AEGIS_FLAVOR := AEGIS256;  --! The current activated AEGIS_FLAVOR.
  ------------------------------------------------- 
  
  -- GLOBAL CONSTANTS - AEGIS-128  ----------------
  ------------------------------------------------- 
    -- global constants 
--    constant DBLK_SIZE           : integer := 128;                  --! The current block size.
--    constant KEY_SIZE            : integer := 128;                  --! The key and iv size.
--    constant TAG_SIZE            : integer := 128;                  --! The tag size.
--    constant LBS_BYTES           : integer := 4;                    --! The number of bits required to hold block size expressed in bytes = log2_ceil(G_DBLK_SIZE/8)
      
    -- global constants - aegis specific      
--    constant AEGIS_STATE_SIZE    : integer := 5;                    --! Number of 128 bit blocks which make the state; defined in \cite WuPreneel.
--    constant INIT_ROUNDS         : integer := 11;                   --! Ten initialization rounds defined in \cite WuPreneel plus one implementation dependent one.
--    constant FINALIZATION_ROUNDS : integer := 6;                    --! Six finaliatzion rounds defined in \cite WuPreneel.    
  -------------------------------------------------
  
--  -- GLOBAL CONSTANTS - AEGIS-256  ----------------
--  ------------------------------------------------- 
    -- global constants 
    constant DBLK_SIZE           : integer := 128;                  --! The current block size.
    constant KEY_SIZE            : integer := 256;                  --! The key and iv size.
    constant TAG_SIZE            : integer := 128;                  --! The tag size.
    constant LBS_BYTES           : integer := 4;                    --! The number of bits required to hold block size expressed in bytes = log2_ceil(G_DBLK_SIZE/8)
     
--    -- global constants - aegis specific      
    constant AEGIS_STATE_SIZE    : integer := 6;                    --! Number of 128 bit blocks which make the state; defined in \cite WuPreneel.
    constant INIT_ROUNDS         : integer := 17;                   --! Sixteen initialization rounds defined in \cite WuPreneel plus one implementation dependent one.
    constant FINALIZATION_ROUNDS : integer := 6;                    --! Six finaliatzion rounds defined in \cite WuPreneel.    
--  -------------------------------------------------   
    
    constant AEGIS_CONST0        : std_logic_vector(127 downto 0) := x"6279e990593722150d08050302010100";  --! Constant value defined in \cite WuPreneel.
    constant AEGIS_CONST1        : std_logic_vector(127 downto 0) := x"dd28b57342311120f12fc26d55183ddb";  --! Constant value defined in \cite WuPreneel.
    
    
    -- Types 
    type AEGIS_BYTE                   is array(7 downto 0) of STD_LOGIC;                                              --! The lowest unit is a AEGIS_BYTE.
    type AEGIS_BLOCK                  is array((DBLK_SIZE/8)-1 downto 0) of AEGIS_BYTE;                               --! AEGIS_BLOCK is main type which represents  a total block.
    type AEGIS_BLOCK_VECTOR           is array (integer range <>) of AEGIS_BLOCK;                                     --! Generic AEGIS_BLOCK_VECTOR.
    type AEGIS_BLOCK_QUARTER          is array(((DBLK_SIZE/8)/4)-1 downto 0) of AEGIS_BYTE;                           --! A forth of a AEGIS_BLOCK used in the AES part.
    type AEGIS_BLOCK_QUARTER_VECTOR   is array(((DBLK_SIZE/8)/((DBLK_SIZE/8)/4))-1 downto 0) of AEGIS_BLOCK_QUARTER;  --! Type is  AEGIS_BLOCK devided by AEGIS_BLOCK_QUARTER's size. This type is mainly used in AES.
    type AEGIS_STATE_VECTOR           is array(AEGIS_STATE_SIZE-1 downto 0) of AEGIS_BLOCK;                           --! A generic state vector containing AEGIS_STATE_SIZE AEGIS_BLOCKs.
    subtype AEGIS_BYTE_REVERT         is std_logic_vector(7 downto 0);                                                --! Helper type to revert the AEGIS_BYTE in a helper function.
    subtype AEGIS_BLOCK_REVERT        is std_logic_vector(DBLK_SIZE-1 downto 0);                                      --! Helper type to revert the AEGIS_BLOCK in a helper function.
    type BDI_ENC                      is (AD, MSG_CIPHER, IV, TAG, NOTSUPPORTED);                                     --! Enumeration type used to clear up the bdi_type encoding.
    type AEGIS_REG_STATE              is (ENABLE, DISABLE);                                                           --! Readable enum type to enable the registers.
    type AEGIS_REG_STATE_VECTOR       is array (integer range <>) of AEGIS_REG_STATE;                                 --! A generic vector of AEGIS_REG_STATE.
    type AEGIS_COUNTER_STATE          is (IDLE, RESET, CIPHER_OR_PLAIN_COUNT, AD_COUNT, NOTHING_TO_COUNT);            --! This type is used to control in aegis_finalize the counting process of adlen and msglen.
    type AES_LOOKUP_TABLE             is array (0 to 255) of AEGIS_BYTE;                                              --! AES lookup table type. 
  
    -- Types for MUX-Selection
    type M_CIPHER_OR_PLAIN            is (PLAIN, CIPHER);                                                             --! Enum Type to make things a little clearer.
    type M_SELECT                     is (INIT, AD, CIPHER_OR_PLAIN, FINALIZE);                                       --! Type to control the m select mux in the data path.
    subtype M_SELECT_REVERT           is std_logic_vector(1 downto 0);                                                --! Helper type to revert M_SELECT in a helper funciton.
    type SELECT_OUTPUT                is (CIPHER_OR_PLAIN, TAG);                                                      --! Type to control the output mux in the data path.
  
    -- Helper Functions
    function concatBytes (a, b, c, d: AEGIS_BYTE) return AEGIS_BLOCK_QUARTER;                                         --! Takes four AEGIS_BYTE values and generates a AEGIS_BLOCK_QUARTER.
    function xorByte(a: in AEGIS_BYTE; b: in AEGIS_BYTE) return AEGIS_BYTE;                                           --! xor function of two AEGIS_BYTE values.
    function andByte(a: in AEGIS_BYTE; b: in AEGIS_BYTE) return AEGIS_BYTE;                                           --! and function of two AEGIS_BYTE values.
    function xorBlock(a: in AEGIS_BLOCK; b: in AEGIS_BLOCK) return AEGIS_BLOCK;                                       --! xor function of two AEGIS_BLOCK values.
    function andBlock(a: in AEGIS_BLOCK; b: in AEGIS_BLOCK) return AEGIS_BLOCK;                                       --! and function of two AEGIS_BLOCK values.
    function toByte(a: in std_logic_vector(7 downto 0)) return AEGIS_BYTE;                                            --! Builds a AEGIS_BYTE using a std_logic_vector.
    function toBlock(a: in std_logic_vector(127 downto 0)) return AEGIS_BLOCK;                                        --! Builds a AEGIS_BLOCK using a std_logic_vector.
    function toVector(a: in AEGIS_BLOCK; b: in AEGIS_BLOCK; c: in AEGIS_BLOCK;                                        --! Builds a AEGIS_STATE_VECTOR using AEGIS_BLOCK (only defined for AEGIS-128).
        d: in AEGIS_BLOCK; e: in AEGIS_BLOCK) return AEGIS_STATE_VECTOR;
    function toStdVectorBlock(a: in AEGIS_BLOCK) return AEGIS_BLOCK_REVERT;                                           --! Returns a std_logic_vector using a AEGIS_BLOCK.
    function toStdVectorByte(a: in AEGIS_BYTE) return AEGIS_BYTE_REVERT;                                              --! Returns a std_logic_vector using a AEGIS_BYTE.
    function toStdVectorPlainCipher(a: in M_CIPHER_OR_PLAIN) return std_logic;                                        --! Returns a std_logic using a M_CIPHER_OR_PLAIN.
    function toStdVectorSelectM(a: in M_SELECT) return M_SELECT_REVERT;                                               --! Returns a std_logic_vector using a M_SELECT.
    function toStdVectorSelectOutput(a: in SELECT_OUTPUT) return std_logic;                                           --! Returns a std_logic using a SELECT_OUTPUT.
    function toBdiEnc(a: in std_logic_vector(2 downto 0)) return BDI_ENC;                                             --! Returns BDI_ENC value using a std_logic_vector.
    function reverseBlock(a: in AEGIS_BLOCK) return AEGIS_BLOCK;                                                      --! Reverting the byte order of a AEGIS_BLOCK.
    function padBlock(a: in AEGIS_BLOCK; size: in integer range 0 to 16) return AEGIS_BLOCK;                          --! Pads a AEGIS_BLOCK according to size.
  
end aegis_package; 

-- For definition show aegis_package description.
--! @cond Doxygen_Suppress
package body aegis_package is
 
  function concatBytes (a, b, c, d: AEGIS_BYTE) return AEGIS_BLOCK_QUARTER is
    variable retValue : AEGIS_BLOCK_QUARTER;
    begin
        retValue(0) := a;
        retValue(1) := b;
        retValue(2) := c;
        retValue(3) := d;
    return retValue;
  end function;
  
  function reverseBlock(a: in AEGIS_BLOCK) return AEGIS_BLOCK is
    variable retValue : AEGIS_BLOCK := toBlock(x"00000000000000000000000000000000");
    begin
        for i in 0 to 15 loop       
            retValue(i) := a(15-i);         
        end loop;
    return retValue;
  end function;   

  function toBdiEnc(a: in std_logic_vector(2 downto 0)) return BDI_ENC is
  begin
       if a(2 downto 1) = "00" then
         return AD;
       elsif a(2 downto 1) = "01" then
         return MSG_CIPHER;
       elsif a(2 downto 0) = "110" then
         return IV;
       elsif a(2 downto 0) = "100" then
         return TAG;
       else
         return NOTSUPPORTED;        
       end if;
  end function;
  
  function toStdVectorSelectOutput(a: in SELECT_OUTPUT) return std_logic is
  begin
     if a = CIPHER_OR_PLAIN then
        return '0';
     else
        return '1';
     end if;
  end function;
   
  function toStdVectorSelectM(a: in M_SELECT) return M_SELECT_REVERT is
  begin
     if a = INIT then
        return "00";
     elsif a = AD then
        return "01";
     elsif a = CIPHER_OR_PLAIN then
        return "10";
     else
        return "11";
     end if;
  end function;
  
  function toStdVectorPlainCipher(a: in M_CIPHER_OR_PLAIN) return std_logic is
    begin
        if a = PLAIN then
            return '0';
        else
            return '1';
        end if;
  end function;
  
  function toStdVectorByte(a: in AEGIS_BYTE) return AEGIS_BYTE_REVERT is
    variable retValue : std_logic_vector(7 downto 0) := x"00";
    begin
       for i in 0 to 7 loop       
          retValue(i) := a(i);          
        end loop;
        return retValue;
    end function;

  function toStdVectorBlock(a: in AEGIS_BLOCK) return AEGIS_BLOCK_REVERT is
      variable retValue : std_logic_vector(DBLK_SIZE-1 downto 0) := x"00000000000000000000000000000000";
  begin
     for i in 0 to 15 loop       
        retValue(((i+1)*8)-1 downto (i*8)) := toStdVectorByte(a(i));          
      end loop;
      return retValue;
  end function;

  function xorBlock(a: in AEGIS_BLOCK; b: in AEGIS_BLOCK) return AEGIS_BLOCK is
    variable retValue : AEGIS_BLOCK := toBlock(x"00000000000000000000000000000000");
  begin
     for i in 0 to 15 loop       
       retValue(i) := xorByte(a(i), b(i));            
     end loop;
     return retValue;
  end function;
  
  function andBlock(a: in AEGIS_BLOCK; b: in AEGIS_BLOCK) return AEGIS_BLOCK is
     variable retValue : AEGIS_BLOCK := toBlock(x"00000000000000000000000000000000");
   begin
      for i in 0 to 15 loop       
        retValue(i) := andByte(a(i), b(i));            
      end loop;
      return retValue;
   end function;
 
  function andByte(a: in AEGIS_BYTE; b: in AEGIS_BYTE) return AEGIS_BYTE is
       variable retValue : AEGIS_BYTE := x"00";
     begin
         for i in 0 to 7 loop       
           retValue(i) := a(i) and b(i);            
         end loop;
         return retValue;
     end function;
     
  function xorByte(a: in AEGIS_BYTE; b: in AEGIS_BYTE) return AEGIS_BYTE is
    variable retValue : AEGIS_BYTE := x"00";
  begin
      for i in 0 to 7 loop       
        retValue(i) := a(i) xor b(i);            
      end loop;
      return retValue;
  end function;
  
  function toByte(a: in std_logic_vector(7 downto 0)) return AEGIS_BYTE is
    variable retValue : AEGIS_BYTE := x"00";    
    begin
        for i in 0 to 7 loop       
            retValue(i) := a(i);            
        end loop;
        return retValue;
    end function;
    
  function toBlock(a: in std_logic_vector(127 downto 0)) return AEGIS_BLOCK is
    variable retValue : AEGIS_BLOCK;
    begin
        for i in 0 to 15 loop  
            retValue(i) := toByte(a(((i+1)*8)-1 downto (i*8)));            
        end loop;
        return retValue;
    end function;
   
  function toVector(a: in AEGIS_BLOCK; b: in AEGIS_BLOCK; c: in AEGIS_BLOCK; d: in AEGIS_BLOCK; e: in AEGIS_BLOCK) return AEGIS_STATE_VECTOR is
    variable retValue : AEGIS_STATE_VECTOR;
      begin
          retValue(4) := a;
          retValue(3) := b;
          retValue(2) := c;
          retValue(1) := d;
          retValue(0) := e;            
          
          return retValue;
      end function; 
      
       
   function padBlock(a: in AEGIS_BLOCK; size: in integer range 0 to 16) return AEGIS_BLOCK is
     variable retValue : AEGIS_BLOCK := toBlock(x"00000000000000000000000000000000");        
     begin
         if size = 0 then
             return retValue;
         end if;         
        
         if size > 0 then
            retValue(0) := a(0);
         end if;
         
         if size > 1 then
            retValue(1) := a(1);
         end if;
                  
         if size > 2 then
            retValue(2) := a(2);
         end if;
         
          if size > 3 then
            retValue(3) := a(3);
         end if;
         
         if size > 4 then
            retValue(4) := a(4);
         end if;
                  
         if size > 5 then
            retValue(5) := a(5);
         end if;      
         
         if size > 6 then
            retValue(6) := a(6);
         end if;
         
         if size > 7 then
            retValue(7) := a(7);
         end if;
                  
         if size > 8 then
            retValue(8) := a(8);
         end if;
         
         if size > 9 then
            retValue(9) := a(9);
         end if;
         
         if size > 10 then
            retValue(10) := a(10);
         end if;
                  
         if size > 11 then
            retValue(11) := a(11);
         end if;    
         
         if size > 12 then
             retValue(12) := a(12);
          end if;
          
          if size > 13 then
             retValue(13) := a(13);
          end if;
                   
          if size > 14 then
             retValue(14) := a(14);
          end if;
          
           if size > 15 then
             retValue(15) := a(15);
          end if;                
        
         return retValue;
   end function;
end package body aegis_package;
--! @endcond