###############################################################################
# do.txt
# This file was auto-generated by aeadtvgen v2.0.0
###############################################################################
# Parameter:
#
# add_partial            - False
# block_size             - 128
# block_size_ad          - 128
# cc_hls                 - False
# cc_pad_ad              - 0
# cc_pad_d               - 0
# cc_pad_enable          - False
# cc_pad_style           - 1
# ciph_exp               - False
# ciph_exp_noext         - False
# gen_custom_mode        - 0
# io (W,SW)              - [128, 32]
# key_size               - 256
# lib_name               - aegis256
# max_ad                 - 10
# max_block_per_sgmt     - 9999
# max_d                  - 10
# max_io_per_line        - 8
# min_ad                 - 0
# min_d                  - 0
# msg_format             - ['npub', 'ad', 'data', 'tag']
# npub_size              - 256
# nsec_size              - 0
# offline                - False
# reverse_ciph           - False
# tag_size               - 128
###############################################################################

#### Authenticated Encryption
#### MsgID=  1, KeyID=  1 Ad Size =    9, Pt Size =    4
# Instruction: Opcode=Authenticated Encryption
# TB :20101 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=4 bytes
HDR = 52000004000000000000000000000000
DAT = BE718420000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = 7696077EF4FBA8A6791FE8CCF9DC25B8
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  2, KeyID=  2 Ad Size =    8, Ct Size =    0
# Instruction: Opcode=Authenticated Decryption
# TB :30202 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=0 bytes
HDR = 43000000000000000000000000000000
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Encryption
#### MsgID=  3, KeyID=  3 Ad Size =    7, Pt Size =    2
# Instruction: Opcode=Authenticated Encryption
# TB :20303 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=2 bytes
HDR = 52000002000000000000000000000000
DAT = 439F0000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = AF3561D89D1C6669833231867BDF4AC7
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  4, KeyID=  4 Ad Size =    5, Ct Size =    0
# Instruction: Opcode=Authenticated Decryption
# TB :30404 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=0 bytes
HDR = 43000000000000000000000000000000
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Encryption
#### MsgID=  5, KeyID=  5 Ad Size =    4, Pt Size =    0
# Instruction: Opcode=Authenticated Encryption
# TB :20505 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=0 bytes
HDR = 52000000000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = C04E185B572B81B15D74E0FF81C9C972
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  6, KeyID=  5 Ad Size =    6, Ct Size =    8
# Instruction: Opcode=Authenticated Decryption
# TB :30506 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=8 bytes
HDR = 43000008000000000000000000000000
DAT = B90313DBC757CEEC0000000000000000
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Encryption
#### MsgID=  7, KeyID=  6 Ad Size =    9, Pt Size =    3
# Instruction: Opcode=Authenticated Encryption
# TB :20607 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=3 bytes
HDR = 52000003000000000000000000000000
DAT = 73E8F100000000000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = D597292DFC6D33F06F083F40ACD1ED92
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  8, KeyID=  7 Ad Size =    7, Ct Size =    1
# Instruction: Opcode=Authenticated Decryption
# TB :30708 (Encoding used by testbench)
# Info :                Plaintext, EOT=1, Last=1, Length=1 bytes
HDR = 43000001000000000000000000000000
DAT = B2000000000000000000000000000000
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Encryption
#### MsgID=  9, KeyID=  8 Ad Size =    6, Pt Size =   10
# Instruction: Opcode=Authenticated Encryption
# TB :20809 (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=10 bytes
HDR = 5200000A000000000000000000000000
DAT = 25ECF2BF7FABE4B47DFF000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = A6348CAC267EC5481A85F59699EB0E23
# Status: Success
STT = E0000000000000000000000000000000

#### Authenticated Encryption
#### MsgID= 10, KeyID=  8 Ad Size =   10, Pt Size =    6
# Instruction: Opcode=Authenticated Encryption
# TB :2080A (Encoding used by testbench)
# Info :               Ciphertext, EOT=1, Last=0, Length=6 bytes
HDR = 52000006000000000000000000000000
DAT = 631734291E2100000000000000000000
# Info :                      Tag, EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = F46654291F8B2B0F1C89F16C6C910A00
# Status: Success
STT = E0000000000000000000000000000000

###EOF
