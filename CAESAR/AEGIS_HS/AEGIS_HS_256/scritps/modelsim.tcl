set SOURCE_FILES "../../src_rtl/"
set TB_FILES "../../src_tb/"
set INTERFACE_REPO "../../AEAD"
set TOP_LEVEL_NAME AEAD_TB
set STOP_AT_FAULT True
set CUSTOM_DO_FILE "wave.do"
set PWIDTH 128
#set SWIDTH 256
set SWIDTH 32

# ----------------------------------------
# Set implementation files
set src_vhdl [subst {
    "$SOURCE_FILES/AEAD_pkg.vhd"
	"../src_rtl/aegis_package.vhd"
	"$SOURCE_FILES/aes_add_key.vhd"
	"$SOURCE_FILES/aes_mult2_w_sbox.vhd"
	"$SOURCE_FILES/aes_mult3_w_sbox.vhd"
	"$SOURCE_FILES/aes_sbox.vhd"	
	"$SOURCE_FILES/aes_mc.vhd"	
    "$SOURCE_FILES/aes_mc_sb.vhd"	
    "$SOURCE_FILES/aes_shift_rows.vhd"	
    "$SOURCE_FILES/aes_round.vhd"		
	"$SOURCE_FILES/aegis_block_reg.vhd"
	"$SOURCE_FILES/aegis_state_reg.vhd"
	"$SOURCE_FILES/aegis_mux_2_to_1.vhd"
	"$SOURCE_FILES/aegis_mux_4_to_1.vhd"		
    "$SOURCE_FILES/aegis_finalize_generate.vhd"
	"$SOURCE_FILES/aegis_finalize.vhd"
    "$SOURCE_FILES/aegis_input_output.vhd"
	"$SOURCE_FILES/aegis_initialization_calculate.vhd"
	"$SOURCE_FILES/aegis_initialization.vhd"
	"$SOURCE_FILES/aegis_state_update.vhd"
	"$SOURCE_FILES/aegis_tag.vhd"
	"$SOURCE_FILES/aegis_crypto.vhd"		
    "$SOURCE_FILES/aegis_verification.vhd"		
	"$SOURCE_FILES/aegis_cipher_core_controller.vhd"	
	"$SOURCE_FILES/aegis_cipher_core_data_path.vhd"	
    "$SOURCE_FILES/CipherCore.vhd"
    "$SOURCE_FILES/PreProcessor.vhd"
    "$SOURCE_FILES/PostProcessor.vhd"
    "$SOURCE_FILES/fwft_fifo.vhd"
    "$SOURCE_FILES/AEAD.vhd"
    "$SOURCE_FILES/AEAD_Arch.vhd"
}]

#"$SOURCE_FILES/CipherCore_Wrapper.vhd"
#"$SOURCE_FILES/AEAD_Wrapper.vhd"
# ----------------------------------------
# Set simulation files
set tb_vhdl [subst {
    "$TB_FILES/design_pkg.vhd"
    "$TB_FILES/std_logic_1164_additions.vhd"
    "$TB_FILES/AEAD_TB.vhd"
}]
# ----------------------------------------
# Python interface for creating a distro
proc get_src {src}  {return $src}

# ----------------------------------------
# Create compilation libs
proc ensure_lib { lib } { if ![file isdirectory $lib] { vlib $lib } }
ensure_lib          ./libs/
ensure_lib          ./libs/work/
vmap       work     ./libs/work/

# ----------------------------------------
# Compile implementation files
alias imp_com {
    echo "imp_com"
    foreach f $src_vhdl {vcom -quiet -work work $f}
}

# ----------------------------------------
# Compile simulation files
alias sim_com {
    echo "sim_com"
    foreach f $tb_vhdl {vcom -quiet -work work $f}
}

# ----------------------------------------
# Compile simulation files
alias com {
    echo "com"
    imp_com
    sim_com
}

# ----------------------------------------
# Add wave form and run
alias run_wave {
    echo "\[exec\] run_wave"
	
 #   add wave -group PreProcessor  -ports     -radix hexadecimal $TOP_LEVEL_NAME/uut/u_input/*
 #   add wave -group PreProcessor  -internals -radix hexadecimal $TOP_LEVEL_NAME/uut/u_input/*
 #   add wave -group CipherCore    -ports     -radix hexadecimal $TOP_LEVEL_NAME/uut/u_cc/*
 #   add wave -group CipherCore    -internals -radix hexadecimal $TOP_LEVEL_NAME/uut/u_cc/*
#	add wave -group Controller    -ports     -radix hexadecimal $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_controller_inst/*
#    add wave -group Controller    -internals -radix hexadecimal $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_controller_inst/*
 #   add wave -group PostProcessor -ports     -radix hexadecimal $TOP_LEVEL_NAME/uut/u_output/*
 #   add wave -group PostProcessor -internals -radix hexadecimal $TOP_LEVEL_NAME/uut/u_output/*
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/key_block
    add wave $TOP_LEVEL_NAME/uut/u_cc/key_valid
    add wave $TOP_LEVEL_NAME/uut/u_cc/key_ready
    add wave $TOP_LEVEL_NAME/uut/u_cc/key_update
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/iv_block
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/bdo_padded
    add wave $TOP_LEVEL_NAME/uut/u_cc/bdo_valid
    add wave $TOP_LEVEL_NAME/uut/u_cc/bdo_ready    
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/bdi_block
    add wave $TOP_LEVEL_NAME/uut/u_cc/bdi_valid
    add wave $TOP_LEVEL_NAME/uut/u_cc/bdi_ready
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_controller_inst/current_state
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_controller_inst/next_state
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/state
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/m_padded
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/aegis_state_update_inst/m
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/aegis_initialization_inst/enable_keyxoriv
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/aegis_initialization_inst/enable_key
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/aegis_initialization_inst/m_internal
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/aegis_initialization_inst/input_select_internal
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/aegis_initialization_inst/key_reg_out
    add wave $TOP_LEVEL_NAME/uut/u_cc/aegis_cipher_core_data_path_inst/aegis_initialization_inst/key_xor_iv_reg_out
    

    # Configure wave panel
    configure wave -namecolwidth 180
    configure wave -valuecolwidth 200
    configure wave -signalnamewidth 1
    configure wave -timelineunits ns
    WaveRestoreZoom {0 ps} {2000 ns}
    configure wave -justifyvalue right
    configure wave -rowmargin 8
    configure wave -childrowmargin 5
}


# ----------------------------------------
# Compile all the design files and elaborate the top level design with -novopt
alias ldd {
    com
    set run_do_file [file isfile $CUSTOM_DO_FILE]
    if {$run_do_file == 1} {
        vsim -novopt -t ps -L work $TOP_LEVEL_NAME -gG_PWIDTH=$PWIDTH -gG_SWIDTH=$SWIDTH  -do $CUSTOM_DO_FILE  -gG_STOP_AT_FAULT=$STOP_AT_FAULT
    } else {
      vsim -novopt -t ps -L work $TOP_LEVEL_NAME -gG_PWIDTH=$PWIDTH -gG_SWIDTH=$SWIDTH  -gG_STOP_AT_FAULT=$STOP_AT_FAULT
      run_wave
    }
    run 100000 ns
}

# ----------------------------------------
# Print out user commmand line aliases
alias h {
    echo "List Of Command Line Aliases"
    echo
    echo "imp_com                       -- Compile implementation files"
    echo
    echo "sim_com                       -- Compile simulation files"
    echo
    echo "com                           -- Compile files in the correct order"
    echo
    echo "ldd                           -- Compile and run"
    echo
}
h
