
Documentation of an AEGIS VHDL implementation
==========================

*Source files are prepared for a doxygen documentation.*

# Introduction

The presented implementation is a VHDL realization of a proposed authenticated encryption algorithm called AEGIS conceived by [Wu et al.][1] and submitted to the [Competition for Authenticated Encryption: Security, Applicability, and Robustness (CAESAR)][2]. AEGIS is a finalist for high-speed use cases (use case 2) and has three flavors: AEGIS-128, AEGIS-256 and AEGIS-128L. This realization has excluded AEGIS-128L because it was announced that either AEGIS-128 or AEGIS-128L will be dropped and AEGIS-128 is most likely to be chosen. 

# Repository structure

| Folder               | Description                                               |
|----------------------|-----------------------------------------------------------|
| doc                  | Contains some block diagrams, variant and assumption file |
| src_rtl              | Common implementation files for both flavors.             |
| src_tb               | Common test bench files for both flavors.                 |
| AEGIS_HS_###/KAT     | Precomputed test vectors.                                 |
| AEGIS_HS_###/scripts | Modelsim test bench script.                               |
| AEGIS_HS_###/src_rtl | Implemenation dependent package.                          |

# Getting started
The presented implementation was developed using the [CAESAR Hardware API v1.0][3].
If not described in this documentation, further information can be found there and especially in the [Implementer's Guide][4].

## Run ModelSim simulation
To run the simulation start ModelSim and change the working directory to "AEGIS_HS\AEGIS_HS_***\scripts". Then execute the tcl command "source modelsim.tcl" than "ldd". The pre-computed test vectors are being simulated. If everything worked out you will get the information "Error: PASS (0): SIMULATION FINISHED". If no response is received one possible problem could be, that the simulation runtime is to short or the bus width is not configured correctly (s. Bus configuration). Go into the "modelsim.tcl" script and find the lines "run x ns", SDI_WIDTH and PDI_WIDTH. Alter the lines if necessary and rerun the simulation. 

## Bus configuration
The cipher is independent of the attached bus widths. Possible bus widths are listed in the following table. All combinations are possible.

| Flavor    | PDI           | SDI              |
|-----------|---------------|------------------|
| aegis-128 | 32, 64, 128   | 32, 64, 128      |
| aegis-256 | 32, 64, 128   | 32, 64, 128, 256 |

Please take care to adjust the files modelsim.tcl and make sure to use the correct --io parameter in the test vector generation.


# AEGIS implementation blocks
In this section some of the building blocks of the implementation are displayed in block diagrams. If not noted otherwise signals are 128 bit signals.

## aegis_cipher_core_data_path
![](doc/aegis_cipher_core_data_path.png)

## aegis_verification
![](doc/aegis_verification.png)

## aegis_input_output
![](doc/aegis_input_output.png)

## aegis_initialization
![](doc/aegis_initialization_128.png)

## aegis_initialization_calculate
![](doc/aegis_initialization_calculate_128.png)

## aegis_state_update
![](doc/aegis_state_update.png)

## aegis_finalize_generate
![](doc/aegis_finalize_generate.png)

## aegis_tag
![](doc/aegis_tag.png)

## aegis_crypto
![](doc/aegis_crypto.png)

[1]: https://competitions.cr.yp.to/round3/aegisv11.pdf "AEGIS: A Fast Authenticated Encryption  Algorithm"
[2]: https://competitions.cr.yp.to/caesar.html "CAESAR: Competition for Authenticated Encryption: Security, Applicability, and Robustness"
[3]: https://cryptography.gmu.edu/athena/index.php?id=CAESAR "ATHENA Autmoated Tool for Hardware Evaluation"
[4]: https://cryptography.gmu.edu/athena/CAESAR_HW_API/CAESAR_HW_Implementers_Guide_v2.0.pdf  "Implementer’s Guide to Hardware Implementations Compliant with the CAESAR Hardware API"