----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-10
--! @brief      The aegis_state_update stores and updates the state.
--! @details    The aegis_state_update stores and updates the state. It consist out of a number of AES rounds feeded 
--!             feeded back into the register. The output of the last round is xored with the m signal. 
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_state_update.vhd aegis_state_update\endlink.
entity aegis_state_update is
  Port (
    clk                             : in  STD_LOGIC;                --! Clock signal.
    next_state_valid                : in  boolean;                  --! Signal asserts the validity of the next_state signal. If next_state_valid = true the aegis_state_reg is enabled otherwise disabled.  
    force_state_to_init             : in  boolean;                  --! Signal indicates that the state should be forced to the calculated state of aegis_initialization which is inserted with the signal state_init.
    state_init                      : in  AEGIS_STATE_VECTOR;       --! Signal of aegis_initialization which represent the initialization state.
    m                               : in  AEGIS_BLOCK;              --! The signal m is xored with the last state flip flop.
    state                           : out AEGIS_STATE_VECTOR;       --! The current state.
    state_xor                       : out AEGIS_BLOCK;              --! Depending which flavor is active, different outputs:  AEGIS128: s1 xor s4, AEGIS256: s1 xor s4 xor s5, AEGIS128L:.
    state_s3_look_ahead             : out AEGIS_BLOCK               --! This signal is used to let the aegis_finalization entity update m_finalize at the right timing. 
);
end aegis_state_update;

--! Architecture of \link aegis_state_update.vhd aegis_state_update\endlink entity.
architecture Behavioral of aegis_state_update is  

  signal aes_round_out              : AEGIS_STATE_VECTOR;           --! Output signal of the AES rounds.
  signal reg_out                    : AEGIS_STATE_VECTOR;           --! Output of the D flip flops. This signal represents the current state.
  signal mux_out                    : AEGIS_STATE_VECTOR;           --! Output of the input muxes.
  signal state_next                 : AEGIS_STATE_VECTOR;           --! If next_state_valid = true the aegis_state_reg is enabled otherwise disabled.
  signal input_select_init_mux      : std_logic;                    --! In the init-phase, force state to the input of the aegis_initialization-calculation input.
  signal enable_state               : AEGIS_REG_STATE;              --! Enable signal of the state register.
  
begin   
    
    -- GENEARTE OUTPUT SIGNALS
    --! The output of the flip flop is the current state.
    state <= reg_out; 
    
    --! This signal is used to let the aegis_finalization entity update m_finalize at the right timing.
    state_s3_look_ahead <= state_next(3);
  
    --! The state_xor signal is used in aegis_finalization and in aegis_crypto. Therefore, it is only
    --! generated once here and then feed into these entities. The signal depends on the flavor.
    G_AEGIS_STATE_XOR_AEGIS128: 
    if ACTIVE_AEGIS_FLAVOR = AEGIS128 generate
        state_xor <= xorBlock(reg_out(1), reg_out(4));
    end generate G_AEGIS_STATE_XOR_AEGIS128;
 
    G_AEGIS_STATE_XOR_AEGIS256: 
    if ACTIVE_AEGIS_FLAVOR = AEGIS256 generate
        state_xor <= xorBlock(xorBlock(reg_out(1), reg_out(4)),reg_out(5));
    end generate G_AEGIS_STATE_XOR_AEGIS256;
  
  
    -- STATE UPDATE
    
    --! Control the input muxes.
    input_select_init_mux <= '1' when force_state_to_init else '0';
    
    --! If next state is valid store it into the flip flop.
    enable_state <= ENABLE when next_state_valid else DISABLE;
    
    --! Since the output of round I has to be feeded
    --! back into the register I+1 a case decision is made for the last flip flop
    --! to feed the signal back into flip flop 0.
    G_AEGIS_STATE_NEXT :
    for I in 0 to AEGIS_STATE_SIZE-2 generate
      state_next(I+1) <= aes_round_out(I);
    end generate;

    --! The last feeded back state has to be xored with the current m signal.
    state_next(0) <= xorBlock(aes_round_out(AEGIS_STATE_SIZE-1), m);  
  
    --! The heart of the aegis_state_update entity: a flip flop which stores
    --! the state.
    aegis_state_reg_inst : entity work.aegis_state_reg
        port map(
            clk => clk,
            en  => enable_state, 
            D   => mux_out,
            Q   => reg_out
        );
    
    --! Generate AEGIS_STATE_SIZE-1 AES rounds and D Flip Flops.
    G_AEGIS_STATE_UPDATE_COLUMN : 
    for I in 0 to AEGIS_STATE_SIZE-1 generate
        
        --! This mux selects depending on input_select_init_mux when to 
        --! force the state to the values of aegis_initialization (initialization
        --! state) and when to feed back the result of the AES rounds into the 
        --! Flip Flop.
        init_mux_inst: entity work.aegis_mux_2_to_1
            port map (
                input_0      => state_next(I),
                input_1      => state_init(I),
                input_select => input_select_init_mux, 
                output       => mux_out(I)
            );
        
        --! Generate AES Rounds and connect the key inputs of the I-th round with the
        --! with the (i+1)-th round. Therefore, a case decision is made for the last one.
        G_AES_ROUND_LAST: 
        if (I = AEGIS_STATE_SIZE-1) generate   
            aes_round_inst: entity work.aes_round 
                port map(                
                    value_in  => reg_out(I),
                    key_in    => reg_out(0),
                    value_out => aes_round_out(I)
                );
        end generate G_AES_ROUND_LAST;
               
        G_AES_ROUND: 
        if (I < AEGIS_STATE_SIZE-1) generate   
            aes_round_inst: entity work.aes_round 
                port map(             
                    value_in  => reg_out(I),
                    key_in    => reg_out(I+1),
                    value_out => aes_round_out(I)
                );
        end generate G_AES_ROUND;
   end generate;
 
end Behavioral;
