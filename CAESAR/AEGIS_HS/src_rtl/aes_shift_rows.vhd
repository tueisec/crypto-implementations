----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-08
--! @brief      Performs AES Shift Rows Operation.
--! @details    For a detailed description of AES Shift Rows operation see Wikipedia \cite aes_sr.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aes_shift_rows.vhd aes_shift_rows\endlink.
entity aes_shift_rows is
  Port ( 
        value_in: in AEGIS_BLOCK;                       --! A 128bit AEGIS_BLOCK as input value.
        columns: out AEGIS_BLOCK_QUARTER_VECTOR         --! The signal contains the shifted values separated in 32 bit AEGIS_BLOCK_QUARTERs.
    );
end aes_shift_rows;

--! Architecture of \link aes_shift_rows.vhd aes_shift_rows\endlink entity.
architecture Behavioral of aes_shift_rows is

begin

    -- Shifting values 
    columns(0) <= concatBytes(value_in(0), value_in(5), value_in(10), value_in(15));
    columns(1) <= concatBytes(value_in(4), value_in(9), value_in(14), value_in(3));
    columns(2) <= concatBytes(value_in(8), value_in(13), value_in(2), value_in(7));
    columns(3) <= concatBytes(value_in(12), value_in(1), value_in(6), value_in(11));

end Behavioral;