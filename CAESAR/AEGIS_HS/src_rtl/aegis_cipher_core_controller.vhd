----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-07
--! @brief      The ageis_cipher_core_controller controls all operations in aegis_cipher_core_data_path.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL; 
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_cipher_core_controller.vhd aegis_cipher_core_controller\endlink.
entity aegis_cipher_core_controller is   
    Generic (        
        G_ASYNC_RSTN    : boolean;          --! Async active low reset.
        G_DBLK_SIZE     : integer;          --! Data block size.
        G_KEY_SIZE      : integer;          --! Key block size.
        G_LBS_BYTES     : integer           --! The number of bits required to hold block size expressed in bytes = log2_ceil(G_DBLK_SIZE/8)
    );
    Port (
        clk                                 : in  std_logic;                                                    --! Clock signal.
        rst                                 : in  std_logic;                                                    --! Reset signal.
        decrypt                             : in  std_logic;                                                    --! This signal indicates a decryption or a encryption. 
        bdi_type                            : in  BDI_ENC;                                                      --! bdi_type indicates which data is transmitted over the bdi signal.
        bdi_size                            : in  std_logic_vector(G_LBS_BYTES+1    -1 downto 0);               --! This signal indicates how many bytes are valid in this block.
        bdi_valid                           : in  std_logic;                                                    --! This signal is asserted when data on bdi is valid.
        bdi_ready                           : out std_logic;                                                    --! This signal acknowledges the data assert on the bdi signal.
        bdi_eot                             : in  std_logic;                                                    --! This signal indicates that the current bdi block is the last of the current type.
        bdi_eoi                             : in  std_logic;                                                    --! This signal indicates that the current bdi block is the last of the whole input.
        key_valid                           : in  std_logic;                                                    --! This signal is asserted when data on the key signal is valid. 
        key_ready                           : out std_logic;                                                    --! This signal indicates that the key has successfully processed.
        key_update                          : in  std_logic;                                                    --! If key_update is asserted the aegis_cipher_core_data_path stores the new key.
        bdo_valid                           : out std_logic;                                                    --! This signal indicates the validness of the current bdo block.
        bdo_ready                           : in  std_logic;                                                    --! This signal acknowledges the data assert on the bod signal.
        bdo_size                            : out std_logic_vector(G_LBS_BYTES+1    -1 downto 0);               --! Indicates the number of valid bytes in the current bdo block.
        msg_auth_valid                      : out std_logic;                                                    --! Strobe signal for msg_auth.
        msg_auth_ready                      : in  std_logic;                                                    --! Acknowledge signal for msg_auth.
        
        
        -- Internal outputs to DataPath        
        bdo_size_padding                    : out integer range 0 to 16;                                        --! Size of the current output block. Data path pads the output according to this signal.
        force_state_to_init                 : out boolean;                                                      --! Signal 'force_state_to_init_s' forces input muxes of the aegis_state_update to the input values of aegis_initialization.
        next_state_valid                    : out boolean;                                                      --! State register update
        enable_key                          : out AEGIS_REG_STATE_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);  --! Enable signal of the key register in the aegis_initialization. 
        enable_keyxoriv                     : out AEGIS_REG_STATE_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);  --! Enable signal of the keyxoriv register in the aegis_initialization.
        counter_state                       : out AEGIS_COUNTER_STATE;                                          --! Signal indicates when to add the current bdi_size. Is used to calculate adlen and msglen in aegis_finalize.
        select_m_cipher_or_plain            : out M_CIPHER_OR_PLAIN;                                            --! Signal controls a mux in the aegis_cipher_core_data_path.
        select_m                            : out M_SELECT;                                                     --! Signal controls a mux in the aegis_cipher_core_data_path which choses the correct m for the aegis_state_update.
        select_output                       : out SELECT_OUTPUT                                                 --! Specifies which signal shuld redirect to the bdo output signal. 
    );
end aegis_cipher_core_controller;

--! Architecture of \link aegis_cipher_core_controller.vhd aegis_cipher_core_controller\endlink entity.
architecture Behavioral of aegis_cipher_core_controller is
 
 --! Types for the state machine. IDLE: The idle state sets default values and waits until input and key arrives. 
type FSM_STATES is (
    IDLE, 
    IVANDKEY, --! Initialization of the state register.
    IVANDKEY_256, --! Initialization of the state register for second 
    AD, --! State to process the associated data and 
    PLAIN_OR_CIPHER, 
    TAG_WAIT, 
    TAG, 
    VERIFICATION);  
    
    
signal current_state: FSM_STATES;                                                               --! Holds current state.
signal next_state: FSM_STATES;                                                                  --! Holds the next state.
signal counter: integer := INIT_ROUNDS;                                                         --! Counts substates in the initalie phase and tag generation phases
signal bdo_size_internal: integer range 0 to 16;                                                --! Holds current output length
signal decrypt_internal: std_logic := '0';                                                      --! Since 'decrypt' signal could have been changed before finalization is finished is is stored.
signal bdi_eoi_internal: std_logic := '0';                                                      --! Since 'eoi' is reset before it can be evaluated store it.

begin


bdo_size_padding <= bdo_size_internal;
bdo_size       <= std_logic_vector(to_unsigned(bdo_size_internal, bdo_size'length));
select_m_cipher_or_plain <= CIPHER when decrypt_internal = '1' else PLAIN;


GENERATE_ASYNC_ACTIVE_LOW : if G_ASYNC_RSTN = true generate

    --! Next state logic with asynchronous reset.
    state_update_async: process (clk, rst)
    begin
        if rst = '0' then
            current_state <= IDLE;
        else 
            if (rising_edge(clk)) then
                current_state <= next_state;   --state change.
            end if;
        end if;
    end process;

end generate;

GENERATE_SYNC_ACTIVE_HIGH: if G_ASYNC_RSTN = false generate
    
    --! Next state logic with synchronous reset.
    state_update_sync: process (clk)
    begin        
        if (rising_edge(clk)) then
            if rst = '1' then
                current_state <= IDLE;
            else
                current_state <= next_state;   --state change.
            end if;
        end if;        
    end process;

end generate;  

--! Finite State Machine of the cipher.
state_logic: process(
    current_state, counter, bdi_type, bdi_valid, msg_auth_ready, bdi_eoi, bdi_eoi_internal,
    bdi_size, key_update, key_valid, bdi_eot, decrypt_internal, bdo_ready)
begin
    
    -- Setting default values for FSM
    select_output <= CIPHER_OR_PLAIN;
    bdo_size_internal <= 16;        
    next_state <= IDLE;
    bdi_ready <= '0';
    key_ready <= '0'; 
    bdo_valid <= '0'; 
    enable_key <= (others => DISABLE);        
    enable_keyxoriv <= (others => DISABLE);       
    msg_auth_valid <= '0';
    force_state_to_init <= false;
    next_state_valid <= false;
    counter_state <= IDLE;                                   
    select_m <= INIT;
    
    -- Start state depending decision         
    case current_state is
        when IDLE =>        
            force_state_to_init <= true;                                                        -- Activate aegis_initialization to deliver initial state.                 
            counter_state <= RESET;                                                             -- Reset aegis_counter, beeing ready to calc adlen and msglen.
                        
            if bdi_valid = '1' and bdi_type = IV and (key_valid = '1' or key_update = '0') then -- If input is valid and type IV go to next state
                bdo_size_internal <= to_integer(unsigned(bdi_size));                            -- Use current bdi_size to set up padding and bdo_size output signal.
                
                enable_keyxoriv(0) <= ENABLE;                                                   -- Save IV in register for succeeding Initialization rounds               
                
                if key_update = '1' then 
                    enable_key <= (others => ENABLE);                                           -- If key should be updated and valid save it into register
                end if;
                
                if ACTIVE_AEGIS_FLAVOR = AEGIS256 then
                    next_state <= IVANDKEY_256;
                    bdi_ready <= '1';                    
                else
                    next_state_valid <= true;                                                   -- Next state should be processed into the aegis_state_reg.
                    next_state <= IVANDKEY;                                                     -- Go to IVANDKEY (counter is now counting the initialization)
                end if;    
            end if;
        when IVANDKEY_256 =>
            force_state_to_init <= true; 
            next_state <= IVANDKEY_256;
            
            if bdi_valid = '1' and bdi_type = IV  then 
             
                    bdo_size_internal <= to_integer(unsigned(bdi_size));                        -- Use current bdi_size to set up padding and bdo_size output signal.
                             
                    enable_keyxoriv <= (others => ENABLE) ;                                     -- Save IV in register for succeeding Initialization rounds               
                
                    if key_update = '1' and key_valid = '1' then 
                        enable_key <= (others => ENABLE);                                       -- If key should be updated and valid save it into register
                    end if;
                                        
                    next_state_valid <= true;                                                   -- Next state should be processed into the aegis_state_reg.
                    next_state <= IVANDKEY;                                                     -- Go to IVANDKEY (counter is now counting the initialization)                     
            end if;                                    
        when IVANDKEY =>
            
            if counter = 1 and bdi_valid = '1' and bdi_type = AD then                           -- If already associated data is valid work on it    
                    
                bdo_size_internal <= to_integer(unsigned(bdi_size));                            -- Use current bdi_size to set up padding and bdo_size output signal.
                next_state_valid <= true;                                                       -- Next state should be processed into the aegis_state_reg.
                bdi_ready <= '1';                                                               -- Acknowledge input
                counter_state <= AD_COUNT;                                                      -- Count AD block
                next_state <= AD;                                                               -- Go to next state        
                select_m <= AD;                                                                 -- Deliver aegis_state_update appropiate m
                
                if bdi_eoi_internal = '1' or bdi_eoi = '1' then                                 
                    next_state <= TAG_WAIT;                                                     -- If this AD block was already end of input go to Tag generation
                elsif bdi_eot = '1' then
                    next_state <= PLAIN_OR_CIPHER;                                              -- If this was the end of type go to PLAIN_OR_CIPHER             
                end if;
            
            elsif counter = 1 and bdi_valid = '1' and bdi_type = MSG_CIPHER then                -- If already associated data is valid work on it               
               next_state <= PLAIN_OR_CIPHER;                                                   -- Next state is of course PLAIN_OR_CIPHER
               
               if (bdo_ready = '1') then                                                        -- Only engage if counterpart is ready to receive. 
                   bdo_size_internal <= to_integer(unsigned(bdi_size));                         -- Use current bdi_size to set up padding and bdo_size output signal. 
                   next_state_valid <= true;                                                    -- Next state should be processed into the aegis_state_reg.
                   bdi_ready <= '1';                                                            -- Acknowledge input
                   counter_state <= CIPHER_OR_PLAIN_COUNT;                                      -- Count MSG or CIPHER block
                   bdo_valid <= '1';                                                            -- Current bdo block is valid.
                   select_m <= CIPHER_OR_PLAIN;                                                 -- The state should be updated with the plain text.
                   
                   if bdi_eoi_internal = '1' or bdi_eoi = '1' or bdi_eot = '1' then             
                       next_state <= TAG_WAIT;                                                  -- If this is already the end of type or input jump to the Tag generation.
                   end if;
               end if;
            elsif counter = 1 and bdi_eoi_internal = '1' then                                   
                next_state <= TAG_WAIT;                                                         -- If the end of input is reached and initialization is finishre jump directly to the tag generation.
                select_m <= FINALIZE;                                                           -- State update with m_finalize.                    
            elsif counter = 1 then                  
                next_state <= AD;                                                               -- If none of the above cases where met jump to the next possible type so we are not stuck.
            elsif counter < INIT_ROUNDS-1 then
                next_state <= IVANDKEY;                                                         -- This case is used while Initialization
                next_state_valid <= true;                                                       -- Next state should be processed into the aegis_state_reg.
                if (counter = 2) then
                    counter_state <= NOTHING_TO_COUNT;                                          -- Trigger counter to set m_finalize correctly        
                end if; 
            else                            
                bdi_ready <= '1';                                                               -- When IVANDKEY is reached the first time acknowledge everything.
                if key_update = '1' then
                    key_ready <= '1';
                end if;
                next_state <= IVANDKEY;
                next_state_valid <= true;                                                       -- Next state should be processed into the aegis_state_reg.
            end if;            
        when AD =>             
            next_state <= AD;                                                                   -- If nothing else happens stay in AD.
                   
            if bdi_valid = '1' and bdi_type = MSG_CIPHER then                               
               next_state <= PLAIN_OR_CIPHER;                                                   -- If MSG_CIPHER_TAG type was transmitted jump to the correct case.
                           
               if (bdo_ready = '1') then                                                        -- Process only the data if the other side is ready to receive something.
                   bdo_size_internal <= to_integer(unsigned(bdi_size));
                   next_state_valid <= true;                                                    -- Next state should be processed into the aegis_state_reg.
                   bdi_ready <= '1';                 
                   counter_state <= CIPHER_OR_PLAIN_COUNT;                                      -- aegis_finalize has to count bdi_size => msglen
                   bdo_valid <= '1';
                   select_m <= CIPHER_OR_PLAIN;
                   
                   if bdi_eoi = '1' or bdi_eot = '1' then                                       -- If current inputwas the last for MSG_CIPHER_TAG type or this is the end of input jump to tag generation.
                       next_state <= TAG_WAIT;                      
                   end if;
               end if;
                
            elsif bdi_valid = '1' and bdi_type = AD then                                        -- Process associated data and update state with it.
                bdo_size_internal <= to_integer(unsigned(bdi_size));
                next_state_valid <= true;                                                       -- Next state should be processed into the aegis_state_reg.
                next_state <= AD;                                                               -- Stay in the AD case.
                bdi_ready <= '1';                                                               -- Acknowledge input.
                counter_state <= AD_COUNT;                                                      -- aegis_finalize should count the bdi_size. => adlen.
                select_m <= AD;                                                                 -- State update with m_ad. 
                
                if bdi_eoi = '1' then                                                           -- If current input was the last jump to Tag generation.
                    next_state <= TAG_WAIT;                   
                end if;     
            end if;  
            
        when PLAIN_OR_CIPHER =>
        
          next_state <= PLAIN_OR_CIPHER; 
          counter_state <= IDLE; 
                          
          if bdo_ready = '1' and bdi_valid = '1' and bdi_type = MSG_CIPHER then                 -- Process only the data if the other side is ready to receive something and data is valid.      
             bdo_size_internal <= to_integer(unsigned(bdi_size));                               -- Set bdo_size_internal so bdo_size is updated and bdo is padded correctly.
             next_state_valid <= true;                                                          -- Next state should be processed into the aegis_state_reg.
             bdi_ready <= '1';                                                                  -- Acknowledge input.
             counter_state <= CIPHER_OR_PLAIN_COUNT;                                            -- aegis_finalize has to count bdi_size => msglen
             bdo_valid <= '1';                                                                  -- Strobe output.
             select_m <= CIPHER_OR_PLAIN;
                 
             next_state <= PLAIN_OR_CIPHER; 
             if bdi_eoi = '1' or bdi_eot = '1' then
                next_state <= TAG_WAIT;                 
             end if;                   
          end if; 
            
        when TAG_WAIT =>  
              next_state_valid <= true;                                                         -- Next state should be processed into the aegis_state_reg.
              next_state <= TAG;     
              select_m <= FINALIZE;        
        when TAG =>             
            select_output <= TAG;
            select_m <= FINALIZE;
            
             if counter = 0 and decrypt_internal = '1' then
                next_state <= VERIFICATION;
             elsif counter = 0 then                    
                if bdo_ready = '1' then
                    bdo_valid <= '1';                                                           -- If encryption is selected strobe to the other side that tag is valid.
                    next_state <= IDLE;                    
                end if;
             else               
                next_state_valid <= true;                                                       -- Next state should be processed into the aegis_state_reg.
                next_state <= TAG;                                                              -- Stay in the TAG case.
             end if;

        when VERIFICATION =>           
            select_output <= TAG;                                                               -- Is necessary so aegis_verification gets the correct input to verify.                                           
            select_m <= FINALIZE;                                                               -- In principle not necessary here. Just for completness.
            
            if (bdi_valid = '1' and msg_auth_ready = '1') then                                  -- Process only the data if the other side is ready to receive msg_auth.
                bdi_ready <= '1';
                next_state <= IDLE;                                                             -- Return to IDLE.
                msg_auth_valid <= '1';                                                          -- Now the verification signal is valid.
            else                                 
                next_state <= VERIFICATION;                                                     -- Stay here until other side is ready to receive msg_auth.              
            end if;
    end case;
end process;

--! The 'store_important_values' process registers some values which are needed through out the
--! runtime but are not necessarily kept valid by the caller. 
store_important_values: process(clk)
begin
    if rising_edge(clk) then
        decrypt_internal <= decrypt_internal;        
        bdi_eoi_internal <= bdi_eoi_internal;   
        
        if (current_state = IVANDKEY and counter = INIT_ROUNDS) then
            decrypt_internal <= decrypt;
            bdi_eoi_internal <= bdi_eoi;
        end if;        
    end if;
end process;


--! The 'counter_proc' process takes care about the initialization and the tag generation phase. These phases have 
--! predefined runtimes. Therefore the counts every clock cycle. INIT_ROUNDS depends on the AEGIS_FLAVOR.
counter_proc: process(clk)
begin
    if (rising_edge(clk)) then
        if current_state = IVANDKEY then
            if counter > 0 then
                counter <= counter - 1;
            else
                counter <= counter;
            end if;
        elsif current_state = TAG then            
             if counter > 0 then
                counter <= counter - 1;
            else
                counter <= counter;
            end if;
        elsif current_state = TAG_WAIT then
            counter <= FINALIZATION_ROUNDS;    
        else
            counter <= INIT_ROUNDS;
        end if;
    end if;
end process;

end Behavioral;
