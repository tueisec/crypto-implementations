----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-07
--! @brief      A unclocked 2-to-1 Multiplexer for AEGIS_BLOCK type.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_mux_2_to_1.vhd aegis_mux_2_to_1\endlink.
entity aegis_mux_2_to_1 is
    Port (
        input_0         : in  AEGIS_BLOCK;      --! First input addressed by input_select = '0'
        input_1         : in  AEGIS_BLOCK;      --! Second input addressed by input_select = '1'
        input_select    : in STD_LOGIC;         --! Selects the input.
        output          : out AEGIS_BLOCK       --! Output signal.
    );
end aegis_mux_2_to_1;

--! Architecture of \link aegis_mux_2_to_1.vhd aegis_mux_2_to_1\endlink entity.
architecture Behavioral of aegis_mux_2_to_1 is

begin

--! Selection process which is triggered by all inputs.
select_proc: process (input_select, input_0, input_1)
begin
  if (input_select = '0') then
    output <= input_0;
  else
    output <= input_1;
  end if;
end process;   

end Behavioral;
