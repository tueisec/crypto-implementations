----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-10
--! @brief      The aegis_tag entity generates the tag.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_tag.vhd aegis_tag\endlink.
entity aegis_tag is    
    Port (         
        state_s0            : in  AEGIS_BLOCK;   --! Current state block number 0.
        state_s2            : in  AEGIS_BLOCK;   --! Current state block number 2.
        state_s3            : in  AEGIS_BLOCK;   --! Current state block number 3.
        state_xor           : in  AEGIS_BLOCK;   --! Depending which flavor is active, different inputs  (s. aegis_state_update) for more information.      
        tag                 : out AEGIS_BLOCK    --! Outputs the tag.
    );
end aegis_tag;

--! Architecture of \link aegis_tag.vhd aegis_tag\endlink entity.
architecture Behavioral of aegis_tag is
    signal s2_xor_s3            : AEGIS_BLOCK;              --! Intermediate result. 
    signal s0_xor_s2_xor_s3     : AEGIS_BLOCK;              --! Intermediate result. 
  
begin
   
    s2_xor_s3 <= xorBlock(state_s2, state_s3);
    s0_xor_s2_xor_s3 <= xorBlock(state_s0, s2_xor_s3);
    tag <= xorBlock(s0_xor_s2_xor_s3, state_xor);
    
end Behavioral;
