----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-07
--! @brief      The aegis_initialization_calculate calculates the initial state depending on the AEGIS flavor.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_initialization_calculate.vhd aegis_initialization_calculate\endlink.
entity aegis_initialization_calculate is
    Generic ( 
        G_DBLK_SIZE                               : integer;                                --! Data block size.
        G_KEY_SIZE                                : integer                                 --! Key block size.
    );
    Port ( 
        key                 : in  AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);  --! Current key to calculate initial state.
        iv                  : in  AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);  --! Current initialization vector to calculate initial state.
        state_init          : out AEGIS_STATE_VECTOR                                        --! The new initial state.
    );
end aegis_initialization_calculate;

--! Architecture of \link aegis_initialization_calculate.vhd aegis_initialization_calculate\endlink entity.
architecture Behavioral of aegis_initialization_calculate is

begin
    
    G_AEGIS128: if ACTIVE_AEGIS_FLAVOR = AEGIS128 generate        
        state_init(0) <= xorBlock(key(0), iv(0));
        state_init(1) <= toBlock(AEGIS_CONST1);
        state_init(2) <= toBlock(AEGIS_CONST0);
        state_init(3) <= xorBlock(toBlock(AEGIS_CONST0), key(0));
        state_init(4) <= xorBlock(toBlock(AEGIS_CONST1), key(0));
    end generate;
    
    G_AEGIS256: if ACTIVE_AEGIS_FLAVOR = AEGIS256 generate
        state_init(0) <= xorBlock(key(0), iv(0));
        state_init(1) <= xorBlock(key(1), iv(1));
        state_init(2) <= toBlock(AEGIS_CONST1);
        state_init(3) <= toBlock(AEGIS_CONST0);
        state_init(4) <= xorBlock(toBlock(AEGIS_CONST0), key(0));
        state_init(5) <= xorBlock(toBlock(AEGIS_CONST1), key(1));        
    end generate;   
   
end Behavioral;
