----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-09
--! @brief      Verification by comparision of bdi and bdo.
--! @details    Verification by comparision of bdi and bdo to create msg_auth signal. 
--!             The timing when this signal is valid is controlled by aegis_cipher_core_controller.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_verification.vhd aegis_verification\endlink.
entity aegis_verification is
    Port ( 
        bdo:        in AEGIS_BLOCK;         --! bdo value to be verified.
        bdi:        in AEGIS_BLOCK;         --! bdi value to be verified.
        msg_auth:   out std_logic           --! Result of a comparison between bdo and bdi.
  );
end aegis_verification;

--! Architecture of \link aegis_verification.vhd aegis_verification\endlink entity.
architecture Behavioral of aegis_verification is

begin
    
msg_auth <= '1' when bdi = bdo else '0';    -- Verification is simply a comparison between input and output.

end Behavioral;
