----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-08
--! @brief      Performs a full AES Round.
--! @details    For a detailed description of the AES Round operation see Wikipedia \cite aes.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aes_round.vhd aes_round\endlink.
entity aes_round is
    Port (            
        value_in : in AEGIS_BLOCK;                          --! A 128 bit signal on which a AES Round should be performed.
        key_in   : in AEGIS_BLOCK;                          --! 128 bit key value to be xor on to the output value.
        value_out : out AEGIS_BLOCK                         --! 128 bit value containing the result of the AES Round.
    );
end aes_round;

--! Architecture of \link aes_round.vhd aes_round\endlink entity.
architecture Behavioral of aes_round is
 
signal columns_shift_rows : AEGIS_BLOCK_QUARTER_VECTOR;     --! Containing the result of the Shift Rows operation.
signal columns_mc_sb_rows : AEGIS_BLOCK_QUARTER_VECTOR;     --! Containing the result of the Mix Columns and the Sub Bytes operation.    

begin

--! Shift Rows operation
aes_shift_rows_inst: entity work.aes_shift_rows
  port map ( 
        value_in => value_in,
        columns => columns_shift_rows    
    );

--! Mix Columns and Sub Bytes operation for four columns.
G_MC_SB: for I in 0 to 3 generate
    aes_mc_sb_inst: entity work.aes_mc_sb
        port map(
          column_in => columns_shift_rows(I),
          column_out => columns_mc_sb_rows(I)
        );
end generate;   

--! Add the 128 bit key
aes_addKey_inst : entity work.aes_add_key
     port map ( 
         columns => columns_mc_sb_rows,
         key_in => key_in,
         value_out => value_out);
         
end Behavioral;
