----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-08
--! @brief      aes_add_key performs the Add Key AES operation.
--! @details    For a detailed description of AES Add Key operation see Wikipedia \cite aes_ak.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aes_add_key.vhd aes_add_key\endlink.
entity aes_add_key is
    Port ( 
        columns         : in  AEGIS_BLOCK_QUARTER_VECTOR;       --! The 128 bit values returned by the aes_mc_sb entity.
        key_in          : in  AEGIS_BLOCK;                      --! The 128 bit key value which should be xored. 
        value_out       : out AEGIS_BLOCK                       --! The return value of the Add Key operation.
    );
end aes_add_key;

--! Architecture of \link aes_add_key.vhd aes_add_key\endlink entity.
architecture Behavioral of aes_add_key is    
    signal column_0 : AEGIS_BLOCK_QUARTER;                      --! Temporal 32 bit value. 
    signal column_1 : AEGIS_BLOCK_QUARTER;                      --! Temporal 32 bit value.     
    signal column_2 : AEGIS_BLOCK_QUARTER;                      --! Temporal 32 bit value.  
    signal column_3 : AEGIS_BLOCK_QUARTER;                      --! Temporal 32 bit value.  
begin
    
    -- Map Columns to temporal 32 bit values.
    column_0 <= columns(0);
    column_1 <= columns(1);
    column_2 <= columns(2);
    column_3 <= columns(3);     
    
    -- Generate the sixteen necessary xor commands.
    G_0_TO_3: for I in 0 to 3 generate
      value_out(I) <= xorByte(key_in(I), column_0(I-0));  
    end generate;
    
    G_4_TO_7: for I in 4 to 7 generate
      value_out(I) <= xorByte(key_in(I), column_1(I-4));  
    end generate;
    
    G_8_TO_11: for I in 8 to 11 generate
      value_out(I) <= xorByte(key_in(I), column_2(I-8));  
    end generate;
    
    G_12_TO_15: for I in 12 to 15 generate
      value_out(I) <= xorByte(key_in(I), column_3(I-12));  
    end generate;   

end Behavioral;
