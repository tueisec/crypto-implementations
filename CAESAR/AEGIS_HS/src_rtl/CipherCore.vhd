----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-09
--! @brief      The CipherCore is the top level modul of the AEGIS implementation.
--! @details    The CipherCore is the top level modul of the AEGIS implementation. It connects the aegis_cipher_core_controller to the aegis_cipher_core_data_path.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL; 
use work.aegis_package.all;
--! @endcond

--! Entity description of \link CipherCore.vhd CipherCore\endlink.
entity CipherCore is
    generic (
        -- Reset behavior
        G_ASYNC_RSTN    : boolean := False;                                        --! Async active low reset
        -- Block size (bits)
        G_DBLK_SIZE     : integer := DBLK_SIZE;                                    --! Data block size
        G_KEY_SIZE      : integer := KEY_SIZE;                                     --! Key block size
        G_TAG_SIZE      : integer := TAG_SIZE;                                     --! Tag block size
               
        G_LBS_BYTES     : integer := 4;                                            --! The number of bits required to hold block size expressed in bytes = log2_ceil(G_DBLK_SIZE/8)
        
        --G_MAX_LEN       : integer := SINGLE_PASS_MAX;
        G_MAX_LEN       : integer := 32;                                           --! Maximum supported AD/message/ciphertext length = 2^G_MAX_LEN-1
        -- Maximum supported AD/message/ciphertext length = 2^G_MAX_LEN-1
        -- G_MAX_LEN       : integer := SINGLE_PASS_MAX;
        -- Algorithm parameter to simulate run-time behavior
        -- Warning: Do not set any number higher than 32
        G_LAT_KEYINIT   : integer := 4;                                            --! Key inialization latency
        G_LAT_PREP      : integer := 12;                                           --! Pre-processing latency (init)
        G_LAT_PROC_AD   : integer := 10;                                           --! Processing latency (per block)
        G_LAT_PROC_DATA : integer := 16;                                           --! Processing latency (per block)
        G_LAT_POST      : integer := 20                                            --! Post-processing latency (tag)
    );
  Port (        
        -- Global
        clk             : in  std_logic;                                           --! Clock signal.
        rst             : in  std_logic;                                           --! Reset signal.
        -- PreProcessor (data)
        key             : in  std_logic_vector(G_KEY_SIZE       -1 downto 0);      --! Signal containing the SDI data.
        bdi             : in  std_logic_vector(G_DBLK_SIZE      -1 downto 0);      --! All public data is sent over this signal.
        -- PreProcessor (controls)
        key_ready       : out std_logic;                                           --! This signal indicates that the key has successfully processed.
        key_valid       : in  std_logic;                                           --! This signal is asserted when data on the key signal is valid. 
        key_update      : in  std_logic;                                           --! If key_update is asserted the data_path stores the new key.
        decrypt         : in  std_logic;                                           --! This signal indicates a decryption or a encryption.
        bdi_ready       : out std_logic;                                           --! This signal acknowledges the data assert on the bdi signal.
        bdi_valid       : in  std_logic;                                           --! This signal is asserted when data on bdi is valid. 
        bdi_type        : in  std_logic_vector(3                -1 downto 0);      --! bdi_type indicates which data is transmitted over the bdi signal. Valid bdi_types for AEGIS are defined in the type BDI_ENC which can be found in the aegis_package.
        bdi_partial     : in  std_logic;                                           --! Not used in this implementation.
        bdi_eot         : in  std_logic;                                           --! This signal indicates that the current bdi block is the last of the current type.
        bdi_eoi         : in  std_logic;                                           --! This signal indicates that the current bdi block is the last of the whole input.
        bdi_size        : in  std_logic_vector(G_LBS_BYTES+1    -1 downto 0);      --! This signal indicates how many bytes are valid in this block.
        bdi_valid_bytes : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);      --! Not used in this implementation.
        bdi_pad_loc     : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);      --! Not used in this implementation.
        -- PostProcessor
        bdo             : out std_logic_vector(G_DBLK_SIZE      -1 downto 0);      --! All public data which is sent to the PostProcessor
        bdo_valid       : out std_logic;                                           --! This signal indicates the validness of the current bdo block.
        bdo_ready       : in  std_logic;                                           --! This signal acknowledges the data assert on the bod signal.
        bdo_size        : out std_logic_vector(G_LBS_BYTES+1    -1 downto 0);      --! Indicates the number of valid bytes in the current bdo block.
        msg_auth        : out std_logic;                                           --! Indicates if authentication was valid.                                          
        msg_auth_valid  : out std_logic;                                           --! Strobe signal for msg_auth.
        msg_auth_ready  : in  std_logic                                            --! Acknowledge signal for msg_auth.
     );
end CipherCore;

--! Architecture of \link CipherCore.vhd CipherCore\endlink entity.
architecture Structure of CipherCore is

-- Signals between controller and data path.
signal bdo_size_padding             : integer range 0 to 16;                                        --! Size of the current output block. Data path pads the output according to this signal.                             
signal force_state_to_init          : boolean;                                                      --! Signal 'force_state_to_init_s' forces input muxes of the aegis_state_update to the input values of aegis_initialization.
signal select_m_cipher_or_plain     : M_CIPHER_OR_PLAIN;                                            --! Signal controls a mux in the aegis_cipher_core_data_path.
signal select_m                     : M_SELECT;                                                     --! Signal controls a mux in the aegis_cipher_core_data_path which choses the correct m for the aegis_state_update.
signal select_output                : SELECT_OUTPUT;                                                --! Specifies which signal shuld redirect to the bdo output signal.
signal next_state_valid             : boolean;                                                      --! State register update
signal enable_key                   : AEGIS_REG_STATE_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);  --! Enable signal of the key register in the aegis_initialization. 
signal enable_keyxoriv              : AEGIS_REG_STATE_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);  --! Enable signal of the keyxoriv register in the aegis_initialization.
signal counter_state                : AEGIS_COUNTER_STATE;                                          --! Signal indicates when to add the current bdi_size. Is used to calculate adlen and msglen in aegis_finalize.

begin

-- Data path instance
aegis_cipher_core_data_path_inst:  entity work.aegis_cipher_core_data_path
    generic map (
        G_DBLK_SIZE                 => G_DBLK_SIZE,
        G_KEY_SIZE                  => G_KEY_SIZE,
        G_LBS_BYTES                 => G_LBS_BYTES
    )
    port map ( 
        clk                         => clk,
        bdi                         => bdi,
        key                         => key,
        bdo                         => bdo,
        msg_auth                    => msg_auth,        
        bdi_size                    => bdi_size,
        bdo_size_padding            => bdo_size_padding,        
        key_update                  => key_update,
        force_state_to_init         => force_state_to_init,
        next_state_valid            => next_state_valid,
        enable_key                  => enable_key,
        enable_keyxoriv             => enable_keyxoriv,
        counter_state               => counter_state,
        select_m_cipher_or_plain    => select_m_cipher_or_plain,
        select_m                    => select_m,
        select_output               => select_output            
    );

-- Controller instance
aegis_cipher_core_controller_inst: entity work.aegis_cipher_core_controller    
    generic map (
        G_ASYNC_RSTN                => G_ASYNC_RSTN,
        G_DBLK_SIZE                 => G_DBLK_SIZE,
        G_KEY_SIZE                  => G_KEY_SIZE,
        G_LBS_BYTES                 => G_LBS_BYTES
    )
    port map (
        clk                         => clk,
        rst                         => rst,
        decrypt                     => decrypt,
        bdi_type                    => toBdiEnc(bdi_type),
        bdi_size                    => bdi_size,
        bdi_valid                   => bdi_valid,
        bdi_ready                   => bdi_ready,
        bdi_eot                     => bdi_eot,
        bdi_eoi                     => bdi_eoi,
        key_valid                   => key_valid,
        key_ready                   => key_ready,
        key_update                  => key_update,
        bdo_valid                   => bdo_valid,
        bdo_ready                   => bdo_ready,
        msg_auth_valid              => msg_auth_valid,
        msg_auth_ready              => msg_auth_ready,
        bdo_size                    => bdo_size,
        bdo_size_padding            => bdo_size_padding,
        force_state_to_init         => force_state_to_init,   
        next_state_valid            => next_state_valid,
        enable_key                  => enable_key,
        enable_keyxoriv             => enable_keyxoriv,   
        counter_state               => counter_state,
        select_m_cipher_or_plain    => select_m_cipher_or_plain,
        select_m                    => select_m,
        select_output               => select_output
    );
end structure;
