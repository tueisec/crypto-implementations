----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-10
--! @brief      The aegis_crypto entity generates the either the cipher or the plain text.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_crypto.vhd aegis_crypto\endlink.
entity aegis_crypto is
    port (
        state_s2            : in  AEGIS_BLOCK;      --! Current state block number 2.
        state_s3            : in  AEGIS_BLOCK;      --! Current state block number 3.
        state_xor           : in  AEGIS_BLOCK;      --! Depending which flavor is active, different inputs  (s. aegis_state_update) for more information.  
        cipherOrPlain_in    : in  AEGIS_BLOCK;      --! Current cipher or plain block.
        cipherOrPlain_out   : out AEGIS_BLOCK       --! Current cipher or plain block.
    );
end aegis_crypto;

--! Architecture of \link aegis_crypto.vhd aegis_crypto\endlink entity.
architecture Behavioral of aegis_crypto is
    signal s2_and_s3                    : AEGIS_BLOCK;       --! Intermediate result. 
    signal s2_and_s3_xor_cipherOrPlain  : AEGIS_BLOCK;       --! Intermediate result. 
begin
    
    s2_and_s3 <= andBlock(state_s2, state_s3);
    s2_and_s3_xor_cipherOrPlain <= xorBlock(s2_and_s3, cipherOrPlain_in);
    cipherOrPlain_out <= xorBlock(s2_and_s3_xor_cipherOrPlain, state_xor);

end Behavioral;
