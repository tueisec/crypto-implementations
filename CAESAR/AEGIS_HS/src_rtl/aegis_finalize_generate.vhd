----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-10
--! @brief      The aegis_finalize_generate entity counts adlen, msglen and generates m_finalize.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
use IEEE.NUMERIC_STD.all;
--! @endcond

--! Entity description of \link aegis_finalize_generate.vhd aegis_finalize_generate\endlink.
entity aegis_finalize_generate is
    Generic(
        G_LBS_BYTES                 : integer                                           --! The number of bits required to hold block size expressed in bytes = log2_ceil(G_DBLK_SIZE/8)
    );
    Port ( 
        clk                         : in  std_logic;                                    --! Clock signal.
        counter_state               : in  AEGIS_COUNTER_STATE;                          --! Controller signal to indicate when to count bdi_size.
        bdi_size                    : in  std_logic_vector(G_LBS_BYTES-1+1 downto 0);   --! Top level signal indicating the size of the current bdi block.
        state_s3_next               : in  AEGIS_BLOCK;                                  --! Used to generate m_finalize.
        m_finalize_fast_forward     : out AEGIS_BLOCK;                                  --! m_finalize output.
        fast_forward_select         : out std_logic;                                    --! Indicates when use m_finalize_fast_forward is valid.
        enable_finalize_reg         : out AEGIS_REG_STATE                               --! Indicates when to store m_finalize_fast_forward in aegis_finalize.
    );
end aegis_finalize_generate;

--! Architecture of \link aegis_finalize_generate.vhd aegis_finalize_generate\endlink entity.
architecture Behavioral of aegis_finalize_generate is
    signal msg_counter_in                     : unsigned(61 downto 0);                      --! Internal msglen input used to easily calculate.
    signal msg_counter_out                    : unsigned(61 downto 0);                      --! Internal msglen output used to easily calculate.
    signal ad_counter_out                     : unsigned(61 downto 0);                      --! Internal adlen output used to easily calculate. 
    signal ad_counter_in                      : unsigned(61 downto 0);                      --! Internal adlen input used to easily calculate. 
    signal bdi_size_int                       : unsigned(G_LBS_BYTES-1+2 downto 0);         --! used to shift bdi_size by 3 (same as multiplication with 16) 

    signal counter_out                        : unsigned(127 downto 0) := (others => '0');  --! Counter output.
    signal counter_in                         : unsigned(127 downto 0) := (others => '0');  --! Counter using adlen and msglen as input.
    signal fast_forward_select_internal       : std_logic;                                  --! Internal signal.
begin

--! Simple counter update.
counter_proc: process(clk)
begin
    if (rising_edge(clk)) then  
        counter_out <= counter_in;
    end if;
end process;    

fast_forward_select <= fast_forward_select_internal;

fast_forward_select_internal <= '1' when
    counter_state = CIPHER_OR_PLAIN_COUNT or
    counter_state = AD_COUNT or
    counter_state = NOTHING_TO_COUNT else '0';
    
enable_finalize_reg <= ENABLE when fast_forward_select_internal = '1' else DISABLE;

-- bdi_size shifted by one bit to the left. Since adlen and msglen is a bit count and not a byte count.
bdi_size_int <= unsigned(bdi_size) & "0";

-- Msglen counter with integrated shift
counter_in(127 downto 66) <= msg_counter_in;
msg_counter_out <= counter_out(127 downto 66);
msg_counter_in <= 
    msg_counter_out + bdi_size_int  when counter_state = CIPHER_OR_PLAIN_COUNT else
    (others => '0') when counter_state = NOTHING_TO_COUNT or counter_state = RESET else
    msg_counter_out;      

-- Adlen counter with integrated shift
counter_in(63 downto 2) <= ad_counter_in;
ad_counter_out <= counter_out(63 downto 2);
ad_counter_in <= 
    ad_counter_out + bdi_size_int when counter_state = AD_COUNT else
    (others => '0') when  counter_state = NOTHING_TO_COUNT or counter_state = RESET else 
    ad_counter_out; 
                  
-- Generate m_finalize.                
m_finalize_fast_forward <= xorBlock(toBlock(std_logic_vector(counter_in)), state_s3_next); -- immediate transmission of m_finalize calculated from state_next(3) 

end Behavioral;
