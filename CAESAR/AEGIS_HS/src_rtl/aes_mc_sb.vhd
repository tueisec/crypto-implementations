----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-08
--! @brief      aes_mc_sb combines the Mix Column \cite aes_mc and the Sub Bytes \cite aes_sb AES operation.
--! @details    aes_mc_sb connects the aes_mult2_w_sbox, aes_mult3_w_sbox and aes_sbox entity with the aes_mc entity.
--!             mult2, mult3 and sbox operations are performed using a lookup table.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aes_mc_sb.vhd aes_mc_sb\endlink.
entity aes_mc_sb is
    Port (         
        column_in  : in  AEGIS_BLOCK_QUARTER;       --! Signal contains the column on which the Mix Column and the Sub Bytes operation should be performed.
        column_out : out AEGIS_BLOCK_QUARTER        --! Result of the performed operations.
    );
end aes_mc_sb;

--! Architecture of \link aes_mc_sb.vhd aes_mc_sb\endlink entity.
architecture Behavioral of aes_mc_sb is        

    signal mult2    : AEGIS_BLOCK_QUARTER;          --! Contains all values of the received column with performed sbox lookup and multplied by 2 (1).
    signal mult3    : AEGIS_BLOCK_QUARTER;          --! Contains all values of the received column with performed sbox lookup and multplied by 3 (x+1).
    signal sbox     : AEGIS_BLOCK_QUARTER;          --! Contains all values exchanged by a value of the sbox.
    
begin
    
    -- Generate for each byte the necessary AES operations.
    G_SBOX_MULT: for I in 0 to 3 generate
    
        sbox_inst: entity work.aes_sbox
            port map (
                value_in  => column_in(I),
                value_out => sbox(I));
                
        mult2_inst: entity work.aes_mult2_w_sbox
            port map (
                value_in  => column_in(I),
                value_out => mult2(I));
                
        mult3_inst: entity work.aes_mult3_w_sbox
            port map (
                value_in  => column_in(I),
                value_out => mult3(I));
                 
    end generate;
    
    -- Combine the precalculated values to generate the output.
    aes_mc_inst: entity work.aes_mc
        port map (
            mult2     => mult2,
            mult3     => mult3,
            sbox      => sbox,
            value_out => column_out);
            
end Behavioral;
