----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-08
--! @brief      Operation mult3[sbox[value_in]].
--! @details    aes_mult3_w_sbox conducts the operation mult3[sbox[value_in]] in aes_mc_sb. Since
--!             this is a high speed implementation a lookup table is used.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aes_mult3_w_sbox.vhd aes_mult3_w_sbox\endlink.
entity aes_mult3_w_sbox is
    Port ( 
        value_in  : in  AEGIS_BYTE;     --! Input value.
        value_out : out AEGIS_BYTE      --! Ouput is a return value of the lookup table which represents mult3[sbox[value_in]]
    );
end aes_mult3_w_sbox;

--! Architecture of \link aes_mult3_w_sbox.vhd aes_mult3_w_sbox\endlink entity.
architecture Behavioral of aes_mult3_w_sbox is

--! Lookup Table which represents the operation mult3[sbox[value_in]]
constant mult3_with_sbox_lookup : AES_LOOKUP_TABLE := (
  0 => x"A5",   1 => x"84",   2 => x"99",   3 => x"8D",   4 => x"0D",   5 => x"BD",   6 => x"B1",   7 => x"54",   8 => x"50",   9 => x"03",  10 => x"A9",  11 => x"7D",  12 => x"19",  13 => x"62",  14 => x"E6",  15 => x"9A",
 16 => x"45",  17 => x"9D",  18 => x"40",  19 => x"87",  20 => x"15",  21 => x"EB",  22 => x"C9",  23 => x"0B",  24 => x"EC",  25 => x"67",  26 => x"FD",  27 => x"EA",  28 => x"BF",  29 => x"F7",  30 => x"96",  31 => x"5B",
 32 => x"C2",  33 => x"1C",  34 => x"AE",  35 => x"6A",  36 => x"5A",  37 => x"41",  38 => x"02",  39 => x"4F",  40 => x"5C",  41 => x"F4",  42 => x"34",  43 => x"08",  44 => x"93",  45 => x"73",  46 => x"53",  47 => x"3F",
 48 => x"0C",  49 => x"52",  50 => x"65",  51 => x"5E",  52 => x"28",  53 => x"A1",  54 => x"0F",  55 => x"B5",  56 => x"09",  57 => x"36",  58 => x"9B",  59 => x"3D",  60 => x"26",  61 => x"69",  62 => x"CD",  63 => x"9F",
 64 => x"1B",  65 => x"9E",  66 => x"74",  67 => x"2E",  68 => x"2D",  69 => x"B2",  70 => x"EE",  71 => x"FB",  72 => x"F6",  73 => x"4D",  74 => x"61",  75 => x"CE",  76 => x"7B",  77 => x"3E",  78 => x"71",  79 => x"97",
 80 => x"F5",  81 => x"68",  82 => x"00",  83 => x"2C",  84 => x"60",  85 => x"1F",  86 => x"C8",  87 => x"ED",  88 => x"BE",  89 => x"46",  90 => x"D9",  91 => x"4B",  92 => x"DE",  93 => x"D4",  94 => x"E8",  95 => x"4A",
 96 => x"6B",  97 => x"2A",  98 => x"E5",  99 => x"16", 100 => x"C5", 101 => x"D7", 102 => x"55", 103 => x"94", 104 => x"CF", 105 => x"10", 106 => x"06", 107 => x"81", 108 => x"F0", 109 => x"44", 110 => x"BA", 111 => x"E3",
112 => x"F3", 113 => x"FE", 114 => x"C0", 115 => x"8A", 116 => x"AD", 117 => x"BC", 118 => x"48", 119 => x"04", 120 => x"DF", 121 => x"C1", 122 => x"75", 123 => x"63", 124 => x"30", 125 => x"1A", 126 => x"0E", 127 => x"6D",
128 => x"4C", 129 => x"14", 130 => x"35", 131 => x"2F", 132 => x"E1", 133 => x"A2", 134 => x"CC", 135 => x"39", 136 => x"57", 137 => x"F2", 138 => x"82", 139 => x"47", 140 => x"AC", 141 => x"E7", 142 => x"2B", 143 => x"95",
144 => x"A0", 145 => x"98", 146 => x"D1", 147 => x"7F", 148 => x"66", 149 => x"7E", 150 => x"AB", 151 => x"83", 152 => x"CA", 153 => x"29", 154 => x"D3", 155 => x"3C", 156 => x"79", 157 => x"E2", 158 => x"1D", 159 => x"76",
160 => x"3B", 161 => x"56", 162 => x"4E", 163 => x"1E", 164 => x"DB", 165 => x"0A", 166 => x"6C", 167 => x"E4", 168 => x"5D", 169 => x"6E", 170 => x"EF", 171 => x"A6", 172 => x"A8", 173 => x"A4", 174 => x"37", 175 => x"8B",
176 => x"32", 177 => x"43", 178 => x"59", 179 => x"B7", 180 => x"8C", 181 => x"64", 182 => x"D2", 183 => x"E0", 184 => x"B4", 185 => x"FA", 186 => x"07", 187 => x"25", 188 => x"AF", 189 => x"8E", 190 => x"E9", 191 => x"18",
192 => x"D5", 193 => x"88", 194 => x"6F", 195 => x"72", 196 => x"24", 197 => x"F1", 198 => x"C7", 199 => x"51", 200 => x"23", 201 => x"7C", 202 => x"9C", 203 => x"21", 204 => x"DD", 205 => x"DC", 206 => x"86", 207 => x"85",
208 => x"90", 209 => x"42", 210 => x"C4", 211 => x"AA", 212 => x"D8", 213 => x"05", 214 => x"01", 215 => x"12", 216 => x"A3", 217 => x"5F", 218 => x"F9", 219 => x"D0", 220 => x"91", 221 => x"58", 222 => x"27", 223 => x"B9",
224 => x"38", 225 => x"13", 226 => x"B3", 227 => x"33", 228 => x"BB", 229 => x"70", 230 => x"89", 231 => x"A7", 232 => x"B6", 233 => x"22", 234 => x"92", 235 => x"20", 236 => x"49", 237 => x"FF", 238 => x"78", 239 => x"7A",
240 => x"8F", 241 => x"F8", 242 => x"80", 243 => x"17", 244 => x"DA", 245 => x"31", 246 => x"C6", 247 => x"B8", 248 => x"C3", 249 => x"B0", 250 => x"77", 251 => x"11", 252 => x"CB", 253 => x"FC", 254 => x"D6", 255 => x"3A");

begin

value_out <= mult3_with_sbox_lookup(to_integer(unsigned(value_in))); -- Table lookup.

end Behavioral;
