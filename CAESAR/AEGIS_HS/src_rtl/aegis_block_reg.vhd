----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-07
--! @brief      D Flip Flop which stores the AEGIS_BLOCK.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_block_reg.vhd aegis_block_reg\endlink.
entity aegis_block_reg is
    Port (
        clk : in  STD_LOGIC;           --! Clock signal.
        en  : in  AEGIS_REG_STATE;     --! Enabel signal.
        D   : in  AEGIS_BLOCK;         --! Input signal.
        Q   : out AEGIS_BLOCK          --! Output signal.
    );
end aegis_block_reg;

--! Architecture of \link aegis_block_reg.vhd aegis_block_reg\endlink entity.
architecture Behavioral of aegis_block_reg is  
begin  
 
 --! The update_proc process updates the output whenever rising edge of the clk signal and the enable
 --! signal is true at the same time.
update_proc: process (clk, en)
begin
  if rising_edge(clk) then
    if en = ENABLE then 
        Q <= D;
    end if;
  end if;  
end process;  

end Behavioral;