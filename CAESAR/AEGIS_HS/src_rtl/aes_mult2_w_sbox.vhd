----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-08
--! @brief      Operation mult2[sbox[value_in]].
--! @details    aes_mult2_w_sbox conducts the operation mult2[sbox[value_in]] in the aes_mc_sb entity. Since
--!             this is a high speed implementation a lookup table is used.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
use ieee.numeric_std.all;
--! @endcond

--! Entity description of \link aes_mult2_w_sbox.vhd aes_mult2_w_sbox\endlink.
entity aes_mult2_w_sbox is
    Port ( 
        value_in  : in  AEGIS_BYTE;   --! Input value.
        value_out : out AEGIS_BYTE    --! Ouput is a return value of the lookup table which represents mult2[sbox[value_in]]
    );
end aes_mult2_w_sbox;

--! Architecture of \link aes_mult2_w_sbox.vhd aes_mult2_w_sbox\endlink entity.
architecture Behavioral of aes_mult2_w_sbox is

--! Lookup Table which represents the operation mult2[sbox[value_in]]
constant mult2_with_sbox_lookup : AES_LOOKUP_TABLE := (
  0 => x"C6",   1 => x"F8",   2 => x"EE",   3 => x"F6",   4 => x"FF",   5 => x"D6",   6 => x"DE",   7 => x"91",   8 => x"60",   9 => x"02",  10 => x"CE",  11 => x"56",  12 => x"E7",  13 => x"B5",  14 => x"4D",  15 => x"EC",
 16 => x"8F",  17 => x"1F",  18 => x"89",  19 => x"FA",  20 => x"EF",  21 => x"B2",  22 => x"8E",  23 => x"FB",  24 => x"41",  25 => x"B3",  26 => x"5F",  27 => x"45",  28 => x"23",  29 => x"53",  30 => x"E4",  31 => x"9B",
 32 => x"75",  33 => x"E1",  34 => x"3D",  35 => x"4C",  36 => x"6C",  37 => x"7E",  38 => x"F5",  39 => x"83",  40 => x"68",  41 => x"51",  42 => x"D1",  43 => x"F9",  44 => x"E2",  45 => x"AB",  46 => x"62",  47 => x"2A",
 48 => x"08",  49 => x"95",  50 => x"46",  51 => x"9D",  52 => x"30",  53 => x"37",  54 => x"0A",  55 => x"2F",  56 => x"0E",  57 => x"24",  58 => x"1B",  59 => x"DF",  60 => x"CD",  61 => x"4E",  62 => x"7F",  63 => x"EA",
 64 => x"12",  65 => x"1D",  66 => x"58",  67 => x"34",  68 => x"36",  69 => x"DC",  70 => x"B4",  71 => x"5B",  72 => x"A4",  73 => x"76",  74 => x"B7",  75 => x"7D",  76 => x"52",  77 => x"DD",  78 => x"5E",  79 => x"13",
 80 => x"A6",  81 => x"B9",  82 => x"00",  83 => x"C1",  84 => x"40",  85 => x"E3",  86 => x"79",  87 => x"B6",  88 => x"D4",  89 => x"8D",  90 => x"67",  91 => x"72",  92 => x"94",  93 => x"98",  94 => x"B0",  95 => x"85",
 96 => x"BB",  97 => x"C5",  98 => x"4F",  99 => x"ED", 100 => x"86", 101 => x"9A", 102 => x"66", 103 => x"11", 104 => x"8A", 105 => x"E9", 106 => x"04", 107 => x"FE", 108 => x"A0", 109 => x"78", 110 => x"25", 111 => x"4B",
112 => x"A2", 113 => x"5D", 114 => x"80", 115 => x"05", 116 => x"3F", 117 => x"21", 118 => x"70", 119 => x"F1", 120 => x"63", 121 => x"77", 122 => x"AF", 123 => x"42", 124 => x"20", 125 => x"E5", 126 => x"FD", 127 => x"BF",
128 => x"81", 129 => x"18", 130 => x"26", 131 => x"C3", 132 => x"BE", 133 => x"35", 134 => x"88", 135 => x"2E", 136 => x"93", 137 => x"55", 138 => x"FC", 139 => x"7A", 140 => x"C8", 141 => x"BA", 142 => x"32", 143 => x"E6",
144 => x"C0", 145 => x"19", 146 => x"9E", 147 => x"A3", 148 => x"44", 149 => x"54", 150 => x"3B", 151 => x"0B", 152 => x"8C", 153 => x"C7", 154 => x"6B", 155 => x"28", 156 => x"A7", 157 => x"BC", 158 => x"16", 159 => x"AD",
160 => x"DB", 161 => x"64", 162 => x"74", 163 => x"14", 164 => x"92", 165 => x"0C", 166 => x"48", 167 => x"B8", 168 => x"9F", 169 => x"BD", 170 => x"43", 171 => x"C4", 172 => x"39", 173 => x"31", 174 => x"D3", 175 => x"F2",
176 => x"D5", 177 => x"8B", 178 => x"6E", 179 => x"DA", 180 => x"01", 181 => x"B1", 182 => x"9C", 183 => x"49", 184 => x"D8", 185 => x"AC", 186 => x"F3", 187 => x"CF", 188 => x"CA", 189 => x"F4", 190 => x"47", 191 => x"10",
192 => x"6F", 193 => x"F0", 194 => x"4A", 195 => x"5C", 196 => x"38", 197 => x"57", 198 => x"73", 199 => x"97", 200 => x"CB", 201 => x"A1", 202 => x"E8", 203 => x"3E", 204 => x"96", 205 => x"61", 206 => x"0D", 207 => x"0F",
208 => x"E0", 209 => x"7C", 210 => x"71", 211 => x"CC", 212 => x"90", 213 => x"06", 214 => x"F7", 215 => x"1C", 216 => x"C2", 217 => x"6A", 218 => x"AE", 219 => x"69", 220 => x"17", 221 => x"99", 222 => x"3A", 223 => x"27",
224 => x"D9", 225 => x"EB", 226 => x"2B", 227 => x"22", 228 => x"D2", 229 => x"A9", 230 => x"07", 231 => x"33", 232 => x"2D", 233 => x"3C", 234 => x"15", 235 => x"C9", 236 => x"87", 237 => x"AA", 238 => x"50", 239 => x"A5",
240 => x"03", 241 => x"59", 242 => x"09", 243 => x"1A", 244 => x"65", 245 => x"D7", 246 => x"84", 247 => x"D0", 248 => x"82", 249 => x"29", 250 => x"5A", 251 => x"1E", 252 => x"7B", 253 => x"A8", 254 => x"6D", 255 => x"2C");

begin

value_out <= mult2_with_sbox_lookup(to_integer(unsigned(value_in)));   -- Table lookup.

end Behavioral;
