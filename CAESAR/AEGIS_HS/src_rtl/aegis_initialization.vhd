----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-07
--! @brief      The aegis_initialization calculates the initial state.
--! @details    The aegis_initialization takes a initialization vector and the key calculates the 
--!             initial state and takes care about m_init through out the initialization.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aegis_initialization.vhd aegis_initialization\endlink.
entity aegis_initialization is 
    Generic ( 
        G_DBLK_SIZE                               : integer;                                                            --! Data block size.
        G_KEY_SIZE                                : integer                                                             --! Key block size.
    );
    Port ( 
        clk                                       : in STD_LOGIC;                                                       --! Clock signal.
        key                                       : in AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);         --! The current key. For AEGIS256 this signal contains two times 128 bit.
        key_update                                : in STD_LOGIC;                                                       --! If the signal is asserted key is used to calculate initial state, otherwise stored key of run before.
        enable_key                                : in AEGIS_REG_STATE_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);     --! Signal controls the key register which stores the current key.
        iv                                        : in AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);         --! The current initialization vector. For AEGIS256 this signal contains two times 128 bit.
        enable_keyxoriv                           : in AEGIS_REG_STATE_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);     --! Signal controls the keyxoriv register. The value is used through out the initialization to generate m.
        force_state_to_init                       : in boolean;                                                         --! Signal is used in this entity to activate the muxes which are controlling m.
        m_init                                    : out AEGIS_BLOCK;                                                    --! Signal m to update the state.
        state_init                                : out AEGIS_STATE_VECTOR                                              --! The initialization state calculated by aegis_initialization_calculate.
    );
end aegis_initialization;

--! Architecture of \link aegis_initialization.vhd aegis_initialization\endlink entity.
architecture Behavioral of aegis_initialization is   
      
    signal state_init_internal                  : AEGIS_STATE_VECTOR;                                                        --! The initialization state calculated by aegis_initialization_calculate.
    signal input_select_internal                : std_logic_vector((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0) := (others => '0');  --! The signal controls in case of AEGIS256 two muxes in case of AEGIS128 one mux. These muxes output the correct m value for the state update.
    signal key_reg_out                          : AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);                   --! Internal signal which represents the output of the key register.
    signal key_to_calculate                     : AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);                   --! Internal signal which eighter is equal to key_reg_out or key depending on key_update.
    signal key_xor_iv_reg_out                   : AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);                   --! Internal signal which contains the output of the key_xor_iv register.
    signal m_internal                           : AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);                   --! Internal signal which is used while controlling the output of m_init.
    signal iv_internal                          : AEGIS_BLOCK_VECTOR((G_KEY_SIZE/G_DBLK_SIZE)-1 downto 0);                   --! Signal contains the initialization vector. 
    
begin

    state_init <= state_init_internal;
    
    --! Calculate the initialization state.
    aegis_initialization_calculate_inst: entity work.aegis_initialization_calculate
        generic map(
            G_DBLK_SIZE => G_DBLK_SIZE,
            G_KEY_SIZE => G_KEY_SIZE
        )
        port map ( 
            key => key_to_calculate,
            iv => iv_internal,
            state_init => state_init_internal
        );    
        
    G_AEGIS128: 
    if ACTIVE_AEGIS_FLAVOR = AEGIS128 generate
    
        --! This process osscilates select_m_mux_0 to control m_init.
        select_proc_128: process(clk)
        begin
            if rising_edge(clk) then
                if force_state_to_init = false then
                    input_select_internal(0) <= not input_select_internal(0);
                else
                    input_select_internal(0) <= '0';
                end if;
            end if;
        end process;
      
        m_init <= m_internal(0); 
        iv_internal <= iv;  
    end generate G_AEGIS128;
     
    G_AEGIS256: 
    if ACTIVE_AEGIS_FLAVOR = AEGIS256 generate
        
        --! This process osscilates select_m_mux_0 and select_m_mux_1 to control m_init.
        select_proc_256: process(clk)
        begin
            if rising_edge(clk) then
                if force_state_to_init = false then
                    if input_select_internal(1) = '1' then
                        input_select_internal(0) <= not input_select_internal(0);
                    end if;
                    
                    input_select_internal(1) <= not input_select_internal(1);
                     
                 else
                    input_select_internal <= (others => '0');
                end if;
            end if;
        end process;
      
        --! select_m_mux_1 is used to generate correct m_init.
        select_m_mux_1: entity work.aegis_mux_2_to_1 
            port map (
                input_0      => m_internal(0),
                input_1      => m_internal(1),
                input_select => input_select_internal(1),
                output       => m_init
            );
        
        --! This register stores the first 128 bit of the initialization vector becaus bdi
        --! is only 128 bit and 256 bit have to be used to calculate state_init.   
        aegis_block_reg_iv_inst: entity work.aegis_block_reg
            port map (
                clk => clk,
                en  => enable_keyxoriv(0), 
                D   => iv(0),
                Q   => iv_internal(0));       
        
        iv_internal(1) <= iv(1);        
    end generate G_AEGIS256;
      


    G_INITIALIZE: 
    for I in 0 to (G_KEY_SIZE/G_DBLK_SIZE)-1 generate
      
        --! This mux controls the usage of the correct key to calculate the initialization state.
        --! Either the current key or the stored key.
        aegis_mux_2_to_1_key_update_inst: entity work.aegis_mux_2_to_1 
            port map (
                input_0      => key_reg_out(I),
                input_1      => key(I),
                input_select => key_update,
                output       => key_to_calculate(I)
            );
     
        --! select_m_mux_0 is used to generate correct m_init.
        select_m_mux_0 : entity work.aegis_mux_2_to_1 
            port map (
                input_0      => key_reg_out(I),
                input_1      => key_xor_iv_reg_out(I),
                input_select => input_select_internal(0), 
                output       => m_internal(I)
            );
       
        --! This register stores the key.
        aegis_block_reg_key_inst: entity work.aegis_block_reg      
            port map (
                clk => clk,
                en  => enable_key(I), 
                D   => key(I),
                Q   => key_reg_out(I)
            );
        
        --! This register stores the key xored with the iv.
        aegis_block_reg_keyxoriv_inst: entity work.aegis_block_reg
            port map (
                clk => clk,
                en  => enable_keyxoriv(I), 
                D   => state_init_internal(I),
                Q   => key_xor_iv_reg_out(I)
            );
                
    end generate;
    
end Behavioral;