----------------------------------------------------
--! @file
--! @author     Emanuele Strieder
--! @date       2018-06-08
--! @brief      Performs the Mix Column AES operation.
--! @details    For a detailed description of AES Mix Column operation see Wikipedia \cite aes_mc. The multiplication are delivered by aes_mc_sb.
--! @copyright  Copyright © 2018 Emanuele Strieder
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
---------------------------------------------------- 

--! @cond Doxygen_Suppress
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aegis_package.all;
--! @endcond

--! Entity description of \link aes_mc.vhd aes_mc\endlink.
entity aes_mc is
    port (
        mult2     : in  AEGIS_BLOCK_QUARTER;    --! Precalculated mult2 values delivered by aes_mc_sb.
        mult3     : in  AEGIS_BLOCK_QUARTER;    --! Precalculated mult3 values delivered by aes_mc_sb.
        sbox      : in  AEGIS_BLOCK_QUARTER;    --! Precalculated sbox values delivered by aes_mc_sb.
        value_out : out AEGIS_BLOCK_QUARTER     --! The signal contains a 32 bit AES column with performed Mix Column operation.
    );
end aes_mc;

--! Architecture of \link aes_mc.vhd aes_mc\endlink entity.
architecture Behavioral of aes_mc is

begin
 
  -- Use precalculated values to generate output for the aes_mc_sb entity.
  value_out(0) <=  xorByte(mult2(0), xorByte(mult3(1), xorByte(sbox(2), sbox(3))));
  value_out(1) <=  xorByte(mult2(1), xorByte(mult3(2), xorByte(sbox(0), sbox(3))));
  value_out(2) <=  xorByte(mult2(2), xorByte(mult3(3), xorByte(sbox(0), sbox(1))));
  value_out(3) <=  xorByte(mult2(3), xorByte(mult3(0), xorByte(sbox(2), sbox(1))));
  
end Behavioral;
