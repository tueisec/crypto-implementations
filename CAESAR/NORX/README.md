Norx3241_HS:
	
	- wordsize: 32 bit
	- round number: 4
	- parallelisation: 1
	- tag size: 128 bit
	
	- full F permutation as core
	- standard API
	
Norx3241_LW8:

	- wordsize: 32 bit
	- round number: 4
	- parallelisation: 1
	- tag size: 128 bit
	
	- iterative, only part of G, 8 bit operations
	- lightweight API
	
Norx3241_LW32:

	- wordsize: 32 bit
	- round number: 4
	- parallelisation: 1
	- tag size: 128 bit
	
	- iterative, G as core, 32 bit operations
	- lightweight API
	

Norx version 3.0
https://norx.io/data/norx.pdf