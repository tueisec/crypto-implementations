###############################################################################
# pdi.txt
# This file was auto-generated by aeadtvgen v2.0.0
###############################################################################
# Parameter:
#
# add_partial            - False
# block_size             - 384
# block_size_ad          - 384
# cc_hls                 - False
# cc_pad_ad              - 0
# cc_pad_d               - 0
# cc_pad_enable          - False
# cc_pad_style           - 1
# ciph_exp               - False
# ciph_exp_noext         - False
# gen_custom_mode        - 0
# io (W,SW)              - [128, 32]
# key_size               - 128
# lib_name               - norx3241v3
# max_ad                 - 200
# max_block_per_sgmt     - 9999
# max_d                  - 200
# max_io_per_line        - 8
# min_ad                 - 0
# min_d                  - 0
# msg_format             - ['npub', 'ad', 'data', 'tag']
# npub_size              - 128
# nsec_size              - 0
# offline                - False
# reverse_ciph           - False
# tag_size               - 128
###############################################################################

#### Authenticated Encryption
#### MsgID=  1, KeyID=  1 Ad Size =    0, Pt Size =    0
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=1 EOT=1, Last=0, Length=16 bytes
HDR = D6000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=0 bytes
HDR = 12000000000000000000000000000000
# Info :                Plaintext, EOI=0 EOT=1, Last=1, Length=0 bytes
HDR = 43000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  2, KeyID=  1 Ad Size =    0, Ct Size =    0
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=1 EOT=1, Last=0, Length=16 bytes
HDR = D6000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=0 bytes
HDR = 12000000000000000000000000000000
# Info :               Ciphertext, EOI=0 EOT=1, Last=0, Length=0 bytes
HDR = 52000000000000000000000000000000
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = 703063636AA81DFE2193D1479E5ABB68

#### Authenticated Encryption
#### MsgID=  3, KeyID=  2 Ad Size =    1, Pt Size =    0
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=1 EOT=1, Last=0, Length=1 bytes
HDR = 16000001000000000000000000000000
DAT = A0000000000000000000000000000000
# Info :                Plaintext, EOI=0 EOT=1, Last=1, Length=0 bytes
HDR = 43000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  4, KeyID=  2 Ad Size =    1, Ct Size =    0
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=1 EOT=1, Last=0, Length=1 bytes
HDR = 16000001000000000000000000000000
DAT = A0000000000000000000000000000000
# Info :               Ciphertext, EOI=0 EOT=1, Last=0, Length=0 bytes
HDR = 52000000000000000000000000000000
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = D6A1F2D1877E1994D5C10DD94AA5362A

#### Authenticated Encryption
#### MsgID=  5, KeyID=  3 Ad Size =    0, Pt Size =    1
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=0 bytes
HDR = 12000000000000000000000000000000
# Info :                Plaintext, EOI=1 EOT=1, Last=1, Length=1 bytes
HDR = 47000001000000000000000000000000
DAT = FF000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  6, KeyID=  3 Ad Size =    0, Ct Size =    1
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=0 bytes
HDR = 12000000000000000000000000000000
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=1 bytes
HDR = 56000001000000000000000000000000
DAT = 8E000000000000000000000000000000
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = CC498EC893E1839E8D064E32D8F7DD0D

#### Authenticated Encryption
#### MsgID=  7, KeyID=  4 Ad Size =    1, Pt Size =    1
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=1 bytes
HDR = 12000001000000000000000000000000
DAT = A0000000000000000000000000000000
# Info :                Plaintext, EOI=1 EOT=1, Last=1, Length=1 bytes
HDR = 47000001000000000000000000000000
DAT = FF000000000000000000000000000000

#### Authenticated Decryption
#### MsgID=  8, KeyID=  4 Ad Size =    1, Ct Size =    1
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=1 bytes
HDR = 12000001000000000000000000000000
DAT = A0000000000000000000000000000000
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=1 bytes
HDR = 56000001000000000000000000000000
DAT = EE000000000000000000000000000000
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = 8955A68570C613285E173D919C85B8A7

#### Authenticated Encryption
#### MsgID=  9, KeyID=  5 Ad Size =   48, Pt Size =   48
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=48 bytes
HDR = 12000030000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECF
# Info :                Plaintext, EOI=1 EOT=1, Last=1, Length=48 bytes
HDR = 47000030000000000000000000000000
DAT = FF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E

#### Authenticated Decryption
#### MsgID= 10, KeyID=  5 Ad Size =   48, Ct Size =   48
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=48 bytes
HDR = 12000030000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECF
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=48 bytes
HDR = 56000030000000000000000000000000
DAT = 3DB8C4811E99FA9101F489C871A032DA0B721E82160F73F044C9F928272980C82A5C756AF0F938B8288C0EB6B47B20E4
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = 6B82864ABFFBB145896071D202435494

#### Authenticated Encryption
#### MsgID= 11, KeyID=  6 Ad Size =   47, Pt Size =   47
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=47 bytes
HDR = 1200002F000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCE00
# Info :                Plaintext, EOI=1 EOT=1, Last=1, Length=47 bytes
HDR = 4700002F000000000000000000000000
DAT = FF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D00

#### Authenticated Decryption
#### MsgID= 12, KeyID=  6 Ad Size =   47, Ct Size =   47
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=47 bytes
HDR = 1200002F000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCE00
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=47 bytes
HDR = 5600002F000000000000000000000000
DAT = 0AD508DABC57C0E7FA8DBA72A60B072680760F335877F188FCA83D41E90A74F74FAF6FF772AA7E3373FA5DCB8B446100
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = BD0C11656150679EEFC3D328BF0127B8

#### Authenticated Encryption
#### MsgID= 13, KeyID=  7 Ad Size =   49, Pt Size =   49
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=49 bytes
HDR = 12000031000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0000000000000000000000000000000
# Info :                Plaintext, EOI=1 EOT=1, Last=1, Length=49 bytes
HDR = 47000031000000000000000000000000
DAT = FF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F000000000000000000000000000000

#### Authenticated Decryption
#### MsgID= 14, KeyID=  7 Ad Size =   49, Ct Size =   49
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=49 bytes
HDR = 12000031000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0000000000000000000000000000000
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=49 bytes
HDR = 56000031000000000000000000000000
DAT = A2317BB84A5638DBCE96AEE6AC49E4E8CA67EA643730C0F85F6465BD74C7B99439F98F48A13F8DE8430E301609B161B9AF000000000000000000000000000000
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = 368E32A58B06BF776B7C360A9801ACDE

#### Authenticated Encryption
#### MsgID= 15, KeyID=  8 Ad Size =   96, Pt Size =   96
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=96 bytes
HDR = 12000060000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF
# Info :                Plaintext, EOI=1 EOT=1, Last=1, Length=96 bytes
HDR = 47000060000000000000000000000000
DAT = FF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E

#### Authenticated Decryption
#### MsgID= 16, KeyID=  8 Ad Size =   96, Ct Size =   96
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=96 bytes
HDR = 12000060000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=96 bytes
HDR = 56000060000000000000000000000000
DAT = 402A6A7966E747FE151A40E5A348A977EACA024869CC069419D29428713A0276DCA2E657400EF516239D3811AAB32B4770999BBC830FE024184BAE2CA47A967CB9DBB7D810F6ECC514410EE17D5D4D65802B8828E9D51DB845611D6C6AE9F709
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = C62BA151EB47410C5540F34794AF157E

#### Authenticated Encryption
#### MsgID= 17, KeyID=  9 Ad Size =  144, Pt Size =  144
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=144 bytes
HDR = 12000090000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F
DAT = 202122232425262728292A2B2C2D2E2F
# Info :                Plaintext, EOI=1 EOT=1, Last=1, Length=144 bytes
HDR = 47000090000000000000000000000000
DAT = FF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F606162636465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E
DAT = 7F808182838485868788898A8B8C8D8E

#### Authenticated Decryption
#### MsgID= 18, KeyID=  9 Ad Size =  144, Ct Size =  144
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=144 bytes
HDR = 12000090000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F
DAT = 202122232425262728292A2B2C2D2E2F
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=144 bytes
HDR = 56000090000000000000000000000000
DAT = 8AC7974C01C17D4EED20BCE0534307B6A2D87F1E8E85B27FFC5AF9155D12FDA8DBF5033B8ADAC942DC3E9F3FFC829CC79CAAAFAD64F147B97309DECF1C78691267A4AE1B43BA342D9E91B0124F2A763820FC950E787C74654D3B2179E15F014B5CC91FBC62271FAEF8DCD296478B34460AAA654866955091249E037916191EFA
DAT = E98A57B73B9272E8C9E1D484D2020145
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = 144951F734CECD3D843350C8606E4DF7

#### Authenticated Encryption
#### MsgID= 19, KeyID= 10 Ad Size =  192, Pt Size =  192
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Encryption
INS = 20000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=192 bytes
HDR = 120000C0000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F
DAT = 202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F
# Info :                Plaintext, EOI=1 EOT=1, Last=1, Length=192 bytes
HDR = 470000C0000000000000000000000000
DAT = FF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F606162636465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E
DAT = 7F808182838485868788898A8B8C8D8E8F909192939495969798999A9B9C9D9E9FA0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBE

#### Authenticated Decryption
#### MsgID= 20, KeyID= 10 Ad Size =  192, Ct Size =  192
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=192 bytes
HDR = 120000C0000000000000000000000000
DAT = A0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F
DAT = 202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=192 bytes
HDR = 560000C0000000000000000000000000
DAT = BF883FC7773A318BADF3DFEB4C6F0964BE478F48DAF35F42C3273ED5DBAF596EC517597633CC1C0280DFA5BE838376754601D035A001A89EBAC60CEFD2D114359C1EC294C9ECF82E4140C3556378C21DA98FA11F2FBABD68A133B78D8AE4C07D796A73233FF2FB7237409B828F0B5F78BA693E14EC260C46F791587830AA0638
DAT = 4F29A1EF0E24105412A8B0261042820DD99DC330D62644815DFC303E45CFC60722A0421B395A60D64D7BE2231A6E979B995EDE0CDF016AF71444EE56D62C2855
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = 84E4CDBBEBCB509EFB8E096D5A0C7E93

#### Authenticated Decryption
#### MsgID= 21, KeyID= 11 Ad Size =  172, Ct Size =  180
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = 6F59D2C2C3826F8CD97B3088854AFE13
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=172 bytes
HDR = 120000AC000000000000000000000000
DAT = 79BE545E8B9D4A50FEF2B9B26CE4918B9392D283FB175DF06E9A98583559BE49F67A48766B3861658D82DAAB993776105088153300698FB1B0AB5CA8AF0E9BFBC2833DFF9C724C2A11C4969C97C4ABEF0DC63ED2073EC53245BCA96378E1304D8C003B187A4168C2365437E777E3AC9598D7B7317F12DE83755A31CB4B9F6886
DAT = 453441747D79435AB63B43AF34FFC0F8B6395DE28697233B02848A11AB4CC07A90ED245EEA1F34E53CB65E6500000000
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=180 bytes
HDR = 560000B4000000000000000000000000
DAT = AA6AC960EF65B19724DB837838BB93A30E0690F3069DC191264A8B7DBFB580384E74B6D0ACA9AD7C0C7AF4BFE01C3967CAC1E95B4E7897DF3950B3458F501CCB8AFA69FDBEA97D432B072E8DBAA4F2EB104A0E8D147D0DE50075B01A247D17FD75B246ACCC4CD98986A3C46864946ABFBFD0AD2CF09311AF1F5E5041405A8BC5
DAT = 5EE46EB3925672C98A155B5D7D73B4F88AC494DC3A9061549708899967FA470726503BF0121DE2259233F69843B310A4E55ADF68000000000000000000000000
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = 58689C2930D83B8410DACEDE11C112BD

#### Authenticated Decryption
#### MsgID= 22, KeyID= 12 Ad Size =  194, Ct Size =   46
# Instruction: Opcode=Activate Key
INS = 70000000000000000000000000000000
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = AD09B231C896C04CFCE61C31BE5DF7FB
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=194 bytes
HDR = 120000C2000000000000000000000000
DAT = 29C58C8086131598D0C430C924AF9339F38C7AF248900AC01A1E5A85B9FEA58240D0912EE743C6475649B71F72B679B60A7D8F81E599806FE3B69D309D077C4F57858B28A83904664E87D907FED00C9A653D046DF96A7270A6247384D39E226F7130E0202A700FB94E9928592B048F1B05076AB9541E1FEE6E2BB8110F43B39D
DAT = 40542938E07F94B4A8C25C06E1A6DCEAF1C722756F2C6E2EAB228BE45B3063D28B5805A7B172D9E04A5E66F4A0D6C45C4470253993E6F312B50627ED7C977E4457620000000000000000000000000000
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=46 bytes
HDR = 5600002E000000000000000000000000
DAT = DA2757D79B9C8D7759CC50F0C5F05507490346011B32E1C36DC008134D4C69826325BDAA1E64339EDFE48D9ED75C0000
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = E9C2DB26BBEE26FCDA9F5EB160F8E49C

#### Authenticated Decryption
#### MsgID= 23, KeyID= 12 Ad Size =  102, Ct Size =   23
# Instruction: Opcode=Authenticated Decryption
INS = 30000000000000000000000000000000
# Info :                     Npub, EOI=0 EOT=1, Last=0, Length=16 bytes
HDR = D2000010000000000000000000000000
DAT = 10FFF58BB99A80FB87CEE4034CD2942B
# Info :          Associated Data, EOI=0 EOT=1, Last=0, Length=102 bytes
HDR = 12000066000000000000000000000000
DAT = 78B36185AC6AC4521EB99273F9FC066D7636B4C327A5D628FAF7C8BE5CCAC058CDA5858DBB19EA2FDEB2161CF6CA58F6F4AB51648D2F45EC49A18D34E0D98252A2F969CA5362026CD433BF8A5CCCE28AF49D52780E3CA8B9AB9FB44F7D727F070FE6334D1EBC00000000000000000000
# Info :               Ciphertext, EOI=1 EOT=1, Last=0, Length=23 bytes
HDR = 56000017000000000000000000000000
DAT = A08958C5C0764D58563546E40727EE8E53A29F0E5F9917000000000000000000
# Info :                      Tag, EOI=0 EOT=1, Last=1, Length=16 bytes
HDR = 83000010000000000000000000000000
DAT = EB82274A826A9F7236A8D99244F61CF8

###EOF
