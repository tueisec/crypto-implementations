-------------------------------------------------------------------------------
--! @file       f_circuit.vhd
--! @brief      Norx core permutation F.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.norx_pkg.all;

entity f_circuit is
	port(
		state_f_pre 	: in std_logic_vector(16*w-1 downto 0);
		state_f_post 	: out std_logic_vector(16*w-1 downto 0)
	);
end f_circuit;

architecture RTL of f_circuit is
	signal state_f_tmp : std_logic_vector(16*w-1 downto 0);
begin

	-- S <= diag(col(S))

	-- column operation
	c : entity work.col port map(
		state_col_pre => state_f_pre,
		state_col_post => state_f_tmp
	);
	
	-- diagonal operation
	d : entity work.diag port map(
		state_diag_pre => state_f_tmp,
		state_diag_post => state_f_post
	);

end RTL;

