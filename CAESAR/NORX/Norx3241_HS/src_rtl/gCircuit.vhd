-------------------------------------------------------------------------------
--! @file       gCircuit.vhd
--! @brief      Norx G Function, part of the norx core permutation F.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.norx_pkg.all;

entity gCircuit is
	port(
		ai : in  std_logic_vector(w-1 downto 0);
		bi : in  std_logic_vector(w-1 downto 0);
		ci : in  std_logic_vector(w-1 downto 0);
		di : in  std_logic_vector(w-1 downto 0);

		ao : out  std_logic_vector(w-1 downto 0);
		bo : out  std_logic_vector(w-1 downto 0);
		co : out  std_logic_vector(w-1 downto 0);
		do : out  std_logic_vector(w-1 downto 0)
	);
end gCircuit;

architecture structure of gCircuit is

	signal a_tmp, b_tmp, c_tmp, d_tmp : std_logic_vector(w-1 downto 0);
	signal a_out, d_out, c_out : std_logic_vector(w-1 downto 0);
	signal ab_s, ab_s2, cd_s, cd_s2 : std_logic_vector(w-1 downto 0);

begin

	-- a_tmp
	ab_s	<= (ai(w-2 downto 0) and bi(w-2 downto 0)) & '0';
	a_tmp	<= ai xor bi xor ab_s;

	-- d_tmp
	d_tmp	<= std_logic_vector(rotate_right(unsigned(a_tmp xor di), r0));
	
	-- c_tmp
	cd_s	<= (ci(w-2 downto 0) and d_tmp(w-2 downto 0)) & '0';
	c_tmp 	<= ci xor d_tmp xor cd_s;

	-- b_tmp
	b_tmp	<= std_logic_vector(rotate_right(unsigned(c_tmp xor bi), r1));

	-- a_out
	ab_s2	<= (a_tmp(w-2 downto 0) and b_tmp(w-2 downto 0)) & '0';
	a_out	<= a_tmp xor b_tmp xor ab_s2;

	-- d_out
	d_out	<= std_logic_vector(rotate_right(unsigned(a_out xor d_tmp), r2));
	
	-- c_out
	cd_s2	<= (c_tmp(w-2 downto 0) and d_out(w-2 downto 0)) & '0';
	c_out	<= c_tmp xor d_out xor cd_s2;
	
	-- b_out
	bo	<= std_logic_vector(rotate_right(unsigned(b_tmp xor c_out), r3));

	-- forward outputs
	ao <= a_out;
	co <= c_out;
	do <= d_out;


end structure;
