--! @file       CipherCore.vhd
--! @brief      Ciphercore unit implementing Norx3241v3 with use of full F function.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use ieee.std_logic_misc.all;
use work.AEAD_pkg.ALL;
use work.norx_pkg.all;

entity CipherCore is
    generic (
        --! Reset behavior
        G_ASYNC_RSTN    : boolean := False; --! Async active low reset
        --! Block size (bits)
        G_DBLK_SIZE     : integer := 128;   --! Data
        G_KEY_SIZE      : integer := 128;   --! Key
        G_TAG_SIZE      : integer := 128;   --! Tag
        --! The number of bits required to hold block size expressed in
        --! bytes = log2_ceil(G_DBLK_SIZE/8)
        G_LBS_BYTES     : integer := 4;
        --! Maximum supported AD/message/ciphertext length = 2^G_MAX_LEN-1
        G_MAX_LEN       : integer := SINGLE_PASS_MAX
    );
    port (
        --! Global
        clk             : in  std_logic;
        rst             : in  std_logic;
        --! PreProcessor (data)
        key             : in  std_logic_vector(G_KEY_SIZE       -1 downto 0);
        bdi             : in  std_logic_vector(G_DBLK_SIZE      -1 downto 0);
        --! PreProcessor (controls)
        key_ready       : out std_logic;
        key_valid       : in  std_logic;
        key_update      : in  std_logic;
        decrypt         : in  std_logic;
        bdi_ready       : out std_logic;
        bdi_valid       : in  std_logic;
        bdi_type        : in  std_logic_vector(3                -1 downto 0);
        bdi_partial     : in  std_logic;
        bdi_eot         : in  std_logic;
        bdi_eoi         : in  std_logic;
        bdi_size        : in  std_logic_vector(G_LBS_BYTES+1    -1 downto 0);
        bdi_valid_bytes : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);
        bdi_pad_loc     : in  std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);
        --! PostProcessor
        bdo             : out std_logic_vector(G_DBLK_SIZE      -1 downto 0);
        bdo_valid       : out std_logic;
        bdo_ready       : in  std_logic;
        bdo_size        : out std_logic_vector(G_LBS_BYTES+1    -1 downto 0);
        msg_auth        : out std_logic;
        msg_auth_valid  : out std_logic;
        msg_auth_ready  : in  std_logic
    );
end entity CipherCore;

architecture structure of CipherCore is

	-- state machine state
	type norx_state is (IDLE, LOAD_KEY, LOAD_NPUB, INIT, WAIT_BDI, ABSORB, ENCDEC, FINAL, TAG_OUT);
	signal state : norx_state;

	-- counter signals
	signal cnt_res		: std_logic;
	signal cnt_en		: std_logic;
	signal cnt_val		: std_logic_vector(2 downto 0);	-- adapt size

	-- key register
	signal key_st		: std_logic_vector(4*w-1 downto 0);
	signal key_en		: std_logic;

	-- endianess conversion
	signal bdi_little	: std_logic_vector(12*w-1 downto 0);
	signal bdi_little_pd	: std_logic_vector(12*w-1 downto 0);
	signal bdo_little	: std_logic_vector(12*w-1 downto 0);
	signal key_little	: std_logic_vector(4*w-1 downto 0);

	-- padding
	signal pad			: std_logic_vector(7 downto 0);

	-- handshake signals local
	signal bdi_ready_l	: std_logic;
   	signal bdo_valid_l  : std_logic;
    
	-- cipher core core (core of the core, it's not supposed to be funny)
	signal s_old		: std_logic_vector(16*w-1 downto 0);
	signal s_new		: std_logic_vector(16*w-1 downto 0);
	signal s_pre		: std_logic_vector(16*w-1 downto 0);
	signal s_post		: std_logic_vector(16*w-1 downto 0);
	signal s_rnd		: std_logic_vector(16*w-1 downto 0);
	signal s_filt0		: std_logic_vector(12*w-1 downto 0);
	signal s_filt1		: std_logic_vector(12*w-1 downto 0);
	signal m_pad		: std_logic_vector(12*w-1 downto 0);
	signal s_dec_back	: std_logic_vector(16*w-1 downto 0);
	signal s_init		: std_logic_vector(16*w-1 downto 0);
	signal s_new_sel	: std_logic_vector(1 downto 0);
	signal s_en		: std_logic;

	signal d_in0		: std_logic_vector(7 downto 0);
	signal d_in1_r		: std_logic_vector(12*w-1 downto 0);
	signal d_in1_c		: std_logic_vector(4*w-1 downto 0);
	signal d_in0_en	: std_logic;
	signal d_in1_c_en	: std_logic;
	signal d_in1_r_en	: std_logic;

	signal tag		: std_logic_vector(7 downto 0);
	
	signal out_sel		: std_logic;

	signal decrypt_r	: std_logic;
	signal eot_r		: std_logic;
	signal eoi_r		: std_logic;

	signal bdi_valid_bytes_ltl : std_logic_vector(G_DBLK_SIZE/8    -1 downto 0);

begin

	-- forward local signals to global outputs
	bdi_ready <= bdi_ready_l;
	bdo_valid <= bdo_valid_l;

	---------------------------
	-- endianness conversion --
	---------------------------
	bdi_little 	<= changeEndianness(bdi, w);
	key_little 	<= changeEndianness(key, w);
	
	-- if bdo valid, forward bdo_little to output + change endianness
	-- else set output bdo to Z high impedance, to prevent leaking internal state
	with bdo_valid_l select bdo <=
	   changeEndianness(bdo_little, w) when '1',
	   (others => 'Z') when others; 	
	
	-- register to save bdi
	-- alternative: can be omitted, when reading bdi after F (at the time it's needed). But then preprocessor looses time, big performance loss
	bdi_reg : process(clk)
	begin
		if rising_edge(clk) then
			if (bdi_ready_l and bdi_valid) = '1' then
				bdi_little_pd <= bdi_little(12*w-1 downto w) & pad & bdi_little(w-8-1 downto 0);	-- save padded bdi in register at handshake
				bdi_valid_bytes_ltl <= changeEndiannessBit(bdi_valid_bytes, 4);				-- also save related valid_bytes
			end if;
		end if;
	end process bdi_reg;

	-- select right padding byte (last byte, in highspeed api, the 01 is generated by preprocessor)
	with bdi_valid_bytes(1 downto 0) select pad <=
		bdi_little(w-1 downto w-8) when "11",	-- if all data valid, don't use padding byte, just use data
		x"80" when "00",			-- if more than last byte invalid, use 80
		x"81" when others;			-- if only last byte invalid, use 81

	-- select output
	with out_sel select bdo_little <=
		s_old(4*w-1 downto 0) & (8*w-1 downto 0 => '0') when '0',	-- tag
		s_rnd(16*w-1 downto 4*w) when others;				-- pt, ct output 

	-- xor encode tag and current tag (if one of the bits is '1', authentication failed)
	msg_auth <= not or_reduce(bdi_little(12*w-1 downto 8*w) xor s_old(4*w-1 downto 0));

	-------------------------------------------------------
	-- norx state, stores the ciphercores internal state --
	-------------------------------------------------------
	state_reg : process(clk)
	begin
		if rising_edge(clk) then
			if s_en = '1' then
				s_old <= s_new;		-- each clock cycle, new state is written
			end if;
		end if;
	end process state_reg;
	

	-- xor pre F --
	s_pre <= s_old(16*w-1 downto 8) & (s_old(7 downto 0) xor d_in0);
 
	--with d_in0_en select d_in0 <=
	--	x"00"	when '0',
	--	tag	when others;

	-- enable tag input for xor only when needed, else 0x00 
	d_in0 <= tag and (7 downto 0 => d_in0_en);


	------------------------------------------
	-- F block (core of the norx algorithm) --
	------------------------------------------
	f_blk : entity work.f_circuit port map(
		state_f_pre	=> s_pre,
		state_f_post	=> s_post
	);


	-- xor post F
	s_rnd <= s_post xor (d_in1_r & d_in1_c);

	--with d_in1_r_en select d_in1_r <=
	--	(others => '0')	when '0',
	--	bdi_little_pd	when others;

	-- enable xor input bdi only when needed
	d_in1_r <= bdi_little_pd and (12*w-1 downto 0 => d_in1_r_en);

	--with d_in1_c_en select d_in1_c <=
	--	(others => '0')	when '0',
	--	key_st		when others;

	-- enable xor input key only when needed
	d_in1_c <= key_st and (4*w-1 downto 0 => d_in1_c_en); 

	------------------------------
	-- state block input select --
	------------------------------
	with s_new_sel select s_new <=
		s_init	when "00",		-- select initialization constants
		s_rnd	when "01",		-- select round, enc abs... (F permutation + evtl. xors)
		s_dec_back when others;		-- select state input for decode step

	-- s_dec_back for decode step
	s_filt0 <= s_post(16*w-1 downto 4*w);						
	s_filt1 <= filterValidBytes(s_filt0, bdi_valid_bytes_ltl);
	m_pad	<= s_filt1 xor bdi_little_pd;
	s_dec_back <= (s_post(16*w-1 downto 4*w) xor m_pad) & s_rnd(4*w-1 downto 0);		-- filtered and padded M
	

	-- init norx state (combination of key, nonce and constants)
	s_init <= 	(bdi_little(12*w-1 downto 11*w) &			-- nonce
			bdi_little(11*w-1 downto 10*w) &
			bdi_little(10*w-1 downto 9*w) &
			bdi_little(9*w-1 downto 8*w) &
			key_st(4*w-1 downto 3*w) &				-- key
			key_st(3*w-1 downto 2*w) &
			key_st(2*w-1 downto 1*w) &
			key_st(1*w-1 downto 0) &
			u8 & u9 & u10 & u11 & 					-- constants
			(u12 xor std_logic_vector(to_unsigned(w, w))) & 	-- constants xor norx parameters
			(u13 xor std_logic_vector(to_unsigned(l, w))) & 
			(u14 xor std_logic_vector(to_unsigned(p, w))) & 
			(u15 xor std_logic_vector(to_unsigned(t, w))));

	------------------
	-- key register --
	------------------
	key_reg : process(clk)
	begin
		if rising_edge(clk) then
			if key_en = '1' then
				key_st	<= key_little;	-- save key for later use
			end if;
		end if;
	end process key_reg;

	----------------------------------------------
	-- eot, eoi and decrypt stored at handshake --
	----------------------------------------------
	handsh_reg : process(clk)
	begin
		if rising_edge(clk) then
			if (bdi_ready_l and bdi_valid) = '1' then
				decrypt_r <= decrypt;			-- save eot, eoi, decrypt at handshake, to guarantee valid signals for later use
				eot_r <= bdi_eot;
				eoi_r <= bdi_eoi;
			end if;
		end if;
	end process handsh_reg;

	-------------
	-- counter --
	-------------
	count_reg : process(clk)
	begin
		if rising_edge(clk) then
			if cnt_res = '1' then
				cnt_val <= "000";
			elsif cnt_en = '1' then
				cnt_val <= std_logic_vector(unsigned(cnt_val) + 1);
			end if;
		end if;
	end process count_reg;


	-------------------
	-- state machine --
	-------------------
	-- state transitions
	fsm_state_trans : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then

			else
				case state is

					-- IDLE state: wait for new enc/dec
					when IDLE =>
						if bdi_valid = '1' then
							if key_update = '1' then
								state <= LOAD_KEY;	-- if key_update, load new key
							else
								state <= LOAD_NPUB;	-- if not, skip load_key and load new npub (nonce)
							end if;
						end if;

					-- LOAD_KEY state: when valid key, then save key in register
					when LOAD_KEY =>
						if key_valid = '1' then
							state <= LOAD_NPUB;
						end if;

					-- LOAD_NPUB state: when valid bdi, push key nonce and constants into state register 
					when LOAD_NPUB =>
						if bdi_valid = '1' then
							state <= INIT;
						end if;

					-- INIT state: initialize norx internal state (l times F perm. + xor key) 
					when INIT =>
						if (cnt_val(0) and cnt_val(1)) = '1' then
							if eoi_r = '1' then
								state <= FINAL;
							else	
								state <= WAIT_BDI;
							end if;
						end if;

					-- WAIT_BDI state: wait for bdi_valid, then decide if AD (absorb) or PT/CT (encdec)
					when WAIT_BDI =>
						if bdi_valid = '1' then
							if bdi_type(2 downto 1) = BDI_TYPE_ASS then
								state <= ABSORB;
							else 
								state <= ENCDEC;
							end if;
						end if;

					-- ABSORB state: absorbs associate data, if count value = 3 (4 steps needed), go to either WAIT_BDI or FINAL, if no data left (eot, eoi)
					when ABSORB =>
						if (cnt_val(1) and cnt_val(0)) = '1' then
							if (bdi_eot and bdi_eoi) = '1' then
								state <= FINAL;
							else
								state <= WAIT_BDI;
							end if;
						end if;

					-- ENCDEC state: only leave when count at 3. When input valid (no complete pad block) wait also for bdo_ready, wait for post processor to be ready for bdo data
					-- if no data left go to FINALIZE, else repeat with wait for bdi
					when ENCDEC =>
						if (cnt_val(1) and cnt_val(0) and (bdo_ready or not bdi_valid_bytes(bdi_valid_bytes'length-1))) = '1' then
							if (eot_r and eoi_r) = '1' then
								state <= FINAL;
							else
								state <= WAIT_BDI;
							end if;
						end if;

					-- FINAL state: Finalize (4*F + key + 4*F + key => 8 steps), leave at count = 7
					when FINAL =>
						if (cnt_val(2) and cnt_val(1) and cnt_val(0)) = '1' then
							state <= TAG_OUT;
						end if;

					-- TAG_OUT state: if decrypt, wait for valid input data (Tag from encrypt), then compare and give result of comparison
					-- if encrypt, just send tag out
					when TAG_OUT =>
						if decrypt_r = '1' then
							if (bdi_valid and msg_auth_ready) = '1' then
								state <= IDLE;
							end if;
						else
							if bdo_ready = '1' then
								state <= IDLE; 
							end if;
						end if;
					
				end case;

			end if;
		end if;
	end process fsm_state_trans;

	-- state machine outputs
	fsm_comb_out : process(state, cnt_val, bdi_valid, msg_auth_ready, decrypt_r, eot_r, bdi_valid_bytes_ltl, bdo_ready)
	begin
		case state is
			
			when IDLE =>
				-- tag
				tag		<= "--------";
				-- handshake
				key_ready	<= '0';
				bdi_ready_l	<= '0';
				bdo_valid_l	<= '0';
				msg_auth_valid	<= '0';
				-- key register
				key_en		<= '0';
				-- core (state/F)
				s_new_sel	<= "--";
				s_en		<= '0';
				d_in0_en	<= '-';
				d_in1_r_en	<= '-';
				d_in1_c_en	<= '-';
				-- counter
				cnt_res		<= '1';
				cnt_en		<= '0';
				-- output select
				out_sel		<= '-';
	
			when LOAD_KEY =>
				-- tag
				tag		<= "--------";
				-- handshake
				key_ready	<= '1';
				bdi_ready_l	<= '0';
				bdo_valid_l	<= '0';
				msg_auth_valid	<= '0';
				-- key register
				key_en		<= '1';
				-- core (state/F)
				s_new_sel	<= "--";
				s_en		<= '0';
				d_in0_en	<= '-';
				d_in1_r_en	<= '-';
				d_in1_c_en	<= '-';
				-- counter
				cnt_res		<= '0';
				cnt_en		<= '0';
				-- output select
				out_sel		<= '-';

			when LOAD_NPUB =>
				-- tag
				tag		<= "--------";
				-- handshake
				key_ready	<= '0';
				bdi_ready_l	<= '1';
				bdo_valid_l	<= '0';
				msg_auth_valid	<= '0';
				-- key register
				key_en		<= '0';
				-- core (state/F)
				s_new_sel	<= "00";
				s_en		<= bdi_valid;
				d_in0_en	<= '-';
				d_in1_r_en	<= '-';
				d_in1_c_en	<= '-';
				-- counter
				cnt_res		<= '0';
				cnt_en		<= '0';
				-- output select
				out_sel		<= '-';

			when INIT =>
				-- tag
				tag		<= "--------";			-- no tag needed
				-- handshake
				key_ready	<= '0';
				bdi_ready_l	<= '0';
				bdo_valid_l	<= '0';
				msg_auth_valid	<= '0';
				-- key register
				key_en		<= '0';
				-- core (state/F)
				s_new_sel	<= "01";			-- select round
				s_en		<= '1';				-- enable state register
				d_in0_en	<= '0';
				d_in1_r_en	<= '0';
				d_in1_c_en	<= cnt_val(1) and cnt_val(0);	-- insert key in 4th step
				-- counter
				cnt_res		<= cnt_val(1) and cnt_val(0);
				cnt_en		<= '1';
				-- output select
				out_sel		<= '-';

			when WAIT_BDI =>
				-- tag
				tag		<= "--------";
				-- handshake
				key_ready	<= '0';
				bdi_ready_l	<= '1';
				bdo_valid_l	<= '0';
				msg_auth_valid	<= '0';
				-- key register
				key_en		<= '0';
				-- core (state/F)
				s_new_sel	<= "--";
				s_en		<= '0';
				d_in0_en	<= '-';
				d_in1_r_en	<= '-';
				d_in1_c_en	<= '-';
				-- counter
				cnt_res		<= '0';
				cnt_en		<= '0';
				-- output select
				out_sel		<= '-';

			when ABSORB =>
				-- tag
				tag		<= x"01";
				-- handshake
				key_ready	<= '0';
				bdi_ready_l	<= '0';
				bdo_valid_l	<= '0';
				msg_auth_valid	<= '0';
				-- key register
				key_en		<= '0';
				-- core (state/F)
				s_new_sel	<= "01";
				s_en		<= '1';
				d_in0_en	<= not (cnt_val(1) or cnt_val(0));	-- insert tag in first step
				d_in1_r_en	<= cnt_val(1) and cnt_val(0);		-- insert AD (bdi) in 4th step
				d_in1_c_en	<= '0';
				-- counter
				cnt_res		<= cnt_val(1) and cnt_val(0);		
				cnt_en		<= '1';
				-- output select
				out_sel		<= '-';

			when ENCDEC =>	
				-- tag
				tag		<= x"02";
				-- handshake
				key_ready	<= '0';
				bdi_ready_l	<= '0';
				bdo_valid_l	<= cnt_val(1) and cnt_val(0) and bdi_valid_bytes_ltl(bdi_valid_bytes_ltl'length-(w/8));	-- bdo valid only when 4th step and bdi contains valid data
				msg_auth_valid	<= '0';
				-- key register
				key_en		<= '0';
				-- core (state/F)
				s_new_sel	<= (cnt_val(1) and cnt_val(0) and decrypt_r) & "1";	-- when decode and in 4th step use dec_back else use normal round for state
				s_en		<= not (cnt_val(1) and cnt_val(0)) or bdo_ready or not bdi_valid_bytes_ltl(bdi_valid_bytes_ltl'length-(w/8));	-- same as for counter(6 lines below), only update state if counter changes too
				d_in0_en	<= not (cnt_val(1) or cnt_val(0));	-- insert tag in 1st step
				d_in1_r_en	<= cnt_val(1) and cnt_val(0);		-- insert bdi in 4th step
				d_in1_c_en	<= '0';
				-- counter
				cnt_res		<= cnt_val(1) and cnt_val(0);
				cnt_en		<= not (cnt_val(1) and cnt_val(0)) or bdo_ready or not bdi_valid_bytes_ltl(bdi_valid_bytes_ltl'length-(w/8));	-- count to 3, then only proceed if bdo not needed or postprocessor ready
				-- output select
				out_sel		<= '1';

			when FINAL =>
				-- tag
				tag		<= x"08";
				-- handshake
				key_ready	<= '0';
				bdi_ready_l	<= '0';
				bdo_valid_l	<= '0';
				msg_auth_valid	<= '0';
				-- key register
				key_en		<= '0';
				-- core (state/F)
				s_new_sel	<= "01";
				s_en		<= '1';
				d_in0_en	<= not(cnt_val(2) or cnt_val(1) or cnt_val(0));	-- tag in 1st step
				d_in1_r_en	<= '0';
				d_in1_c_en	<= cnt_val(1) and cnt_val(0);	-- key in 4th and 8th step
				-- counter
				cnt_res		<= cnt_val(2) and cnt_val(1) and cnt_val(0);
				cnt_en		<= '1';
				-- output select
				out_sel		<= '-';

			when TAG_OUT =>
				-- tag
				tag		<= "--------";
				-- handshake
				key_ready	<= '0';
				bdi_ready_l	<= decrypt_r;
				bdo_valid_l	<= not decrypt_r;
				msg_auth_valid	<= msg_auth_ready and bdi_valid;
				-- key register
				key_en		<= '0';
				-- core (state/F)
				s_new_sel	<= "--";
				s_en		<= '0';
				d_in0_en	<= '-';
				d_in1_r_en	<= '-';
				d_in1_c_en	<= '-';
				-- counter
				cnt_res		<= '0';
				cnt_en		<= '0';
				-- output select
				out_sel		<= '0';
			
		end case;
	end process fsm_comb_out;

end structure;
