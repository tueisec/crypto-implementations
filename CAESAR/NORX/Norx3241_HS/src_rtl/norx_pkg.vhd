library IEEE;
-------------------------------------------------------------------------------
--! @file       norx_pkg.vhd
--! @brief      Package for Norx constants and function. 
--!
--!             Entity for norx highspeed core
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

package norx_pkg is

	-------------------------------------------------------
	--CONSTANTS--------------------------------------------
	-------------------------------------------------------
	
	-- apply global settings here, user has to change when using different configuration
	constant G_DBLK_SIZE 	: integer := 384;
	constant G_NORX_L	: integer := 4;
	constant G_TAG_SIZE	: integer := 128;

	-- instance parameters
	constant NORX_W 	: integer := (G_DBLK_SIZE/384)*32;

	constant w : integer := NORX_W;
	constant l : integer := G_NORX_L;
	constant p : integer := 1;
	constant t : integer := G_TAG_SIZE;

	-- 32 bit
	constant r0 : integer := 8;
	constant r1 : integer := 11;
	constant r2 : integer := 16;
	constant r3 : integer := 31;

	constant u8 : std_logic_vector(31 downto 0) := x"A3D8D930";
	constant u9 : std_logic_vector(31 downto 0) := x"3FA8B72C";
	constant u10 : std_logic_vector(31 downto 0) := x"ED84EB49";
	constant u11 : std_logic_vector(31 downto 0) := x"EDCA4787";
	constant u12 : std_logic_vector(31 downto 0) := x"335463EB";
	constant u13 : std_logic_vector(31 downto 0) := x"F994220B";
	constant u14 : std_logic_vector(31 downto 0) := x"BE0BF5C9";
	constant u15 : std_logic_vector(31 downto 0) := x"D7C49104";
	-- end 32 bit

	-- 64 bit 
--	constant r0 : integer := 8;
--	constant r1 : integer := 19;
--	constant r2 : integer := 40;
--	constant r3 : integer := 63;	

--	constant u8 : std_logic_vector(63 downto 0) := x"B15E641748DE5E6B";
--	constant u9 : std_logic_vector(63 downto 0) := x"AA95E955E10F8410";
--	constant u10 : std_logic_vector(63 downto 0) := x"28D1034441A9DD40";
--	constant u11 : std_logic_vector(63 downto 0) := x"7F31BBF964E93BF5";
--	constant u12 : std_logic_vector(63 downto 0) := x"B5E9E22493DFFB96";
--	constant u13 : std_logic_vector(63 downto 0) := x"B980C852479FAFBD";
--	constant u14 : std_logic_vector(63 downto 0) := x"DA24516BF55EAFD4";
--	constant u15 : std_logic_vector(63 downto 0) := x"86026AE8536F1501";
	-- end 64 bit

	constant SL_PRES	: std_logic_vector(2 downto 0) := "000";
	constant SL_CXOR	: std_logic_vector(2 downto 0) := "001";
	constant SL_RXOR	: std_logic_vector(2 downto 0) := "010";
	constant SL_CORE	: std_logic_vector(2 downto 0) := "011";
	constant SL_ENDE	: std_logic_vector(2 downto 0) := "100";
	
	constant SL_KEY		: std_logic := '0';
	constant SL_TAG		: std_logic := '1';

	constant TAG_ABSORB_HEADER	: std_logic_vector(w-1 downto 0) := std_logic_vector(to_unsigned(16#01#, w));
	constant TAG_ABSORB_TRAILER	: std_logic_vector(w-1 downto 0) := std_logic_vector(to_unsigned(16#04#, w));
	constant TAG_ENCDEC 		: std_logic_vector(w-1 downto 0) := std_logic_vector(to_unsigned(16#02#, w));
	constant TAG_FINALIZE		: std_logic_vector(w-1 downto 0) := std_logic_vector(to_unsigned(16#08#, w));

	-------------------------------------------------------
	--TYPES------------------------------------------------
	------------------------------------------------------- 

  	type stateArrayType is array (0 to 15) of std_logic_vector(w-1 downto 0);
	type arrayType4 is array (0 to 3) of std_logic_vector(w-1 downto 0);
	type arrayType12 is array (0 to 11) of std_logic_vector(w-1 downto 0);


	-------------------------------------------------------
	--FUNCTIONS--------------------------------------------
	-------------------------------------------------------
	
	function changeEndiannessBit(slvIn : in std_logic_vector; wordsize : in integer) return std_logic_vector;
	function filterValidBytes(slvIn : std_logic_vector; bytesVal : std_logic_vector) return std_logic_vector;
	function changeEndianness(slvIn : std_logic_vector; wordsize : integer) return std_logic_vector;

	function slvToStateArray(slvIn : std_logic_vector(16*w-1 downto 0)) return stateArrayType;
	function stateArrayToSlv(arrIn : stateArrayType) return std_logic_vector;
	
	function slvToArray4(slvIn : std_logic_vector(4*w-1 downto 0)) return arrayType4;
	function array4ToSlv(arrIn : arrayType4) return std_logic_vector;

	function slvToArray12(slvIn : std_logic_vector(12*w-1 downto 0)) return arrayType12;
	function array12ToSlv(arrIn : arrayType12) return std_logic_vector;

end;

package body norx_pkg is

	-------------------------------------------------------
	--FUNCTIONS--------------------------------------------
	------------------------------------------------------- 
	function changeEndiannessBit(slvIn : in std_logic_vector; wordsize : in integer) return std_logic_vector is
		variable slvOut : std_logic_vector(slvIn'length-1 downto 0);
		variable numWords : integer := slvIn'length/wordsize;
		variable wordIdx : integer;
	begin
		for i in 0 to numWords-1 loop
			wordIdx := i*wordsize;
			for j in 0 to wordsize - 1 loop
				slvOut(wordIdx + j) := slvIn(wordIdx + (wordsize-j-1));
			end loop;
		end loop;	
		return slvOut;
	end changeEndiannessBit;

	-- filters out bytes which are not valid
	function filterValidBytes(slvIn : std_logic_vector; bytesVal : std_logic_vector) return std_logic_vector is
		variable slvOut : std_logic_vector(slvIn'length-1 downto 0);
	begin
		for i in 0 to slvIn'length-1 loop
			slvOut(i) := slvIn(i) and bytesVal(i/8);
		end loop;
		return slvOut;
	end filterValidBytes;

	-- changes endianness of vector
	function changeEndianness(slvIn : in std_logic_vector; wordsize : in integer) return std_logic_vector is
		
		variable slvOut 	: std_logic_vector(slvIn'length-1 downto 0);
		variable numWords 	: integer := slvIn'length/wordsize;
		variable wordIdx	: integer;
	begin
		for I in 0 to numWords-1 loop
			wordIdx := I*wordsize;
			for J in 0 to (wordsize/8)-1 loop
				slvOut(wordIdx + (J+1)*8-1 downto wordIdx + J*8) := slvIn(wordIdx + ((wordsize/8)-J)*8-1 downto wordIdx + ((wordsize/8)-J-1)*8);
			end loop;
		end loop;
	
		return slvOut;
	end changeEndianness;


	--converts state from std_logic_vector to state array (s0, s1, s2, s3...)
	function slvToStateArray(slvIn : std_logic_vector(16*w-1 downto 0))
	return stateArrayType is
		variable arrOut : stateArrayType;
	begin
		arrOut(0) := slvIn(16*w-1 downto 15*w);
		arrOut(1) := slvIn(15*w-1 downto 14*w);
		arrOut(2) := slvIn(14*w-1 downto 13*w);
		arrOut(3) := slvIn(13*w-1 downto 12*w);

		arrOut(4) := slvIn(12*w-1 downto 11*w);
		arrOut(5) := slvIn(11*w-1 downto 10*w);
		arrOut(6) := slvIn(10*w-1 downto 9*w);
		arrOut(7) := slvIn(9*w-1 downto 8*w);

		arrOut(8) := slvIn(8*w-1 downto 7*w);
		arrOut(9) := slvIn(7*w-1 downto 6*w);
		arrOut(10) := slvIn(6*w-1 downto 5*w);
		arrOut(11) := slvIn(5*w-1 downto 4*w);

		arrOut(12) := slvIn(4*w-1 downto 3*w);
		arrOut(13) := slvIn(3*w-1 downto 2*w);
		arrOut(14) := slvIn(2*w-1 downto 1*w);
		arrOut(15) := slvIn(1*w-1 downto 0*w);
		return arrOut;
	end slvToStateArray;


	--converts state from state array to std_logic_vector
	function stateArrayToSlv(arrIn : stateArrayType)
	return std_logic_vector is
		variable slvOut : std_logic_vector(16*w-1 downto 0);
	begin
		slvOut(16*w-1 downto 15*w) := arrIn(0);
		slvOut(15*w-1 downto 14*w) := arrIn(1);
		slvOut(14*w-1 downto 13*w) := arrIn(2);
		slvOut(13*w-1 downto 12*w) := arrIn(3);

		slvOut(12*w-1 downto 11*w) := arrIn(4);
		slvOut(11*w-1 downto 10*w) := arrIn(5);
		slvOut(10*w-1 downto 9*w) := arrIn(6);
		slvOut(9*w-1 downto 8*w) := arrIn(7);

		slvOut(8*w-1 downto 7*w) := arrIn(8);
		slvOut(7*w-1 downto 6*w) := arrIn(9);
		slvOut(6*w-1 downto 5*w) := arrIn(10);
		slvOut(5*w-1 downto 4*w) := arrIn(11);

		slvOut(4*w-1 downto 3*w) := arrIn(12);
		slvOut(3*w-1 downto 2*w) := arrIn(13);
		slvOut(2*w-1 downto 1*w) := arrIn(14);
		slvOut(1*w-1 downto 0*w) := arrIn(15);
		return slvOut;
	end stateArrayToSlv;

	-- converts std_logic_vector to array with 4 elements 
	function slvToArray4(slvIn : std_logic_vector(4*w-1 downto 0))
	return arrayType4 is
		variable arrOut : arrayType4;
	begin
		arrOut(0) := slvIn(4*w-1 downto 3*w);
		arrOut(1) := slvIn(3*w-1 downto 2*w);
		arrOut(2) := slvIn(2*w-1 downto 1*w);
		arrOut(3) := slvIn(1*w-1 downto 0*w);

		return arrOut;
	end function slvToArray4;

	-- converts 4 el. array back to std_logic_vector
	function array4ToSlv(arrIn : arrayType4)
	return std_logic_vector is
		variable slvOut : std_logic_vector(4*w-1 downto 0);
	begin
		slvOut(4*w-1 downto 3*w) := arrIn(0);
		slvOut(3*w-1 downto 2*w) := arrIn(1);
		slvOut(2*w-1 downto 1*w) := arrIn(2);
		slvOut(1*w-1 downto 0*w) := arrIn(3);

		return slvOut;
	end array4ToSlv;

	-- converts std_logic_vector to array with 12 elements 
	function slvToArray12(slvIn : std_logic_vector(12*w-1 downto 0))
	return arrayType12 is
		variable arrOut : arrayType12;
	begin
		arrOut(0) := slvIn(12*w-1 downto 11*w);
		arrOut(1) := slvIn(11*w-1 downto 10*w);
		arrOut(2) := slvIn(10*w-1 downto 9*w);
		arrOut(3) := slvIn(9*w-1 downto 8*w);

		arrOut(4) := slvIn(8*w-1 downto 7*w);
		arrOut(5) := slvIn(7*w-1 downto 6*w);
		arrOut(6) := slvIn(6*w-1 downto 5*w);
		arrOut(7) := slvIn(5*w-1 downto 4*w);

		arrOut(8) := slvIn(4*w-1 downto 3*w);
		arrOut(9) := slvIn(3*w-1 downto 2*w);
		arrOut(10) := slvIn(2*w-1 downto 1*w);
		arrOut(11) := slvIn(1*w-1 downto 0*w);

		return arrOut;
	end slvToArray12;

	-- converts 12 el. array back to std_logic_vector
	function array12ToSlv(arrIn : arrayType12)
	return std_logic_vector is
		variable slvOut : std_logic_vector(12*w-1 downto 0);
	begin
		slvOut(12*w-1 downto 11*w) := arrIn(0);
		slvOut(11*w-1 downto 10*w) := arrIn(1);
		slvOut(10*w-1 downto 9*w) := arrIn(2);
		slvOut(9*w-1 downto 8*w) := arrIn(3);

		slvOut(8*w-1 downto 7*w) := arrIn(4);
		slvOut(7*w-1 downto 6*w) := arrIn(5);
		slvOut(6*w-1 downto 5*w) := arrIn(6);
		slvOut(5*w-1 downto 4*w) := arrIn(7);
	
		slvOut(4*w-1 downto 3*w) := arrIn(8);
		slvOut(3*w-1 downto 2*w) := arrIn(9);
		slvOut(2*w-1 downto 1*w) := arrIn(10);
		slvOut(1*w-1 downto 0*w) := arrIn(11);

		return slvOut;
	end array12ToSlv;

end package body;
