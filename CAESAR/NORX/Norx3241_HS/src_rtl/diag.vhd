library ieee;
-------------------------------------------------------------------------------
--! @file       diag.vhd
--! @brief      diagonal operation, part of the core permutation F.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

use ieee.std_logic_1164.all;
use work.norx_pkg.all;

entity diag is
	port(
		state_diag_pre : in std_logic_vector(16*w-1 downto 0);
		state_diag_post : out std_logic_vector(16*w-1 downto 0)
	);
end diag;

architecture RTL of diag is
	signal sInArr, sOutArr : stateArrayType;
begin
	-- std_logic_vector to state array for easier indexing
	sInArr <= slvToStateArray(state_diag_pre);

	-- G operation for each diagonal
	g0 : entity work.gCircuit port map(
		ai => sInArr(0),
		bi => sInArr(5),
		ci => sInArr(10),
		di => sInArr(15),

		ao => sOutArr(0),
		bo => sOutArr(5),
		co => sOutArr(10),
		do => sOutArr(15)
	);

	g1 : entity work.gCircuit port map(
		ai => sInArr(1),
		bi => sInArr(6),
		ci => sInArr(11),
		di => sInArr(12),

		ao => sOutArr(1),
		bo => sOutArr(6),
		co => sOutArr(11),
		do => sOutArr(12)
	);

	g2 : entity work.gCircuit port map(
		ai => sInArr(2),
		bi => sInArr(7),
		ci => sInArr(8),
		di => sInArr(13),

		ao => sOutArr(2),
		bo => sOutArr(7),
		co => sOutArr(8),
		do => sOutArr(13)
	);

	g3 : entity work.gCircuit port map(
		ai => sInArr(3),
		bi => sInArr(4),
		ci => sInArr(9),
		di => sInArr(14),

		ao => sOutArr(3),
		bo => sOutArr(4),
		co => sOutArr(9),
		do => sOutArr(14)
	);
	
	-- state array back to slv again
	state_diag_post <= stateArrayToSlv(sOutArr);
	
end RTL;