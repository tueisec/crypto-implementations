-------------------------------------------------------------------------------
--! @file       StateRegister.vhd
--! @brief      Shift Register to store internal Norx state words.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.design_pkg.all;

entity StateRegister is
	port(
		clk       : in std_logic;
		shift_en  : in std_logic;
		state_in  : in std_logic_vector(w-1 downto 0);
		state_out : out std_logic_vector(w-1 downto 0)
	);
end StateRegister;


architecture Structure of StateRegister is

	signal state_block : std_logic_vector(4*w-1 downto 0);

begin

	-- rotate state w bit, state_in is one of the permutated state blocks or init vector
	process(clk)
	begin
		if rising_edge(clk) then
			if shift_en = '1' then
				state_block(4*w-1 downto 3*w) <= state_block(3*w-1 downto 2*w);
				state_block(3*w-1 downto 2*w) <= state_block(2*w-1 downto 1*w);
				state_block(2*w-1 downto 1*w) <= state_block(1*w-1 downto 0*w);
				state_block(1*w-1 downto 0*w) <= state_in;
			end if;
		end if;
	end process;

	-- first w bit as output
	state_out <= state_block(4*w-1 downto 3*w);

end Structure;
