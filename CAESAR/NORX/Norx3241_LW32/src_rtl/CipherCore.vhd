-------------------------------------------------------------------------------
--! @file       CipherCore.vhd
--! @brief      Ciphercore implementing Norx3231v3 lightweight, with iterative use of the G function.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.design_pkg.all;
use work.caesar_lwapi_pkg.all;

entity CipherCore is
    Port ( 
            clk             : in   STD_LOGIC;
            rst             : in   STD_LOGIC;
            --PreProcessor===============================================
            ----!key----------------------------------------------------
            key             : in   STD_LOGIC_VECTOR (SW      -1 downto 0);
            key_valid       : in   STD_LOGIC;
            key_ready       : out  STD_LOGIC;
            ----!Data----------------------------------------------------
            bdi             : in   STD_LOGIC_VECTOR (PW       -1 downto 0);
            bdi_valid       : in   STD_LOGIC;
            bdi_ready       : out  STD_LOGIC;
            bdi_partial     : in   STD_LOGIC;
            bdi_pad_loc     : in   STD_LOGIC_VECTOR (PWdiv8   -1 downto 0);
            bdi_valid_bytes : in   STD_LOGIC_VECTOR (PWdiv8   -1 downto 0);
            bdi_size        : in   STD_LOGIC_VECTOR (3       -1 downto 0);
            bdi_eot         : in   STD_LOGIC;
            bdi_eoi         : in   STD_LOGIC;
            bdi_type        : in   STD_LOGIC_VECTOR (4       -1 downto 0);
            decrypt_in      : in   STD_LOGIC;
            key_update      : in   STD_LOGIC;
            --!Post Processor=========================================
            bdo             : out  STD_LOGIC_VECTOR (PW       -1 downto 0);
            bdo_valid       : out  STD_LOGIC;
            bdo_ready       : in   STD_LOGIC;
            --bdo_size        : out  STD_LOGIC_VECTOR (3 -1 downto 0);
            bdo_type        : out  STD_LOGIC_VECTOR (4       -1 downto 0);
            bdo_valid_bytes : out  STD_LOGIC_VECTOR (PWdiv8   -1 downto 0);
            end_of_block    : out  STD_LOGIC;
            decrypt_out     : out  STD_LOGIC;
            --msg_auth signals
            msg_auth_valid  : out std_logic;
            msg_auth_ready  : in std_logic;
            msg_auth        : out std_logic
         );
            
end CipherCore;

architecture structure of CipherCore is

	-- endianness conversion
	signal key_little	: std_logic_vector(w-1 downto 0);
	signal bdi_little	: std_logic_vector(w-1 downto 0);
	signal bdo_little	: std_logic_vector(w-1 downto 0);
	signal bdo_big		: std_logic_vector(w-1 downto 0);

	signal bdi_little_pad	: std_logic_vector(w-1 downto 0);

	signal bdi_ready_l	: std_logic;
	signal bdo_valid_l	: std_logic;

	signal eot_r, eoi_r	: std_logic;
	signal decrypt_r	: std_logic;

	-- shift register signals
	signal shift_enable	: std_logic_vector(3 downto 0);
	signal state_reg_in0	: std_logic_vector(w-1 downto 0);
	signal state_reg_in1	: std_logic_vector(w-1 downto 0);
	signal state_reg_in2	: std_logic_vector(w-1 downto 0);
	signal state_reg_in3	: std_logic_vector(w-1 downto 0);
	signal state_reg_out0	: std_logic_vector(w-1 downto 0);
	signal state_reg_out1	: std_logic_vector(w-1 downto 0);
	signal state_reg_out2	: std_logic_vector(w-1 downto 0);
	signal state_reg_out3	: std_logic_vector(w-1 downto 0);
	signal sb0_sel		: std_logic_vector(1 downto 0);
	signal sb1_sel		: std_logic_vector(1 downto 0);
	signal sb2_sel		: std_logic_vector(1 downto 0);
	signal sb3_sel		: std_logic_vector(1 downto 0);
	signal xor_data_post	: std_logic_vector(w-1 downto 0);
	signal xor_data_pre	: std_logic_vector(w-1 downto 0);
	signal xor_data_post_filt : std_logic_vector(w-1 downto 0);
	signal xor_data_post_pad  : std_logic_vector(w-1 downto 0);
	signal init_const0	: std_logic_vector(w-1 downto 0);
	signal init_const1	: std_logic_vector(w-1 downto 0);
	signal xordata_b2s	: std_logic_vector(w-1 downto 0);
	signal sel_xordata_b2s	: std_logic;

	-- g unit signals
	signal g_out0			: std_logic_vector(w-1 downto 0);
	signal g_out1			: std_logic_vector(w-1 downto 0);
	signal g_out2			: std_logic_vector(w-1 downto 0);
	signal g_out3			: std_logic_vector(w-1 downto 0);
	signal g_out3_key		: std_logic_vector(w-1 downto 0);
	signal state_reg_out3_tag	: std_logic_vector(w-1 downto 0);
	signal tag			: std_logic_vector(7 downto 0);
	signal tag_g_en			: std_logic;
	signal key_g_en			: std_logic;
	-- g counter
	signal f_done			: std_logic;
	signal cnt_g			: std_logic_vector(3 downto 0);
	signal cnt_g_en			: std_logic;
	signal cnt_g_res		: std_logic;
	signal g_en			: std_logic_vector(3 downto 0);

	-- key register signals
	signal key_en		: std_logic;
	signal key_new		: std_logic;
	signal key_stored_chunk	: std_logic_vector(w-1 downto 0);

	-- counter
	signal cnt_res		: std_logic;
	signal cnt_en		: std_logic;
	signal cnt_val		: std_logic_vector(3 downto 0);
	signal cnt0, cnt3	: std_logic;
	signal cnt7, cnt11	: std_logic;

	signal sel_blk		: std_logic_vector(1 downto 0);
	signal out_sel		: std_logic;

	signal no_data_left	: std_logic;
	signal need01pad	: std_logic;

	-- state machine
	type state_type is (IDLE, LOAD_KEY, LOAD_NPUB, INIT, WAIT_BDI, ABSORB_F, ABSORB_D, ENCDEC_F, ENCDEC_D, FINAL, TAG_OUT);
	signal state		: state_type;


	-- TODO: debug signals
	signal sel_f : std_logic_vector(1 downto 0);

begin

	-------------
	-- outputs --
	-------------
	-- forward local outputs to global outputs
	bdi_ready <= bdi_ready_l;
	bdo_valid <= bdo_valid_l;
	decrypt_out <= decrypt_r;


	-- set high impedance, if output not needed (not valid), prevent leaking state information
	with bdo_valid_l select bdo <=
		bdo_big when '1',
		(others => 'Z') when others;
	
	-- select output
	with out_sel select bdo_little <=
		state_reg_out3 when '0',		-- tag
		xor_data_post_filt when others;		-- pt/ct


	------------------------------------------------------------
	-- endianness conversion w bit (little->big, big->little) --
	------------------------------------------------------------
	gen_little : for i in 0 to 3 generate
		-- input convert - big -> little
		key_little((i+1)*8-1 downto i*8) <= key((4-i)*8-1 downto (4-i-1)*8);
		bdi_little((i+1)*8-1 downto i*8) <= bdi((4-i)*8-1 downto (4-i-1)*8);
		-- output convert - little -> big
		bdo_big((i+1)*8-1 downto i*8) <= bdo_little((4-i)*8-1 downto (4-i-1)*8);
	end generate gen_little;
	

	---------------------------
	-- input padding, 32 bit --
	---------------------------
	pad_proc : process(bdi_little, bdi_valid_bytes, cnt0, cnt11, no_data_left, need01pad) 
	begin
		if no_data_left = '0' then
			bdi_little_pad(w-1 downto w-8)  <= bdi_little(w-1 downto w-8) 
				or (x"81" and (7 downto 0 => (cnt11 and not bdi_valid_bytes(0) and bdi_valid_bytes(1))))	-- when last byte, pad with either 81 (only last byte not valid)
				or (x"80" and (7 downto 0 => (cnt11 and not bdi_valid_bytes(0) and not bdi_valid_bytes(1))))	-- or 80 (more than last byte not valild)
				or (x"01" and (7 downto 0 => (not cnt11 and not bdi_valid_bytes(0) and bdi_valid_bytes(1))));
			bdi_little_pad(w-9 downto w-16) <= bdi_little(w-9 downto w-16)
				or (x"01" and (7 downto 0 => (not bdi_valid_bytes(1) and bdi_valid_bytes(2))));			-- when invalid and last byte valid, pad with 01
			bdi_little_pad(w-17 downto w-24) <= bdi_little(w-17 downto w-24)
				or (x"01" and (7 downto 0 => (not bdi_valid_bytes(2) and bdi_valid_bytes(3))));			-- when invalid and last byte valid, pad with 01
			bdi_little_pad(w-25 downto 0)   <= bdi_little(w-25 downto 0);
		else
			bdi_little_pad(w-1 downto w-8)   <= (x"80" and (7 downto 0 => cnt11));					-- 80 for last byte
			bdi_little_pad(w-9 downto w-16)  <= x"00";								-- 00 in between
			bdi_little_pad(w-17 downto w-24) <= x"00";	
			bdi_little_pad(w-25 downto 0)    <= (x"01" and (7 downto 0 => (cnt0 or need01pad)));			-- add 01 pad when first block or first block after full block
		end if;
	end process pad_proc;


	--------------------------------------
	-- xor data (absorb encode, decode) --
	--------------------------------------
	-- select stateblock for encode, decode, absorb operation
	with cnt_val(3 downto 2) select xor_data_pre <=
		state_reg_out0 when "00",			-- cycle 0 to 3: stateblock 0, state word 0 to 3
		state_reg_out1 when "01",			-- cycle 4 to 7: stateblock 1, state word 4 to 7
		state_reg_out2 when others;			-- cycle 8 to 11: stateblock 2, state word 8 to 11

	-- state xor bdi
	xor_data_post <= xor_data_pre xor bdi_little_pad;

	-- set invalid bytes (bdi_valid_bytes = 0) to zero
	xor_data_post_filt <= 	(xor_data_post(32-1 downto 24) and (7 downto 0 => (bdi_valid_bytes(0) and not no_data_left))) &
				(xor_data_post(24-1 downto 16) and (7 downto 0 => (bdi_valid_bytes(1) and not no_data_left))) &
				(xor_data_post(16-1 downto 8) and (7 downto 0 => (bdi_valid_bytes(2) and not no_data_left))) &
				(xor_data_post(8-1 downto 0) and (7 downto 0 => (bdi_valid_bytes(3) and not no_data_left)));

	-- pad filtered bytes (for decode step needed: state xor pad(M))
	xor_data_post_pad <= xor_data_post_filt or (	(bdi_little_pad(32-1 downto 24) and (7 downto 0 => (not bdi_valid_bytes(0) or no_data_left))) &
							(bdi_little_pad(24-1 downto 16) and (7 downto 0 => (not bdi_valid_bytes(1) or no_data_left))) &
							(bdi_little_pad(16-1 downto 8) and (7 downto 0 => (not bdi_valid_bytes(2) or no_data_left))) &
							(bdi_little_pad(8-1 downto 0) and (7 downto 0 => (not bdi_valid_bytes(3) or no_data_left))));
	
	-- select data to write back to state
	with sel_xordata_b2s select xordata_b2s <=
		xor_data_post when '0',				-- encode, absorb
		xor_data_pre xor xor_data_post_pad when others;	-- decode
	

	-----------------------------------
	-- G unit (core of the algorithm)--
	-----------------------------------
	g_unit : entity work.gCircuit port map(
		ai => state_reg_out0,		-- g unit input words a,b,c,d
		bi => state_reg_out1,
		ci => state_reg_out2,
		di => state_reg_out3_tag,
		ao => g_out0,			-- g unit output words a,b,c,d
		bo => g_out1,
		co => g_out2,
		do => g_out3
	);

	-- add tag to 3rd input signal (s15)
	-- key_en and tag_en enable xors input signal, 
	-- 0: xor does not change state
	-- 1: tag or key will be inserted into state
	state_reg_out3_tag <= state_reg_out3(w-1 downto 8) & (state_reg_out3(7 downto 0) xor (tag and (tag'range => tag_g_en)));	
	-- add key to 3rd output signal
	g_out3_key <= g_out3 xor (key_stored_chunk and (key_stored_chunk'range => key_g_en));


	--------------------------------
	-- state w-bit shift register --
	--------------------------------
	stateblock0 : entity work.StateRegister port map(
		clk		=> clk,
		shift_en	=> shift_enable(0),
		state_in	=> state_reg_in0,
		state_out	=> state_reg_out0		
	);
	with sb0_sel select state_reg_in0 <= 
		bdi_little	when "00",		-- init with nonce
		g_out0		when "01",		-- use g unit
		xordata_b2s	when others;		-- abs, enc, dec step, data fed back to state

	stateblock1 : entity work.StateRegister port map(
		clk		=> clk,
		shift_en	=> shift_enable(1),
		state_in	=> state_reg_in1,
		state_out	=> state_reg_out1		
	);
	with sb1_sel select state_reg_in1 <=
		key_stored_chunk when "00",		-- init with key
		g_out1		when "01",		-- use g unit
		xordata_b2s	when "10",		-- abs, enc, dec step, data fed back to state
		state_reg_out1	when others;		-- only rotate without any change

	stateblock2 : entity work.StateRegister port map(
		clk		=> clk,
		shift_en	=> shift_enable(2),
		state_in	=> state_reg_in2,
		state_out	=> state_reg_out2		
	);
	with sb2_sel select state_reg_in2 <=
		init_const0	when "00",		-- init with constants
		g_out2		when "01",		-- use g unit
		xordata_b2s	when "10",		-- abs, enc, dec step, data fed back to state
		state_reg_out2	when others;		-- only rotate without any change

	stateblock3 : entity work.StateRegister port map(
		clk		=> clk,
		shift_en	=> shift_enable(03),
		state_in	=> state_reg_in3,
		state_out	=> state_reg_out3		
	);
	with sb3_sel select state_reg_in3 <=
		init_const1	when "00",		-- init with constants
		g_out3_key	when "01",		-- use g unit
		state_reg_out3	when others;		-- only rotate without any change


	-- constants for state initialization, state block 2
	with cnt_val(1 downto 0) select init_const0 <=
		x"A3D8D930" when "00",		-- word 0 at count 0
		x"3FA8B72C" when "01",		-- word 1 at count 1
		x"ED84EB49" when "10",		-- ...
		x"EDCA4787" when others;

	-- constants for state initialization, state block 3
	with cnt_val(1 downto 0) select init_const1 <=
		x"335463EB" xor std_logic_vector(to_unsigned(w, 32)) when "00",		-- xor with w,l,p,t
		x"F994220B" xor std_logic_vector(to_unsigned(l, 32)) when "01",
		x"BE0BF5C9" xor std_logic_vector(to_unsigned(p, 32)) when "10",
		x"D7C49104" xor std_logic_vector(to_unsigned(t, 32)) when others;


	------------------
	-- key register --
	------------------
	key_reg : entity work.key_register port map(
		clk		=> clk,
		key_en		=> key_en,		-- enable key rotation
		key_new		=> key_new,		-- load new key when 1
		key_in		=> key_little,		-- new key fed into register
		key_out		=> key_stored_chunk	-- stored key chunk read from register (4 w-bit key chunks, one after another)
	);


	------------------
	-- main counter --
	------------------
	m_cnt_proc : process(clk)
	begin
		if rising_edge(clk) then
			if cnt_res = '1' then
				cnt_val <= x"0";
			elsif cnt_en = '1' then
				cnt_val <= std_logic_vector(unsigned(cnt_val) + 1);
			end if;
		end if;
	end process m_cnt_proc;

	cnt0 <= not cnt_val(3) and not cnt_val(2) and not cnt_val(1) and not cnt_val(0);
	cnt3 <= cnt_val(1) and cnt_val(0);
	cnt7 <= cnt_val(2) and cnt_val(1) and cnt_val(0);
	cnt11 <= cnt_val(3) and cnt_val(1) and cnt_val(0);

	------------------------------
	-- g counter for 8 G rounds --
	------------------------------
	g_cnt_proc : process(clk)
	begin
		if rising_edge(clk) then
			if cnt_g_res = '1' then
				cnt_g <= x"0";
			elsif cnt_g_en = '1' then
				cnt_g <= std_logic_vector(unsigned(cnt_g) + 1);
			end if;
		end if;
	end process g_cnt_proc;
	
	f_done <= cnt_g(3) and cnt_g(2) and cnt_g(0);

	-- g round control signals
	g_en(3) <= not cnt_g(3) or not cnt_g(2);
	g_en(2) <= not cnt_g(2) or not cnt_g(0) or cnt_g(1);
	g_en(1) <= not cnt_g(2) or cnt_g(1) or cnt_g(3);
	g_en(0) <= (not cnt_g(2) and not cnt_g(1)) or (not cnt_g(2) and not cnt_g(0)) or (not cnt_g(3) and cnt_g(1) and cnt_g(0));

	-- 
	save_handsh_proc : process(clk)
	begin
		if rising_edge(clk) then
			if (bdi_valid and bdi_ready_l) = '1' then
				eoi_r <= bdi_eoi;
				eot_r <= bdi_eot;
				decrypt_r <= decrypt_in;
			end if;
		end if;
	end process save_handsh_proc;

	no_data_left_proc : process(clk)
	begin
		if rising_edge(clk) then
			if state = WAIT_BDI then
				no_data_left <= '0';
			elsif (bdi_valid and bdi_ready_l) = '1' then
				no_data_left <= bdi_eot;
			end if;
		end if;
	end process no_data_left_proc;

	pad01_proc : process(clk)
	begin
		if rising_edge(clk) then
			need01pad <= bdi_valid and bdi_ready_l and bdi_eot and bdi_valid_bytes(0);
		end if;
	end process pad01_proc;

	----------------------------------------
	-- sequential logic for state machine --
	---------------------------------------- 
	sequ_fsm : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				state <= IDLE;
			else
			case state is

			-- wait for new task
			when IDLE =>
				if key_valid = '1' then
					state <= LOAD_KEY;
				elsif bdi_valid = '1' then
					state <= LOAD_NPUB;
				end if;

			-- load key, 4*32 bit, continue to load nonce when counter reaches 3
			when LOAD_KEY =>
				if cnt3 = '1' then
					state <= LOAD_NPUB;
				end if;
			
			-- load npub (nonce), 4*32 bit, continue to init step when done cnt=3
			when LOAD_NPUB =>
				if cnt3 = '1' then
					state <= INIT;
				end if;

			-- init norx state, 4*F permutation (l=4), if no input follows, go to finalize, else wait for input
			when INIT =>
				if (cnt3 and f_done) = '1' then
					if eoi_r = '1' then
						state <= FINAL;
					else
						state <= WAIT_BDI;
					end if;
				end if;

			-- wait for bdi_valid, then either absorb or encode/decode
			when WAIT_BDI =>
				if bdi_valid = '1' then
					if bdi_type = HDR_AD then
						state <= ABSORB_F;
					else
						state <= ENCDEC_F;
					end if;
				end if;

			-- absorb F permutations: 4 cycles for 4 F permutations + tag
			when ABSORB_F =>
				if (cnt3 and f_done) = '1' then
					state <= ABSORB_D;
				end if;
			
			-- absorb data: 12 cycles for reading and 'xoring' 12 data words into state
			-- if eot_r and eoi_r (current data was last block) go to finalize
			-- if last block was full block without padding and last lock of this type, do absorb with full padding block
			-- if still data left, go back to WAIT_BDI
			when ABSORB_D =>
				if (cnt11 and (bdi_valid or no_data_left)) = '1' then
					if (eot_r and eoi_r) = '1' then
						state <= FINAL;
					elsif (bdi_eot and not no_data_left and bdi_valid_bytes(0)) = '1' then
						state <= ABSORB_F;
					else
						state <= WAIT_BDI;
					end if;
				end if;

			-- similar to absorb_f
			when ENCDEC_F =>
				if (cnt3 and f_done) = '1' then
					state <= ENCDEC_D;
				end if;

			-- similar to absorb_d
			when ENCDEC_D =>
				if (cnt11 and ((bdi_valid and bdo_ready) or no_data_left)) = '1' then
					if (bdi_eot and not no_data_left and bdi_valid_bytes(0)) = '1' then
						state <= ENCDEC_F;
					elsif (bdi_eoi or eoi_r) = '1' then
						state <= FINAL;
					else
						state <= WAIT_BDI;
					end if;
				end if;
			
			-- finalize: 4 F, xor key, 4 F, xor key (8 cycles)
			when FINAL =>
				if (cnt7 and f_done) = '1' then
					state <= TAG_OUT;
				end if;

			-- read tag: 4 steps (4*32 bit)
			when TAG_OUT =>
				if cnt3 = '1' then
					state <= IDLE;
				end if;

			end case;
			end if;
		end if;
	end process sequ_fsm; 

	-- TODO: test
	sel_f <= ((cnt_g(3) and cnt_g(1) and cnt_g(0)) or (cnt_g(2) and not cnt_g(1)) or (cnt_g(2) and not cnt_g(0))) & '1';

	-- outputs and control signals
	comb_fsm : process(	state,
				bdi_valid, key_valid, bdo_ready, decrypt_r, no_data_left,
				bdi_eot, bdi_eoi,
				cnt0, cnt3, cnt7,
				f_done, g_en, cnt_g, sel_f, key_g_en)
	begin
	case state is

		when IDLE =>
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= '0';
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= '0';
			cnt_res		<= '1';
			cnt_g_en	<= '0';
			cnt_g_res	<= '1';
			-- key register
			key_en		<= '0';
			key_new		<= '-';
			-- state register signals
			shift_enable	<= "0000";
			sb0_sel		<= "--";
			sb1_sel		<= "--";
			sb2_sel		<= "--";
			sb3_sel		<= "--";
			sel_xordata_b2s	<= '-';
			-- tag
			tag		<= "--------";
			tag_g_en	<= '0';
			key_g_en	<= '0';
			-- output
			out_sel		<= '-';

		when LOAD_KEY =>
			-- handshake signals
			key_ready	<= '1';
			bdi_ready_l	<= '0';
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= key_valid;
			cnt_res		<= cnt3;
			cnt_g_en	<= '0';
			cnt_g_res	<= '0';
			-- key register
			key_en		<= key_valid;
			key_new		<= '1';
			-- state register signals
			shift_enable	<= "0000";
			sb0_sel		<= "--";
			sb1_sel		<= "--";
			sb2_sel		<= "--";
			sb3_sel		<= "--";
			sel_xordata_b2s	<= '-';
			-- tag
			tag		<= "--------";
			tag_g_en	<= '0';
			key_g_en	<= '0';
			-- output
			out_sel		<= '-';

		when LOAD_NPUB =>
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= '1';
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= bdi_valid;
			cnt_res		<= cnt3;
			cnt_g_en	<= '0';
			cnt_g_res	<= '0';
			-- key register
			key_en		<= bdi_valid;
			key_new		<= '0';
			-- state register signals
			shift_enable	<= bdi_valid & bdi_valid & bdi_valid & bdi_valid;
			sb0_sel		<= "00";
			sb1_sel		<= "00";
			sb2_sel		<= "00";
			sb3_sel		<= "00";
			sel_xordata_b2s	<= '-';
			-- tag
			tag		<= "--------";
			tag_g_en	<= '0';
			key_g_en	<= '0';
			-- output
			out_sel		<= '-';

		when INIT =>
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= '0';
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= f_done;
			cnt_res		<= cnt3 and f_done;
			cnt_g_en	<= '1';
			cnt_g_res	<= f_done;
			-- key register
			key_en		<= cnt3 and (cnt_g(3) xor cnt_g(2));
			key_new		<= '0';
			-- state register signals
			shift_enable	<= g_en;
			sb0_sel		<= sel_f;
			sb1_sel		<= sel_f;
			sb2_sel		<= sel_f;
			sb3_sel		<= sel_f;
			sel_xordata_b2s	<= '-';
			-- tag and key for G unit
			tag		<= "--------";
			tag_g_en	<= '0';
			key_g_en	<= cnt3 and (cnt_g(3) xor cnt_g(2));
			-- output
			out_sel		<= '-';

		when WAIT_BDI => 
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= '0';
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= '0';
			cnt_res		<= '0';
			cnt_g_en	<= '0';
			cnt_g_res	<= '0';
			-- key register
			key_en		<= '0';
			key_new		<= '-';
			-- state register signals
			shift_enable	<= "0000";
			sb0_sel		<= "--";
			sb1_sel		<= "--";
			sb2_sel		<= "--";
			sb3_sel		<= "--";
			sel_xordata_b2s	<= '-';
			-- tag
			tag		<= "--------";
			tag_g_en	<= '0';
			key_g_en	<= '0';
			-- output
			out_sel		<= '-';

		when ABSORB_F =>
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= '0';
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= f_done;
			cnt_res		<= cnt3 and f_done;
			cnt_g_en	<= '1';
			cnt_g_res	<= f_done;
			-- key register
			key_en		<= '0';
			key_new		<= '-';
			-- state register signals
			shift_enable	<= g_en;
			sb0_sel		<= sel_f;
			sb1_sel		<= sel_f;
			sb2_sel		<= sel_f;
			sb3_sel		<= sel_f;
			sel_xordata_b2s	<= '-';
			-- tag
			tag		<= x"01";
			tag_g_en	<= cnt0 and (cnt_g(0) and cnt_g(1) and not cnt_g(2) and not cnt_g(3));
			key_g_en	<= '0';
			-- output
			out_sel		<= '-';

		when ABSORB_D =>
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= not no_data_left;
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= bdi_valid or no_data_left;
			cnt_res		<= cnt11 and (bdi_valid or no_data_left);
			cnt_g_en	<= '0';
			cnt_g_res	<= '0';
			-- key register
			key_en		<= '0';
			key_new		<= '-';
			-- state register signals
			shift_enable	<= "0" & (cnt_val(3) and not cnt_val(2)) & (not cnt_val(3) and cnt_val(2)) & (not cnt_val(3) and not cnt_val(2));
			sb0_sel		<= "10";
			sb1_sel		<= "10";
			sb2_sel		<= "10";
			sb3_sel		<= "--";
			sel_xordata_b2s	<= '0';
			-- tag
			tag		<= "--------";
			tag_g_en	<= '0';
			key_g_en	<= '0';
			-- output
			out_sel		<= '-';

		when ENCDEC_F =>
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= '0';
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= f_done;
			cnt_res		<= cnt3 and f_done;
			cnt_g_en	<= '1';
			cnt_g_res	<= f_done;
			-- key register
			key_en		<= '0';
			key_new		<= '-';
			-- state register signals
			shift_enable	<= g_en;
			sb0_sel		<= sel_f;
			sb1_sel		<= sel_f;
			sb2_sel		<= sel_f;
			sb3_sel		<= sel_f;
			sel_xordata_b2s	<= '-';
			-- tag
			tag		<= x"02";
			tag_g_en	<= cnt0 and (cnt_g(0) and cnt_g(1) and not cnt_g(2) and not cnt_g(3));
			key_g_en	<= '0';
			-- output
			out_sel		<= '-';

		when ENCDEC_D =>
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= not no_data_left;
			bdo_valid_l	<= bdi_valid and not no_data_left;
			end_of_block	<= bdi_eot;
			-- counter signals
			cnt_en		<= (bdi_valid and bdo_ready) or no_data_left;
			cnt_res		<= cnt11 and ((bdi_valid and bdo_ready) or no_data_left);
			cnt_g_en	<= '0';
			cnt_g_res	<= '0';
			-- key register
			key_en		<= '0';
			key_new		<= '-';
			-- state register signals
			shift_enable	<= "0" & (cnt_val(3) and not cnt_val(2)) & (not cnt_val(3) and cnt_val(2)) & (not cnt_val(3) and not cnt_val(2));
			sb0_sel		<= "10";
			sb1_sel		<= "10";
			sb2_sel		<= "10";
			sb3_sel		<= "--";
			sel_xordata_b2s	<= decrypt_r;
			-- tag
			tag		<= "--------";
			tag_g_en	<= '0';
			key_g_en	<= '0';
			-- output
			out_sel		<= '1';

		when FINAL =>
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= '0';
			bdo_valid_l	<= '0';
			end_of_block	<= '-';
			-- counter signals
			cnt_en		<= f_done;
			cnt_res		<= cnt7 and f_done;
			cnt_g_en	<= '1';
			cnt_g_res	<= f_done;
			-- key register
			key_en		<= cnt3 and (cnt_g(3) xor cnt_g(2));
			key_new		<= '0';
			-- state register signals
			shift_enable	<= g_en;
			sb0_sel		<= sel_f;
			sb1_sel		<= sel_f;
			sb2_sel		<= sel_f;
			sb3_sel		<= sel_f;
			sel_xordata_b2s	<= '-';
			-- tag
			tag		<= x"08";
			tag_g_en	<= cnt0 and (cnt_g(0) and cnt_g(1) and not cnt_g(2) and not cnt_g(3));
			key_g_en	<= cnt3 and (cnt_g(3) xor cnt_g(2));
			-- output
			out_sel		<= '-';

		when TAG_OUT =>			
			-- handshake signals
			key_ready	<= '0';
			bdi_ready_l	<= '0';
			bdo_valid_l	<= '1';
			end_of_block	<= cnt3;
			-- counter signals
			cnt_en		<= bdo_ready;
			cnt_res		<= '0';
			cnt_g_en	<= '0';
			cnt_g_res	<= '0';
			-- key register
			key_en		<= '0';
			key_new		<= '-';
			-- state register signals
			shift_enable	<= bdo_ready & "000";
			sb0_sel		<= "--";
			sb1_sel		<= "--";
			sb2_sel		<= "--";
			sb3_sel		<= "11";
			sel_xordata_b2s	<= '-';
			-- tag
			tag		<= "--------";
			tag_g_en	<= '0';
			key_g_en	<= '0';
			-- output
			out_sel		<= '0';

	end case;
	end process comb_fsm;

end structure;
