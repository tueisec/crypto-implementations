-------------------------------------------------------------------------------
--! @file       key_register.vhd
--! @brief      key register.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.design_pkg.all;

entity key_register is
	port(
		clk	: in std_logic;
		key_en	: in std_logic;
		key_new	: in std_logic;
		key_in	: in std_logic_vector(w-1 downto 0);
		key_out	: out std_logic_vector(w-1 downto 0)
	);
end key_register;

architecture structure of key_register is

	signal key_stored : std_logic_vector(4*w-1 downto 0);
	signal key_loop_or_new : std_logic_vector(w-1 downto 0);

begin

	-- first w bit of key as output
	key_out	<= key_stored(4*w-1 downto 3*w);

	-- key_new = 1 : insert new key chunks from outside
	-- key_new = 0 : only rotate
	with key_new select key_loop_or_new <=
		key_stored(4*w-1 downto 3*w) when '0',
		key_in when others;
	
	-- when key_en, rotate key register w bit 
	shift_proc : process(clk)
	begin
		if rising_edge(clk) then
			if key_en = '1' then 
				key_stored(4*w-1 downto 3*w) <= key_stored(3*w-1 downto 2*w);
				key_stored(3*w-1 downto 2*w) <= key_stored(2*w-1 downto 1*w);
				key_stored(2*w-1 downto 1*w) <= key_stored(w-1 downto 0);
				key_stored(1*w-1 downto 0*w) <= key_loop_or_new;
			end if;
		end if;
	end process shift_proc;

end structure;
