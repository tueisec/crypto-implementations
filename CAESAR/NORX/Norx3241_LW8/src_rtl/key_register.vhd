-------------------------------------------------------------------------------
--! @file       key_register.vhd
--! @brief      Key register for storing key.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.design_pkg.all;

entity key_register is
	port(
		clk	: in std_logic;
		en	: in std_logic;
		sel	: in std_logic;
		key_in	: in std_logic_vector(7 downto 0);
		key_out	: out std_logic_vector(7 downto 0)
	);
end key_register;

architecture structure of key_register is

	signal key_stored 	: std_logic_vector(127 downto 0);
	signal reg_in		: std_logic_vector(7 downto 0);

begin
	reg_gen : for i in 16 downto 2 generate
	process(clk)
	begin
		if rising_edge(clk) then
			if en = '1' then
				key_stored((i*8)-1 downto (i-1)*8) <= key_stored((i-1)*8-1 downto (i-2)*8);
			end if;
		end if;
	end process;
	end generate reg_gen;

	process(clk)
	begin
		if rising_edge(clk) then
			if en = '1' then
				key_stored(7 downto 0) <= reg_in;
			end if;
		end if;
	end process;

	with sel select reg_in <= 
		key_stored(127 downto 120) when '0',
		key_in when others;

	key_out <= key_stored(127 downto 120);

end structure;
