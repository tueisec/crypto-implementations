-------------------------------------------------------------------------------
--! @file       g8.vhd
--! @brief      Norx G Function, part of the core permutation F.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity g8 is
	port(
		clk	: in std_logic;
		cnt_res	: in std_logic;
		cnt_en	: in std_logic;
		is_diag	: in std_logic;
		ai	: in std_logic_vector(7 downto 0);
		bi	: in std_logic_vector(7 downto 0);
		ci	: in std_logic_vector(7 downto 0);
		di	: in std_logic_vector(7 downto 0);
		ar	: in std_logic_vector(7 downto 0);
		br	: in std_logic_vector(7 downto 0);
		cr	: in std_logic_vector(7 downto 0);
		dr	: in std_logic_vector(7 downto 0);
		o0	: out std_logic_vector(7 downto 0);
		o1	: out std_logic_vector(7 downto 0);
		en	: out std_logic_vector(3 downto 0);
		f_mode	: out std_logic_vector(1 downto 0);
		f_ready	: out std_logic;
		sb_sel_rot : out std_logic_vector(1 downto 0);
		v_ready	: out std_logic;
		k_ready	: out std_logic
	);
end g8;

architecture structure of g8 is

	-- inputs for the operations (selected a,b,c,d)
	signal in0_std		: std_logic_vector(7 downto 0);
	signal in1_std		: std_logic_vector(7 downto 0);
	signal in0_rot		: std_logic_vector(7 downto 0);
	signal in1_rot		: std_logic_vector(7 downto 0);

	-- intermediate signals op0
	signal and_sh		: std_logic_vector(7 downto 0);
	-- intermediate signals op1
	signal op0, op1		: std_logic_vector(7 downto 0);
	signal tmp00, tmp01	: std_logic_vector(7 downto 0);
	signal tmp02		: std_logic;
	signal chk00, chk01, chk02	: std_logic_vector(7 downto 0);
	signal tmp10, tmp11	: std_logic_vector(7 downto 0);
	signal tmp12		: std_logic;
	signal chk10, chk11, chk12	: std_logic_vector(7 downto 0);

	-- counter values
	signal cnt_val	: std_logic_vector(7 downto 0);
	signal a, b, c, d, e, f, g : std_logic;

	-- select signals
	signal sel0, sel1	: std_logic;
	signal sel_op		: std_logic_vector(2 downto 0);

	--TODO: change this only for debug
	signal last_bit : std_logic;

	-- 
	signal k_ready_lc	: std_logic;

begin

	-- operation 0
	
	-- TODO: unefficient here
	with cnt_val(1 downto 0) select last_bit <=
		'0' when "11",
		in0_rot(0) and in1_rot(0) when others;

	and_sh	<= (in0_std(6 downto 0) and in1_std(6 downto 0)) & last_bit;
	o0	<= in0_std xor in1_std xor and_sh;

	-- operation 1
	o1 <= op0 xor op1;
	
	with sel_op select op0 <=
		in0_rot when "000",
		tmp00 when "001",
		tmp01 when "010",
		chk00 when "011",
		chk01 when "100",
		chk02 when others;

	with sel_op select op1 <=
		in1_rot when "000",
		tmp10 when "001",
		tmp11 when "010",
		chk10 when "011",
		chk11 when "100",
		chk12 when others;
	
	chk00 <= in0_rot(7 downto 5) & tmp00(7 downto 3);
	chk01 <= tmp01(2 downto 0) & tmp00(7 downto 3);
	chk02 <= in0_rot(7 downto 1) & tmp02;-- & tmp02;

	chk10 <= in1_rot(7 downto 5) & tmp10(7 downto 3);
	chk11 <= tmp11(2 downto 0) & tmp10(7 downto 3);
	chk12 <= in1_rot(7 downto 1) & tmp12;-- & tmp12; 

	-- operation 1 temporary storage of overwritten bytes
	process(clk)
	begin
		if rising_edge(clk) then
			tmp00 <= in0_std;
			tmp01 <= tmp00;
			tmp02 <= tmp01(7);
			tmp10 <= in1_std;
			tmp11 <= tmp10;
			tmp12 <= tmp11(7);
		end if;
	end process;

	-- select inputs
	with sel0 select in0_std <=
		ai when '0',
		ci when others;

	with sel1 select in1_std <=
		bi when '0',
		di when others;
	
	with sel0 select in0_rot <=
		ar when '0',
		cr when others;

	with sel1 select in1_rot <=
		br when '0',
		dr when others;

	------------------------------------------------
	-- counter (one g round consists of XX steps) --
	------------------------------------------------
	process(clk)
	begin
		if rising_edge(clk) then
			if cnt_res = '1' or cnt_val = x"8B" then
				cnt_val <= (others => '0');
			elsif cnt_en = '1' then
				cnt_val <= std_logic_vector(unsigned(cnt_val) + 1);
			end if;
		end if;
	end process;

	---------------------------------------------------------
	-- from counter value (cnt_val) derive control signals --
	---------------------------------------------------------
	
	a <= cnt_val(6);
	b <= cnt_val(5);
	c <= cnt_val(4);
	d <= cnt_val(3);
	e <= cnt_val(2);
	f <= cnt_val(1);
	g <= cnt_val(0);
	
	sel0 <= b;
	sel1 <= b xor c;

	en(0) <= 	not b and not cnt_val(7);
	en(1) <= 	(((not b and not c) or (b and c)) and not cnt_val(7)) or 
			(cnt_val(7) and (is_diag or d));	-- TODO: maybe optimize
	en(2) <= 	(b and not cnt_val(7)) or 
			(cnt_val(7) and (d or e));
	en(3) <= 	((sel1 or (not a and not c)) and not cnt_val(7)) or 
			(cnt_val(7) and (not is_diag or d));
	
	f_mode(0) <= not c and not cnt_val(7);
	f_mode(1) <= c and not cnt_val(7);
	
	sb_sel_rot <= (not c or a) & (not c or b);
	
	sel_op(2) <= (not a and b and f) or (b and f and g);
	sel_op(1) <= (not a and b and not f and g) or (a and not b and f);
	sel_op(0) <= (a and b and f and g) or (not a and not f and g) or (not a and not b and f); 
	
	f_ready <= cnt_val(7) and not a and not b and not c and d and not e and f and g;
	
	v_ready <= k_ready_lc and d and e and f and g;
	k_ready_lc <= not cnt_val(7) and not a and not b and not c;
	k_ready <= k_ready_lc;
end structure;