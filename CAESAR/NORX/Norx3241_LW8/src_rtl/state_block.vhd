-------------------------------------------------------------------------------
--! @file       state_block.vhd
--! @brief      Shift register for storing internal Norx state words.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity state_block is
	generic(
		is_c : boolean := false
	);
	port(
		clk	: in std_logic;
		en	: in std_logic;
		sel_in	: in std_logic_vector(1 downto 0);
		sel_rot	: in std_logic_vector(1 downto 0);
		d_in	: in std_logic_vector(7 downto 0);
		d_init	: in std_logic_vector(7 downto 0);
		g_res	: in std_logic_vector(7 downto 0);
		s_out	: out std_logic_vector(7 downto 0);
		s_rot	: out std_logic_vector(7 downto 0);
		bdi_val	: in std_logic
	);
end state_block;

architecture structure of state_block is

	signal sb		: std_logic_vector(127 downto 0);
	signal sb_in		: std_logic_vector(7 downto 0);
	signal s_dec		: std_logic_vector(7 downto 0);
	signal s_out_loc	: std_logic_vector(7 downto 0);

begin

	s_out_loc	<= sb(127 downto 120) xor d_in; 
	s_out		<= s_out_loc;

	-- s_dec back to state, either d_in (C) when input valid or S xor pad when not valid 
	-- only needed in rate words
	gen0 : if is_c = false generate
	with bdi_val select s_dec <=
		s_out_loc when '0',				-- TODO: two muxes with both s_out_loc input, remove this mux
		d_in when others;
	end generate gen0;

	------------------------------------------------
	-- state register (shifts for 8b every cycle) --
	------------------------------------------------
	gen : for I in 16 downto 2 generate
	process(clk)
	begin
		if rising_edge(clk) then
			if en = '1' then
				sb(i*8-1 downto (i-1)*8) <= sb((i-1)*8-1 downto (i-2)*8);
			end if;
		end if;
	end process;
	end generate;
	
	-- state blocks input (either closed loop or external input (e.g. g, init,...))
	process(clk)
	begin
		if rising_edge(clk) then
			if en = '1' then
				sb(7 downto 0) <= sb_in;
			end if;
		end if;
	end process;

	-- select rotation of s_rot for rotate operation in g
	with sel_rot select s_rot <=
		sb(103 downto 96) when "00",
		sb(106 downto 99) when "01",
		sb(111 downto 104) when "10",
		sb(126 downto 119) when others;

	-- select data to update first 8 bit of state block
	gen1 : if is_c = false generate
	with sel_in select sb_in <=
		s_out_loc	when "00",	-- rotate only, without g permutation (eventually xor something)
		d_init		when "01",	-- write initialisation constants, key or nonce into register
		g_res		when "10",	-- update state with performed g permutation
		s_dec		when others;	-- decoding C or pad
	end generate gen1;

	gen2 : if is_c = true generate
	with sel_in select sb_in <=
		s_out_loc	when "00",
		d_init		when "01",
		g_res		when others;
	end generate gen2;

end structure;