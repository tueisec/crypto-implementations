-------------------------------------------------------------------------------
--! @file       CipherCore.vhd
--! @brief      Ciphercore, implementing Norx3241v3 with 8 bit operations.
--!
--! @project    CAESAR Candidate Evaluation
--! @author     Sebastian Schelle
--! @copyright  Sebastian Schelle
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--! @note       
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.design_pkg.all;
use work.caesar_lwapi_pkg.all;

entity CipherCore is
    Port ( 
            clk             : in   STD_LOGIC;
            rst             : in   STD_LOGIC;
            --PreProcessor===============================================
            ----!key----------------------------------------------------
            key             : in   STD_LOGIC_VECTOR (SW      -1 downto 0);
            key_valid       : in   STD_LOGIC;
            key_ready       : out  STD_LOGIC;
            ----!Data----------------------------------------------------
            bdi             : in   STD_LOGIC_VECTOR (PW       -1 downto 0);
            bdi_valid       : in   STD_LOGIC;
            bdi_ready       : out  STD_LOGIC;
            bdi_partial     : in   STD_LOGIC;
            bdi_pad_loc     : in   STD_LOGIC_VECTOR (PWdiv8   -1 downto 0);
            bdi_valid_bytes : in   STD_LOGIC_VECTOR (PWdiv8   -1 downto 0);
            bdi_size        : in   STD_LOGIC_VECTOR (3       -1 downto 0);
            bdi_eot         : in   STD_LOGIC;
            bdi_eoi         : in   STD_LOGIC;
            bdi_type        : in   STD_LOGIC_VECTOR (4       -1 downto 0);
            decrypt_in      : in   STD_LOGIC;
            key_update      : in   STD_LOGIC;
            --!Post Processor=========================================
            bdo             : out  STD_LOGIC_VECTOR (PW       -1 downto 0);
            bdo_valid       : out  STD_LOGIC;
            bdo_ready       : in   STD_LOGIC;
            --bdo_size        : out  STD_LOGIC_VECTOR (3 -1 downto 0);
            bdo_type        : out  STD_LOGIC_VECTOR (4       -1 downto 0);
            bdo_valid_bytes : out  STD_LOGIC_VECTOR (PWdiv8   -1 downto 0);
            end_of_block    : out  STD_LOGIC;
            decrypt_out     : out  STD_LOGIC;
            --msg_auth signals
            msg_auth_valid  : out std_logic;
            msg_auth_ready  : in std_logic;
            msg_auth        : out std_logic
         );
            
end CipherCore;

architecture structure of CipherCore is
	
	-- state type
	type norx_state is (IDLE, LOAD_KEY, WAIT_NPUB, LOAD_NPUB, INIT_F, INIT_K, WAIT_BDI, ABS_F, ABS_D, ABS_D_PAD, ENCDEC_F, ENCDEC_D, ENCDEC_D_PAD, FINAL, TAG_OUT);

	--------------------------------------------------------
	-- local handshake signals
	signal bdi_ready_l	: std_logic;
	signal key_ready_l	: std_logic;
	signal bdo_ready_l  : std_logic;
	signal bdo_valid_l  : std_logic;
	signal bdi_valid_sh : std_logic;
	signal bdo_en       : std_logic;
	
	--------------------------------------------------------
	-- norx fsm block
	signal state 		: norx_state;
	
	--------------------------------------------------------
	-- norx state block signals
	signal en		: std_logic_vector(3 downto 0);
	signal sb_sel_in	: std_logic_vector(7 downto 0);
	signal sb_sel_rot	: std_logic_vector(1 downto 0);
	signal const0, const1	: std_logic_vector(7 downto 0);

	--------------------------------------------------------
	-- g core unit signals
	signal ai, bi, ci, di	: std_logic_vector(7 downto 0);
	signal o0, o1		: std_logic_vector(7 downto 0);
	signal en_g		: std_logic_vector(3 downto 0);
	signal cnt_res_g	: std_logic;
	signal cnt_en_g		: std_logic;
	signal v_ready		: std_logic;
	signal k_ready		: std_logic;
	signal ar, br, cr, dr	: std_logic_vector(7 downto 0);
	signal f_mode		: std_logic_vector(1 downto 0);
	signal f_ready		: std_logic;
	signal is_diag		: std_logic;

	--------------------------------------------------------
	-- big endian input
	signal eci_sel_inp	: std_logic_vector(1 downto 0);
	signal eci_data_in	: std_logic_vector(7 downto 0);
	signal eci_val_in	: std_logic;
	-- input endianness conversion
	signal eci_dat_tmp	: std_logic_vector(31 downto 0);
	signal eci_val_tmp	: std_logic_vector(3 downto 0);
	signal eci_eot_tmp	: std_logic_vector(3 downto 0); 
	signal eci_blk_en	: std_logic_vector(3 downto 0);
	signal eci_blk_sel	: std_logic_vector(1 downto 0);
	-- local input values after conversion
	signal din_loc		: std_logic_vector(7 downto 0);
	signal eot_loc		: std_logic;
	signal vin_loc		: std_logic;

	--------------------------------------------------------
	-- little endian output
	signal eco_sel_inp	: std_logic;
	signal eco_data_in	: std_logic_vector(7 downto 0);
	signal eco_val_in	: std_logic;
	signal eco_eot_in	: std_logic;
	signal encdec_out	: std_logic_vector(7 downto 0);
	signal encdec_blk_sel	: std_logic_vector(1 downto 0);
	-- output endian conversion
	signal eco_dat_tmp	: std_logic_vector(31 downto 0);
	signal eco_val_tmp	: std_logic_vector(3 downto 0);
	signal eco_eot_tmp	: std_logic_vector(3 downto 0);
	signal eco_blk_en	: std_logic_vector(3 downto 0);
	signal eco_blk_sel	: std_logic_vector(1 downto 0);
	signal encdec_eob	: std_logic;
	signal eco_en       : std_logic;

	--------------------------------------------------------
	-- global counter signals
	signal cnt_val		: std_logic_vector(7 downto 0);
	signal cnt_res		: std_logic;
	signal cnt_en		: std_logic;
	-- counter marks
	signal cnt0p, cnt7	: std_logic; 
	signal cnt8p, cnt15	: std_logic;
	signal cnt19, cnt51	: std_logic;
	signal cnt55		: std_logic;
	-- counter convenience
	signal a, b, c, d, e, f : std_logic;

	--------------------------------------------------------
	-- select block to write back after enc dec step
	signal encdec_sel_blk	: std_logic_vector(2 downto 0);

	-- last block was full
	signal extra_padding	: std_logic;

	--------------------------------------------------------
	-- key register signals
	signal en_k, new_k	: std_logic;
	signal key_st		: std_logic_vector(7 downto 0);

	--------------------------------------------------------
	-- signals saved at handshake
	signal bdi_eot_r	: std_logic;
	signal bdi_eoi_r	: std_logic;
	signal decrypt_r	: std_logic;
	
	--------------------------------------------------------
	-- padding signals
	signal sel_pad		: std_logic_vector(1 downto 0);
	signal pad		: std_logic_vector(7 downto 0);
	signal padded_first_b	: std_logic;
	signal first_set	: std_logic;
	
	--------------------------------------------------------
	-- state manipulating for data input (key, v, ad, pt, ct)
	signal selData_c	: std_logic_vector(1 downto 0);
	signal selData_r	: std_logic;
	signal d_in_c, d_in_r	: std_logic_vector(7 downto 0);
	signal tag		: std_logic_vector(7 downto 0);
	
	signal bdo_l		: std_logic_vector(7 downto 0);
	
begin


	---------------------------------------------
	-- forward local outputs to global outputs --
	---------------------------------------------
	bdi_ready	<= bdi_ready_l;
	bdo_valid	<= bdo_valid_l and bdo_en;	
	key_ready	<= key_ready_l;
	decrypt_out	<= decrypt_r;

	bdo_ready_l <= bdo_ready;

	-- tristate output, set to Z when output not needed, prevent state leaking
	with (bdo_valid_l and bdo_en) select bdo <=
		bdo_l when '1',
		(others => 'Z') when others;

	---------------------------------
	-- change endianness for input --
	---------------------------------
	-- select input, either key_input or bdi (so only one conversion unit is needed)
	with eci_sel_inp select eci_data_in <=
		bdi when "00",		-- bdi for loading npub, associate data, plaintext or ciphertext 
		key when "01",		-- key for loading key
		pad when others;	-- padding blocks, for padding incomplete data blocks (<48 byte)

	with eci_sel_inp select eci_val_in <=
		bdi_valid and bdi_ready_l when "00",
		key_valid and key_ready_l when "01",
		'0' when others;

	-- enable signals to write in the right 8 bit chunk (from right to left to right to left...)
	--|- cnt -|- eci_blk_en -|
	--|- 000 -|- 0001 -------|
	--|- 001 -|- 0010 -------|
	--|- 010 -|- 0100 -------|		-- 4*8 bit register write from right to left, and back again
	--|- 011 -|- 1000 -------|		-- always read the byte wich would be overwritten
	--|- 100 -|- 0100 -------|		-- endianness changes
	--|- 101 -|- 0010 -------|
	--|- 101 -|- 0001 -------|
	--|- 101 -|- 0010 -------|
	--|- 110 -|- 0100 -------|
	--|- ... -|- .... -------|
	eci_blk_en(0)	<= ((c and b and a) or (not c and not b and not a)) and eco_en;
	eci_blk_en(1)	<= ((not c and not b and a) or (c and b and not a)) and eco_en;
	eci_blk_en(2)	<= ((not c and b and not a) or (c and not b and a)) and eco_en;
	eci_blk_en(3)	<= ((not c and b and a) or (c and not b and not a)) and eco_en;
 	
	-- select signal to read 
	eci_blk_sel	<= (c xor b) & (c xor a);

	-- registers to store input(8 bit key or bdi), in_valid(bdi_valid or key_valid) and eot
	process(clk)
	begin
		if rising_edge(clk) then
			-- no reset needed
			if eci_blk_en(0) = '1' then
				eci_dat_tmp(7 downto 0) <= eci_data_in;
			end if;
			if eci_blk_en(1) = '1' then
				eci_dat_tmp(15 downto 8) <= eci_data_in;
			end if;
			if eci_blk_en(2) = '1' then
				eci_dat_tmp(23 downto 16) <= eci_data_in;
			end if;
			if eci_blk_en(3) = '1' then
				eci_dat_tmp(31 downto 24) <= eci_data_in;
			end if;
		end if;
	end process;

	process(clk)
	begin
		if rising_edge(clk) then
			-- reset better (valid signal)
			if rst = '1' then
				eci_eot_tmp <= "0000";
				eci_val_tmp <= "0000";
			else
				if eci_blk_en(0) = '1' then
					eci_eot_tmp(0) <= bdi_eot;
					eci_val_tmp(0) <= eci_val_in;
				end if;
				if eci_blk_en(1) = '1' then
					eci_eot_tmp(1) <= bdi_eot;
					eci_val_tmp(1) <= eci_val_in;
				end if;
				if eci_blk_en(2) = '1' then
					eci_eot_tmp(2) <= bdi_eot;
					eci_val_tmp(2) <= eci_val_in;
				end if;
				if eci_blk_en(3) = '1' then
					eci_eot_tmp(3) <= bdi_eot;
					eci_val_tmp(3) <= eci_val_in;
				end if;
			end if;
		end if;
	end process;

	-- select the right register blocks to form the little endian inputs
	with eci_blk_sel select din_loc <=
		eci_dat_tmp(7 downto 0)   when "00",
		eci_dat_tmp(15 downto 8)  when "01",
		eci_dat_tmp(23 downto 16) when "10",
		eci_dat_tmp(31 downto 24) when others;
	
	with eci_blk_sel select vin_loc <=
		eci_val_tmp(0) when "00",
		eci_val_tmp(1) when "01",
		eci_val_tmp(2) when "10",
		eci_val_tmp(3) when others;

	with eci_blk_sel select eot_loc <=
		eci_eot_tmp(0) when "00",
		eci_eot_tmp(1) when "01",
		eci_eot_tmp(2) when "10",
		eci_eot_tmp(3) when others;


	----------------------------------
	-- change endianness for output --
	----------------------------------
	-- select output, 0 for tag, 1 for ct and pt
	with eco_sel_inp select eco_data_in <=
		di when '0',            -- tag
		encdec_out when others; -- pt or ct

	-- select the right state block (s0-s3 or s4-s7 or s8-s11), when encoding or decoding
	encdec_blk_sel <= ((f and c) or (f and d) or (f and e)) & 
	                  ((not e and not c and not d) or (e and c) or (e and d));

	with encdec_blk_sel select encdec_out <= 
		ai when "00",
		bi when "01",
		ci when others;

	eco_blk_en  <= eci_blk_en;
	eco_blk_sel <= eci_blk_sel;
	
	eco_eot_in <= eot_loc;

	process(clk)
	begin
		if rising_edge(clk) then
			-- no reset needed
			if eco_blk_en(0) = '1' then
				eco_dat_tmp(7 downto 0) <= eco_data_in;
			end if;
			if eco_blk_en(1) = '1' then
				eco_dat_tmp(15 downto 8) <= eco_data_in;
			end if;
			if eco_blk_en(2) = '1' then
				eco_dat_tmp(23 downto 16) <= eco_data_in;
			end if;
			if eco_blk_en(3) = '1' then
				eco_dat_tmp(31 downto 24) <= eco_data_in;
			end if;
		end if;
	end process;

	process(clk)
	begin
		if rising_edge(clk) then
			-- reset better (valid signals), for strict fpga implementation, reset can be omitted
			if rst = '1' then
				eco_val_tmp <= "0000";	
				eco_eot_tmp <= "0000";
			else
				if eco_blk_en(0) = '1' then
					eco_val_tmp(0) <= eco_val_in;
					eco_eot_tmp(0) <= eco_eot_in;
				end if;
				if eco_blk_en(1) = '1' then
					eco_val_tmp(1) <= eco_val_in;
					eco_eot_tmp(1) <= eco_eot_in;
				end if;
				if eco_blk_en(2) = '1' then
					eco_val_tmp(2) <= eco_val_in;
					eco_eot_tmp(2) <= eco_eot_in;
				end if;
				if eco_blk_en(3) = '1' then
					eco_val_tmp(3) <= eco_val_in;
					eco_eot_tmp(3) <= eco_eot_in;
				end if;
			end if;
		end if;
	end process;
	
	-- select the byte which gets overwritten as bdo big_endian, and corresponding handshake signals
	with eco_blk_sel select bdo_l <=
		eco_dat_tmp(7 downto 0)   when "00",
		eco_dat_tmp(15 downto 8)  when "01",
		eco_dat_tmp(23 downto 16) when "10",
		eco_dat_tmp(31 downto 24) when others;
	
	with eco_blk_sel select bdo_valid_l <=
		eco_val_tmp(0) when "00",
		eco_val_tmp(1) when "01",
		eco_val_tmp(2) when "10",
		eco_val_tmp(3) when others;

	with eco_blk_sel select encdec_eob <=
		eco_eot_tmp(0) when "00",
		eco_eot_tmp(1) when "01",
		eco_eot_tmp(2) when "10",
		eco_eot_tmp(3) when others;

	------------------------------------------------------------------------------------
	-- global counter (for counting diag and col steps, inputs and key iterations...) --
	------------------------------------------------------------------------------------
	process(clk)           -- TODO: adept counter size to maximum value
	begin
		if rising_edge(clk) then
			if cnt_res = '1' then
				cnt_val <= (others => '0');
			elsif cnt_en = '1' then
				cnt_val <= std_logic_vector(unsigned(cnt_val)+1);
			end if;
		end if;
	end process;
	
	-- counter marks for stop signals etc...
	cnt0p <= not a and not b and not c and not d and not e and not f;
	cnt7  <= a and b and c;
	cnt8p <= not a and not b and not c and d and not e;
	cnt15 <= cnt7 and d;
	cnt19 <= a and b and e;
	cnt51 <= a and b and e and f;
	cnt55 <= a and b and c and e and f;

	-- count values to letters for convenience --
	f <= cnt_val(5);
	e <= cnt_val(4);
	d <= cnt_val(3);
	c <= cnt_val(2);
	b <= cnt_val(1);
	a <= cnt_val(0);


	---------------------------------------------
	-- key register: store key for later usage --
	---------------------------------------------
	k_reg : entity work.key_register port map(
		clk	=> clk,
		en	=> en_k,
		sel	=> new_k,
		key_in	=> din_loc,
		key_out	=> key_st
	);


	-----------------------------------------------------------------------------
	-- g unit (core of the algorithm, 32 bit width, performed in 8 bit chunks) --
	-----------------------------------------------------------------------------
	g_u : entity work.g8 port map(
		clk        => clk,
		cnt_res    => cnt_res_g,
		cnt_en     => cnt_en_g,
		is_diag    => is_diag,
		ai         => ai,
		bi         => bi,
		ci         => ci,
		di         => di,
		ar         => ar,
		br         => br,
		cr         => cr,
		dr         => dr,
		o0         => o0,
		o1         => o1,
		en         => en_g,
		f_mode     => f_mode,
		f_ready    => f_ready,
		sb_sel_rot => sb_sel_rot,
		v_ready    => v_ready,
		k_ready    => k_ready
	);

	is_diag <= cnt_val(0);

	----------------------------------------------------------------------------------------
	-- state blocks to store the internal NORX state, consist of 8 bit shifting registers --
	----------------------------------------------------------------------------------------
	-- state block s0 - s3
	st0 : entity work.state_block port map(
		clk	=> clk,
		en	=> en(0),
		sel_in	=> sb_sel_in(1 downto 0),
		sel_rot	=> sb_sel_rot,
		d_in	=> d_in_r,
		d_init	=> din_loc,
		g_res	=> o0,
		s_out	=> ai,
		s_rot	=> ar,
		bdi_val	=> vin_loc
	);

	-- state block s4 - s7
	st1 : entity work.state_block port map(
		clk	=> clk,
		en	=> en(1),
		sel_in	=> sb_sel_in(3 downto 2),
		sel_rot	=> sb_sel_rot,
		d_in	=> d_in_r,
		d_init	=> key_st,
		g_res	=> o1,
		s_out	=> bi,
		s_rot	=> br,
		bdi_val	=> vin_loc
	);

	-- state block s8 - s11
	st2 : entity work.state_block port map(
		clk	=> clk,
		en	=> en(2),
		sel_in	=> sb_sel_in(5 downto 4),
		sel_rot	=> sb_sel_rot,
		d_in	=> d_in_r,
		d_init	=> const0,
		g_res	=> o0,
		s_out	=> ci,
		s_rot	=> cr,
		bdi_val	=> vin_loc
	);

	-- state block s12 - s15
	st3 : entity work.state_block generic map (is_c => false) port map(    -- TODO: does it work, test!
		clk	=> clk,
		en	=> en(3),
		sel_in	=> sb_sel_in(7 downto 6),
		sel_rot	=> sb_sel_rot,
		d_in	=> d_in_c,
		d_init	=> const1,
		g_res	=> o1,
		s_out	=> di,
		s_rot	=> dr,
		bdi_val	=> vin_loc
	);


	----------------------------------------------------------------------------------
	-- state block data inputs (for xor operations) ! x"00" when no xor operation ! --
	----------------------------------------------------------------------------------
	-- for capacity words
	with selData_c select d_in_c <=
		x"00" 	when "00",
		key_st	when "01",
		tag   	when others;

	-- for rate words
	with selData_r select d_in_r <=
		x"00"   when '0',
		din_loc when others;
	
	-- select padding block
	with sel_pad select pad <=
		x"01" when "00",
		x"81" when "01",
		x"00" when "10",
		x"80" when others;


	---------------------------------------------------
	-- enable signal for state blocks during enc dec --
	---------------------------------------------------
	encdec_sel_blk(0) <= (not f and e and not d and not c) or (not f and not e and c) or (not f and not e and d);	-- cnt0 to cnt3
	encdec_sel_blk(1) <= (f and not e and not d and not c) or (not f and e and c) or (not f and e and d);
	encdec_sel_blk(2) <= (f and e and not d and not c) or (f and not e and c) or (f and not e and d);


	--------------------------------------------------
	-- constants (NORX f^2 constants for s9 to s15) --
	--------------------------------------------------
	with cnt_val select const0 <=
		x"A3" when x"00",
		x"D8" when x"01",
		x"D9" when x"02",
		x"30" when x"03",

		x"3F" when x"04",
		x"A8" when x"05",
		x"B7" when x"06",
		x"2C" when x"07",
		
		x"ED" when x"08",
		x"84" when x"09",
		x"EB" when x"0a",
		x"49" when x"0b",

		x"ED" when x"0c",
		x"CA" when x"0d",
		x"47" when x"0e",
		x"87" when others;
   
	with cnt_val select const1 <=
		x"33" when x"00",
		x"54" when x"01",
		x"63" when x"02",
		x"CB" when x"03", -- EB constant, but EB xor 20 (w->32) = CB

		x"F9" when x"04",
		x"94" when x"05",
		x"22" when x"06",
		x"0F" when x"07", -- 0B constant, but 0B xor 04 (l->4) = 0F
		
		x"BE" when x"08",
		x"0B" when x"09",
		x"F5" when x"0a",
		x"C8" when x"0b", -- C9 constant, but C9 xor 01 (p->1) = C8

		x"D7" when x"0c",
		x"C4" when x"0d",
		x"91" when x"0e",
		x"84" when others; -- 04 constant, 04 xor 80 (t->128) = 84


	---------------------------------------------------------
	-- eot and eoi register for storing at every handshake --
	---------------------------------------------------------
	process(clk)
	begin
		if rising_edge(clk) then
			if bdi_valid = '1' and bdi_ready_l = '1' then
				decrypt_r <= decrypt_in;
				bdi_eot_r <= bdi_eot;
				bdi_eoi_r <= bdi_eoi;
			end if;
		end if;
	end process;
	
	-- padding first block state saved 
	process(clk)
	begin
		if rising_edge(clk) then
			if cnt_en_g = '1' then
				padded_first_b <= '0';
			elsif first_set = '1' then
				padded_first_b <= '1';
			end if;
		end if;
	end process;

	-- extra padding
	process(clk)
	begin
		if rising_edge(clk) then
			if cnt0p = '1' then
				extra_padding <= '0';
			elsif (a and b and c and d and not e and f and bdi_eot) = '1' and state = ABS_D then
				extra_padding <= '1';	-- if this is set to 1, one extra round abs or enc/dec is performed with only padding
			end if;
		end if;
	end process;

    process(clk)
    begin
        if rising_edge(clk) then
            bdi_valid_sh <= bdi_valid;	-- bdi_valid from last clk cycle
        end if;
    end process;

	-------------------------------------------------------
	-- state machine sequential part (state transitions) --
	-------------------------------------------------------
	process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				state <= IDLE;
			else
			case state is
			-- wait for next operation
			when IDLE =>
				if key_valid = '1' then
					state <= LOAD_KEY; 
				elsif bdi_valid = '1' then
					state <= WAIT_NPUB;	
				end if;

			-- load key into key register 16 times 8 bit + 4 cycles for endianness conversion
			when LOAD_KEY =>
				if cnt19 = '1' then
					state <= WAIT_NPUB;
				end if;

			-- wait for valid npub
			when WAIT_NPUB =>
				if bdi_valid = '1' and bdi_type = HDR_NPUB then
					state <= LOAD_NPUB;
				end if;

			-- load npub, 20 cycles as for key
			when LOAD_NPUB =>
				if cnt19 = '1' then
					state <= INIT_F;
				end if;

			-- initialize state, 4*F
			when INIT_F =>
				if (cnt7 and f_ready) = '1' then
					state <= INIT_K;
				end if;

			-- initialize xor key
			when INIT_K =>
				if cnt15 = '1' then
					if bdi_eoi_r = '1' then
						state <= FINAL;
					else
						state <= WAIT_BDI;
					end if;
				end if;

			-- wait for AD, PT, CT data
			when WAIT_BDI =>
				if bdi_valid = '1' then
					if bdi_type = HDR_AD then
						state <= ABS_F;
					else
						state <= ENCDEC_F;
					end if;
				end if;
	
			-- absorb 4*F
			when ABS_F =>
				if (cnt7 and f_ready) = '1' then
					if bdi_type = HDR_AD then
						state <= ABS_D;
					else
						state <= ABS_D_PAD;
					end if;
				end if;
			
			-- absorb incoming data
			when ABS_D =>
				if ((a and b and c and d and f and bdi_valid) or (bdi_eot and bdi_valid)) = '1' then
				    state <= ABS_D_PAD;
				end if;

			-- pad after last valid byte
			when ABS_D_PAD =>
				if cnt51 = '1' then
					if bdi_eot_r = '1' and bdi_eoi_r = '1' then
						state <= FINAL;
					elsif extra_padding = '1' then
						state <= ABS_F;
					else
						state <= WAIT_BDI;
					end if;
				end if;

			-- encode 4*F
			when ENCDEC_F =>
				if (cnt7 and f_ready) = '1' then
					if bdi_type = HDR_MSG then
						state <= ENCDEC_D;
					else
						state <= ENCDEC_D_PAD;
					end if;
				end if;

			-- encode incoming data
			when ENCDEC_D =>               
				if ((a and b and c and d and f and bdi_valid) or (bdi_eot and bdi_valid)) = '1' then
					state <= ENCDEC_D_PAD;
				end if;
			
			-- pad after last valid byte
			when ENCDEC_D_PAD =>
				if cnt55 = '1' then
					if bdo_valid_l = '1' then
						state <= ENCDEC_F;
					elsif bdi_eoi_r = '1' then
						state <= FINAL;
					else
						state <= WAIT_BDI;
					end if;
				end if;

			-- finalize 2*4*F
			when FINAL =>
				if (cnt15 and f_ready) = '1' then
					state <= TAG_OUT;
				end if;	

			-- output tag
			when TAG_OUT =>
				if (cnt19 and bdo_ready_l and bdo_valid_l) = '1' then
					state <= IDLE;
				end if;

			end case;
			end if;
		end if;
	end process;

	process(state, bdo_ready, bdi_valid, key_valid, bdi_ready_l,
		cnt0p, cnt7, cnt8p, cnt15, cnt19, cnt51, cnt55, 
		a, b, c, d, e, f, 
		vin_loc, eci_val_in, bdi_valid_sh,
		f_mode, f_ready, en_g, k_ready, v_ready, 
		padded_first_b, encdec_sel_blk, encdec_eob)
	begin
        
		case state is

		when IDLE =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= '1';
			cnt_en		<= '0';
			-- state blocks signals
			en		<= "0000";
			sb_sel_in	<= "--------";
			selData_c	<= "--";
			selData_r	<= '-';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "01";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en      <= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en       <= '1';

		when LOAD_KEY =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= not e;
			-- key register signals
			en_k		<= vin_loc;
			new_k		<= '1';
			-- counter signals main cnt
			cnt_res		<= cnt19;
			cnt_en		<= eci_val_in or e;
			-- state blocks signals
			en		<= "0000";
			sb_sel_in	<= "--------";
			selData_c	<= "--";
			selData_r	<= '-';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "01";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when WAIT_NPUB =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= '0';
			cnt_en		<= '0';
			-- state blocks signals
			en		<= "0000";
			sb_sel_in	<= "--------";
			selData_c	<= "--";
			selData_r	<= '-';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when LOAD_NPUB =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= not e;
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= eci_val_in;
			new_k		<= '0';
			-- counter signals main cnt
			cnt_res		<= cnt19;
			cnt_en		<= eci_val_in or e;
			-- state blocks signals
			en		<= eci_val_in & eci_val_in & eci_val_in & vin_loc;
			sb_sel_in	<= "01010101";
			selData_c	<= "00";
			selData_r	<= '0';
			-- g core signals
			cnt_res_g	<= '1';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when INIT_F =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= cnt7 and f_ready;
			cnt_en		<= f_ready;
			-- state blocks signals
			en		<= en_g;
			sb_sel_in	<= f_mode(1) & '0' & f_mode(0) & '0' & f_mode(1) & '0' & f_mode(0) & '0';
			selData_c	<= "00";
			selData_r	<= '0';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '1';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when INIT_K =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '1';
			new_k		<= '0';
			-- counter signals main cnt
			cnt_res		<= cnt15;
			cnt_en		<= '1';
			-- state blocks signals
			en		<= "1000";
			sb_sel_in	<= "00------";
			selData_c	<= "01";
			selData_r	<= '-';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when WAIT_BDI =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= '1';
			cnt_en		<= '0';
			-- state blocks signals
			en		<= "0000";
			sb_sel_in	<= "--------";
			selData_c	<= "--";
			selData_r	<= '-';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when ABS_F =>
			-- tag
			tag		<= x"01";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= cnt7 and f_ready;
			cnt_en		<= f_ready;
			-- state blocks signals
			en		<= en_g;
			sb_sel_in	<= f_mode(1) & '0' & f_mode(0) & '0' & f_mode(1) & '0' & f_mode(0) & '0';
			selData_c	<= (cnt0p and v_ready) & '0';
			selData_r	<= '0';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '1';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when ABS_D =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= not (e and f and c);
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= cnt51;
			cnt_en		<= (bdi_ready_l and bdi_valid) or (e and f and c);
			-- state blocks signals
			en		<= '0' & (encdec_sel_blk and ((bdi_valid and (c or d or e or f)) & (bdi_valid and (c or d or e or f)) & (bdi_valid and (c or d or e or f))));
			sb_sel_in	<= "--000000";
			selData_c	<= "--";
			selData_r	<= '1';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= (bdi_valid and bdi_ready_l) or (e and f and c);
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';
		
		when ABS_D_PAD =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= cnt51;
			cnt_en		<= '1';
			-- state blocks signals
			en		<= '0' & encdec_sel_blk;
			sb_sel_in	<= "--000000";
			selData_c	<= "--";
			selData_r	<= '1';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "10";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '1';
			sel_pad		<= padded_first_b & (a and b and c and d and not e and f);
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when ENCDEC_F =>
			-- tag
			tag		<= x"02";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= cnt7 and f_ready;
			cnt_en		<= f_ready;
			-- state blocks signals
			en		<= en_g;
			sb_sel_in	<= f_mode(1) & '0' & f_mode(0) & '0' & f_mode(1) & '0' & f_mode(0) & '0';
			selData_c	<= (cnt0p and v_ready) & '0';
			selData_r	<= '0';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '1';
			-- input output endian convert
			eci_sel_inp	<= "01";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';
		
		when ENCDEC_D =>				-- TODO: ask for bdo_ready here and in next state
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= not (e and f);
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= cnt55;
			cnt_en		<= (bdi_valid and bdi_ready_l) or (e and f and c);
			-- state blocks signals
			en		<= '0' & (encdec_sel_blk and ((bdi_valid and (c or d or e or f)) & (bdi_valid and (c or d or e or f)) & (bdi_valid and (c or d or e or f))));
			sb_sel_in	<= "--" & decrypt_r & decrypt_r & decrypt_r & decrypt_r & decrypt_r & decrypt_r;
			selData_c	<= "--";
			selData_r	<= '1';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '1';
			eco_val_in	<= vin_loc;
			eco_en		<= (bdi_valid and bdi_ready_l) or (e and f and c);
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= encdec_eob;
			bdo_en		<= bdi_valid_sh; 
            
		when ENCDEC_D_PAD =>
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= '0';
			new_k		<= '-';
			-- counter signals main cnt
			cnt_res		<= cnt55;
			cnt_en		<= '1';
			-- state blocks signals
			en		<= '0' & encdec_sel_blk;
			sb_sel_in	<= "--" & decrypt_r & decrypt_r & decrypt_r & decrypt_r & decrypt_r & decrypt_r;
			selData_c	<= "--";
			selData_r	<= '1';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "10";
			eco_sel_inp	<= '1';
			eco_val_in	<= vin_loc;
			eco_en		<= '1';
			-- padding
			first_set	<= '1';
			sel_pad		<= padded_first_b & (a and b and c and d and not e and f);
			-- end of blk signal
			end_of_block	<= encdec_eob;
			bdo_en		<= '1';

		when FINAL =>
			-- tag
			tag		<= x"08";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= cnt8p and k_ready;
			new_k		<= '0';
			-- counter signals main cnt
			cnt_res		<= cnt15 and f_ready;
			cnt_en		<= f_ready;
			-- state blocks signals
			en		<= en_g;
			sb_sel_in	<= f_mode(1) & '0' & f_mode(0) & '0' & f_mode(1) & '0' & f_mode(0) & '0';
			selData_c	<= (cnt0p and v_ready) & (cnt8p and k_ready);
			selData_r	<= '0';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '1';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= '0';
			eco_en		<= '1';
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= '0';
			bdo_en		<= '1';

		when TAG_OUT =>					-- TODO: ask for bdo_ready here!!
			-- tag
			tag		<= "--------";
			-- handshake signals
			bdi_ready_l	<= '0';
			key_ready_l	<= '0';
			-- key register signals
			en_k		<= not e and bdo_ready;
			new_k		<= '0';
			-- counter signals main cnt
			cnt_res		<= cnt19 and bdo_ready;
			cnt_en		<= bdo_ready;
			-- state blocks signals
			en		<= (not e and bdo_ready) & "000";
			sb_sel_in	<= "00------";
			selData_c	<= "01";
			selData_r	<= '-';
			-- g core signals
			cnt_res_g	<= '0';
			cnt_en_g	<= '0';
			-- input output endian convert
			eci_sel_inp	<= "00";
			eco_sel_inp	<= '0';
			eco_val_in	<= not e and bdo_ready;
			eco_en		<= bdo_ready;
			-- padding
			first_set	<= '0';
			sel_pad		<= "--";
			-- end of blk signal
			end_of_block	<= cnt19;
			bdo_en		<= '1';

		end case;
	end process;
	

end structure;

