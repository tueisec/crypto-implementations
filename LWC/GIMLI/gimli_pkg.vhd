--------------------------------------------------------------------------
--! @file       gimli_pkg.vhd
--! @brief      Gimli Package for constants and functions
--! @author     Patrick Karl <patrick.karl@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--! @license    This project is released under the GNU Public License.          
--!             The license and distribution terms for this file may be         
--!             found in the file LICENSE in this distribution or at            
--!             http://www.gnu.org/licenses/gpl-3.0.txt 
---------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! API package.
use work.NIST_LWAPI_pkg.all;
--! I/O specific package.
use work.Design_pkg.all;

--==========================================================================
--! Gimli permutation specific parameters and functions.
--==========================================================================
package gimli_pkg is

    --! Number of rows in gimli state
    constant N_ROW_C : integer range 0 to 3 := 3;

    --! Number of columns in gimli state
    constant N_COL_C : integer range 0 to 4 := 4;

    --! Number of rounds to run
    constant N_ROUNDS_C : integer range 8 to 32 := 24;

    --! Number of bits required to encode round counter
    constant ROUND_BITS_C : integer range 0 to log2_ceil(N_ROUNDS_C) := log2_ceil(N_ROUNDS_C);

    --! size of permutation state
    constant GIMLI_STATE_C : integer range 384 to 384 := 384;

    --! Address width depends on CORE_PW
    constant ADDR_WIDTH_C : integer := log2_ceil(GIMLI_STATE_C/CORE_PW);

    --! Word size of each entry in the gimli_state array.
    constant GIMLI_WORD_SIZE_C : integer range 32 to 32 := 32;

    --! Number of beats required to fll word
    constant BEATS_PER_WORD_C : integer range 1 to 4 := GIMLI_WORD_SIZE_C / CORE_PW;

    --! Constant used for constant addition in several permutation rounds
    constant MAGIC_CONST_C : std_logic_vector(GIMLI_WORD_SIZE_C-1 downto 0) := x"9e377900";

    --! Support active high/low
    constant GIMLI_RESET_C : std_logic := '1';

    --! Host-to-Network Byte order
    function hton_Byte( vec : std_logic_vector ) return std_logic_vector;

    --! Network-to-Host Byte order
    function ntoh_Byte( vec : std_logic_vector ) return std_logic_vector;

    --! Host-to-Network Bit order
    function hton_Bit( vec : std_logic_vector ) return std_logic_vector;

    --! Network-to-Host Bit order
    function ntoh_Bit( vec : std_logic_vector ) return std_logic_vector;

    --! Immediate If funtion. If expr is true, return val1 else val2.
    function iif( expr : boolean; val1, val2 : std_logic ) return std_logic;

end gimli_pkg;


--! @brief      Package body of gimli_pkg.
--! @details    An iif function and some utility functions for endianess
--!             manipulation are implemented.
package body gimli_pkg is

  ---------------------------------------------------------------------
  --! Immediate If funtion
  ---------------------------------------------------------------------
  function iif( expr : boolean; val1, val2 : std_logic ) return std_logic is
  begin
    if (expr) then
      return val1;
    else
      return val2;
    end if;
  end function iif;


  ---------------------------------------------------------------------
  --! Functions for Endianess manipulation
  ---------------------------------------------------------------------
  function hton_Byte( vec : std_logic_vector ) return std_logic_vector is
    variable res : std_logic_vector(vec'length - 1 downto 0);
    constant n_bytes  : integer := vec'length/8;
  begin

    assert (vec'length mod 8 = 0)
      report "Vector size must be in multiple of Bytes!" severity failure;

    for i in 0 to (n_bytes - 1) loop
      res(8*(i+1) - 1 downto 8*i) := vec(8*(n_bytes - i) - 1 downto 8*(n_bytes - i - 1));
    end loop;

    return res;
  end function hton_Byte;

  function ntoh_Byte( vec : std_logic_vector ) return std_logic_vector is
  begin
    return hton_byte(vec);
  end function;

  function hton_Bit( vec : std_logic_vector ) return std_logic_vector is
    variable res : std_logic_vector(vec'length - 1 downto 0);
  begin

    for i in 0 to (vec'length - 1) loop
      res(i) := vec(vec'length - i - 1);
    end loop;

    return res;
  end function;

  function ntoh_Bit( vec : std_logic_vector ) return std_logic_vector is
  begin
    return hton_Bit(vec);
  end function;

end gimli_pkg;
