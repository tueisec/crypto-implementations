--------------------------------------------------------------------------------
--! @file       PostProcessor.vhd
--! @brief      Post-processor for NIST LWC API
--!
--! @author     Michael Tempelmeier
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology     
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @author     Panasayya Yalla
--! @copyright  Copyright (c) 2016 Cryptographic Engineering Research Group
--!             ECE Department, George Mason University Fairfax, VA, U.S.A.
--!             All rights Reserved.
--! @license    This project is released under the GNU Public License.          
--!             The license and distribution terms for this file may be         
--!             found in the file LICENSE in this distribution or at            
--!             http://www.gnu.org/licenses/gpl-3.0.txt                         
--! @note       This is publicly available encryption source code that falls    
--!             under the License Exception TSU (Technology and software-       
--!             —unrestricted)                                                  
--------------------------------------------------------------------------------
--! Description
--!
--!  bdo_type is not used at the moment.
--!  However, we encourage authors to provide it, as it helps to adapt the
--!  LWC Core to different use cases. Additionally, it might get needed in a
--!  future version of the PostProcessor.
--!
--!  There is no penalty in terms of area, as this signal gets trimmed during
--!  synthesis.
--!
--! 
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.math_real."ceil";
use IEEE.math_real."log2";
use work.Design_pkg.all;
use work.NIST_LWAPI_pkg.all;


entity PostProcessor is

    Port (
            clk             : in  std_logic;
            rst             : in  std_logic;
            --! Data SIPO ======================================================
            bdo             : in  std_logic_vector(CORE_PW-1 downto 0);
            bdo_valid       : in  std_logic;
            bdo_ready       : out std_logic;
            --! Cipher Core ====================================================
            end_of_block    : in  std_logic;
            bdo_type        : in  std_logic_vector( 3 downto 0); -- not used atm
            bdo_valid_bytes : in  std_logic_vector(CORE_PWdiv8-1 downto 0);
            ---! Header/Tag FIFO ===============================================
            cmd             : in  std_logic_vector(31 downto 0);
            cmd_valid       : in  std_logic;
            cmd_ready       : out std_logic;
            --! Output =========================================================
            do_data         : out std_logic_vector(31 downto 0);
            do_valid        : out std_logic;
            do_last         : out std_logic;
            do_ready        : in  std_logic;
            msg_auth        : in  std_logic;
            msg_auth_ready  : out std_logic;
            msg_auth_valid  : in  std_logic
         );

end PostProcessor;

architecture PostProcessor of PostProcessor is
    signal do_data1             : std_logic_vector(31 downto 0);
    signal do_valid1            : std_logic;
    signal do_last1             : std_logic;
    signal bdo_valid_p          : std_logic;
    signal bdo_ready_p          : std_logic;
    signal bdo_p                : std_logic_vector(31 downto 0);
    signal bdo_cleared          : std_logic_vector(CORE_PW-1 downto 0);
    signal len_SegLenCnt        : std_logic;
    signal en_SegLenCnt         : std_logic;
    signal dout_SegLenCnt       : std_logic_vector(15 downto 0);
    signal load_SegLenCnt       : std_logic_vector(15 downto 0);
    signal last_flit_of_segment : std_logic;

    --Registers
    signal decrypt, nx_decrypt  : std_logic;
    signal eot, nx_eot          : std_logic;

    ---Aliases
    alias cmd_opcode            : std_logic_vector( 3 downto 0) is cmd(31 downto 28);
    alias cmd_seg_length        : std_logic_vector(15 downto 0) is cmd(15 downto  0);

    alias do_data1_opcode       : std_logic_vector( 3 downto 0) is do_data1(31 downto 28);
    alias do_data1_flags        : std_logic_vector( 3 downto 0) is do_data1(27 downto 24);
    alias do_data1_reserved     : std_logic_vector( 7 downto 0) is do_data1(23 downto 16);
    alias do_data1_length       : std_logic_vector(15 downto 0) is do_data1(15 downto  0);


    ---STATES
    type t_state is (S_INIT,
                     --MSG
                     S_HDR_MSG, S_OUT_MSG,
                     --TAG
                     S_HDR_TAG, S_OUT_TAG,
                     S_VER_TAG,
                     --HASHTAG
                     S_HDR_HASH_VALUE, S_OUT_HASH_VALUE,
                     S_STATUS_FAIL, S_STATUS_SUCCESS
                     );
    signal nx_state, pr_state:t_state;


begin

    assert (TAG_INTERNAL = true) report "Tag comparison must be done in the LWC core!" severity failure;

    bdo_cleared <= bdo and Byte_To_Bits_EXP(bdo_valid_bytes); --set unused bytes to zero

    --==========================================================================
    --! SEGMENT LENGTH COUNTER
    --==========================================================================

    -- This counter can be saved, if we do not want to support multiple segments
    SegLen: entity work.StepDownCountLd(StepDownCountLd)
                generic map(
                        N       =>  16,
                        step    =>  4
                            )
                port map
                        (
                        clk     =>  clk ,
                        len     =>  len_SegLenCnt,
                        load    =>  load_SegLenCnt,
                        ena     =>  en_SegLenCnt,
                        count   =>  dout_SegLenCnt
                    );

    load_SegLenCnt       <= cmd_seg_length;
    last_flit_of_segment <= '1' when (to_integer(unsigned(dout_SegLenCnt))<=4) else '0'; --we have 32 bit output words

    --==========================================================================
    --! SIPO
    --==========================================================================
    bdoSIPO: entity work.data_sipo(behavioral) port map
        (
            clk=> clk,
            rst=> rst,

            end_of_input  => end_of_block, -- no need for conversion, as our last serial_in element is also the last parallel_out element
                                           -- end_of_bock should only be evaluated if bdo_valid_p = '1'
            data_s       => bdo_cleared,
            data_valid_s => bdo_valid,
            data_ready_s => bdo_ready,

            data_p       => bdo_p,
            data_valid_p => bdo_valid_p,
            data_ready_p => bdo_ready_p
        );   

    --==========================================================================
    --! REGISTERS
    --==========================================================================

    process (clk)
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                pr_state <= S_INIT;
            else
                pr_state <= nx_state;
            end if;

            decrypt <= nx_decrypt;
            eot     <= nx_eot;

        end if;
    end process;

    --==========================================================================
    --! next state function
    --==========================================================================

    process (pr_state, bdo_valid_p, do_ready, end_of_block, decrypt,
             cmd_valid, msg_auth_valid, msg_auth, last_flit_of_segment,
             cmd_opcode, eot)

    begin
        case pr_state is

            when S_INIT =>
                if (cmd_valid = '1') then
                    if (cmd_opcode = INST_HASH) then
                        nx_state <= S_HDR_HASH_VALUE;
                    else
                        nx_state <= S_HDR_MSG;
                    end if;
                else
                    nx_state <= S_INIT;
                end if;

           --Hash
           when S_HDR_HASH_VALUE =>
                if (do_ready = '1') then
                    nx_state <= S_OUT_HASH_VALUE;
                else
                    nx_state <= S_HDR_HASH_VALUE;
                end if;

           when S_OUT_HASH_VALUE =>
                if (bdo_valid_p = '1' and do_ready = '1' and end_of_block = '1') then
                    nx_state <= S_STATUS_SUCCESS;
                else
                    nx_state <= S_OUT_HASH_VALUE;
                end if;


            --MSG
            when S_HDR_MSG =>
                if (cmd_valid = '1' and do_ready = '1') then
                    if (cmd_seg_length = x"0000") then 
                        if (decrypt='1') then
                            nx_state <= S_VER_TAG;
                        else
                            nx_state <= S_HDR_TAG;
                        end if;
                    else
                        nx_state <= S_OUT_MSG;
                    end if;
                else
                    nx_state <= S_HDR_MSG;
                end if;

            when S_OUT_MSG =>
                if (bdo_valid_p = '1' and do_ready = '1') then 

                    if (last_flit_of_segment = '1') then  -- This line is needed, if the input (and therefore) the output is splitted in multiple segments.
                  --if (end_of_block = '1') then          -- This line can be used, if there is only one input segment and there is no ciphertext expansion.
                                                          -- The latter saves us the output counter.
                    
                       if (eot = '1') then                -- this is the last segment
                          if (decrypt = '1') then
                              nx_state <= S_VER_TAG;
                          else
                              nx_state <= S_HDR_TAG;
                          end if;
                       else                               -- this is not the last segment, we have multiple segments
                            nx_state <= S_HDR_MSG;
                       end if;
                    else                                  -- more output in current segment
                       nx_state <= S_OUT_MSG;
                    end if;
                else
                    nx_state <= S_OUT_MSG;
                end if;


            --TAG
            when S_HDR_TAG =>
                if (do_ready = '1') then
                    nx_state <= S_OUT_TAG;
                else
                    nx_state <= S_HDR_TAG;
                end if;

            when S_OUT_TAG =>
                if (bdo_valid_p = '1' and end_of_block='1' and do_ready='1') then
                    nx_state <= S_STATUS_SUCCESS;
                else
                    nx_state <= S_OUT_TAG;
                end if;

            when S_VER_TAG =>
                if (msg_auth_valid = '1') then
                    if (msg_auth = '1') then
                        nx_state <= S_STATUS_SUCCESS;
                    else
                        nx_state <= S_STATUS_FAIL;
                    end if;
                else
                    nx_state <= S_VER_TAG;
                end if;


            -- STATUS
            when S_STATUS_FAIL =>
                if (do_ready = '1') then
                    nx_state <= S_INIT;
                else
                    nx_state <= S_STATUS_FAIL;
                end if;

            when S_STATUS_SUCCESS =>
                if (do_ready = '1') then
                    nx_state <= S_INIT;
                else
                    nx_state <= S_STATUS_SUCCESS;
                end if;

        end case;
    end process;


    --==========================================================================
    --! output state function
    --==========================================================================

    process(pr_state, bdo_valid_p, bdo_p, decrypt, eot, cmd, cmd_valid, do_ready)
    begin
            -- DEFAULT SIGNALS
            -- external interface
            do_valid1      <='0';
            do_last1       <='0';
            -- LWC core
            bdo_ready_p    <='0';
            msg_auth_ready <='0';
            --header-FIFO
            cmd_ready      <='0';
            -- counters
            len_SegLenCnt  <='0';
            en_SegLenCnt   <='0';
            -- registers
            nx_decrypt     <= decrypt;
            nx_eot         <= eot;
            do_data1       <= (others => '-');

        case pr_state is
                when S_INIT =>
                    
                    if (cmd_valid = '1') then
                        -- We reiceive either INST_HASH, or INST_ENC or INST_DEC
                        -- The LSB of INST_ENC and INST_DEC is '1' for Decryption
                        -- For Hash, this bit is '0', however we never evaluate "decrypt" for Hash.
                        nx_decrypt <= cmd(28);
                    end if;

                    cmd_ready      <= '1';

                --MSG
                when S_HDR_MSG =>
                    cmd_ready      <= do_ready;
                    len_SegLenCnt  <= do_ready and cmd_valid;

                    do_valid1      <= cmd_valid;
                    nx_eot         <= cmd(25); -- preserve EOT flag to support multi segment MSGs

                    if (decrypt = '1') then
                        --header is plaintext
                        do_data1_opcode <= HDR_PT;
                        do_data1_flags(0) <= '1' AND cmd(25); -- last: no TAG is sent after decryption.
                                                              -- If cmd(25) = '0' (EOT ='0') then we have multiple segments,
                                                              -- and this is not the last one.
                    else
                        ---header is ciphertext
                        do_data1_opcode <= HDR_CT;
                        do_data1_flags(0) <= '0'; -- last: we will send a TAG afterwards, this is never the last segment.
                    end if;

                    do_data1_flags(3 downto 1) <= "001";     -- Partial = '0', EOI ='0', EOT = '1'
                                                             -- XXX: The definition for EOI is not intuitive for data out.
                                                             --      At the moment, EOI is defined to be '0'.
                                                             --      However, this might be change to '1' in the future!
                    do_data1_reserved   <= (others => '0');  -- reserved not used.
                    do_data1_length     <= cmd(15 downto 0); -- length forwarded from the cmd FIFO

                when S_OUT_MSG =>
                    bdo_ready_p    <= do_ready;
                    do_valid1      <= bdo_valid_p;
                    do_data1       <= bdo_p;
                    en_SegLenCnt   <= bdo_valid_p and do_ready;

                --TAG
                when S_HDR_TAG =>
                    do_valid1         <= '1';
                    do_data1_opcode   <= HDR_TAG;
                    do_data1_flags    <= "0011"; --Partial = '0', EOI ='0', EOT = '1', Last = '1': No tag is larger than 2^(16-1) bytes
                    do_data1_reserved <= (others => '0'); --reserved not used.
                    do_data1_length   <= std_logic_vector(to_unsigned(TAG_SIZE/8, 16));

                when S_OUT_TAG =>
                    bdo_ready_p   <= do_ready;
                    do_valid1     <= bdo_valid_p;
                    do_data1      <= bdo_p;

                when S_VER_TAG =>
                    msg_auth_ready <= '1';

                --HASHTAG
                when S_HDR_HASH_VALUE =>
                    do_valid1          <= '1';
                    do_data1_opcode    <= HDR_HASH_VALUE;
                    do_data1_flags     <= "0011"; --Partial = '0', EOI ='0', EOT = '1', LAST = '1': No tag is larger than 2^(16-1) bytes
                    do_data1_reserved  <= (others => '0'); -- reserved not used
                    do_data1_length    <= std_logic_vector(to_unsigned(HASH_VALUE_SIZE/8, 16));

                when S_OUT_HASH_VALUE =>
                    bdo_ready_p   <= do_ready;
                    do_valid1     <= bdo_valid_p;
                    do_data1      <= bdo_p; 

                --STATUS
                when S_STATUS_FAIL =>
                    do_valid1     <= '1';
                    do_last1      <= '1';
                    do_data1_opcode       <= INST_FAILURE;
                    do_data1(27 downto 0) <= (others=>'0');

                when S_STATUS_SUCCESS =>
                    do_valid1     <= '1';
                    do_last1      <= '1';
                    do_data1_opcode       <= INST_SUCCESS;
                    do_data1(27 downto 0) <= (others=>'0');
        end case;
    end process;

    --==========================================================================
    --! set axi stream output
    --==========================================================================

    do_valid <= do_valid1;
    do_last  <= do_last1 and do_valid1;
    do_data  <= do_data1 when (do_valid1='1') else (others=>'Z');

end PostProcessor;

