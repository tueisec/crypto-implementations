--------------------------------------------------------------------------------
--! @file       CipherCore.vhd
--! @brief      Top-Level of the Gimli Cipher Implementation
--! @author     Patrick Karl <patrick.karl@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--! @license    This project is released under the GNU Public License.          
--!             The license and distribution terms for this file may be         
--!             found in the file LICENSE in this distribution or at            
--!             http://www.gnu.org/licenses/gpl-3.0.txt 
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.math_real."ceil";
use IEEE.math_real."log2";
--! I/O specific package.
use work.Design_pkg.all;
--! API package.
use work.NIST_LWAPI_pkg.all;
--! Gimli specific package.
use work.gimli_pkg.all;

--! This module contains and connects the module instances of the gimli state RAM,
--! the state manager and the gimli permutation module.
entity CipherCore is
generic (
    G_DBLK_SIZE     : integer := 128  --! Block size
);
Port (
    --!Control Port================================================
    clk             : in   std_logic;
    rst             : in   std_logic;
    --!PreProcessor===============================================
    ----!key----------------------------------------------------
    key             : in   std_logic_vector (CORE_SW      -1 downto 0);
    key_valid       : in   std_logic;
    key_ready       : out  std_logic;
    key_update      : in   std_logic;
    ----!Data----------------------------------------------------
    bdi             : in   std_logic_vector (CORE_PW      -1 downto 0);
    bdi_valid       : in   std_logic;
    bdi_ready       : out  std_logic;
    bdi_pad_loc     : in   std_logic_vector (CORE_PWdiv8  -1 downto 0);
    bdi_valid_bytes : in   std_logic_vector (CORE_PWdiv8  -1 downto 0);
    bdi_size        : in   std_logic_vector (3       -1 downto 0);
    bdi_eot         : in   std_logic;
    bdi_eoi         : in   std_logic;
    bdi_type        : in   std_logic_vector (4       -1 downto 0);
    decrypt_in      : in   std_logic;
    hash_in         : in   std_logic;
    --!PostProcessor=========================================
    bdo             : out  std_logic_vector (CORE_PW      -1 downto 0);
    bdo_valid       : out  std_logic;
    bdo_ready       : in   std_logic;
    bdo_type        : out  std_logic_vector (4       -1 downto 0);
    bdo_valid_bytes : out  std_logic_vector (CORE_PWdiv8  -1 downto 0);
    end_of_block    : out  std_logic;
    decrypt_out     : out  std_logic;
    msg_auth        : out  std_logic;
    msg_auth_valid  : out  std_logic;
    msg_auth_ready  : in   std_logic
);
end CipherCore;

--! @brief Structural description of the CipherCore.
--! @details  The CipherCore module contains instances of the Gimli round function
--!           module, the state manager and a RAM module storing the 384-Bit state.
architecture structure of CipherCore is

  -- Gimli Signals
  signal gimli_start_s          : std_logic;
  signal gimli_done_s           : std_logic;
  signal gimli_wen_to_mem_s     : std_logic;
  signal gimli_addr_to_mem_s    : std_logic_vector(ADDR_WIDTH_C - 1 downto 0);
  signal gimli_data_from_mem_s  : std_logic_vector(CORE_PW - 1 downto 0);
  signal gimli_data_to_mem_s    : std_logic_vector(CORE_PW - 1 downto 0);

  -- RAM signals
  signal ram_wen_s  : std_logic;
  signal ram_addr_s : std_logic_vector(ADDR_WIDTH_C - 1 downto 0);
  signal ram_din_s  : std_logic_vector(CORE_PW - 1 downto 0);
  signal ram_dout_s : std_logic_vector(CORE_PW - 1 downto 0);

  -- State Management signals --
  -- Control signals
  signal gimli_active_s : boolean;
  -- RAM port
  signal mgmt_wen_to_mem_s    : std_logic;
  signal mgmt_addr_to_mem_s   : std_logic_vector(ADDR_WIDTH_C - 1 downto 0);
  signal mgmt_data_from_mem_s : std_logic_vector(CORE_PW - 1 downto 0);
  signal mgmt_data_to_mem_s   : std_logic_vector(CORE_PW - 1 downto 0);

  signal key_s                : std_logic_vector(CORE_SW - 1 downto 0);

  signal bdo_s                : std_logic_vector(CORE_PW - 1 downto 0);
  signal bdo_valid_bytes_s    : std_logic_vector(CORE_PWdiv8 - 1 downto 0);

  signal bdi_s                : std_logic_vector(CORE_PW - 1 downto 0);
  signal bdi_valid_bytes_s    : std_logic_vector(CORE_PWdiv8 - 1 downto 0);
  signal bdi_pad_loc_s        : std_logic_vector(CORE_PWdiv8 - 1 downto 0);

begin

  ------------------------------------------------------------------------------
  -- Signal Connections / Multiplexer
  ------------------------------------------------------------------------------
  gimli_data_from_mem_s <= ram_dout_s;
  mgmt_data_from_mem_s  <= ram_dout_s;

  -- Depending on the current phase, give memory access to Gimli or
  -- to state management.
  ram_wen_s   <= gimli_wen_to_mem_s  when (gimli_active_s) else mgmt_wen_to_mem_s;
  ram_addr_s  <= gimli_addr_to_mem_s when (gimli_active_s) else mgmt_addr_to_mem_s;
  ram_din_s   <= gimli_data_to_mem_s when (gimli_active_s) else mgmt_data_to_mem_s;


  -- Change Byte/Bit order from host-to-network or vice versa.
  key_s             <= ntoh_Byte(key);
  bdo               <= hton_Byte(bdo_s);
  bdo_valid_bytes   <= hton_Bit(bdo_valid_bytes_s);
  bdi_s             <= ntoh_Byte(bdi);
  bdi_valid_bytes_s <= ntoh_Bit(bdi_valid_bytes);
  bdi_pad_loc_s     <= ntoh_Bit(bdi_pad_loc);

  ------------------------------------------------------------------------------
  -- Module Instantiations
  ------------------------------------------------------------------------------
  --! Instance of Gimli Round fucntion module.
  gimli_inst : entity work.gimli(behavioral)
    generic map(
      N_ROUNDS_G => N_ROUNDS_C
    )
    port map(
      clk           => clk,
      reset         => rst,
      start         => gimli_start_s,
      done          => gimli_done_s,
      wen_to_mem    => gimli_wen_to_mem_s,
      addr_to_mem   => gimli_addr_to_mem_s,
      data_from_mem => gimli_data_from_mem_s,
      data_to_mem   => gimli_data_to_mem_s
    );

  --! RAM storing the 384 bit Gimli state
  state_ram_inst : entity work.SPDRam(behavioral)
    generic map(
      DataWidth => CORE_PW,
      AddrWidth => ADDR_WIDTH_C
    )
    port map(
      clk  => clk,
      wen  => ram_wen_s,
      addr => ram_addr_s,
      din  => ram_din_s,
      dout => ram_dout_s
    );

  --! State Manager preparing the Gimli state and extracting data from it.
  state_mgmt_inst : entity work.state_manager(behavioral)
    port map(
      clk             => clk,
      rst             => rst,
      key             => key_s,
      key_valid       => key_valid,
      key_ready       => key_ready,
      key_update      => key_update,
      bdi             => bdi_s,
      bdi_valid       => bdi_valid,
      bdi_ready       => bdi_ready,
      bdi_pad_loc     => bdi_pad_loc_s,
      bdi_valid_bytes => bdi_valid_bytes_s,
      bdi_size        => bdi_size,
      bdi_eot         => bdi_eot,
      bdi_eoi         => bdi_eoi,
      bdi_type        => bdi_type,
      decrypt_in      => decrypt_in,
      hash_in         => hash_in,
      bdo             => bdo_s,
      bdo_valid       => bdo_valid,
      bdo_ready       => bdo_ready,
      bdo_type        => bdo_type,
      bdo_valid_bytes => bdo_valid_bytes_s,
      end_of_block    => end_of_block,
      decrypt_out     => decrypt_out,
      msg_auth        => msg_auth,
      msg_auth_valid  => msg_auth_valid,
      msg_auth_ready  => msg_auth_ready,
      gimli_active    => gimli_active_s,
      gimli_start     => gimli_start_s,
      gimli_done      => gimli_done_s,
      wen_to_mem      => mgmt_wen_to_mem_s,
      addr_to_mem     => mgmt_addr_to_mem_s,
      data_to_mem     => mgmt_data_to_mem_s,
      data_from_mem   => mgmt_data_from_mem_s
    );

end structure;
