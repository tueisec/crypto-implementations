--------------------------------------------------------------------------------
--! @file       state_manager.vhd
--! @brief      Controlling the 384bit permutation state.
--! @author     Patrick Karl <patrick.karl@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--! @license    This project is released under the GNU Public License.          
--!             The license and distribution terms for this file may be         
--!             found in the file LICENSE in this distribution or at            
--!             http://www.gnu.org/licenses/gpl-3.0.txt 
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! API package.
use work.NIST_LWAPI_pkg.all;
--! I/O specific package.
use work.Design_pkg.all;
--! Gimli specific package.
use work.gimli_pkg.all;

--! State manager entity - This module implements the control fsm. It manages the gimli state, i.e.
--! reading or reloading key, reading nonce, extracting cypher,
--! message, tag, hash and starting the gimli permutation.
entity state_manager is
  port (
    --!Control Port=====================================
    clk             : in   std_logic;
    rst             : in   std_logic;
    gimli_active    : out  boolean;
    gimli_start     : out  std_logic;
    gimli_done      : in   std_logic;
    --!PreProcessor=============================================
    ----!key----------------------------------------------------
    key             : in   std_logic_vector (CORE_SW      -1 downto 0);
    key_valid       : in   std_logic;
    key_ready       : out  std_logic;
    key_update      : in   std_logic;
    ----!Data---------------------------------------------------
    bdi             : in   std_logic_vector (CORE_PW      -1 downto 0);
    bdi_valid       : in   std_logic;
    bdi_ready       : out  std_logic;
    bdi_pad_loc     : in   std_logic_vector (CORE_PWdiv8  -1 downto 0);
    bdi_valid_bytes : in   std_logic_vector (CORE_PWdiv8  -1 downto 0);
    bdi_size        : in   std_logic_vector (3            -1 downto 0);
    bdi_eot         : in   std_logic;
    bdi_eoi         : in   std_logic;
    bdi_type        : in   std_logic_vector (4            -1 downto 0);
    decrypt_in      : in   std_logic;
    hash_in         : in   std_logic;
    --!PostProcessor============================================
    bdo             : out  std_logic_vector (CORE_PW      -1 downto 0);
    bdo_valid       : out  std_logic;
    bdo_ready       : in   std_logic;
    bdo_type        : out  std_logic_vector (4            -1 downto 0);
    bdo_valid_bytes : out  std_logic_vector (CORE_PWdiv8  -1 downto 0);
    end_of_block    : out  std_logic;
    decrypt_out     : out  std_logic;
    msg_auth        : out  std_logic;
    msg_auth_valid  : out  std_logic;
    msg_auth_ready  : in   std_logic;
    --!RAM Port================================================
    wen_to_mem      : out std_logic;
    addr_to_mem     : out std_logic_vector(ADDR_WIDTH_C - 1 downto 0);
    data_to_mem     : out std_logic_vector(CORE_PW - 1 downto 0);
    data_from_mem   : in  std_logic_vector(CORE_PW - 1 downto 0)
  );
end state_manager;

--! @brief    Behavioral description of the state_manager.
--! @details  The state manager is implemented as a 4-process FSM, i.e. a common
--!           3-state Mealy FSM with an additional process for address generation
--!           (for better readability). The purpose of this module is to absorb/squeeze
--!           data from the state RAM and trigger permutation. Contains RAM instance
--!           for key storage.
architecture behavioral of state_manager is

  -- Constants for Key (-address) management.
  constant KEY_ADDR_BITS_C  : integer range 2 to 5          := log2_ceil(KEY_SIZE/CORE_PW);
  constant KEY_FIRST_ROW_C  : integer range 2 to 2          := 2;
  constant KEY_SECOND_ROW_C : integer range 3 to 3          := 3;
  -- Constant byte used for padding.
  constant ONE_BYTE_C       : std_logic_vector(7 downto 0)  := x"01";
  constant EMPTY_SIZE_C     : std_logic_vector(2 downto 0)  := (others => '0');

  --! State definition
  type state_t is ( IDLE,
                    INIT_HASH,
                    LOAD_KEY_FIRST_ROW,
                    LOAD_KEY_SECOND_ROW,
                    LOAD_NONCE,
                    LOAD_AD,
                    PADD_LAST_BYTE_AD,
                    LOAD_DATA,
                    PADD_LAST_BYTE_DATA,
                    TRANSMIT_TAG,
                    TRANSMIT_TAG_EXT,
                    VERIFY_TAG,
                    WAIT_ACK,
                    RUN_GIMLI_INIT,
                    RUN_GIMLI_AD,
                    RUN_GIMLI_AD_PADD,
                    RUN_GIMLI_DATA,
                    RUN_GIMLI_DATA_PADD,
                    RUN_GIMLI_HASH
                  );
  signal state_s, n_state_s : state_t;

  -- Control flags / Counters
  signal key_ready_s      : std_logic;
  signal n_key_ready_s    : std_logic;
  signal restore_key_s    : std_logic;
  signal n_restore_key_s  : std_logic;
  signal eoi_s            : std_logic;
  signal n_eoi_s          : std_logic;
  signal eot_s            : std_logic;
  signal n_eot_s          : std_logic;
  signal hash_s           : std_logic;
  signal n_hash_s         : std_logic;
  signal cycle_cnt_s      : integer range 0 to N_ROW_C*BEATS_PER_WORD_C - 1;
  signal col_cnt_s        : integer range 0 to N_COL_C - 1;

  -- RAM signals for key storage
  signal key_ram_wen_s        : std_logic;
  signal key_ram_addr_s       : std_logic_vector(KEY_ADDR_BITS_C - 1 downto 0);
  signal key_ram_din_s        : std_logic_vector(CORE_PW - 1 downto 0);
  signal key_ram_dout_s       : std_logic_vector(CORE_PW - 1 downto 0);
  signal key_addr_row1_s      : std_logic_vector(KEY_ADDR_BITS_C - 1 downto 0);
  signal key_addr_row2_s      : std_logic_vector(KEY_ADDR_BITS_C - 1 downto 0);

  -- Data to gimli RAM
  signal data_to_mem_s        : std_logic_vector(CORE_PW - 1 downto 0);

  -- Output Signals
  signal bdi_ready_s          : std_logic;
  signal n_bdi_ready_s        : std_logic;
  signal gimli_start_s        : std_logic;
  signal n_gimli_start_s      : std_logic;

  signal bdo_s                : std_logic_vector(CORE_PW - 1 downto 0);
  signal n_bdo_s              : std_logic_vector(CORE_PW - 1 downto 0);
  signal bdo_valid_s          : std_logic;
  signal n_bdo_valid_s        : std_logic;
  signal bdo_valid_bytes_s    : std_logic_vector(CORE_PWdiv8 - 1 downto 0);
  signal n_bdo_valid_bytes_s  : std_logic_vector(CORE_PWdiv8 - 1 downto 0);
  signal decrypt_s            : std_logic;
  signal n_decrypt_s          : std_logic;
  signal end_of_block_s       : std_logic;
  signal n_end_of_block_s     : std_logic;

  signal msg_auth_valid_s     : std_logic;
  signal n_msg_auth_valid_s   : std_logic;
  signal msg_auth_s           : std_logic;
  signal n_msg_auth_s         : std_logic;

begin

  ------------------------------------------------------------------------------
  -- Mapping internal signals to output
  ------------------------------------------------------------------------------
  key_ready       <= key_ready_s;
  gimli_start     <= gimli_start_s;
  gimli_active    <= true when (state_s = RUN_GIMLI_INIT
                                or state_s = RUN_GIMLI_AD
                                or state_s = RUN_GIMLI_AD_PADD
                                or state_s = RUN_GIMLI_DATA
                                or state_s = RUN_GIMLI_DATA_PADD
                                or state_s = RUN_GIMLI_HASH) else false;
  bdi_ready       <= bdi_ready_s;
  bdo             <= bdo_s;
  bdo_valid       <= bdo_valid_s;
  bdo_valid_bytes <= bdo_valid_bytes_s;
  end_of_block    <= end_of_block_s;
  decrypt_out     <= decrypt_s;

  -- Hashing only has one output type.
  -- Tag is transmitted in encryption mode in TRANSMIT_TAG and one word in WAIT_ACK state.
  -- Otherwise CT is the only other type in encryption mode.
  -- Every other possible type (which is basically decryption) results in MSG.
  -- In case on wants to hide this if output is *don't care*, just mask it with bdo_valid_s...
  bdo_type <= HDR_HASH_MSG  when (hash_s = '1') else
              HDR_TAG       when (decrypt_s = '0' and (state_s = TRANSMIT_TAG or state_s = WAIT_ACK)) else
              HDR_CT        when (decrypt_s = '0') else
              HDR_PT;

  msg_auth        <= msg_auth_s;
  msg_auth_valid  <= msg_auth_valid_s;

  ------------------------------------------------------------------------------
  -- RAM signals leading to gimli state
  ------------------------------------------------------------------------------
  addr_to_mem <=  std_logic_vector(to_unsigned(cycle_cnt_s + col_cnt_s*BEATS_PER_WORD_C*N_ROW_C, ADDR_WIDTH_C));
  -- If new key is provided, directly write key input into gimli state RAM,
  -- else take old value from key RAM.
  data_to_mem <=  key             when ((state_s = LOAD_KEY_FIRST_ROW
                                      or state_s = LOAD_KEY_SECOND_ROW)
                                      and restore_key_s = '0') else
                  key_ram_dout_s  when ((state_s = LOAD_KEY_FIRST_ROW
                                      or state_s = LOAD_KEY_SECOND_ROW)
                                      and restore_key_s = '1') else
                  data_to_mem_s   when ( state_s = LOAD_NONCE
                                      or state_s = LOAD_AD
                                      or state_s = PADD_LAST_BYTE_AD
                                      or state_s = LOAD_DATA
                                      or state_s = PADD_LAST_BYTE_DATA) else
                  (others => '0') when (state_s = INIT_HASH) else
                  (others => '-');
  -- Simply enable memory whenever a new key is provided and handshaking is performed or
  -- if new data is provided and handshaking is performed or
  -- when last block of AD or DATA is being padded.
  wen_to_mem  <=  '1' when ((state_s = LOAD_KEY_FIRST_ROW or state_s = LOAD_KEY_SECOND_ROW)
                          and ((key_valid = '1' and key_ready_s = '1') or restore_key_s = '1')) else
                  '1' when ((state_s = LOAD_NONCE
                          or state_s = LOAD_AD
                          or state_s = LOAD_DATA)
                          and bdi_valid = '1' and bdi_ready_s = '1')  else
                  '1' when (  state_s = PADD_LAST_BYTE_DATA
                          or  state_s = PADD_LAST_BYTE_AD
                          or  state_s = INIT_HASH) else
                  '0';


  ------------------------------------------------------------------------------
  --! RAM Instantiation for key storage
  ------------------------------------------------------------------------------
  key_ram_inst : entity work.SPDRam(behavioral)
  generic map(
    DataWidth => CORE_PW,
    AddrWidth => KEY_ADDR_BITS_C
  )
  port map(
    clk  => clk,
    wen  => key_ram_wen_s,
    addr => key_ram_addr_s,
    din  => key_ram_din_s,
    dout => key_ram_dout_s
  );

  -- Enable data input when Key shall be loaded and transfer actualy happens (valid & ready)
  key_ram_wen_s   <=  '1' when ((state_s = LOAD_KEY_FIRST_ROW or state_s = LOAD_KEY_SECOND_ROW)
                          and key_valid = '1' and key_ready_s = '1') else
                      '0';
  key_ram_din_s   <=  key;
  key_ram_addr_s  <=  key_addr_row1_s when (state_s = LOAD_KEY_FIRST_ROW) else
                      key_addr_row2_s;

  -- Renaming signals to have better readability later in the code. The Address
  -- Mapping is a little bit tricky because we use the same counters for gimli
  -- state RAM and key RAM.
  -- In the first key row, remove the addr offset from the cycle counter,
  -- becaues we are starting at the second row.
  key_addr_row1_s <=  std_logic_vector(to_unsigned(cycle_cnt_s - BEATS_PER_WORD_C
                      + col_cnt_s*BEATS_PER_WORD_C, KEY_ADDR_BITS_C));

  -- Again, remove the offset from the cycle counter. In the end, add another
  -- offset because one row is already filled with the first 128 bit of the key.
  key_addr_row2_s <=  std_logic_vector(to_unsigned(cycle_cnt_s - KEY_FIRST_ROW_C*BEATS_PER_WORD_C
                      + col_cnt_s*BEATS_PER_WORD_C + N_COL_C*BEATS_PER_WORD_C, KEY_ADDR_BITS_C));


  ------------------------------------------------------------------------------
  -- Control FSM
  ------------------------------------------------------------------------------
  --! Output registers
  output_register_p : process(clk)
  begin
      if rising_edge(clk) then
        if (rst = GIMLI_RESET_C) then
          state_s           <= IDLE;
          key_ready_s       <= '0';
          bdi_ready_s       <= '0';
          decrypt_s         <= '-';
          bdo_s             <= (others => '0');
          bdo_valid_s       <= '0';
          bdo_valid_bytes_s <= (others => '0');
          end_of_block_s    <= '0';
          msg_auth_valid_s  <= '0';
          msg_auth_s        <= '1';
          restore_key_s     <= '0';
          eoi_s             <= '0';
          eot_s             <= '0';
          hash_s            <= '0';
          gimli_start_s     <= '0';
        else
          state_s           <= n_state_s;
          key_ready_s       <= n_key_ready_s;
          bdi_ready_s       <= n_bdi_ready_s;
          decrypt_s         <= n_decrypt_s;
          bdo_s             <= n_bdo_s;
          bdo_valid_s       <= n_bdo_valid_s;
          bdo_valid_bytes_s <= n_bdo_valid_bytes_s;
          end_of_block_s    <= n_end_of_block_s;
          msg_auth_valid_s  <= n_msg_auth_valid_s;
          msg_auth_s        <= n_msg_auth_s;
          restore_key_s     <= n_restore_key_s;
          eoi_s             <= n_eoi_s;
          eot_s             <= n_eot_s;
          hash_s            <= n_hash_s;
          gimli_start_s     <= n_gimli_start_s;
        end if;
      end if;
  end process output_register_p;

  --! Next state logic
  next_state_p : process( rst, state_s, cycle_cnt_s, col_cnt_s, bdi_size,
                          key_valid, key_ready_s, bdi_valid, bdi_type, bdi_ready_s,
                          bdi_eot, gimli_done, bdo_ready, bdo_valid_s, decrypt_s, hash_in,
                          hash_s, msg_auth_valid_s, msg_auth_ready, restore_key_s, eoi_s, eot_s)
  begin
    case state_s is

      -- Wait for input. Depending on request, load key or
      -- initialize state with zero.
      when IDLE =>
        if (bdi_valid = '1' or key_valid = '1') then
          if (hash_in = '1') then
            n_state_s <= INIT_HASH;
          else
            n_state_s <= LOAD_KEY_FIRST_ROW;
          end if;
        else
          n_state_s <= IDLE;
        end if;

      -- When state initialized, go to padding state (if empty hash) or load ad.
      when INIT_HASH =>
        if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          if (eoi_s = '1') then
            n_state_s <= PADD_LAST_BYTE_AD;
          else
            n_state_s <= LOAD_AD;
          end if;
        else
          n_state_s <= INIT_HASH;
        end if;

      -- Load first row of key.
      when LOAD_KEY_FIRST_ROW =>
        if (((key_valid = '1' and key_ready_s = '1') or restore_key_s = '1') and
        cycle_cnt_s >= KEY_FIRST_ROW_C*BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_state_s <= LOAD_KEY_SECOND_ROW;
        else
          n_state_s <= LOAD_KEY_FIRST_ROW;
        end if;

      -- When key is received, load nonce.
      when LOAD_KEY_SECOND_ROW =>
        if (((key_valid = '1' and key_ready_s = '1') or restore_key_s = '1') and
        cycle_cnt_s >= KEY_SECOND_ROW_C*BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_state_s <= LOAD_NONCE;
        else
          n_state_s <= LOAD_KEY_SECOND_ROW;
        end if;

      -- After loading nonce, run gimli permutation.
      when LOAD_NONCE =>
        if (bdi_valid = '1' and bdi_ready_s = '1'
        and cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_state_s <= RUN_GIMLI_INIT;
        else
          n_state_s <= LOAD_NONCE;
        end if;

      -- If no AD is provided, insert padding block, else load AD.
      when RUN_GIMLI_INIT =>
        if (gimli_done = '1') then
          if (eoi_s = '1' or (bdi_valid = '1' and bdi_type /= HDR_AD)) then
            n_state_s <= PADD_LAST_BYTE_AD;
          else
            n_state_s <= LOAD_AD;
          end if;
        else
          n_state_s <= RUN_GIMLI_INIT;
        end if;

      -- Absorb AD. If eot is detected decide to directly padd or to absorb
      -- another AD block.
      when LOAD_AD =>
        if (bdi_valid = '1' and bdi_ready_s = '1' and bdi_eot = '1'
        and ((cycle_cnt_s /= BEATS_PER_WORD_C - 1 or col_cnt_s /= N_COL_C - 1)
        or (to_integer(unsigned(bdi_size)) /= CORE_PWdiv8))) then
          n_state_s <= PADD_LAST_BYTE_AD;
        elsif (bdi_valid = '1' and bdi_ready_s = '1'
        and cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_state_s <= RUN_GIMLI_AD;
        else
          n_state_s <= LOAD_AD;
        end if;

      -- Padd AD and start gimli permutation.
      when PADD_LAST_BYTE_AD =>
        if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_state_s <= RUN_GIMLI_AD_PADD;
        else
          n_state_s <= PADD_LAST_BYTE_AD;
        end if;

      -- If permutation is finished, go back to absorb more AD blocks or go padding.
      when RUN_GIMLI_AD =>
        if (gimli_done = '1') then
          if (eoi_s = '1' or eot_s = '1') then
            n_state_s <= PADD_LAST_BYTE_AD;
          else
            n_state_s <= LOAD_AD;
          end if;
        else
          n_state_s <= RUN_GIMLI_AD;
        end if;

      -- After padded AD block, transmit tag in case of hashing mode,
      -- Absorb data padding block if no PT / CT is available or load data.
      when RUN_GIMLI_AD_PADD =>
        if (gimli_done = '1') then
          if (hash_s = '1') then
            n_state_s <= TRANSMIT_TAG;
          elsif(eoi_s = '1') then
            n_state_s <= PADD_LAST_BYTE_DATA;
          else
            n_state_s <= LOAD_DATA;
          end if;
        else
          n_state_s <= RUN_GIMLI_AD_PADD;
        end if;

      -- Absorb data. If eot is detected, insert padding block. Otherwise start
      -- gimli permutation if block is fully filled.
      when LOAD_DATA =>
        if (bdi_valid = '1' and bdi_ready_s = '1' and bdi_eot = '1'
        and ((cycle_cnt_s /= BEATS_PER_WORD_C - 1 or col_cnt_s /= N_COL_C - 1)
        or (to_integer(unsigned(bdi_size)) /= CORE_PWdiv8))) then
          n_state_s <= PADD_LAST_BYTE_DATA;
        elsif (bdi_valid = '1' and bdi_ready_s = '1'
        and cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_state_s <= RUN_GIMLI_DATA;
        else
          n_state_s <= LOAD_DATA;
        end if;

      -- After inserting PT /CT padding, start gimli permutation.
      when PADD_LAST_BYTE_DATA =>
        if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_state_s <= RUN_GIMLI_DATA_PADD;
        else
          n_state_s <= PADD_LAST_BYTE_DATA;
        end if;

      -- When permutation finishes, insert padding block or absorb another
      -- PT / CT block.
      when RUN_GIMLI_DATA =>
        if (gimli_done = '1') then
          if (eoi_s = '1') then
            n_state_s <= PADD_LAST_BYTE_DATA;
          else
            n_state_s <= LOAD_DATA;
          end if;
        else
          n_state_s <= RUN_GIMLI_DATA;
        end if;

      -- After gimli permuation, either squeeze or verify tag depending
      -- on encryption / decryption mode.
      when RUN_GIMLI_DATA_PADD =>
        if (gimli_done = '1' and decrypt_s = '0') then
          n_state_s <= TRANSMIT_TAG;
        elsif (gimli_done = '1' and decrypt_s = '1') then
          n_state_s <= VERIFY_TAG;
        else
          n_state_s <= RUN_GIMLI_DATA_PADD;
        end if;

      -- Pass tag and wait for acknowlede afterwards or run another gimli
      -- permutation when in hash mode (hash is 32-Byte).
      when TRANSMIT_TAG =>
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1
        and bdo_valid_s = '1' and bdo_ready = '1') then
          if (hash_s = '1') then
            n_state_s <= RUN_GIMLI_HASH;
          else
            n_state_s <= WAIT_ACK;
          end if;
        else
          n_state_s <= TRANSMIT_TAG;
        end if;

      -- Transmit second tag block in hashing mode.
      when TRANSMIT_TAG_EXT =>
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1
        and bdo_valid_s = '1' and bdo_ready = '1') then
          n_state_s <= WAIT_ACK;
        else
          n_state_s <= TRANSMIT_TAG_EXT;
        end if;

      -- Wait for done flag before squeezing second hash tag block.
      when RUN_GIMLI_HASH =>
        if (gimli_done = '1') then
          n_state_s <= TRANSMIT_TAG_EXT;
        else
          n_state_s <= RUN_GIMLI_HASH;
        end if;

      -- After tag is verified or rejected, wait for acknowledgement.
      when VERIFY_TAG =>
        if (bdi_valid = '1' and bdi_ready_s = '1' and bdi_type = HDR_TAG
        and cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_state_s <= WAIT_ACK;
        else
          n_state_s <= VERIFY_TAG;
        end if;

      -- After acknowledgement of result, processing is done.
      when WAIT_ACK =>
        if ((hash_s = '1' or decrypt_s = '0') and bdo_valid_s = '1' and bdo_ready = '1')
        or (msg_auth_valid_s = '1' and msg_auth_ready = '1') then
          n_state_s <= IDLE;
        else
          n_state_s <= WAIT_ACK;
        end if;

      -- If something went wrong and we traversed to invalid state, abort and
      -- go back to IDLE.
      when others =>
        n_state_s <= IDLE;

    end case;
  end process next_state_p;

  --! Output decoder process
  output_decode_p : process(state_s, cycle_cnt_s, col_cnt_s, bdi_valid_bytes, bdi,
                            key_valid, key_ready_s, key_update, bdi_valid, bdi_eoi,
                            bdi_ready_s, bdi_eot, bdi_type, bdi_size, gimli_done,
                            data_from_mem, hash_in, hash_s, decrypt_s, decrypt_in, bdo_s,
                            bdo_valid_s, bdo_valid_bytes_s, bdo_ready, bdi_pad_loc,
                            msg_auth_s, msg_auth_valid_s, msg_auth_ready, end_of_block_s,
                            restore_key_s, eoi_s, eot_s)
  begin
    -- Defaults preventing latches
    n_key_ready_s       <= key_ready_s;
    n_restore_key_s     <= restore_key_s;
    n_bdi_ready_s       <= bdi_ready_s;
    data_to_mem_s       <= data_from_mem;
    n_decrypt_s         <= decrypt_s;
    n_bdo_s             <= bdo_s;
    n_bdo_valid_s       <= bdo_valid_s;
    n_bdo_valid_bytes_s <= bdo_valid_bytes_s;
    n_end_of_block_s    <= end_of_block_s;
    n_msg_auth_s        <= msg_auth_s;
    n_msg_auth_valid_s  <= msg_auth_valid_s;
    n_eoi_s             <= eoi_s;
    n_eot_s             <= eot_s;
    n_hash_s            <= hash_s;
    n_gimli_start_s     <= '0';
    case state_s is

      -- Wait for bdi_valid or key_valid.
      -- Check for new key.
      -- Check for hash mode.
      when IDLE =>
        if (bdi_valid = '1' and hash_in = '1') then
          n_hash_s <= '1';
          if (bdi_size = EMPTY_SIZE_C) then
            n_eoi_s       <= '1';
            n_bdi_ready_s <= '1';
          end if;
        elsif (key_valid = '1' and key_update = '1') then
          n_key_ready_s   <= '1';
        elsif (bdi_valid = '1') then
          n_restore_key_s <= '1';
        end if;

      -- Initialize the state with zero words. In case empty hash is acknowledged
      -- reset bdi_ready again. After filling the state set bdi_ready.
      when INIT_HASH =>
        if (bdi_valid = '1' and bdi_ready_s = '1') then
          n_bdi_ready_s <= '0';
        end if;
        if (cycle_cnt_s >= N_ROW_C * BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1
        and eoi_s = '0') then
          n_bdi_ready_s <= '1';
        end if;

      -- Wait until first 128-Bit of the key are loaded.
      when LOAD_KEY_FIRST_ROW =>
        null;

      -- Load the second 128bit into row 3 of the state.
      when LOAD_KEY_SECOND_ROW =>
        if ((key_valid = '1' and key_ready_s = '1') or restore_key_s = '1') and
        (cycle_cnt_s >= KEY_SECOND_ROW_C*BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          n_key_ready_s   <= '0';
          n_restore_key_s <= '0';
          n_bdi_ready_s   <= '1';
        end if;

      -- Load Nonce into the first row of the state.
      -- Not checking on valid_bytes. It is assumed that 128-Bit Nonce is
      -- provided in fully filled words.
      when LOAD_NONCE =>
        if (bdi_valid = '1' and bdi_ready_s = '1' and bdi_type = HDR_NPUB) then
            data_to_mem_s <= bdi; -- OPT: Check for valid bytes
            n_decrypt_s   <= decrypt_in;
            if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
              n_gimli_start_s <= '1';
              n_bdi_ready_s   <= '0';
              n_eoi_s         <= bdi_eoi;
            end if;
        end if;

      -- After permutation, set bdi_ready only if AD actually follows.
      when RUN_GIMLI_INIT =>
        if (gimli_done = '1' and eoi_s = '0'
        and bdi_valid = '1' and bdi_type = HDR_AD) then
          n_bdi_ready_s <= '1';
        end if;

      -- Anytime data is transferred, xor the data in memory with the AD / HASH
      -- byte and write it back. Include padding in the byte after the last valid
      -- byte. This is the same as processing hashed data.
      when LOAD_AD =>
        if (bdi_valid = '1' and bdi_ready_s = '1'
        and ((hash_s = '1' and bdi_type = HDR_HASH_MSG) or (hash_s = '0' and bdi_type = HDR_AD))) then
          for i in 0 to (CORE_PWdiv8 - 1) loop
            if (bdi_valid_bytes(i) = '1') then
              data_to_mem_s(8*(i+1) - 1 downto 8*i) <= data_from_mem(8*(i+1) - 1 downto 8*i) xor bdi(8*(i+1) - 1 downto 8*i);
            elsif (bdi_pad_loc(i) = '1') then
              data_to_mem_s(8*(i+1) - 1 downto 8*i) <= data_from_mem(8*(i+1) - 1 downto 8*i) xor ONE_BYTE_C;
            end if;
          end loop;
          -- Start permutation after full block.
          if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
            n_bdi_ready_s   <= '0';
            n_gimli_start_s <= '1';
          end if;
          -- Go directly into Padding last byte if eot detected.
          -- If slice wasn't completely filled yet, the 'paddig_start' maker has
          -- already been set, so only the last byte has to be padded.
          if (bdi_eot = '1') then
            n_bdi_ready_s   <= '0';
            n_eot_s			<= bdi_eot;
            n_eoi_s         <= bdi_eoi;
            -- Gimli starts automatically after full block. If the last block is only
            -- partially filled in the last word, don't start gimli but insert padding.
            if (to_integer(unsigned(bdi_size)) /= CORE_PWdiv8) then
              n_gimli_start_s <= '0';
            end if;
          end if;
        end if;

      -- Wait until gimli finished and assert bdi_ready again in
      -- case PT follows (bdi_eoi not set) and module not in hash mode.
      -- Reset eot_s, next eot is correctly detected.
      when RUN_GIMLI_AD_PADD =>
        n_eot_s <= '0';
        if (gimli_done = '1' and eoi_s = '0' and hash_s = '0') then
            n_bdi_ready_s <= '1';
        end if;

      -- Anytime data is transferred, xor the data in memory with the new PT
      -- byte and write it back.
      -- Include padding in the byte after the last valid byte.
      when LOAD_DATA =>
        -- Reset bdo_valid if word is acknowledged. It's not known yet whether
        -- another word can be transmitted.
        if (bdo_valid_s = '1' and bdo_ready = '1') then
          n_bdo_valid_s <= '0';
        end if;
        -- Set bdi_ready if no word is transmitted yet or if previous word is
        -- acknowledged. Else reset and wait for acknowledgement.
        if (bdo_valid_s = '0' or (bdo_valid_s = '1' and bdo_ready = '1')) then
          n_bdi_ready_s <= '1';
        else
          n_bdi_ready_s <= '0';
        end if;
        if (bdi_valid = '1' and bdi_ready_s = '1'
        and (bdi_type = HDR_PT or bdi_type = HDR_CT)) then
          -- Signal no readiness until the following word is acknowledged.
          n_bdi_ready_s       <= '0';
          n_bdo_s             <= (others => '0');
          n_bdo_valid_bytes_s <= (others => '0');
          n_bdo_valid_s       <= '1';
          for i in 0 to (CORE_PWdiv8 - 1) loop
            if (bdi_valid_bytes(i) = '1') then
              if (decrypt_s = '1') then
                data_to_mem_s(8*(i+1) - 1 downto 8*i) <= bdi(8*(i+1) - 1 downto 8*i);
              else
                data_to_mem_s(8*(i+1) - 1 downto 8*i) <= data_from_mem(8*(i+1) - 1 downto 8*i) xor bdi(8*(i+1) - 1 downto 8*i);
              end if;
              n_bdo_s(8*(i+1) - 1 downto 8*i) <= data_from_mem(8*(i+1) - 1 downto 8*i) xor bdi(8*(i+1) - 1 downto 8*i);
              n_bdo_valid_bytes_s(i) <= '1';
            elsif (bdi_pad_loc(i) = '1') then
              data_to_mem_s(8*(i+1) - 1 downto 8*i) <= data_from_mem(8*(i+1) - 1 downto 8*i) xor ONE_BYTE_C;
            end if;
            if (bdi_eot = '1') then
              n_end_of_block_s <= '1';
            end if;
          end loop;
          -- If the receiver is not yet ready, forward backpressure to lose no
          -- word at bdi interface.
          -- Start permutation after full block.
          if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
            n_bdi_ready_s   <= '0';
            n_gimli_start_s <= '1';
          end if;
          -- Prepare padding if eot is detected.
          if (bdi_eot = '1') then
            n_bdi_ready_s   <= '0';
            n_eot_s			<= bdi_eot;
            n_eoi_s         <= bdi_eoi;
            -- Gimli starts automatically after full block. If the last block is only
            -- partially filled in the last word, don't start gimli but insert padding.
            if (to_integer(unsigned(bdi_size)) /= CORE_PWdiv8) then
              n_gimli_start_s <= '0';
            end if;
          end if;
        end if;

      -- In case of decryption, assert bdi_ready again to be prepared to read
      -- the TAG. Disable valid signal if acknowledged by PostProcessor.
      -- Reset eoi_s and eot_s, because final block was transmitted.
      when RUN_GIMLI_DATA_PADD =>
        n_eoi_s <= '0';
        n_eot_s <= '0';
        if (bdo_valid_s = '1' and bdo_ready = '1') then
          n_bdo_valid_s     <= '0';
          n_end_of_block_s  <= '0';
        end if;
        if (gimli_done = '1' and decrypt_s = '1') then
          n_bdi_ready_s <= '1';
        end if;

      -- Padd the last byte with 0x01 and start gimli if
      -- only the last byte has to be padded. Otherwise first pad byte zero in
      -- the current slice.
      when PADD_LAST_BYTE_AD | PADD_LAST_BYTE_DATA =>
        if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
          data_to_mem_s(8*CORE_PWdiv8 - 1 downto 8*(CORE_PWdiv8 - 1)) <=  data_from_mem(8*CORE_PWdiv8 - 1 downto 8*(CORE_PWdiv8 - 1))
                                                                          xor ONE_BYTE_C;
          n_gimli_start_s   <= '1';
        else
          data_to_mem_s(7 downto 0) <= data_from_mem(7 downto 0) xor ONE_BYTE_C;
        end if;
        if (bdo_valid_s = '1' and bdo_ready = '1') then
          n_bdo_valid_s     <= '0';
          n_end_of_block_s  <= '0';
        end if;

      -- Reset bdo signals after acknowledgement. Set bdi_ready only if
      -- input data follows (eoi low) and no padding block is inserted yet (eot low)
      when RUN_GIMLI_AD | RUN_GIMLI_DATA =>
        if (bdo_valid_s = '1' and bdo_ready = '1') then
          n_bdo_valid_s     <= '0';
          n_end_of_block_s  <= '0';
        end if;
        if (gimli_done = '1' and eoi_s = '0' and eot_s = '0') then
            n_bdi_ready_s <= '1';
        end if;

      -- Read out Tag from state
      when TRANSMIT_TAG =>
        if (bdo_valid_s = '0' or (bdo_valid_s = '1' and bdo_ready = '1')) then
          n_bdo_s             <= data_from_mem;
          n_bdo_valid_bytes_s <= (others => '1');
          n_bdo_valid_s       <= '1';
          if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
            n_end_of_block_s  <= not hash_s; -- 32 bit output in HASH mode
            n_gimli_start_s   <= hash_s;     -- start gimli again when hashing
          end if;
        end if;

      -- Run another permutation before squeezing the remaining 16-Byte hash-tag.
      when RUN_GIMLI_HASH =>
        n_eoi_s <= '0';
        if (bdo_valid_s = '1' and bdo_ready = '1') then
          n_bdo_valid_s <= '0';
        end if;

      -- In case of hashing, 32-Byte TAG are transmitted.
      when TRANSMIT_TAG_EXT =>
        if (bdo_valid_s = '0' or (bdo_valid_s = '1' and bdo_ready = '1')) then
          n_bdo_s             <= data_from_mem;
          n_bdo_valid_s       <= '1';
          n_bdo_valid_bytes_s <= (others => '1');
          if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
              n_end_of_block_s  <= '1';
          end if;
        end if;

      -- Compare computed tag with provided tag. If there's one unmatching byte,
      -- reset msg_auth. Wait until the whole tag is processed and then
      -- set msg_auth_valid high.
      when VERIFY_TAG =>
        if (bdi_valid = '1' and bdi_ready_s = '1' and bdi_type = HDR_TAG) then
          if (bdi /= data_from_mem) then
            n_msg_auth_s <= '0';
          end if;
          if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
              n_bdi_ready_s       <= '0';
              n_msg_auth_valid_s  <= '1';
          end if;
        end if;

      -- Wait until authentiation or reception of the last beat of the tag
      -- is acknowledged.
      when WAIT_ACK =>
        if ((hash_s = '1' or decrypt_s = '0') and bdo_valid_s = '1' and bdo_ready = '1') then
          n_bdo_valid_s       <= '0';
          n_bdo_valid_bytes_s <= (others => '0');
          n_end_of_block_s    <= '0';
          n_hash_s            <= '0';
        elsif (msg_auth_valid_s = '1' and msg_auth_ready = '1') then
          n_msg_auth_s 		    <= '1';
          n_msg_auth_valid_s  <= '0';
        end if;

      -- Preventing synthesis erorr...
      when others =>
        null;

    end case;
  end process output_decode_p;

  --! Process generating addresses.
  -- For better readability the address generation was separated from above output decoder.
  p_address_counter : process(clk)
  begin
    if rising_edge(clk) then
      if (rst = GIMLI_RESET_C) then
        cycle_cnt_s <= BEATS_PER_WORD_C;
        col_cnt_s   <= 0;
      else
        case state_s is
          -- Default Address is frst word in second row where key insertion starts.
          -- In hash mode, start at the first word in the first row.
          when IDLE =>
            cycle_cnt_s <= BEATS_PER_WORD_C;
            col_cnt_s   <= 0;
            if (bdi_valid = '1' and hash_in = '1') then
              cycle_cnt_s <= 0;
            end if;

          -- Count from first to last word.
          when INIT_HASH =>
            if (cycle_cnt_s >= N_ROW_C * BEATS_PER_WORD_C - 1) then
              cycle_cnt_s <= 0;
              if (col_cnt_s >= N_COL_C - 1) then
                col_cnt_s <= 0;
              else
                col_cnt_s <= col_cnt_s + 1;
              end if;
            else
              cycle_cnt_s <= cycle_cnt_s + 1;
            end if;

          -- Increase address of second row either any time a byte has been
          -- received and acknowledged or in every cycle when restoring the key
          -- from key RAM.
          when LOAD_KEY_FIRST_ROW =>
            if ((key_valid = '1' and key_ready_s = '1') or restore_key_s = '1') then
              if (cycle_cnt_s >= KEY_FIRST_ROW_C*BEATS_PER_WORD_C - 1) then
                cycle_cnt_s <= BEATS_PER_WORD_C;
                if (col_cnt_s >= N_COL_C - 1) then
                  col_cnt_s   <= 0;
                  cycle_cnt_s <= KEY_FIRST_ROW_C*BEATS_PER_WORD_C;
                else
                  col_cnt_s <= col_cnt_s + 1;
                end if;
              else
                cycle_cnt_s <= cycle_cnt_s + 1;
              end if;
            end if;

          -- Same as previous state for the last row.
          when LOAD_KEY_SECOND_ROW =>
            if ((key_valid = '1' and key_ready_s = '1') or restore_key_s = '1') then
              if (cycle_cnt_s >= KEY_SECOND_ROW_C*BEATS_PER_WORD_C - 1) then
                cycle_cnt_s <= KEY_FIRST_ROW_C*BEATS_PER_WORD_C;
                if (col_cnt_s >= N_COL_C - 1) then
                  col_cnt_s     <= 0;
                  cycle_cnt_s   <= 0;
                else
                  col_cnt_s <= col_cnt_s + 1;
                end if;
              else
                cycle_cnt_s <= cycle_cnt_s + 1;
              end if;
            end if;

          -- Nonce blocks are stored in the first row.
          when LOAD_NONCE =>
            if (bdi_valid = '1' and bdi_ready_s = '1' and bdi_type = HDR_NPUB) then
              if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
                cycle_cnt_s <= 0;
                if (col_cnt_s >= N_COL_C - 1) then
                  col_cnt_s <= 0;
                else
                  col_cnt_s <= col_cnt_s + 1;
                end if;
              else
                cycle_cnt_s <= cycle_cnt_s + 1;
              end if;
            end if;

          -- AD is absorbed in the first row. If padding required and the first padding byte
          -- is already absorbed, set address the last byte of the last word.
          when LOAD_AD =>
            if (bdi_valid = '1' and bdi_ready_s = '1' and (bdi_type = HDR_AD or bdi_type = HDR_HASH_MSG)) then
              if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
                cycle_cnt_s <= 0;
                if (col_cnt_s >= N_COL_C - 1) then
                  col_cnt_s <= 0;
                else
                  col_cnt_s <= col_cnt_s + 1;
                end if;
              else
                cycle_cnt_s <= cycle_cnt_s + 1;
              end if;
              if (bdi_eot = '1' and to_integer(unsigned(bdi_size)) /= CORE_PWdiv8) then
                  cycle_cnt_s <= N_ROW_C*BEATS_PER_WORD_C - 1;
                  col_cnt_s   <= N_COL_C - 1;
              end if;
            end if;

          -- Set counters for first and second padding byte.
          when PADD_LAST_BYTE_AD | PADD_LAST_BYTE_DATA =>
            if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1 and col_cnt_s >= N_COL_C - 1) then
              cycle_cnt_s <= 0;
              col_cnt_s   <= 0;
            else
              cycle_cnt_s <= N_ROW_C*BEATS_PER_WORD_C - 1;
              col_cnt_s   <= N_COL_C - 1;
            end if;

          -- PT / CT is absorbed in the first row. Again, if padding is detected,
          -- set address for last byte if first padding byte was already absorbed.
          when LOAD_DATA =>
            if (bdi_valid = '1' and bdi_ready_s = '1'
            and (bdi_type = HDR_PT or bdi_type = HDR_CT)) then
              if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
                  cycle_cnt_s <= 0;
                if (col_cnt_s >= N_COL_C - 1) then
                  col_cnt_s   <= 0;
                else
                  col_cnt_s <= col_cnt_s + 1;
                end if;
              else
                cycle_cnt_s <= cycle_cnt_s + 1;
              end if;
              if (bdi_eot = '1' and to_integer(unsigned(bdi_size)) /= CORE_PWdiv8) then
                cycle_cnt_s <= N_ROW_C*BEATS_PER_WORD_C - 1;
                col_cnt_s   <= N_COL_C - 1;
              end if;
            end if;

          -- Tag is extracted from the first row only.
          when TRANSMIT_TAG | TRANSMIT_TAG_EXT =>
            if (bdo_valid_s = '0' or (bdo_valid_s = '1' and bdo_ready = '1')) then
              if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
                cycle_cnt_s <= 0;
                if (col_cnt_s >= N_COL_C - 1) then
                  col_cnt_s <= 0;
                else
                  col_cnt_s <= col_cnt_s + 1;
                end if;
              else
                cycle_cnt_s <= cycle_cnt_s + 1;
              end if;
            end if;

          -- Same as TRANSMIT_TAG or TRANSMIT_TAG_EXT.
          when VERIFY_TAG =>
            if (bdi_valid = '1' and bdi_ready_s = '1' and bdi_type = HDR_TAG) then
              if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
                cycle_cnt_s <= 0;
                if (col_cnt_s >= N_COL_C - 1) then
                  col_cnt_s <= 0;
                else
                  col_cnt_s <= col_cnt_s + 1;
                end if;
              else
                cycle_cnt_s <= cycle_cnt_s + 1;
              end if;
            end if;

          -- This statement is not only required for suppressing synthesis tool errors.
          -- In the GIMLI-States and WAIT_ACK state, no addresses are generated.
          when others =>
            null;

        end case;
      end if;
    end if;
  end process p_address_counter;

end architecture behavioral;
