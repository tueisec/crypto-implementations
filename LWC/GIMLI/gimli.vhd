--------------------------------------------------------------------------------
--! @file       gimli.vhd
--! @brief      Implementing the round function.
--! @author     Patrick Karl <patrick.karl@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--! @license    This project is released under the GNU Public License.          
--!             The license and distribution terms for this file may be         
--!             found in the file LICENSE in this distribution or at            
--!             http://www.gnu.org/licenses/gpl-3.0.txt 
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--! API package.
use work.NIST_LWAPI_pkg.all;
--! I/O specific package.
use work.Design_pkg.all;
--! Gimli specific package.
use work.gimli_pkg.all;

--! Implements the round function of the gimli permutation.
--! This lightweight version uses three internal registers for data manipulation.
entity gimli is
  generic (
    N_ROUNDS_G    : positive range 8 to 32  --! Number of permutation rounds
  );
  port(
    --!Control Port=====================================================
    clk           : in  std_logic;
    reset         : in  std_logic;
    start         : in  std_logic;  --! Start pulse
    done          : out std_logic;  --! Done pulse

    --!RAM Port=====================================================
    wen_to_mem    : out std_logic;
    addr_to_mem   : out std_logic_vector(ADDR_WIDTH_C - 1 downto 0);
    data_from_mem : in  std_logic_vector(CORE_PW - 1 downto 0);
    data_to_mem   : out std_logic_vector(CORE_PW - 1 downto 0)
  );
end gimli;

--! @brief    Behavioral description of the gimli round function.
--! @details  Round function realized as a 3-process Mealy FSM. Three internal 32-Bit
--!           registers are used for leightweight implementation.
architecture behavioral of gimli is

  --! Number of rounds as unsigned constant serving as initial value.
  constant NU_ROUND_C : unsigned(ROUND_BITS_C - 1 downto 0) := to_unsigned(N_ROUNDS_C, ROUND_BITS_C);

  --! FSM state definition.
  type state_t is ( IDLE,
                    LOAD_NON_LINEAR_LAYER,
                    NON_LINEAR_LAYER,
                    WRITE_NON_LINEAR_LAYER,
                    LOAD_SMALL_SWAP,
                    WRITE_SMALL_SWAP,
                    LOAD_BIG_SWAP,
                    WRITE_BIG_SWAP,
                    LOAD_CONSTANT_ADDITION,
                    WRITE_CONSTANT_ADDITION
                  );
  -- State signals
  signal state_s   : state_t;
  signal n_state_s : state_t;

  -- Internal counters
  signal cycle_cnt_s          : integer range 0 to N_ROW_C*BEATS_PER_WORD_C - 1;
  signal n_cycle_cnt_s        : integer range 0 to N_ROW_C*BEATS_PER_WORD_C - 1;
  signal col_cnt_s            : integer range 0 to N_COL_C - 1;
  signal n_col_cnt_s          : integer range 0 to N_COL_C - 1;
  signal round_cnt_s          : unsigned(ROUND_BITS_C - 1 downto 0);
  signal n_round_cnt_s        : unsigned(ROUND_BITS_C - 1 downto 0);
  signal resized_round_cnt_s  : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);

  -- Round specific flags
  signal do_small_swap_s : boolean;
  signal do_big_swap_s   : boolean;

  -- Internal signals
  signal done_s     : std_logic;
  signal n_done_s   : std_logic;

  signal x_reg_s    : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);
  signal y_reg_s    : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);
  signal z_reg_s    : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);
  signal n_x_reg_s  : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);
  signal n_y_reg_s  : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);
  signal n_z_reg_s  : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);

begin

  -- Mapping signals to output.
  done        <= done_s;
  addr_to_mem <=  std_logic_vector(to_unsigned(cycle_cnt_s + col_cnt_s*BEATS_PER_WORD_C*N_ROW_C, ADDR_WIDTH_C));
  wen_to_mem  <= '1' when ( state_s = WRITE_NON_LINEAR_LAYER    or
                            state_s = WRITE_SMALL_SWAP          or
                            state_s = WRITE_BIG_SWAP            or
                            state_S = WRITE_CONSTANT_ADDITION)  else '0';


  -- Setting swapping flags.
  do_small_swap_s     <= true when (round_cnt_s(1 downto 0) = "00") else false;
  do_big_swap_s       <= true when (round_cnt_s(1 downto 0) = "10") else false;
  resized_round_cnt_s <= std_logic_vector(resize(round_cnt_s, GIMLI_WORD_SIZE_C));

  --! Signal Registers
  register_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = GIMLI_RESET_C) then
        state_s       <= IDLE;
        cycle_cnt_s   <= 0;
        col_cnt_s     <= 0;
      else
        state_s       <= n_state_s;
        done_s        <= n_done_s;
        cycle_cnt_s   <= n_cycle_cnt_s;
        col_cnt_s     <= n_col_cnt_s;
        round_cnt_s   <= n_round_cnt_s;
        x_reg_s       <= n_x_reg_s;
        y_reg_s       <= n_y_reg_s;
        z_reg_s       <= n_z_reg_s;
      end if;
    end if;
  end process register_p;

  --! Next state logic
  next_state_p : process( start, state_s, cycle_cnt_s, col_cnt_s, round_cnt_s,
                          do_big_swap_s, do_small_swap_s)
  begin

    case state_s is
      -- Wait for start interrupt
      when IDLE =>
        if (start = '1') then
          n_state_s <= LOAD_NON_LINEAR_LAYER;
        else
          n_state_s <= IDLE;
        end if;

      -- Read one column into the registers
      when LOAD_NON_LINEAR_LAYER =>
        if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1) then
          n_state_s <= NON_LINEAR_LAYER;
        else
          n_state_s <= LOAD_NON_LINEAR_LAYER;
        end if;

      -- Apply sp_box
      when NON_LINEAR_LAYER =>
        n_state_s <= WRITE_NON_LINEAR_LAYER;

      -- Write registers back into memory. Afterwards read next column, go to
      -- swapping states or return to IDLE if last round is finished.
      when WRITE_NON_LINEAR_LAYER =>
        if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1) then
          if (col_cnt_s >= N_COL_C - 1 and do_small_swap_s) then
            n_state_s <= LOAD_SMALL_SWAP;
          elsif (col_cnt_s >= N_COL_C - 1 and do_big_swap_s) then
            n_state_s <= LOAD_BIG_SWAP;
          elsif (col_cnt_s >= N_COL_C - 1 and round_cnt_s <= 1) then
            n_state_s <= IDLE;
          else
            n_state_s <= LOAD_NON_LINEAR_LAYER;
          end if;
        else
          n_state_s <= WRITE_NON_LINEAR_LAYER;
        end if;

      -- Load swapping candidates
      when LOAD_SMALL_SWAP =>
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and (col_cnt_s = 1 or col_cnt_s = 3)) then
          n_state_s <= WRITE_SMALL_SWAP;
        else
          n_state_s <= LOAD_SMALL_SWAP;
        end if;

      -- Load swapping candidates
      when LOAD_BIG_SWAP =>
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and (col_cnt_s = 2 or col_cnt_s = 3)) then
          n_state_s <= WRITE_BIG_SWAP;
        else
          n_state_s <= LOAD_BIG_SWAP;
        end if;

      -- Write back exchanged swapping candidates. Afterwards go to constant addition.
      when WRITE_SMALL_SWAP =>
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s = 1) then
          n_state_s <= LOAD_SMALL_SWAP;
        elsif (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s = 3) then
          n_state_s <= LOAD_CONSTANT_ADDITION;
        else
          n_state_s <= WRITE_SMALL_SWAP;
        end if;

      -- Write back exchanged swapping candidates and afterwards go back into
      -- loading non-linear-layer.
      when WRITE_BIG_SWAP =>
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s = 2) then
          n_state_s <= LOAD_BIG_SWAP;
        elsif(cycle_cnt_s >= BEATS_PER_WORD_C - 1 and col_cnt_s = 3) then
          n_state_s <= LOAD_NON_LINEAR_LAYER;
        else
          n_state_s <= WRITE_BIG_SWAP;
        end if;

      -- Load word for constant addition.
      when LOAD_CONSTANT_ADDITION =>
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
          n_state_s <= WRITE_CONSTANT_ADDITION;
        else
          n_state_s <= LOAD_CONSTANT_ADDITION;
        end if;

      -- And go back into non-linear-layer if constant and round counter is absorbed.
      when WRITE_CONSTANT_ADDITION =>
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
          n_state_s <= LOAD_NON_LINEAR_LAYER;
        else
          n_state_s <= WRITE_CONSTANT_ADDITION;
        end if;

      when others =>
        n_state_s <= IDLE;

    end case;
  end process next_state_p;

  --! Output Decoder
  output_decode_p : process(state_s, cycle_cnt_s, col_cnt_s, round_cnt_s,
                            x_reg_s, y_reg_s, z_reg_s, data_from_mem,
                            do_small_swap_s, do_big_swap_s, resized_round_cnt_s)
    variable x_v, y_v, z_v            : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);
    variable tmp1_v, tmp2_v, tmp3_v   : std_logic_vector(GIMLI_WORD_SIZE_C - 1 downto 0);
  begin

    -- Defaults preventing latches
    n_done_s        <= '0';
    n_cycle_cnt_s   <= cycle_cnt_s;
    n_col_cnt_s     <= col_cnt_s;
    n_round_cnt_s   <= round_cnt_s;
    n_x_reg_s       <= x_reg_s;
    n_y_reg_s       <= y_reg_s;
    n_z_reg_s       <= z_reg_s;
    data_to_mem     <= (others => '-');

    case state_s is
      -- When traversing from Last to first state, we finished rounds.
      -- Set counter to initial value zero.
      when IDLE =>
        n_cycle_cnt_s <= 0;
        n_col_cnt_s   <= 0;
        n_round_cnt_s <= NU_ROUND_C;

      -- Depending on the number of cycles required to load one gimli word,
      -- fill the three internal 32 bit registers.
      -- If traversing from WRITE state to LOAD state, increase attempt counter.
      when LOAD_NON_LINEAR_LAYER =>
        if (cycle_cnt_s < BEATS_PER_WORD_C) then
          n_x_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s) <= data_from_mem;
        elsif (cycle_cnt_s >= BEATS_PER_WORD_C and cycle_cnt_s < 2*BEATS_PER_WORD_C) then
          n_y_reg_s(CORE_PW*(cycle_cnt_s-BEATS_PER_WORD_C+1) - 1 downto CORE_PW*(cycle_cnt_s-BEATS_PER_WORD_C)) <= data_from_mem;
        elsif (cycle_cnt_s >= 2*BEATS_PER_WORD_C) then
          n_z_reg_s(CORE_PW*(cycle_cnt_s-2*BEATS_PER_WORD_C+1) - 1 downto CORE_PW*(cycle_cnt_s-2*BEATS_PER_WORD_C)) <= data_from_mem;
        end if;
        if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1) then
          n_cycle_cnt_s <= 0; --OPT: Do sp_box here and skip NON_LINEAR_LAYER... Saving one cycle at cost of ressources
        else
          n_cycle_cnt_s <= cycle_cnt_s + 1;
        end if;

      -- Reset cycle count for next write state. Perform Non-Linear layer
      -- permutation for the column that is currently loaded in x, y and z.
      when NON_LINEAR_LAYER =>
        x_v := x_reg_s(7 downto 0) & x_reg_s(31 downto 8);
        y_v := y_reg_s(22 downto 0) & y_reg_s(31 downto 23);
        z_v := z_reg_s;
        tmp1_v := y_v and z_v;
        tmp2_v := x_v or z_v;
        tmp3_v := x_v and y_v;
        n_z_reg_s <= x_v xor (z_v(30 downto 0) & '0') xor (tmp1_v(29 downto 0) & "00");
        n_y_reg_s <= y_v xor x_v xor (tmp2_v(30 downto 0) & '0');
        n_x_reg_s <= z_v xor y_v xor (tmp3_v(28 downto 0) & "000");

      -- Write back the new words into the RAM
      -- If theres no swapping in this round, increase round counter.
      -- If this was the last round, assert done and reset round counter.
      when WRITE_NON_LINEAR_LAYER =>
        if (cycle_cnt_s < BEATS_PER_WORD_C) then
          data_to_mem <= x_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s);
        elsif (cycle_cnt_s >= BEATS_PER_WORD_C and cycle_cnt_s < 2*BEATS_PER_WORD_C) then
          data_to_mem <= y_reg_s(CORE_PW*(cycle_cnt_s-BEATS_PER_WORD_C+1) - 1 downto CORE_PW*(cycle_cnt_s-BEATS_PER_WORD_C));
        elsif (cycle_cnt_s >= 2*BEATS_PER_WORD_C) then
          data_to_mem <= z_reg_s(CORE_PW*(cycle_cnt_s-2*BEATS_PER_WORD_C+1) - 1 downto CORE_PW*(cycle_cnt_s-2*BEATS_PER_WORD_C));
        end if;
        if (cycle_cnt_s >= N_ROW_C*BEATS_PER_WORD_C - 1) then
          n_cycle_cnt_s <= 0;
          if (col_cnt_s >= N_COL_C - 1) then
            n_col_cnt_s <= 0;
            if (round_cnt_s <= 1) then
              n_round_cnt_s <= NU_ROUND_C;
              n_done_s      <= '1';
            elsif not (do_small_swap_s or do_big_swap_s) then
              n_round_cnt_s <= round_cnt_s - 1;
            end if;
          else
            n_col_cnt_s <= col_cnt_s + 1;
          end if;
        else
          n_cycle_cnt_s <= cycle_cnt_s + 1;
        end if;

      -- Load candidates to swap into registers.
      -- Increase column counter after loading first candidate (col 0 or 2) and
      -- decrease afterwards to be prepared for write state.
      when LOAD_SMALL_SWAP =>
        if (col_cnt_s = 0 or col_cnt_s = 2) then
          n_x_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s) <= data_from_mem;
        else
          n_y_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s) <= data_from_mem;
        end if;
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
          n_cycle_cnt_s <= 0;
          if (col_cnt_s = 0 or col_cnt_s = 2) then
            n_col_cnt_s <= col_cnt_s + 1;
          else
            n_col_cnt_s <= col_cnt_s - 1;
          end if;
        else
          n_cycle_cnt_s <= cycle_cnt_s + 1;
        end if;

      -- Write the swapped words into memory again.
      -- Increase column simply until we wrote last column, then reset column
      -- to be prepared for next state.
      when WRITE_SMALL_SWAP =>
        if (col_cnt_s = 0 or col_cnt_s = 2) then
          data_to_mem <= y_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s);
        else
          data_to_mem <= x_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s);
        end if;
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
          n_cycle_cnt_s <= 0;
          if (col_cnt_s >= N_COL_C - 1) then
            n_col_cnt_s <= 0;
          else
            n_col_cnt_s <= col_cnt_s + 1;
          end if;
        else
          n_cycle_cnt_s <= cycle_cnt_s + 1;
        end if;

      -- Load candidates to swap into registers.
      -- Increase column counter after loading first candidate (col 0 or 1) and
      -- decrease afterwards to be prepared for write state.
      when LOAD_BIG_SWAP =>
        if (col_cnt_s = 0 or col_cnt_s = 1) then
          n_x_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s) <= data_from_mem;
        else
          n_y_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s) <= data_from_mem;
        end if;
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
          n_cycle_cnt_s <= 0;
          if (col_cnt_s = 0 or col_cnt_s = 1) then
            n_col_cnt_s <= col_cnt_s + 2;
          else
            n_col_cnt_s <= col_cnt_s - 2;
          end if;
        else
          n_cycle_cnt_s <= cycle_cnt_s + 1;
        end if;

      -- Increase column counter after loading first candidate (col 0 or 1)
      -- by two. If writing second of first swap (col 2) decrease to col 1 to
      -- be prepared for next load. If last candidate of second swap is written
      -- (col 3) reset counter.
      when WRITE_BIG_SWAP =>
        if (col_cnt_s = 0 or col_cnt_s = 1) then
          data_to_mem <= y_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s);
        else
          data_to_mem <= x_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s);
        end if;
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
          n_cycle_cnt_s <= 0;
          if (col_cnt_s = 0 or col_cnt_s = 1) then
            n_col_cnt_s <= col_cnt_s + 2;
          elsif (col_cnt_s = 2) then
            n_col_cnt_s <= col_cnt_s - 1;
          elsif (col_cnt_s >= N_COL_C - 1) then
            n_col_cnt_s <= 0;
            n_round_cnt_s <= round_cnt_s - 1;
          end if;
        else
          n_cycle_cnt_s <= cycle_cnt_s + 1;
        end if;

      -- Load word into xreg.
      when LOAD_CONSTANT_ADDITION =>
        n_x_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s) <= data_from_mem;
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
          n_cycle_cnt_s <= 0;
        else
          n_cycle_cnt_s <= cycle_cnt_s + 1;
        end if;

      -- Write back word after adding constant and round counter
      when WRITE_CONSTANT_ADDITION =>
        data_to_mem <=  x_reg_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s)
                        xor (MAGIC_CONST_C(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s)
                        or resized_round_cnt_s(CORE_PW*(cycle_cnt_s+1) - 1 downto CORE_PW*cycle_cnt_s));
        if (cycle_cnt_s >= BEATS_PER_WORD_C - 1) then
          n_cycle_cnt_s <= 0;
          n_round_cnt_s <= round_cnt_s - 1;
        else
          n_cycle_cnt_s <= cycle_cnt_s + 1;
        end if;

      -- Preventing warning / error
      when others =>
        null;

    end case;
  end process output_decode_p;

end architecture behavioral;
