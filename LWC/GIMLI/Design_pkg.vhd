------------------------------------------------------------------------------
--! @file       Design_pkg.vhd (Design package for Lightweight)
--! @brief      Lightweight API package
--! @author     Patrick Karl <patrick.karl@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--! @license    This project is released under the GNU Public License.          
--!             The license and distribution terms for this file may be         
--!             found in the file LICENSE in this distribution or at            
--!             http://www.gnu.org/licenses/gpl-3.0.txt 
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;


package Design_pkg is

    --! I/O parameters
    constant CORE_PW             : integer := 32; --! can be set to 8, 16, 32
    constant CORE_SW             : integer := 32; --! can be set to 8, 16, 32


   ------ DO NOT CHANGE ANYTHING BELOW! ------

    constant CORE_PWdiv8         : integer := CORE_PW/8;
    constant CORE_SWdiv8         : integer := CORE_SW/8;

    --! Design parameters
    constant DBLK_SIZE        : integer := 128;   --! Data
    constant KEY_SIZE         : integer := 256;   --! Key
    constant TAG_SIZE         : integer := 128;   --! Tag
    constant HASH_VALUE_SIZE  : integer := 256;   --! Hashtag


    --! Log(2) ceil
    function log2_ceil (N: natural) return natural;



end Design_pkg;

package body Design_pkg is

  ---------------------------------------------------------------------
  --! Log of base 2
  ---------------------------------------------------------------------

  function log2_ceil (N: natural) return natural is
  begin
       if ( N = 0 ) then
           return 0;
       elsif N <= 2 then
           return 1;
       else
          if (N mod 2 = 0) then
              return 1 + log2_ceil(N/2);
          else
              return 1 + log2_ceil((N+1)/2);
          end if;
       end if;
  end function log2_ceil;

end Design_pkg;
