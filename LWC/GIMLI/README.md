
Documentation of a GIMLI VHDL implementation
==========================

*This project uses a **prelease** with **reduced functionality** and **different internal names **of the [Proposed Hardware API for Lightweight Cryptography][3]!*

*Please refer to the current version of the [Development Package][5] and [Implementer's Guide][4], when developing own designs!*

# Introduction

The presented implementation is a VHDL realization of a proposed authenticated encryption algorithm and hash function called GIMLI conceived by [Bernstein et al.][1] and submitted to the [NIST Lightweight Cryptography][2]. 

# Repository structure

| Folder               | Description                                                  |
|----------------------|--------------------------------------------------------------|
| ./                   | Contains the CipherCore Implementation of Gimli.             |
| ./LWC                | Implementation files and testbench for the proposed LWC API. |
| ./LWC/deprecated     | Files needed before VHDL-2008.                               |
| ./modelsim/KAT       | Precomputed test vectors.                                    |
| ./modelsim/scripts   | Modelsim script to run the testvectors.                      |


# Getting started

## Run ModelSim simulation
To run the simulation start ModelSim and change the working directory to "modelsim\scripts". Then execute the tcl command "source modelsim.tcl" than "ldd". The pre-computed test vectors are being simulated. If everything worked out you will get the information "Error: PASS (0): SIMULATION FINISHED". If no response is received one possible problem could be, that the simulation runtime is to short. Go into the "modelsim.tcl" script and increase the runtime in the line "run x ns".

## Data path configuration
The cipher is independent of the bus widths of the proposed API. Possible data path widths are listed in the following table.

| Flavor    | CORE\_PW (bdi/bdo) | CORE\_SW (key)   |
|-----------|--------------------|------------------|
| gimli     | 8, 16, 32          | 8, 16, 32        |

The data path widths are set in "Design_pkg.vhd"



[1]: https://csrc.nist.gov/CSRC/media/Projects/Lightweight-Cryptography/documents/round-1/spec-doc/gimli-spec.pdf "Gimli 20190329"
[2]: https://csrc.nist.gov/Projects/Lightweight-Cryptography "NIST Lightweight Cryptography"
[3]: https://groups.google.com/a/list.nist.gov/forum/#!topic/lwc-forum/dEa9HwBQ9uo "Proposed Hardware API for Lightweight Cryptography"
[4]: https://cryptography.gmu.edu/athena/LWC/LWC_HW_Implementers_Guide.pdf "Implementer's Guide"
[5]: https://github.com/GMUCERG/LWC "Development Package"