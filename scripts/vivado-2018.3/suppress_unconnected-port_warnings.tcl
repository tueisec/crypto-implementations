# Suppress unconnected port warnings.
# bdo_type is not used in the Postprocessor. This is for future extentions
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port bdo_type[3]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port bdo_type[2]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port bdo_type[1]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port bdo_type[0]} }

# For ccw = w = 32 and ccsw = sw = 32: No PISOs and SIPO are needed. Data is simply routed through. Thus, there is no clocked process
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design DATA_PISO has unconnected port clk} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design DATA_PISO has unconnected port rst} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design KEY_PISO has unconnected port clk} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design KEY_PISO has unconnected port rst} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design DATA_SIPO has unconnected port rst} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design DATA_SIPO has unconnected port clk} }
# For ccw = w = 32: There is no need to evaluate end_of_input
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design DATA_SIPO has unconnected port end_of_input} }

# The Postprocessor does not need the partial, eoi and last information from the Preprocessor
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[27]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[26]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[24]} }

# The "reserved" field of the header is not used in the Postprocessor
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[23]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[22]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[21]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[20]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[19]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[18]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[17]} }
set_msg_config -suppress -id {Synth 8-3331} -string {{WARNING: [Synth 8-3331] design PostProcessor has unconnected port cmd[16]} }

