## This file is a general .xdc for the Basys3 rev B board

# Clock signal
set_property PACKAGE_PIN W5 [get_ports clk]
set_property IOSTANDARD LVCMOS33 [get_ports clk]
create_clock -period 10.000 -name clk -waveform {0.000 5.000} [get_ports clk]

##Reset Button
set_property PACKAGE_PIN U18 [get_ports rst_a]
set_property IOSTANDARD LVCMOS33 [get_ports rst_a]


#USB-RS232 Interface
set_property PACKAGE_PIN B18 [get_ports serial_rx]
set_property IOSTANDARD LVCMOS33 [get_ports serial_rx]
set_property PACKAGE_PIN A18 [get_ports serial_tx]
set_property IOSTANDARD LVCMOS33 [get_ports serial_tx]

# LED
set_property PACKAGE_PIN P1 [get_ports LED_rs232Error]
set_property IOSTANDARD LVCMOS33 [get_ports LED_rs232Error]



