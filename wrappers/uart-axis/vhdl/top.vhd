--------------------------------------------------------------------------------
--! @file       top.vhd
--! @brief      Example toplevel designed for the CAESAR/NIST-LWC competition
--!
--! @author     Michael Tempelmeier
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology     
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.          
--!             The license and distribution terms for this file may be         
--!             found in the file LICENSE in this distribution or at            
--!             http://www.gnu.org/licenses/gpl-3.0.txt                                                                
--!
--! @note       This module instantiates the uart_axis_wrapper and the CAESAR
--!             or proposed LWC API.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library xil_defaultlib;
use xil_defaultlib.wrapper.ALL;

entity top is
    Port ( clk : in STD_LOGIC;
           rst_a : in STD_LOGIC;
           LED_rs232Error : out STD_ULOGIC;
           serial_rx : in STD_ULOGIC;
           serial_tx : out STD_ULOGIC
           );
end top;




architecture Behavioral of top is

signal rst       : STD_LOGIC;

signal pdi_valid : STD_LOGIC;
signal pdi_ready : STD_LOGIC;
signal pdi       : STD_LOGIC_VECTOR(PDI_WIDTH-1 DOWNTO 0);

signal sdi_valid : STD_LOGIC;
signal sdi_ready : STD_LOGIC;
signal sdi       : STD_LOGIC_VECTOR(SDI_WIDTH-1 DOWNTO 0);

signal do_valid  : STD_LOGIC;
signal do_ready  : STD_LOGIC;
signal do        : STD_LOGIC_VECTOR(PDI_WIDTH-1 DOWNTO 0);



begin

sync_external_signals: process(clk)
-- not needed due to "metastabillity"
-- but due to routing delays.
-- never ever feed one external input to more than one FF!
begin
    if rising_edge(clk) then
        rst <= rst_a;
    end if;
end process sync_external_signals;


UART: entity work.uart_axis_wrapper(Behavioral)

  port map ( clk => clk,
             rst => rst,
             error => LED_rs232Error,
             serial_rx => serial_rx,
             serial_tx => serial_tx,
             
             do => do,
             do_valid => do_valid, 
             do_ready => do_ready,
             
             pdi => pdi,
             pdi_valid => pdi_valid,
             pdi_ready => pdi_ready,
                          
             sdi => sdi,
             sdi_valid => sdi_valid,
             sdi_ready => sdi_ready
             ); 



LWC: entity work.LWC(structure)

    port map ( clk => clk,
               rst => rst,
               
               pdi_data  => pdi,
               pdi_valid => pdi_valid,
               pdi_ready => pdi_ready,
               
               sdi_data  => sdi,
               sdi_valid => sdi_valid,
               sdi_ready => sdi_ready,
               
               do_data  => do,
               do_valid => do_valid,
               do_ready => do_ready,
               do_last  => open       --we don't need this inforamtion,
                                      --because we know how many bytes we want to read from the UART
               ); 

-- uncomment if you want to instantiate an AEAD entity as used during CAESAR
--AEAD: entity work.AEAD(structure)
--
--    port map ( clk => clk,
--               rst => rst,
--               
--               pdi_data => pdi,
--               pdi_valid => pdi_valid,
--               pdi_ready => pdi_ready,
--               
--               sdi_data => sdi,
--               sdi_valid => sdi_valid,
--               sdi_ready => sdi_ready,
--               
--               do_data => do,
--               do_valid => do_valid,
--               do_ready => do_ready
--               ); 

  
end Behavioral;
