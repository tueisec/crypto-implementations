--------------------------------------------------------------------------------
--! @file       wrapper_pkg.vhd
--! @brief      Package for the uart_axis_wrapper
--!
--! @author     Michael Tempelmeier <michael.tempelmeier@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--!
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package wrapper is

-- Hardware configuration
constant PDI_WIDTH           : INTEGER := 32;
constant SDI_WIDTH           : INTEGER := 32;
constant PDI_FIFO_SIZE_BYTES : INTEGER := 1024; -- same FIFO size for pdi, do
constant SDI_FIFO_SIZE_BYTES : INTEGER := 128;

-- UART configuration
constant UART_RST_SEQ_EN	: boolean := false;     -- Enable the generation of the reset sequence indicator flag
constant UART_RST_SEQ_LEN	: integer := 8;         -- Number of consecutive zero bytes for reset sequence
constant UART_BAUD_RATE 	: integer := 115200;    -- BAUD rate of the connection
constant UART_CLK_FREQ  	: integer := 100000000;	-- System clock frequency [MHz]
-- UART COMAND CODES
constant UART_CMD_WRITE_PDI       : std_ulogic_vector (7 downto 0) := B"1000_0000";
constant UART_CMD_WRITE_PDI_BURST : std_ulogic_vector (7 downto 0) := B"1000_0001";
constant UART_CMD_WRITE_SDI       : std_ulogic_vector (7 downto 0) := B"0100_0000";
constant UART_CMD_WRITE_SDI_BURST : std_ulogic_vector (7 downto 0) := B"0100_0001";
constant UART_CMD_READ_DO         : std_ulogic_vector (7 downto 0) := B"0010_0000";

-- no need to change things below
function log2(i : natural) return integer;

constant PDI_FIFO_DEPTH : INTEGER := PDI_FIFO_SIZE_BYTES/(PDI_WIDTH/8);
constant SDI_FIFO_DEPTH : INTEGER := SDI_FIFO_SIZE_BYTES/(SDI_WIDTH/8);

constant PDI_FIFO_FULL_THRESH_WIDTH : INTEGER := log2(PDI_FIFO_DEPTH);
constant SDI_FIFO_FULL_THRESH_WIDTH : INTEGER := log2(SDI_FIFO_DEPTH);

end;



package body wrapper is

function log2( i : natural) return integer is
    variable temp    : integer := i;
    variable ret_val : integer := 0;
  begin
    while temp > 1 loop
      ret_val := ret_val + 1;
      temp    := temp / 2;
    end loop;

    return ret_val;
  end function;

 end package body;
