--------------------------------------------------------------------------------
--! @file       arm_and_flush.vhd
--! @brief      bistable flip flop
--!
--! @author     Michael Tempelmeier <michael.tempelmeier@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--!
--! @note       This modules implements a bistable flip flop.
--!             It can be armed (waiting for flush), flushed (or idle).
--!             If flushed, it can only be armed if either rst or done is asserted.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity arm_and_flush is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           arm : in STD_LOGIC;
           flush : in STD_LOGIC;
           done : in STD_LOGIC;
           armed : out STD_LOGIC);
end arm_and_flush;

architecture RTL of arm_and_flush is

type fifo_state_t is (s_idle, s_arm, s_flush);
signal fifo_state : fifo_state_t;  

begin

pdi_delay: process (clk)
begin
    if rising_edge(clk) then
        if (rst = '1') then
            fifo_state <= s_idle;
            armed <= '0';
        else
            case (fifo_state) is
                when s_idle =>
                    if arm = '1' then
                        armed <= '1';
                        fifo_state <= s_arm;
                    else
                        armed <= '0';
                    end if;
                when s_arm =>
                    if flush = '1' then
                        armed <= '0';
                        fifo_state <= s_flush;
                    else
                        armed <= '1';
                    end if;
                when s_flush =>
                    if (done = '1') then
                        armed <= '0';
                        fifo_state <= s_idle;
                    else
                        armed <= '0';
                    end if;
            end case;
        end if;
     end if;
end process pdi_delay;
        

end RTL;
