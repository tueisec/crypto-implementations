--------------------------------------------------------------------------------
--! @file       uart_fsm.vhd
--! @brief      This is the toplevel module of the uart_axis_wrapper.
--!
--! @author     Michael Tempelmeier <michael.tempelmeier@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--!
--! @note       Protocol interpreter and handshaking converter
--!             interprets commands from the uart using ack/stb handshaking
--!             and reads/writes according to the cmd to 3 fifo based streams
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library xil_defaultlib;
use xil_defaultlib.wrapper.ALL;

use IEEE.NUMERIC_STD.ALL;



entity uart_fsm is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           
           -- status info
           fsm_done  : out STD_LOGIC;
           fsm_error : out STD_LOGIC;
           pdi_burst_en : out STD_LOGIC;
           sdi_burst_en : out STD_LOGIC;
           expected_sdi_bytes : out STD_LOGIC_VECTOR(31 downto 0);
           expected_pdi_bytes : out STD_LOGIC_VECTOR(31 downto 0);

           
           --uart rx stb/ack handshake
           uart_byte_rx : in STD_ULOGIC_VECTOR(7 downto 0);
           uart_byte_rx_stb : in STD_ULOGIC;
           uart_byte_rx_ack : out STD_ULOGIC;
           
           --uart tx stb/ack handshake           
           uart_byte_tx : out STD_ULOGIC_VECTOR(7 downto 0);
           uart_byte_tx_stb : out STD_ULOGIC;
           uart_byte_tx_ack : in STD_ULOGIC;
           
           --pdi valid/ready handshake
           uart_byte_pdi :out STD_LOGIC_VECTOR(7 downto 0);
           uart_byte_pdi_valid : out STD_LOGIC;
           uart_byte_pdi_ready : in STD_LOGIC;
           
           --sdi valid/ready handshake
           uart_byte_sdi :out STD_LOGIC_VECTOR(7 downto 0);
           uart_byte_sdi_valid : out STD_LOGIC;
           uart_byte_sdi_ready : in STD_LOGIC;
           
           --do valid/ready handshake
           uart_byte_do : in STD_LOGIC_VECTOR(7 downto 0);
           uart_byte_do_valid : in STD_LOGIC;
           uart_byte_do_ready : out STD_LOGIC            
           
           );
               
end uart_fsm;


architecture Behavioral of uart_fsm is


type state_t is (done, decode_cmd, ack_cmd, rx_hdr0, ack_hdr0, rx_hdr1, ack_hdr1, rx_hdr2, ack_hdr2,
                 rx_hdr3, ack_hdr3, receive, set_receive_valid, ack_receive, transmit, set_tx_stb, wait_tx_ack_done);
                 
type cmd_t is (RX_PDI, RX_SDI, TX_DO, error);                 

--state registers
signal current_state, next_state : state_t; -- main states
signal current_byte_count, next_byte_count : unsigned (31 downto 0); --2^31 -1 possible substates for length
signal current_byte_count_std : std_logic_vector(31 downto 0); -- always has the same as value as current_byte_count
signal current_cmd, next_cmd : cmd_t; -- substate for commands
-- support for burst-enabled Fifos
signal current_pdi_burst_en, next_pdi_burst_en : std_logic; --indicates if pdi should be transmitted in burst mode
signal current_sdi_burst_en, next_sdi_burst_en : std_logic; --indicates if sdi should be transmitted in burst mode

-- registers to buffer the inputs
-- they are needed to be fully compatible with the ack/stb and valid/ready protocolls and to avoid a Mealy FSM
-- they could be merged to one single 8 bit registers. In FPGAs this would save 8 LUTs/FF as the multiplexing
-- comes for free, however to increase readability, I choose to separate them.
signal current_tx_byte, next_tx_byte : std_ulogic_vector (7 downto 0); 
signal current_rx_byte, next_rx_byte : std_ulogic_vector (7 downto 0); 

signal expected_sdi_bytes_reg, expected_pdi_bytes_reg : std_logic_vector (31 downto 0);
signal store_pdi_words, store_sdi_words : boolean;

begin
 
--make sure that there are no secret bits at the pdi-port 
uart_byte_pdi <= std_logic_vector(current_rx_byte) when current_cmd = RX_PDI else (others=>'0');
--make sure there are no public data on the sdi-port;
--could be omitted from a security point of view, but is used for consitency
uart_byte_sdi <= std_logic_vector(current_rx_byte) when current_cmd = RX_SDI else (others =>'0');

--assign the registered FMS value directly to the output
uart_byte_tx <= current_tx_byte;

--assign the internal FSM error cmd to the output
-- can be used to detect an error in the protocol
--as the current/next_cmd is buffered every clk cycle, it is only valid in all other states
fsm_error <= '1' when (current_cmd = error AND current_state /= decode_cmd) else '0';

--stupid vivado, or stupid designer who has no clue about casting  :P
--the FSM needs slices of current_byte_count, however vivado doesn't allow:
-- (std_logic_vector(<unsigned_type>)(<range>)) combined in one statement
current_byte_count_std <= std_logic_vector(current_byte_count);




pdi_words: process (clk)
begin
if (rising_edge(clk)) then
  if (rst='1') then
    expected_pdi_bytes_reg <= (others => '0');
  else
    if (store_pdi_words) then
      expected_pdi_bytes_reg <= current_byte_count_std(31 downto 0); 
    else
      expected_pdi_bytes_reg <= expected_pdi_bytes_reg;
    end if; --enable
  end if;
end if;
end process pdi_words;

expected_pdi_bytes <= expected_pdi_bytes_reg;
pdi_burst_en <= current_pdi_burst_en;

sdi_words: process (clk)
begin
if (rising_edge(clk)) then
  if (rst='1') then
    expected_sdi_bytes_reg <= (others => '0');
  else
    if (store_sdi_words) then
      expected_sdi_bytes_reg <= current_byte_count_std(31 downto 0); 
    else
      expected_sdi_bytes_reg <= expected_sdi_bytes_reg;
    end if; --enable
  end if;
end if;
end process sdi_words;

expected_sdi_bytes <= expected_sdi_bytes_reg;
sdi_burst_en <= current_sdi_burst_en;

state_reg: process (clk)
begin
if (rising_edge(clk)) then
  if (rst = '1') then
    current_state <= decode_cmd;
    current_byte_count <= (others => '0'); --Reset not needed, but comes with no costs on FPGA
    current_cmd <=RX_PDI;                  --Reset not needed, but comes with no costs on FPGA
    current_tx_byte <= (others => '0');    --Reset not needed, but comes with no costs on FPGA
    current_rx_byte <= (others => '0');    --Reset not needed, but comes with no costs on FPGA
    current_pdi_burst_en <= '0';
    current_sdi_burst_en <= '0';
  else 
    current_state <= next_state;
    current_byte_count <= next_byte_count;
    current_cmd <= next_cmd;
    current_tx_byte <= next_tx_byte;
    current_rx_byte <= next_rx_byte;
    current_pdi_burst_en <= next_pdi_burst_en;
    current_sdi_burst_en <= next_sdi_burst_en;
  end if;
end if;
end process state_reg;


state_logic: process (current_state, uart_byte_rx, current_byte_count, current_cmd, current_byte_count_std, current_tx_byte, current_rx_byte,
                      uart_byte_do, uart_byte_rx_stb, uart_byte_tx_ack, uart_byte_pdi_ready, uart_byte_sdi_ready, uart_byte_do_valid,
                      current_pdi_burst_en, current_sdi_burst_en)
begin
--default: no changes on state regs
next_state <= current_state;
next_byte_count <= current_byte_count;
next_cmd <= current_cmd;
next_tx_byte <= current_tx_byte;
next_rx_byte <= current_rx_byte;
next_pdi_burst_en <= current_pdi_burst_en;
next_sdi_burst_en <= current_sdi_burst_en;

--default: not (ready for) receiving
uart_byte_rx_ack <= '0';
uart_byte_pdi_valid <= '0';
uart_byte_sdi_valid <= '0';

--default: not (ready for) transmitting)
uart_byte_do_ready <= '0';
uart_byte_tx_stb <= '0';

--default: fsm not done
fsm_done <= '0';

-- default do not latch/store pdi/sdi word count
store_pdi_words <= false;
store_sdi_words <= false;

case current_state is
    when done => -- one burst at the output that all bytes have been received/transmitted
        fsm_done <='1';
        next_state <= decode_cmd;   
      
    when decode_cmd =>
        next_pdi_burst_en <= '0'; -- default we do not want
        next_sdi_burst_en <= '0'; -- to have any bursts
        case uart_byte_rx is
            when UART_CMD_WRITE_PDI =>
                next_cmd <= RX_PDI;
            when UART_CMD_WRITE_SDI =>
                next_cmd <= RX_SDI;
            when UART_CMD_WRITE_PDI_BURST =>
                next_cmd <= RX_PDI;
                next_pdi_burst_en <= '1';
            when UART_CMD_WRITE_SDI_BURST =>
                next_cmd <= RX_SDI;
                next_sdi_burst_en <= '1';
            when UART_CMD_READ_DO =>
                next_cmd <= TX_DO;
            when others => --should never occur
                next_cmd <= error;
        end case;
        if (uart_byte_rx_stb = '1') then
            next_state <= ack_cmd;
        end if;

    when ack_cmd =>
        uart_byte_rx_ack <= '1';
        if (uart_byte_rx_stb = '0') then
            next_state <= rx_hdr0;
        end if;
      
    when rx_hdr0 =>
        --receive the first of 4 bytes and combine them to 32 bit length information
        next_byte_count <= unsigned(std_logic_vector(uart_byte_rx) & current_byte_count_std(23 downto 0));
    
        if(uart_byte_rx_stb = '1') then
            next_state <= ack_hdr0;
        end if;
  
    when ack_hdr0 =>
        uart_byte_rx_ack <= '1';
        if (uart_byte_rx_stb = '0') then
            next_state <=rx_hdr1;
        end if;

    when rx_hdr1 =>
        --receive the second of 4 bytes and combine them to 32 bit length information  
        next_byte_count <= unsigned(current_byte_count_std(31 downto 24) &
                                    std_logic_vector(uart_byte_rx) &
                                    current_byte_count_std(15 downto 0));
        if(uart_byte_rx_stb = '1') then
            next_state <= ack_hdr1;
        end if;
  
    when ack_hdr1 =>
        uart_byte_rx_ack <= '1';
        if (uart_byte_rx_stb = '0') then
            next_state <=rx_hdr2;
        end if;
    
    when rx_hdr2 =>
        --receive the third of 4 bytes and combine them to 32 bit length information
        next_byte_count <= unsigned(current_byte_count_std(31 downto 16) &
                                    std_logic_vector(uart_byte_rx) &
                                    current_byte_count_std(7 downto 0));
        if(uart_byte_rx_stb = '1') then
            next_state <= ack_hdr2;
        end if;
    
    when ack_hdr2 =>
        uart_byte_rx_ack <= '1';
        if (uart_byte_rx_stb = '0') then
            next_state <=rx_hdr3;
        end if; 
           
    when rx_hdr3 =>
        --receive the last of 4 bytes and combine them to 32 bit length information    
        next_byte_count <= unsigned(current_byte_count_std(31 downto 8) &
                                    std_logic_vector(uart_byte_rx));
        if(uart_byte_rx_stb = '1') then
            next_state <= ack_hdr3;
        end if;
      
    when ack_hdr3 =>
        uart_byte_rx_ack <= '1';
        if (uart_byte_rx_stb = '0') then
            case current_cmd is
                when RX_PDI =>
                    next_state <=receive;
                    store_pdi_words <= true;
                when RX_SDI =>
                    next_state <=receive;
                    store_sdi_words <= true;
                when TX_DO =>
                    next_state <= transmit;
                when others => --should not happen
                    next_state <= decode_cmd;
             end case;
        end if;
        
    when receive =>
        if (current_byte_count = 0) then --there is nothing left to do: last byte is transmitted for current_byte_
            next_state <= done;
            next_pdi_burst_en <= '0'; -- clear any burst flags
            next_sdi_burst_en <= '0'; -- clear any burst flags
        else
            if (uart_byte_rx_stb = '1') then 
                next_rx_byte <= uart_byte_rx;
                next_state <= set_receive_valid;
            end if;
        end if;
        
    when set_receive_valid =>
        if (current_cmd = RX_PDI) then --cmd is a substate register
            uart_byte_pdi_valid <= '1'; --output depends on substate
            if(uart_byte_pdi_ready = '1') then --netxt state depens on substate and input
                next_state <= ack_receive;
            end if;
        else --current_cmd = RX_SDI, or we are screwed -- cmd is a substate regiser
            uart_byte_sdi_valid <= '1';  --output depends on substate
            if (uart_byte_sdi_ready = '1') then --next state depends on substate and input
                next_state <= ack_receive;
            end if;
        end if;
    
    when ack_receive =>
        uart_byte_rx_ack <= '1';
        if (uart_byte_rx_stb = '0') then
            next_state <= receive; --next state
            next_byte_count <= current_byte_count - 1; --next substate
        end if;
  
    when transmit =>
        if (current_byte_count = 0) then --this is a substate
            next_state <= done;
        else
            uart_byte_do_ready <= '1'; --output depends on substate
            if (uart_byte_do_valid = '1') then --next state depends on input
                next_tx_byte <= std_ulogic_vector(uart_byte_do); --models an FF with enable for *_tx_byte
                next_state <= set_tx_stb;
            end if;
        end if;
    
    when set_tx_stb =>
        uart_byte_tx_stb <= '1';
        if (uart_byte_tx_ack = '1') then 
            next_state <= wait_tx_ack_done;
        end if;

    when wait_tx_ack_done =>
        if uart_byte_tx_ack = '0' then --was 1 bevore due to FSM logic
            next_state <= transmit; --next state
            next_byte_count <= current_byte_count - 1; --next substate
        end if;

    when others =>
        null;
end case;
end process state_logic;     


end Behavioral;
