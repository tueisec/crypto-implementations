--------------------------------------------------------------------------------
--! @file       fifo.vhd
--! @brief      Simple First-In-First_Out
--!
--! @author     Patrick Karl <patrick.karl@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--!
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fifo is
    generic(
        DATA_WIDTH_G    : integer range 8 to 32;
        LOG2DEPTH_G     : integer
    );
    port(
        clk             : in    std_logic;
        rst             : in    std_logic;

        -- Input Port
        s_axis_tdata    : in    std_logic_vector(DATA_WIDTH_G - 1 downto 0);
        s_axis_tvalid   : in    std_logic;
        s_axis_tready   : out   std_logic;

        -- Output Port
        m_axis_tdata    : out   std_logic_vector(DATA_WIDTH_G - 1 downto 0);
        m_axis_tvalid   : out   std_logic;
        m_axis_tready   : in    std_logic;

        prog_full_th    : in    std_logic_vector(LOG2DEPTH_G - 1 downto 0);
        prog_full       : out   std_logic
    );
    -- Infer distributed RAM!
    attribute ram_style : string;
    attribute ram_style of fifo : entity is "distributed";

end entity fifo;

architecture behavioral of fifo is

    -- FIFO depth in words
    constant DEPTH_C : integer := 2**LOG2DEPTH_G;

    -- Memory type and signal definition
    type mem_t is array (0 to DEPTH_C - 1) of std_logic_vector(DATA_WIDTH_G - 1 downto 0);
    signal mem_s            : mem_t;

    -- Internal handshake signals
    signal s_axis_tready_s  : std_logic := '0';
    signal m_axis_tvalid_s  : std_logic := '0';

    -- Internal flags
    signal empty_s          : std_logic;
    signal full_s           : std_logic;
    signal wr_ptr_s         : integer range 0 to DEPTH_C - 1;
    signal rd_ptr_s         : integer range 0 to DEPTH_C - 1;
    signal entries_s        : integer range 0 to DEPTH_C;


begin

    -- Output data
    m_axis_tdata    <= mem_s(rd_ptr_s);
    m_axis_tvalid   <= m_axis_tvalid_s;
    s_axis_tready   <= s_axis_tready_s;

    -- Set flags
    prog_full       <= '1' when (entries_s >= to_integer(unsigned(prog_full_th))) else '0';
    full_s          <= '1' when (entries_s >= DEPTH_C)      else '0';
    empty_s         <= '1' when (entries_s <= 0)            else '0';
    s_axis_tready_s <= not full_s;
    m_axis_tvalid_s <= not empty_s;


    -- Counting the numbers of entries, setting rd-/wr-pointers and
    -- writing the data into the memory.
    p_ptr : process(clk)
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                wr_ptr_s    <= 0;
                rd_ptr_s    <= 0;
                entries_s   <= 0;
            else

                -- Increase entry counter if data is written but not read
                -- Decrease entry counter if data is read but not written
                if (s_axis_tvalid = '1' and s_axis_tready_s = '1'
                and (m_axis_tvalid_s = '0' or m_axis_tready = '0')) then
                    entries_s <= entries_s + 1;
                elsif ((s_axis_tvalid = '0' or s_axis_tready_s = '0')
                and m_axis_tvalid_s = '1' and m_axis_tready = '1') then
                    entries_s <= entries_s - 1;
                end if;

                -- Write into memory and increase write pointer
                if (s_axis_tvalid = '1' and s_axis_tready_s = '1') then
                    mem_s(wr_ptr_s) <= s_axis_tdata;
                    if (wr_ptr_s >= DEPTH_C - 1) then
                        wr_ptr_s <= 0;
                    else
                        wr_ptr_s <= wr_ptr_s + 1;
                    end if;
                end if;

                -- Increase read pointer if data is read
                if (m_axis_tvalid_s = '1' and m_axis_tready = '1') then
                    if (rd_ptr_s >= DEPTH_C - 1) then
                        rd_ptr_s <= 0;
                    else
                        rd_ptr_s <= rd_ptr_s + 1;
                    end if;
                end if;

            end if;
        end if;
    end process p_ptr;

end architecture behavioral;
