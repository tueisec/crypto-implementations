--------------------------------------------------------------------------------
--! @file       sipo.vhd
--! @brief      A simple width converter
--!
--! @author     Michael Tempelmeier <michael.tempelmeier@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--!
--! @note       This module can convert a smaller axi-stream to a larger axi-stream.
--!             It has only been tested for multiples of bytes.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity sipo is
   Generic(
   SERIAL_WIDTH : integer;
   PARALLEL_WIDTH  : integer
   );
  
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           serial : in STD_LOGIC_VECTOR (SERIAL_WIDTH-1 downto 0);
           serial_valid : in STD_LOGIC;
           serial_ready : out STD_LOGIC;
           parallel : out STD_LOGIC_VECTOR (PARALLEL_WIDTH-1 downto 0);
           parallel_valid : out STD_LOGIC;
           parallel_ready : in STD_LOGIC
           );
end sipo;

architecture Behavioral of sipo is

constant counter_max : integer := PARALLEL_WIDTH/SERIAL_WIDTH;
signal shift_reg : std_logic_vector (PARALLEL_WIDTH-1 downto 0);
signal counter : integer range 0 to counter_max; --yes there are PARALLEL_WIDTH/SERIAL_WIDTH + 1 states!
-- if counter = 0 the shift reg is empty
-- if counter = PARALLEL_WITH/SERIAL_WIDTH the shift reg is empty.

begin

parallel <= shift_reg;

process (clk) --shift register with enable
begin
    if (rising_edge(clk)) then
        if (rst = '1') then
            counter <= 0;
        else
            case counter is
                when counter_max =>
                    if (parallel_ready = '1') then --output is be used
                        if (serial_valid = '1') then -- and there is more input
                            counter <= 1; -- so we are not empty in the next cycle and we shift everything by one
                            shift_reg(SERIAL_WIDTH-1 downto 0) <= serial;          
                            for i in 2 to PARALLEL_WIDTH/SERIAL_WIDTH loop
                                shift_reg((i*SERIAL_WIDTH)-1 downto ((i-1)*SERIAL_WIDTH)) <= shift_reg(((i-1)*SERIAL_WIDTH)-1 downto (i-2)*SERIAL_WIDTH);
                            end loop;
                        else  --there is not more input
                            counter <= 0; -- so we are empty
                        end if;
                    end if;               
                
                when others => 
                    if (serial_valid = '1') then
                        shift_reg(SERIAL_WIDTH-1 downto 0) <= serial;          
                        for i in 2 to PARALLEL_WIDTH/SERIAL_WIDTH loop
                            shift_reg((i*SERIAL_WIDTH)-1 downto ((i-1)*SERIAL_WIDTH)) <= shift_reg(((i-1)*SERIAL_WIDTH)-1 downto (i-2)*SERIAL_WIDTH);
                        end loop;
                        counter <= counter +1;
                    end if;
           end case;               
       end if;        
    end if;
end process;

parallel_valid <= '1' when (counter = counter_max) else '0';
serial_ready <= '0' when (counter = counter_max AND parallel_ready = '0') else '1';
end Behavioral;
