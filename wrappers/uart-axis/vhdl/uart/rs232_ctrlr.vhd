--------------------------------------------------------------------------------
--! @file       rs232_ctrlr.vhd
--! @brief      A simple RS232 wrapper module
--!
--! @author     Christian P. Feist, Michael Tempelmeier <michael.tempelmeier@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--!
--! @note       A simple RS232 wrapper module that makes use of the Rs232RefComp
--!             component and standardizes the interface with the one use throughout
--!             this design.
--!             Use tx_stb_i to request the sending of data and wait until
--!             tx_ack_o is asserted. This does not mean the data has been sent yet,
--!             but that the next request can be issued without breaking anything.
--!             Use rx_stb_o and rx_ack_i to receive data. Note that it may take a while
--!             for rx_stb_o to be de-asserted after rx_ack_i is asserted since the FSM
--!             waits for rda_s to drop.
--!             Finally, the module can still function as usual if an error is indicated
--!             via rx_err_stb_o. This simply makes it possible to detect errors.
--!             Keep rx_err_ack_i high until rx_err_stb_o is de-asserted.
-- Revision:
-- Revision 0.01 - File Created
--	Revision 0.02 - Added sequence monitor process rst_seq_p to generate the reset sequence flag
--	Revision 0.03 - Changed rst_seq_p to a synchronous process and reset to active high, removed latch and default values for generics.
--  Revision 0.04 - Only synthesize reset logic if enabled. This avoids "unused sequential" warnings
-- Additional Comments:
--      Xilinx suggests synchronous and active high resets on Series 7 and newer
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- For logarithmic sizes
use IEEE.MATH_REAL.ALL;

-- Paramters:
-- baud rate configured through generics (BAUD_RATE_g and CLK_FREQ_g)
-- idle level high
-- start bit 0
-- 8 data bits LSB first
-- parity bit EVEN
-- stop bit 1


entity rs232_ctrlr is
    generic(
        RST_SEQ_EN_g	: boolean;	-- Enable the generation of the reset sequence indicator flag
        RST_SEQ_LEN_g	: integer;	-- Number of consecutive zero bytes for reset sequence
        BAUD_RATE_g 	: integer;	-- BAUD rate of the connection
        CLK_FREQ_g  	: integer	-- System clock frequency [MHz]
    );

    port(
        -- IN
        clk_sys_i    : in std_ulogic;                     -- System clock
        reset_i      : in std_ulogic;                     -- Synchronous reset (active high)

        rxd_i        : in std_ulogic;                     -- Serial data in

        rx_ack_i     : in std_ulogic;                     -- Read data processed acknowledgement
        tx_stb_i     : in std_ulogic;                     -- Write data valid/available
        tx_dat_i     : in std_ulogic_vector(7 downto 0);  -- Data to be sent
        rx_err_ack_i : in std_ulogic;                     -- Error acknowledge

        -- OUT
        txd_o        : out std_ulogic;                    -- Serial data out

        rx_stb_o     : out std_ulogic;                    -- Received data valid/available
        tx_ack_o     : out std_ulogic;                    -- Write data has been sent
        rx_dat_o     : out std_ulogic_vector(7 downto 0); -- Received data
        rx_err_stb_o : out std_ulogic;                    -- Error indicator

        reset_seq_o  : out std_ulogic := '0'              -- Reset sequence indicator flag, must be enabled with RST_SEQ_EN_g = True
    );
end rs232_ctrlr;

architecture rtl of rs232_ctrlr is
    -- Component declarations
    component uart_if is
    generic(
        BAUD_RATE_G         : integer;
        CLK_FREQ_G          : integer;
        EVEN_PARITY_G       : boolean;
        N_PAYLOAD_BITS_G    : integer range 5 to 8
    );
    port(
        clk                     : in    std_ulogic;
        reset                   : in    std_ulogic;
        parity_error            : out   std_ulogic;
        frame_error             : out   std_ulogic;
        txd                     : out   std_ulogic;
        rxd                     : in    std_ulogic;
        data_to_uart            : in    std_ulogic_vector(7 downto 0);
        data_to_uart_valid      : in    std_ulogic;
        data_to_uart_ready      : out   std_ulogic;
        data_from_uart          : out   std_ulogic_vector(7 downto 0);
        data_from_uart_valid    : out   std_ulogic
    );
    end component uart_if;

    -- Type declarations
    type rx_state is (RX_IDLE_e, RX_WAIT_e);
    type tx_state is (TX_IDLE_e, TX_START_e, TX_WAIT_e);

    -- Internal signals
    signal dbin_reg : std_ulogic_vector(7 downto 0) := (others => '0'); -- Data bus input to Rs232RefComp
    signal dbout_s  : std_ulogic_vector(7 downto 0);                    -- Data bus output from Rs232RefComp
    signal rda_s    : std_ulogic;                                       -- Read data available from Rs232RefComp (pulsed)
    signal tbe_s    : std_ulogic;                                       -- Transmission bus empty from Rs232RefComp
    signal wr_reg   : std_ulogic := '0';                                -- Write strobe to Rs232RefComp
    signal pe_s	    : std_ulogic;                                       -- Parity error from Rs232RefComp
    signal fe_s     : std_ulogic;                                       -- Frame error from Rs232RefComp

    signal rx_cur_state_reg : rx_state := RX_IDLE_e;                    -- Current state of the RX FSM
    signal rx_next_state_s  : rx_state;                                 -- Next state of the RX FSM
    signal tx_cur_state_reg : tx_state := TX_IDLE_e;                    -- Current state of the TX FSM
    signal tx_next_state_s  : tx_state;                                 -- Next state of the TX FSM

    signal rx_err_stb_reg   : std_ulogic := '0';                        -- Internal version of rx_err_stb_o
    signal tx_ack_reg       : std_ulogic := '0';                        -- Internal version of tx_ack_o

begin
    -- Internal signals to port mappings
    rx_err_stb_o <= rx_err_stb_reg;
    tx_ack_o <= tx_ack_reg;

    i_uart_if : uart_if
    generic map(
        BAUD_RATE_G         => BAUD_RATE_g,
        CLK_FREQ_G          => CLK_FREQ_g,
        EVEN_PARITY_G       => true,
        N_PAYLOAD_BITS_G    => 8
    )
    port map(
        clk                     => clk_sys_i,
        reset                   => reset_i,
        parity_error            => pe_s,
        frame_error             => fe_s,
        txd                     => txd_o,
        rxd                     => rxd_i,
        data_to_uart            => dbin_reg,
        data_to_uart_valid      => wr_reg,
        data_to_uart_ready      => tbe_s,
        data_from_uart          => dbout_s,
        data_from_uart_valid    => rda_s
    );

    -- RX FSM next state logic
    rx_next_state_logic_p : process(rx_cur_state_reg, rda_s, rx_ack_i)
    begin
        case rx_cur_state_reg is
            -- Wait for data to have been received
            when RX_IDLE_e =>
                if (rda_s = '0') then
                    rx_next_state_s <= RX_IDLE_e;
                else
                    rx_next_state_s <= RX_WAIT_e;
                end if;

            -- Data received, wait for user to process/acknowledge data
            when RX_WAIT_e =>
                if (rx_ack_i = '0' or rda_s = '1') then
                    rx_next_state_s <= RX_WAIT_e;
                else
                    rx_next_state_s <= RX_IDLE_e;
                end if;

        end case;
    end process rx_next_state_logic_p;

    -- RX FSM state save and output logic
    rx_fsm_p : process(clk_sys_i)
    begin
        if (rising_edge(clk_sys_i)) then
            if (reset_i = '1') then
                -- Reset internal signals driven here
                rx_cur_state_reg <= RX_IDLE_e;
                rx_err_stb_reg <= '0';

                -- Reset outputs driven here
                rx_stb_o <= '0';
                rx_dat_o <= (others => '0');
            else
                -- State save logic
                rx_cur_state_reg <= rx_next_state_s;

                -- Output logic
                case rx_cur_state_reg is
                    -- Set data and valid indicator when data has been received
                    when RX_IDLE_e =>
                        if (rx_next_state_s = RX_WAIT_e) then
                            rx_stb_o <= '1';
                            rx_dat_o <= dbout_s;
                        end if;

                        -- Reset error indicator if it was acknowledged
                        if (rx_err_ack_i = '1') then
                            rx_err_stb_reg <= '0';
                        end if;

                    -- Wait for data to have been processed/acknowledged
                    when RX_WAIT_e =>
                        if (rx_next_state_s = RX_IDLE_e) then
                            rx_stb_o <= '0';
                        end if;

                        -- Set the error indicator if there was an un-acknowledged error or a new error
                        rx_err_stb_reg <= rx_err_stb_reg OR pe_s OR fe_s;
                end case;
            end if;
        end if;
    end process rx_fsm_p;

    -- TX FSM next state logic
    tx_next_state_logic_p : process(tx_cur_state_reg, tx_stb_i, tbe_s, wr_reg, tx_ack_reg)
    begin
        case tx_cur_state_reg is
            -- Wait for transmit request and transmit bus to be empty
            when TX_IDLE_e =>
                if (tx_stb_i = '0' or tbe_s = '0') then
                    tx_next_state_s <= TX_IDLE_e;
                else
                    tx_next_state_s <= TX_START_e;
                end if;

            -- Wait for data transfer to start
            when TX_START_e =>
                if (tbe_s = '1') then
                    tx_next_state_s <= TX_START_e;
                else
                    tx_next_state_s <= TX_WAIT_e;
                end if;

            -- Acknowledge and wait for request to be dropped
            when TX_WAIT_e =>
                if (tx_stb_i = '1') then
                    tx_next_state_s <= TX_WAIT_e;
                else
                    tx_next_state_s <= TX_IDLE_e;
                end if;
        end case;
    end process tx_next_state_logic_p;

    -- TX FSM state save and output logic
    tx_fsm_p : process(clk_sys_i)
    begin
        if (rising_edge(clk_sys_i)) then
            if (reset_i = '1') then
                -- Reset internal signals driven here
                tx_cur_state_reg <= TX_IDLE_e;
                tx_ack_reg <= '0';
                dbin_reg <= (others => '0');
                wr_reg <= '0';
            else
                -- State save logic
                tx_cur_state_reg <= tx_next_state_s;

                -- Output logic
                case tx_cur_state_reg is
                    when TX_IDLE_e =>
                        if (tx_next_state_s = TX_START_e) then
                            -- Set data and enable write (keep high until done!)
                            dbin_reg <= tx_dat_i;
                            wr_reg <= '1';
                        end if;

                    when TX_START_e =>
                        -- Nothing to do except wait
                        null;

                    when TX_WAIT_e =>
                        if (tx_next_state_s = TX_IDLE_e) then
                            -- De-assert acknowledge once stb is de-asserted
                            tx_ack_reg <= '0';
                        else
                            wr_reg <= '0';

                            -- Transmission will begin, can now acknowledge
                            if (tbe_s = '1') then
                                tx_ack_reg <= '1';
                            end if;
                        end if;

                end case;
            end if;
        end if;
    end process tx_fsm_p;

 g_reset_sequence : if RST_SEQ_EN_g = true generate
    --signals are only used if reset logic is generated
    signal rst_seq_cntr_reg : unsigned(integer(ceil(log2(real(RST_SEQ_LEN_g+1))))-1 downto 0) := (others => '0'); -- Counts the number of consecutive zero-bytes received
    signal rx_done_reg      : std_ulogic := '0'; -- This flag is used by the reset sequence monitor and indicates that the current dbout_s value has already been accounted

    begin

    -- This process monitors the number of consecutive zero bytes received and asserts
    -- the reset_seq_o flag if RST_SEQ_LEN_g is reached
    reset_seq_p : process(clk_sys_i)
    begin
        if (rising_edge(clk_sys_i)) then
            if (reset_i = '1') then
                -- Reset registers driven here
                rst_seq_cntr_reg <= (others => '0');
                rx_done_reg <= '0';
                -- Reset outputs driven here
                reset_seq_o <= '0';
            else
                if (rx_done_reg = '0' and rda_s = '1') then	-- New byte
                    -- Increment/reset zero byte sequence counter
                    if (dbout_s = x"00") then
                        rst_seq_cntr_reg <= rst_seq_cntr_reg + to_unsigned(1, rst_seq_cntr_reg'length);
                    else
                        rst_seq_cntr_reg <= (others => '0');
                    end if;

                    -- This byte has been accounted for
                    rx_done_reg <= '1';
                elsif (rx_done_reg = '1' and rda_s = '0') then
                    rx_done_reg <= '0';
                end if;

                -- Assert indicator flag if sequence of zero bytes complete
                if ((rst_seq_cntr_reg = to_unsigned(RST_SEQ_LEN_g, rst_seq_cntr_reg'length))) then
                    reset_seq_o <= '1';
                end if;
            end if;
        end if;
    end process reset_seq_p;

 end generate g_reset_sequence;

end rtl;
