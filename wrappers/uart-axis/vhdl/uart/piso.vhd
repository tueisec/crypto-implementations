--------------------------------------------------------------------------------
--! @file       piso.vhd
--! @brief      A simple width converter
--!
--! @author     Michael Tempelmeier <michael.tempelmeier@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--!
--! @note       This module can convert a larger axi-stream to a smaller axi-stream.
--!             It has only been tested for multiples of bytes.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity piso is
   Generic(
   SERIAL_WIDTH : integer;
   PARALLEL_WIDTH  : integer
   );
  
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           serial : out STD_LOGIC_VECTOR (SERIAL_WIDTH-1 downto 0);
           serial_valid : out STD_LOGIC;
           serial_ready : in STD_LOGIC;
           parallel : in STD_LOGIC_VECTOR (PARALLEL_WIDTH-1 downto 0);
           parallel_valid : in STD_LOGIC;
           parallel_ready : out STD_LOGIC
           );
end piso;

architecture Behavioral of piso is

constant counter_max : integer := PARALLEL_WIDTH/SERIAL_WIDTH;
signal shift_reg : std_logic_vector (PARALLEL_WIDTH-1 downto 0);
signal counter : integer range 0 to PARALLEL_WIDTH/SERIAL_WIDTH; --yes there are PARALLEL_WIDTH/SERIAL_WIDTH + 1 states!
-- if counter = 0 the shift reg is empty
-- if counter = PARALLEL_WITH/SERIAL_WIDTH the shift reg is empty.

begin

serial <= shift_reg(PARALLEL_WIDTH-1 downto PARALLEL_WIDTH-SERIAL_WIDTH);

process (clk) --shift register with enable
begin
    if (rising_edge(clk)) then
        if (rst = '1') then
            counter <= 0;
            parallel_ready <='1';
            serial_valid <='0';
        else
            case counter is
                when 0 =>
                    if (parallel_valid = '1') then
                        counter <= counter_max;
                        shift_reg <= parallel;
                        serial_valid <= '1';
                        parallel_ready <='0';
                    else
                        serial_valid <= '0';
                        parallel_ready <='1';
                    end if;
                when 1 =>
                    if (serial_ready = '1') then
                        parallel_ready <= '1';
                        if (parallel_valid = '1') then
                            shift_reg <= parallel;
                            serial_valid <= '1';
                            counter <= counter_max;
                        else 
                            serial_valid <= '0';
                            counter <= counter -1;
                        end if;
                    else
                        parallel_ready <= '0';
                        serial_valid <= '1';
                    end if;                     
                                        
                when others =>
                    serial_valid <= '1';
                    parallel_ready <= '0';
                    if (serial_ready = '1') then
                        for i in 2 to PARALLEL_WIDTH/SERIAL_WIDTH loop
                            shift_reg((i*SERIAL_WIDTH)-1 downto ((i-1)*SERIAL_WIDTH)) <= shift_reg(((i-1)*SERIAL_WIDTH)-1 downto (i-2)*SERIAL_WIDTH);
                        end loop;
                        shift_reg(SERIAL_WIDTH-1 downto 0) <= (others=> '-'); --don't care
                        counter <= counter -1;
                    end if;
            end case;
        end if;                                                        
    end if;
end process;

end Behavioral;

