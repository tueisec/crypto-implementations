--------------------------------------------------------------------------------
--! @file       uart_axis_wrapper.vhd
--! @brief      This is the toplevel module of the uart_axis_wrapper.
--!
--! @author     Michael Tempelmeier <michael.tempelmeier@tum.de>
--! @copyright  Copyright (c) 2019 Chair of Security in Information Technology
--!             ECE Department, Technical University of Munich, GERMANY
--!
--! @license    This project is released under the GNU Public License.
--!             The license and distribution terms for this file may be
--!             found in the file LICENSE in this distribution or at
--!             http://www.gnu.org/licenses/gpl-3.0.txt
--!
--! @note       This is the toplevel module of the uart_axis_wrapper.
--!             It provides two output axi streams and one input axi streams
--!             and converts them to a uart
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library xil_defaultlib;
use xil_defaultlib.wrapper.ALL;

entity uart_axis_wrapper is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           error : out STD_ULOGIC;
           serial_rx : in STD_ULOGIC;
           serial_tx : out STD_ULOGIC;

           do: in STD_LOGIC_VECTOR(PDI_WIDTH-1 downto 0);
           do_valid : in STD_LOGIC;
           do_ready : out STD_LOGIC;

           pdi: out STD_LOGIC_VECTOR (PDI_WIDTH-1 downto 0);
           pdi_valid: out STD_LOGIC;
           pdi_ready: in STD_LOGIC;

           sdi: out STD_LOGIC_VECTOR (SDI_WIDTH-1 downto 0);
           sdi_valid: out STD_LOGIC;
           sdi_ready: in STD_LOGIC
           );
end uart_axis_wrapper;


architecture Behavioral of uart_axis_wrapper is

component  sipo is
   Generic(
   SERIAL_WIDTH : integer;
   PARALLEL_WIDTH  : integer
   );

    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           serial : in STD_LOGIC_VECTOR (SERIAL_WIDTH-1 downto 0);
           serial_valid : in STD_LOGIC;
           serial_ready : out STD_LOGIC;
           parallel : out STD_LOGIC_VECTOR (PARALLEL_WIDTH-1 downto 0);
           parallel_valid : out STD_LOGIC;
           parallel_ready : in STD_LOGIC
           );
end component;

component piso is
   Generic(
   SERIAL_WIDTH : integer;
   PARALLEL_WIDTH  : integer
   );

    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           serial : out STD_LOGIC_VECTOR (SERIAL_WIDTH-1 downto 0);
           serial_valid : out STD_LOGIC;
           serial_ready : in STD_LOGIC;
           parallel : in STD_LOGIC_VECTOR (PARALLEL_WIDTH-1 downto 0);
           parallel_valid : in STD_LOGIC;
           parallel_ready : out STD_LOGIC
           );
end component;

component uart_fsm is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;

           fsm_done : out STD_LOGIC;
           fsm_error : out STD_LOGIC;
           expected_sdi_bytes : out STD_LOGIC_VECTOR(31 downto 0);
           expected_pdi_bytes : out STD_LOGIC_VECTOR(31 downto 0);

           sdi_burst_en : out STD_LOGIC;
           pdi_burst_en : out STD_LOGIC;

           uart_byte_rx : in STD_ULOGIC_VECTOR(7 downto 0);
           uart_byte_rx_stb : in STD_ULOGIC;
           uart_byte_rx_ack : out STD_ULOGIC;

           uart_byte_tx : out STD_ULOGIC_VECTOR(7 downto 0);
           uart_byte_tx_stb : out STD_ULOGIC;
           uart_byte_tx_ack : in STD_ULOGIC;

           uart_byte_pdi :out STD_LOGIC_VECTOR(7 downto 0);
           uart_byte_pdi_valid : out STD_LOGIC;
           uart_byte_pdi_ready : in STD_LOGIC;

           uart_byte_sdi :out STD_LOGIC_VECTOR(7 downto 0);
           uart_byte_sdi_valid : out STD_LOGIC;
           uart_byte_sdi_ready : in STD_LOGIC;

           uart_byte_do : in STD_LOGIC_VECTOR(7 downto 0);
           uart_byte_do_valid : in STD_LOGIC;
           uart_byte_do_ready : out STD_LOGIC

           );
end component;

signal uart_pdi  : STD_LOGIC_VECTOR(PDI_WIDTH-1 DOWNTO 0);
signal uart_pdi_valid : STD_ULOGIC;
signal uart_pdi_ready : STD_ULOGIC;

signal uart_do  : STD_LOGIC_VECTOR(PDI_WIDTH-1 DOWNTO 0);
signal uart_do_valid : STD_ULOGIC;
signal uart_do_ready : STD_ULOGIC;

signal uart_sdi  : STD_LOGIC_VECTOR(SDI_WIDTH-1 DOWNTO 0);
signal uart_sdi_valid : STD_ULOGIC;
signal uart_sdi_ready : STD_ULOGIC;


signal uart_byte_pdi_ready : STD_LOGIC;
signal uart_byte_pdi_valid : STD_LOGIC;
signal uart_byte_pdi       : STD_LOGIC_VECTOR(7 downto 0);

signal uart_byte_sdi_ready : STD_LOGIC;
signal uart_byte_sdi_valid : STD_LOGIC;
signal uart_byte_sdi       : STD_LOGIC_VECTOR(7 downto 0);

signal uart_byte_do_ready : STD_LOGIC;
signal uart_byte_do_valid : STD_LOGIC;
signal uart_byte_do       : STD_LOGIC_VECTOR(7 downto 0);


signal uart_byte_rx : STD_ULOGIC_VECTOR(7 downto 0);
signal uart_byte_rx_stb : STD_ULOGIC;
signal uart_byte_rx_ack : STD_ULOGIC;

signal uart_byte_tx : STD_ULOGIC_VECTOR(7 downto 0);
signal uart_byte_tx_stb : STD_ULOGIC;
signal uart_byte_tx_ack : STD_ULOGIC;

signal uart_error : STD_ULOGIC;
signal fsm_error : STD_LOGIC;
signal fsm_done : STD_LOGIC;

signal expected_sdi_bytes : STD_LOGIC_VECTOR(31 downto 0);
signal expected_pdi_bytes : STD_LOGIC_VECTOR(31 downto 0);
signal pdi_burst_en : STD_LOGIC;
signal sdi_burst_en : STD_LOGIC;


signal sdi_not_delayed : STD_LOGIC;
signal sdi_fifo_valid : STD_LOGIC;
signal sdi_fifo_ready : STD_LOGIC;
signal sdi_fifo_full : STD_LOGIC;
signal sdi_arm : STD_LOGIC;
signal sdi_armed : STD_LOGIC;
signal sdi_flush : STD_LOGIC;
signal sdi_done : STD_LOGIC;


signal pdi_not_delayed :STD_LOGIC;
signal pdi_fifo_valid : STD_LOGIC;
signal pdi_fifo_ready : STD_LOGIC;
signal pdi_fifo_full : STD_LOGIC;
signal pdi_arm : STD_LOGIC;
signal pdi_armed : STD_LOGIC;
signal pdi_flush : STD_LOGIC;
signal pdi_done : STD_LOGIC;

begin

assert (SDI_WIDTH = 32 or SDI_WIDTH = 16 or SDI_WIDTH = 8) report "This module only supports SDI_WIDTH=32,16,8!" severity failure;
assert (PDI_WIDTH = 32 or PDI_WIDTH = 16 or PDI_WIDTH = 8) report "This module only supports PDI_WIDTH=32,16,8!" severity failure;


error <= uart_error OR fsm_error;

RS232: entity xil_defaultlib.rs232_ctrlr(rtl)

  generic map (
        RST_SEQ_EN_g => UART_RST_SEQ_EN,   -- Enable the generation of the reset sequence indicator flag
        RST_SEQ_LEN_g => UART_RST_SEQ_LEN, -- Number of consecutive zero bytes for reset sequence
        BAUD_RATE_g => UART_BAUD_RATE,	   -- BAUD rate of the connection
        CLK_FREQ_g => UART_CLK_FREQ
  )

  port map (
    clk_sys_i => clk,                      -- System clock
    reset_i => rst,                        -- Synchronous reset (active high)
    rxd_i => serial_rx,                    -- Serial data in
    rx_ack_i => uart_byte_rx_ack,          -- Read data processed acknowledgement
    tx_stb_i => uart_byte_tx_stb,          -- Write data valid/available
    tx_dat_i => uart_byte_tx,              -- Data to be sent
    rx_err_ack_i => '0',                   -- Error acknowledge

    txd_o => serial_tx,                    -- Serial data out
    rx_stb_o => uart_byte_rx_stb,          -- Received data valid/available
    tx_ack_o => uart_byte_tx_ack,          -- Write data has been sent
    rx_dat_o => uart_byte_rx,              -- Received data
    rx_err_stb_o => uart_error,            -- Error indicator
    reset_seq_o  => open                   -- Reset sequence indicator flag
    );


Controll_FSM : uart_fsm
    Port map(
     clk => clk,
     rst => rst,

     fsm_error => fsm_error,
     fsm_done => fsm_done,
     expected_sdi_bytes => expected_sdi_bytes,
     expected_pdi_bytes => expected_pdi_bytes,
     sdi_burst_en => sdi_burst_en,
     pdi_burst_en => pdi_burst_en,

     uart_byte_rx => uart_byte_rx,
     uart_byte_rx_stb => uart_byte_rx_stb,
     uart_byte_rx_ack => uart_byte_rx_ack,

     uart_byte_tx =>uart_byte_tx,
     uart_byte_tx_stb => uart_byte_tx_stb,
     uart_byte_tx_ack => uart_byte_tx_ack,

     uart_byte_pdi => uart_byte_pdi,
     uart_byte_pdi_valid => uart_byte_pdi_valid,
     uart_byte_pdi_ready => uart_byte_pdi_ready,

     uart_byte_sdi => uart_byte_sdi,
     uart_byte_sdi_valid => uart_byte_sdi_valid,
     uart_byte_sdi_ready => uart_byte_sdi_ready,

     uart_byte_do => uart_byte_do,
     uart_byte_do_valid => uart_byte_do_valid,
     uart_byte_do_ready => uart_byte_do_ready
        );


pdi_delay: entity xil_defaultlib.arm_and_flush(RTL)
    port map(
        clk => clk,
        rst => rst,
        armed => pdi_armed,
        arm => pdi_arm,
        flush => pdi_flush,
        done => pdi_done
    );

pdi_done <= not pdi_fifo_valid;                  -- pdi-fifo is empty (flushed), when pdi_fifo_valid is low
pdi_arm <= pdi_burst_en;                         -- arm pdi_delay, if pdi_burst is enabled
pdi_flush <= fsm_done or pdi_fifo_full;          -- flush pdi_delay, if fsm is done or pdi_fifo is full

-- delay pdi-transmissions if pdi_delay is armed
pdi_valid <= pdi_fifo_valid and pdi_not_delayed; -- both, pdi_ready and pdi_valid must be low
pdi_fifo_ready <= pdi_ready and pdi_not_delayed; -- to avoid any transmission
pdi_not_delayed <= not pdi_armed;                -- as we 'and' them with the armed-status, we need to invert the signal



sdi_delay: entity xil_defaultlib.arm_and_flush(RTL)
    port map(
        clk => clk,
        rst => rst,
        armed => sdi_armed,
        arm => sdi_arm,
        flush => sdi_flush,
        done => sdi_done
    );

sdi_done <= not sdi_fifo_valid;                  -- sdi-fifo is empty (flushed), when sdi_fifo_valid is lo
sdi_arm <= sdi_burst_en;                         -- arm sdi_delay, if sdi_burst is enabled
sdi_flush <= fsm_done or sdi_fifo_full;          -- flush sdi_delay, if fsm is done or pdi_fifo is full

-- delay sdi-transmissions if sdi_delay is armed
sdi_valid <= sdi_fifo_valid and sdi_not_delayed; -- both, sdi_ready and sdi_valid must be low
sdi_fifo_ready <= sdi_ready and sdi_not_delayed; -- to avoid any transmission
sdi_not_delayed <= not sdi_armed;                -- as we 'and' them with the armed-status, we need to invert the signal


   ------- SDI_WIDTH configuration ------
   g_sdi_sipo : if (SDI_WIDTH /= 8) generate
      SDI_SIPO: sipo
      Generic map(
         SERIAL_WIDTH   => 8,
         PARALLEL_WIDTH => SDI_WIDTH
         )
      Port map (
         clk            => clk,
         rst            => rst,
         serial         => uart_byte_sdi,
         serial_valid   => uart_byte_sdi_valid,
         serial_ready   => uart_byte_sdi_ready,
         parallel       => uart_sdi,
         parallel_valid => uart_sdi_valid,
         parallel_ready => uart_sdi_ready
      );
   end generate g_sdi_sipo;

   -- Bypass sdi_sipo if no width conversion is required
   g_bypass_sdi_sipo : if (SDI_WIDTH = 8) generate
      uart_sdi             <= uart_byte_sdi;
      uart_sdi_valid       <= uart_byte_sdi_valid;
      uart_byte_sdi_ready  <= uart_sdi_ready;
   end generate g_bypass_sdi_sipo;

   SDI_FIFO : entity work.fifo(behavioral)
   generic map(
        DATA_WIDTH_G    => SDI_WIDTH,
        LOG2DEPTH_G     => log2(SDI_FIFO_DEPTH)
   )
   port map(
        clk             => clk,
        rst             => rst,
        s_axis_tdata    => uart_sdi,
        s_axis_tvalid   => uart_sdi_valid,
        s_axis_tready   => uart_sdi_ready,
        m_axis_tdata    => sdi,
        m_axis_tvalid   => sdi_fifo_valid,
        m_axis_tready   => sdi_fifo_ready,
        prog_full_th    => expected_sdi_bytes(SDI_FIFO_FULL_THRESH_WIDTH-1+log2(SDI_WIDTH/8) downto log2(SDI_WIDTH/8)),
        prog_full       => sdi_fifo_full
   );

   ------- PDI_WIDTH configuration ------
   g_do_piso : if (PDI_WIDTH /= 8) generate
      DO_PISO : piso
      Generic map(
         SERIAL_WIDTH => 8,
         PARALLEL_WIDTH => PDI_WIDTH
         )
      Port map (
         clk            => clk,
         rst            => rst,
         serial         => uart_byte_do,
         serial_valid   => uart_byte_do_valid,
         serial_ready   => uart_byte_do_ready,
         parallel       => uart_do,
         parallel_valid => uart_do_valid,
         parallel_ready => uart_do_ready
      );
   end generate g_do_piso;

   -- Bypass do piso if no width conversion is required
   g_bypass_do_piso : if (PDI_WIDTH = 8) generate
      uart_byte_do        <= uart_do;
      uart_byte_do_valid  <= uart_do_valid;
      uart_do_ready       <= uart_byte_do_ready;
   end generate g_bypass_do_piso;


   g_pdi_sipo : if (PDI_WIDTH /= 8) generate
      PDI_SIPO : sipo
      Generic map(
         SERIAL_WIDTH   => 8,
         PARALLEL_WIDTH => PDI_WIDTH
      )
      Port map (
         clk            => clk,
         rst            => rst,
         serial         => uart_byte_pdi,
         serial_valid   => uart_byte_pdi_valid,
         serial_ready   => uart_byte_pdi_ready,
         parallel       => uart_pdi,
         parallel_valid => uart_pdi_valid,
         parallel_ready => uart_pdi_ready
      );
   end generate g_pdi_sipo;

   -- Bypass pdi_sipo if no width conversion is required.
   g_bypass_pdi_sipo : if (PDI_WIDTH = 8) generate
      uart_pdi             <= uart_byte_pdi;
      uart_pdi_valid       <= uart_byte_pdi_valid;
      uart_byte_pdi_ready  <= uart_pdi_ready;
   end generate g_bypass_pdi_sipo;

   PDI_FIFO : entity work.fifo(behavioral)
   generic map(
      DATA_WIDTH_G    => PDI_WIDTH,
      LOG2DEPTH_G     => log2(PDI_FIFO_DEPTH)
   )
   port map(
      clk             => clk,
      rst             => rst,
      s_axis_tdata    => uart_pdi,
      s_axis_tvalid   => uart_pdi_valid,
      s_axis_tready   => uart_pdi_ready,
      m_axis_tdata    => pdi,
      m_axis_tvalid   => pdi_fifo_valid,
      m_axis_tready   => pdi_fifo_ready,
      prog_full_th    => expected_pdi_bytes(PDI_FIFO_FULL_THRESH_WIDTH-1+log2(PDI_WIDTH/8) downto log2(PDI_WIDTH/8)),
      prog_full       => pdi_fifo_full
   );

   DO_FIFO : entity work.fifo(behavioral)
   generic map(
      DATA_WIDTH_G    => PDI_WIDTH,
      LOG2DEPTH_G     => log2(PDI_FIFO_DEPTH)
      )
   port map(
      clk             => clk,
      rst             => rst,
      s_axis_tdata    => do,
      s_axis_tvalid   => do_valid,
      s_axis_tready   => do_ready,
      m_axis_tdata    => uart_do,
      m_axis_tvalid   => uart_do_valid,
      m_axis_tready   => uart_do_ready,
      prog_full_th    => (others =>'1'),
      prog_full       => open
   );


end Behavioral;
