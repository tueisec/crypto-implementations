# Lightweight Cryptography API - UART-wrapper

The [UART-axis-wrapper][link_wrapper] is an extention of the proposed *Hardware API for Leightweight Cryptography*. It provides an UART interface allowing to directly communicate with the [LWC-development-package][link_lwc].
<br></br>

## Overall API Structure
The API consists of two blocks i.e. the *UART-Wrapper* and the *Leightweight-Core LWC*: 

![Figure 1][fig_1]

The UART-wrapper implements a low-level UART interface for data  transmission and reception. The received data is decoded by a FSM and written into FIFOs after width conversion (serial-in-parallel-out / parallel-in-serial-out). Now the LWC has access to the data. 
The *PreProcessor* fetches and decodes the data, stimulates the *CryptoCore* and pases header information to the *Header-FIFO*. The *PostProcessor* receives data from the *CryptoCore* and writes it back to a FIFO.
<br></br>

## Configuring the UART-wrapper
The wrapper comes with a configuration file to adjust the word width and FIFO sizes:

* **wrapper_pkg.vhd**
The wrapper package contains parameters configuring the UART-wrapper part of the design. Depending on your ressource/performance requirements, you can adjust the following parameters:

	|Parameter 			| Description  						| Supported Range 	| 
	|-----------------			|-----------------						|---------			|
	| SDI_WIDTH¹			| Width of the SDI-FIFO 				| 8 / 16 / 32 		|
	| PDI_WIDTH¹			| Width of the PDI-/ DO-FIFO 			| 8 / 16 / 32		|
	| SDI_FIFO_SIZE_BYTES 	| Size of the SDI-FIFO in Bytes		| Any			|
	| PDI_FIFO_SIZE_BYTES 	| Size of the PDI-/DO-FIFO in Bytes	| Any			|

The LWC is configured in a separate package called *design_pkg.vhd*. For more information, refer to the material at [Further Ressources](#more_info).

¹ The LWC module of the Developer's Package requires SDI_WIDTH and PDI_WIDTH to be of same size!
<br></br>

## Transmitting Test Vectors 
The [LWC-development-package][link_lwc] provides tools to generate *Known-Answer-Test (KAT)* files. To send the data to the HW platform, a python script is provided. To use it: 

1. copy the desired KAT files to `$root/uart-axis/python/KAT` 
1. change directory (cd) to `$root/uart-axis/python`
1. run the script `python3 cli.py`

*cli.py* provides a debug mode to transmit one message after the other. In this mode, the KATs are accumulated in the SDI/PDI-FIFO and transmitted in burst mode. To enable/disable it, set `dbg=True/False` (line 21).
<br></br>
		
## Further Ressources  <a name="more_info"></a>
For a detailed description on the *Leightweight Cryptography API*, refer to the following:

+ UART-wrapper-package: [uart-axis-wrapper][link_wrapper]
+ LWC-development-package: [LWC][link_lwc]
+ Detailed Documentation available at: <https://cryptography.gmu.edu/athena/index.php?id=LWC>

		 
[link_wrapper]: https://gitlab.lrz.de/tueisec/crypto-implementations
[link_lwc]: https://github.com/GMUCERG/LWC
[fig_1]: uart.png
