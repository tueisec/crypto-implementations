################################################################################
# @author     Michael Pehl
# @date       2019-05-12
# @brief      Part of the KRIMP-LAB @ TUEISEC
# @copyright  Copyright (c) 2019 Chair of Security in Information Technology
#             ECE Department, Technical University of Munich, GERMANY
################################################################################

import time
import serial
import commandParser as parser
import binascii
import os
import sys
import argparse
import getSerial


def send_pdi(interface, length, data):
	if (interface.isOpen()==False):
		print("interface not open")
		exit(1)
	if (burst == True):
		interface.write(bytes([0x81])) # PDI request-burst
	else:
		interface.write(bytes([0x80])) # PDI request
	interface.write(length.to_bytes(4, byteorder = 'big'))
	interface.write(data)
	print("pdi: ", length+5," byte (including 5 Bytes of header) sent: ",  binascii.hexlify(data))

def send_sdi(interface, length, data):
	if (interface.isOpen()==False):
		print("interface not open")
		exit(1)
	if (burst == True):
		interface.write(bytes([0x41])) # SDI request-burst
	else:
		interface.write(bytes([0x40])) # SDI request
	interface.write(length.to_bytes(4, byteorder = 'big'))
	interface.write(data)
	print("sdi: ", length+5," byte (including 5 bytes of header) sent: ",  binascii.hexlify(data))

def receive_pdo(interface, length):
	if (interface.isOpen()==False):
		print("interface not open")
		exit(1)

	interface.write(bytes([0x20])) # PDO request
	interface.write(length.to_bytes(4, byteorder = 'big'))
	received = bytearray()
	for i in range(0,length):
		received=received+ser.read()
	return(received)


if __name__ == '__main__':

	# configure the argparser
	argparser = argparse.ArgumentParser()
	argparser.add_argument('-s', '--single', dest='dbg', action='store_true',
                       help='''
	Send one KAT at a time.
	''')

	argparser.add_argument('-d', '--debug', dest='burst', action='store_true',
                       help='''
	Accumulate sdi/pid data in a hardware FIFO to counteract UART latency.
	This is usefull for hardware debugging. Since there is no delay
	between consecutive pdi/sdi words, a complete encryption/decrytion
	can be observed within one trigger event.
	''')

	argparser.add_argument('--KAT', dest='KAT_PATH', action='store',
                       default='./KAT/', help='''
	Path to the KAT Folder (default: %(default)s)
	''')

	argparser.add_argument('--uart', dest='uart_port', action='store',
                       default=getSerial.serial_ports()[0], help='''
	Use this uart device (default: First UART device (%(default)s)).
	''')

	argparser.add_argument('--list-uarts',  dest='list_uarts', action='store_true',
                       help='''
	List available UART ports and exit.
	''')


	# parse the input and set arguments
	args = argparser.parse_args()
	dbg = args.dbg              # default: false
	burst = args.burst          # default: false
	KAT_FOLDER = args.KAT_PATH  # default: './KAT/'
	uart_port= args.uart_port   # default: getSerial.serial_ports()[0]

	if (args.list_uarts):
		print(getSerial.serial_ports())
		exit(0)

	# configure the serial connections
	print("Init serial connection")
	ser = serial.Serial(
	    port=uart_port,
	    baudrate=115200,
	    parity=serial.PARITY_EVEN,
	    stopbits=serial.STOPBITS_ONE,
	    bytesize=serial.EIGHTBITS
	)
	print("Serial connection established")

	# parse the KAT files and return iterators
	sdiObject=parser.commandParser(os.path.join(KAT_FOLDER, 'sdi.txt'), tag2="KeyID")
	sdiIter=iter(sdiObject)
	pdiObject=parser.commandParser(os.path.join(KAT_FOLDER, 'pdi.txt'), tag2="KeyID")
	pdoObject=parser.commandParser(os.path.join(KAT_FOLDER, 'do.txt'))
	pdoIter=iter(pdoObject)


	# step through the pdi.txt and compare the result with the ones in do.txt.
	# If needed: get new key for SDI
	sdiTag2=None
	for [pdiTag1, pdiTag2, pdi] in pdiObject:
	# pdiTag1 : pdi_MssgID
	# pdiTag2 : pdi_KeyID
	# sdiTag1 : sdi_MsgID
	# sdiTag2 : sdi_KeyID
		pdo=next(pdoObject)[1]
		print("msg: ",pdiTag1, " secret: ",pdiTag2)
		if not(pdiTag2 == sdiTag2) and not (pdiTag2 == "0"):
		# check for new key. KeyID=0 stand for hash instruction.
		# Thus no key is needed
			[sdiTag1, sdiTag2, sdi]=next(sdiObject)
			send_sdi(ser, len(sdi), sdi)
		send_pdi(ser, len(pdi), pdi)
		received_bytes = receive_pdo(ser, len(pdo))

		print("received: ", binascii.hexlify(received_bytes))
		print("expected: ", binascii.hexlify(pdo))

		if (pdo == received_bytes):
			print('\x1b[1;32;40m' +"ok"+ '\x1b[0m')
		else:
			print('\x1b[6;30;41m' +"not ok"+ '\x1b[0m')
		if (dbg) :
			input("press any key for next test")
		print ("-------------------------------------")

	ser.close()
	exit(0)
